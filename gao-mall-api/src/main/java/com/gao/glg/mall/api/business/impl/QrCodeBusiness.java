package com.gao.glg.mall.api.business.impl;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.business.IQrCodeBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Map;

@Service
public class QrCodeBusiness implements IQrCodeBusiness {


    @Override
    public Map<String, Object> get(JSONObject data) {
        return null;
    }

    @Override
    public Map<String, Object> base64(String url) {
        return Collections.emptyMap();
    }

    @Override
    public Map<String, Object> base64String(String text, int width, int height) {
        if((width < 50 || height < 50) && (width > 500 || height > 500) && text.length() >= 999){
            throw new BusinessException(ErrorConstant.QR_CODE_PARAM_NOT_LEGITIMATE);
        }

        return Collections.emptyMap();
    }
}
