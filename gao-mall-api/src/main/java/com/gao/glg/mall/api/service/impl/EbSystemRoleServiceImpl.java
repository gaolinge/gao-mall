package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.request.web.WebRoleSearchRequest;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemRoleDomain;
import com.gao.glg.mall.api.mapper.EbSystemRoleMapper;
import com.gao.glg.mall.api.service.EbSystemRoleService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemRoleServiceImpl implements EbSystemRoleService  {
    @Resource
    private EbSystemRoleMapper systemRoleMapper;

    @Override
    public List<EbSystemRoleDomain> selectAll() {
        return systemRoleMapper.select();
    }

    @Override
    public List<EbSystemRoleDomain> page(WebRoleSearchRequest request) {
        return systemRoleMapper.page(request);
    }

    @Override
    public EbSystemRoleDomain getByRoleName(String roleName) {
        return systemRoleMapper.getByRoleName(roleName);
    }

    @Override
    public void save(EbSystemRoleDomain domain) {
        systemRoleMapper.save(domain);
    }

    @Override
    public EbSystemRoleDomain getById(Integer id) {
        return systemRoleMapper.getById(id);
    }

    @Override
    public void update(EbSystemRoleDomain domain) {
        systemRoleMapper.update(domain);
    }

    @Override
    public void delete(Integer id) {
        systemRoleMapper.delete(id);
    }
}
