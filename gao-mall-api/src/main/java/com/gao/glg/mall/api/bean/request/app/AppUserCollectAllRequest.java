package com.gao.glg.mall.api.bean.request.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
@Data
public class AppUserCollectAllRequest implements Serializable {

    @ApiModelProperty(value = "商品ID")
    @Size(min = 1, message = "请选择产品")
    private List<Integer> id;

    @ApiModelProperty(value = "产品类型|store=普通产品,product_seckill=秒杀产品(默认 普通产品 store)")
    @NotBlank(message = "请选择产品类型")
    private String category;
}
