package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.app.AppCalcOrderPriceRequest;
import com.gao.glg.mall.api.bean.request.app.AppCreateOrderRequest;
import com.gao.glg.mall.api.bean.request.app.AppPreOrderRequest;
import com.gao.glg.mall.api.bean.response.app.AppCalcOrderPriceResponse;
import com.gao.glg.mall.api.bean.response.app.AppOrderDataResponse;
import com.gao.glg.mall.api.bean.response.app.AppPreOrderResponse;

import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/4
 */
public interface IOrderBusiness {

    /**
     * 订单头部数量
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    AppOrderDataResponse orderDataNum();

    /**
     * 下单
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/10
     * @version : 1.0.0
     */
    Map<String, Object> preOrder(AppPreOrderRequest request);

    /**
     * 加载预下单
     * <p/>
     *
     * @param preOrderNo
     * @return
     * @author : gaolinge
     * @date : 2024/12/10
     * @version : 1.0.0
     */
    AppPreOrderResponse loadPreOrder(String preOrderNo);

    /**
     * 计算订单价格
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/10
     * @version : 1.0.0
     */
    AppCalcOrderPriceResponse calcPrice(AppCalcOrderPriceRequest request);

    /**
     * 创建订单
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    Map<String, Object> createOrder(AppCreateOrderRequest request);

}
