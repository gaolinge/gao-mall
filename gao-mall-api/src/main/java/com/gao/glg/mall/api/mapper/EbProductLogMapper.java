package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbProductLogDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductLogMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductLogDomain domain);

    /**
     * 查询
     */
    List<EbProductLogDomain> select();

    /**
     * 更新
     */
    int update(EbProductLogDomain domain);
}
