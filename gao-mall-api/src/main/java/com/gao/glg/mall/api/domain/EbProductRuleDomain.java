package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbStoreProductRuleDomain
 * @author gaolinge
 */
@Data
public class EbProductRuleDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 规格名称
     */
    private String ruleName;

    /** 
     * 规格值
     */
    private String ruleValue;


}
