package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.service.EbSystemConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 配置
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/17
 */
@Slf4j
@RestController
@RequestMapping("/admin/system/config")
@Api(tags = "设置 -- Config")
public class WebConfigController {

    @Autowired
    private EbSystemConfigService systemConfigService;

    //@PreAuthorize("hasAuthority('admin:system:config:getuniq')")
    @ApiOperation(value = "表单配置根据key获取")
    @RequestMapping(value = "/getuniq", method = RequestMethod.GET)
    public RestResult<Object> getuniq(@RequestParam String key) {
        return RestResult.success(systemConfigService.getValueByKey(key));
    }


    /*
    //@PreAuthorize("hasAuthority('admin:system:config:info')")
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public RestResult<HashMap<String, String>> info(@RequestParam(value = "formId") Integer formId) {
        return RestResult.success(systemConfigService.info(formId));
    }


    //@PreAuthorize("hasAuthority('admin:system:config:save:form')")
    @ApiOperation(value = "整体保存表单数据")
    @RequestMapping(value = "/save/form", method = RequestMethod.POST)
    public RestResult<String> saveFrom(@RequestBody @Validated SystemFormCheckRequest request) {
        return RestResult.success(systemConfigService.save(request));
    }

    //@PreAuthorize("hasAuthority('admin:system:config:check')")
    @ApiOperation(value = "检测表单name是否存在")
    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public RestResult<Boolean> check(@RequestParam String name) {
        return RestResult.success(systemConfigService.check(name));
    }

    //@PreAuthorize("hasAuthority('admin:system:config:saveuniq')")
    @ApiOperation(value = "表单配置中仅仅存储")
    @RequestMapping(value = "/saveuniq", method = RequestMethod.POST)
    public RestResult<Boolean> justSaveUniq(@RequestParam String key, @RequestParam String value) {
        return RestResult.success(systemConfigService.updateOrSaveValueByName(key, value));
    }



    //@PreAuthorize("hasAuthority('admin:system:config:get')")
    @ApiOperation(value = "根据key获取配置")
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public RestResult<List<SystemConfig>> getByKey(@RequestParam String key) {
        return RestResult.success(systemConfigService.getListByKey(key));
    }

    //@PreAuthorize("hasAuthority('admin:system:config:update')")
    @ApiOperation(value = "更新配置信息")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RestResult<List<SystemConfig>> getByKey(@RequestBody @Validated List<SystemConfigAdminRequest> requestList) {
        if (systemConfigService.updateByList(requestList)) {
            return RestResult.success();
        }
        return RestResult.failed();
    }*/
}



