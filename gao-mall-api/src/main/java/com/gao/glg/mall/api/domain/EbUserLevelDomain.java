package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbUserLevelDomain
 * @author gaolinge
 */
@Data
public class EbUserLevelDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 用户uid
     */
    private Integer uid;

    /** 
     * 等级vip
     */
    private Integer levelId;

    /** 
     * 会员等级
     */
    private Integer grade;

    /** 
     * 0:禁止,1:正常
     */
    private Boolean status;

    /** 
     * 备注
     */
    private String mark;

    /** 
     * 是否已通知
     */
    private Boolean remind;

    /** 
     * 是否删除,0=未删除,1=删除
     */
    private Boolean isDel;

    /** 
     * 享受折扣
     */
    private Integer discount;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 过期时间
     */
    private Date expiredTime;


}
