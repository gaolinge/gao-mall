package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemConfigDomain
 * @author gaolinge
 */
@Data
public class EbSystemConfigDomain {
    /** 
     * 配置id
     */
    private Integer id;

    /** 
     * 字段名称
     */
    private String name;

    /** 
     * 字段提示文字
     */
    private String title;

    /** 
     * 表单id
     */
    private Integer formId;

    /** 
     * 值
     */
    private String value;

    /** 
     * 是否隐藏
     */
    private Boolean status;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
