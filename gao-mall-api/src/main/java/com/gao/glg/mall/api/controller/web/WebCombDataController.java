package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebGroupDataRequest;
import com.gao.glg.mall.api.bean.request.web.WebGroupDataSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebGroupDataResponse;
import com.gao.glg.mall.api.business.ICombDataBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Slf4j
@RestController
@RequestMapping("/admin/comb/data")
@Api(tags = "设置 -- 组合数据 -- 详情")
public class WebCombDataController {

    @Autowired
    private ICombDataBusiness combDataBusiness;

    //@PreAuthorize("hasAuthority('admin:system:group:data:list')")
    @ApiOperation(value = "分页组合数据详情")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebGroupDataResponse>> page(@Validated WebGroupDataSearchRequest request) {
        return RestResult.success(combDataBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:system:group:data:save')")
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult<String> create(@RequestBody @Validated WebGroupDataRequest request) {
        combDataBusiness.create(request);
        return RestResult.success();

    }

    //@PreAuthorize("hasAuthority('admin:system:group:data:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public RestResult<String> delete(@RequestParam(value = "id") Integer id) {
        combDataBusiness.delete(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:group:data:update')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RestResult<String> update(@RequestParam Integer id, @RequestBody @Validated WebGroupDataRequest request) {
        combDataBusiness.update(id, request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:group:data:info')")
    @ApiOperation(value = "组合数据详情信息")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public RestResult<WebGroupDataResponse> detail(@RequestParam(value = "id") Integer id) {
        return RestResult.success(combDataBusiness.detail(id));
    }
}



