package com.gao.glg.mall.api.mapper;

import com.gao.glg.mall.api.bean.request.web.WebProductReplySearchRequest;
import com.gao.glg.mall.api.domain.EbProductReplyDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductReplyMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductReplyDomain domain);

    /**
     * 查询
     */
    List<EbProductReplyDomain> select();

    /**
     * 更新
     */
    int update(EbProductReplyDomain domain);

    /**
     * 查询
     */
    EbProductReplyDomain getByPidLimitOne(Integer pid);

    /**
     * 查询
     */
    Long replyCount(@Param("pid") Integer pid, @Param("type") Integer type);

    /**
     * 查询
     */
    EbProductReplyDomain sumScore(Integer pid);

    /**
     * 查询
     */
    List<EbProductReplyDomain> page(@Param("request") WebProductReplySearchRequest request);

    /**
     * 查询
     */
    EbProductReplyDomain getById(Integer id);

    /**
     * 查询
     */
    Integer countByOidAndPidAndUid(@Param("oid") Integer oid, @Param("pid") Integer pid, @Param("uid") Integer uid);
}
