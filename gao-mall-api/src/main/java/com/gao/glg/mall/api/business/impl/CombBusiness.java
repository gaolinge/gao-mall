package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.bean.request.web.WebGroupRequest;
import com.gao.glg.mall.api.bean.request.web.WebGroupSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppCombinationIndexResponse;
import com.gao.glg.mall.api.bean.response.app.AppCombinationResponse;
import com.gao.glg.mall.api.bean.response.web.WebGroupResponse;
import com.gao.glg.mall.api.business.ICombBusiness;
import com.gao.glg.mall.api.domain.EbCombinationDomain;
import com.gao.glg.mall.api.domain.EbPinkDomain;
import com.gao.glg.mall.api.domain.EbSystemGroupDomain;
import com.gao.glg.mall.api.service.EbCombService;
import com.gao.glg.mall.api.service.EbPinkService;
import com.gao.glg.mall.api.service.EbSystemGroupService;
import com.gao.glg.mall.api.utils.CrmebUtil;
import com.gao.glg.mall.api.utils.StreamUtil;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class CombBusiness implements ICombBusiness {

    @Autowired
    private EbPinkService pinkService;
    @Autowired
    private EbCombService combService;
    @Autowired
    private EbSystemGroupService systemGroupService;

    @Override
    public AppCombinationIndexResponse index() {
        List<EbCombinationDomain> combinationDomains = combService.listByStock(6);
        if (CollectionUtils.isEmpty(combinationDomains)) {
            return null;
        }

        Integer totalPeople = pinkService.getTotalPeople();
        List<EbPinkDomain> pinkDomains = pinkService.findSizePink(3);
        List<String> avatarList = StreamUtil.map(pinkDomains, EbPinkDomain::getAvatar);

        List<AppCombinationResponse> combinationList = BeanCopierUtil.copyList(
                combinationDomains, AppCombinationResponse.class);
        combinationList.forEach(e -> {
            int percentIntVal = CrmebUtil.percentInstanceIntVal(e.getQuota(), e.getQuotaShow());
            e.setQuotaPercent(percentIntVal);
        });

        AppCombinationIndexResponse response = new AppCombinationIndexResponse();
        response.setAvatarList(avatarList);
        response.setTotalPeople(totalPeople);
        response.setProductList(combinationList);
        return response;
    }

    @Override
    public PagerInfo<WebGroupResponse> page(WebGroupSearchRequest request) {
        return PagerUtil.page(request, () -> {
            List<EbSystemGroupDomain> domains = systemGroupService.page(request);
            return BeanCopierUtil.copyList(domains, WebGroupResponse.class);
        });
    }

    @Override
    public void create(WebGroupRequest request) {
        EbSystemGroupDomain domain = new EbSystemGroupDomain();
        domain.setName(request.getName());
        domain.setInfo(request.getInfo());
        domain.setFormId(request.getFormId());
        systemGroupService.save(domain);
    }

    @Override
    public void delete(Integer id) {
        systemGroupService.delete(id);
    }

    @Override
    public void update(Integer id, WebGroupRequest request) {
        EbSystemGroupDomain domain = new EbSystemGroupDomain();
        domain.setId(id);
        domain.setName(request.getName());
        domain.setInfo(request.getInfo());
        domain.setFormId(request.getFormId());
        systemGroupService.update(domain);
    }

    @Override
    public WebGroupResponse detail(Integer id) {
        EbSystemGroupDomain domain = systemGroupService.getById(id);
        return BeanCopierUtil.copy(domain, WebGroupResponse.class);
    }
}
