package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.request.web.WebExpressSearchRequest;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbExpressDomain;
import com.gao.glg.mall.api.mapper.EbExpressMapper;
import com.gao.glg.mall.api.service.EbExpressService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbExpressServiceImpl implements EbExpressService  {
    @Resource
    private EbExpressMapper expressMapper;

    @Override
    public List<EbExpressDomain> page(WebExpressSearchRequest request) {
        return expressMapper.page(request);
    }

    @Override
    public List<EbExpressDomain> listByType(String type) {
        return expressMapper.listByType(type);
    }

    @Override
    public EbExpressDomain getById(Integer id) {
        return expressMapper.getById(id);
    }

    @Override
    public void update(EbExpressDomain domain) {
        expressMapper.update(domain);
    }
}
