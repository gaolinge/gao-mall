package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebProductAddRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebProductInfoResponse;
import com.gao.glg.mall.api.bean.response.web.WebProductResponse;
import com.gao.glg.mall.api.bean.response.web.WebProductTabsResponse;
import com.gao.glg.mall.api.business.IProductBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品服务
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/16
 */
@Slf4j
@RestController
@RequestMapping("/admin/product")
@Api(tags = "商品服务")
public class WebProductController {

    @Autowired
    private IProductBusiness productBusiness;

    //@PreAuthorize("hasAuthority('admin:product:list')")
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebProductResponse>> webList(@Validated WebProductSearchRequest request) {
        return RestResult.success(productBusiness.webList(request));
    }

    //@PreAuthorize("hasAuthority('admin:product:tabs:headers')")
    @ApiOperation(value = "商品表头数量")
    @RequestMapping(value = "/tabs/headers", method = RequestMethod.GET)
    public RestResult<List<WebProductTabsResponse>> tabs() {
        return RestResult.success(productBusiness.tabs());
    }

    //@PreAuthorize("hasAuthority('admin:product:info')")
    @ApiOperation(value = "商品详情")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public RestResult<WebProductInfoResponse> info(@PathVariable Integer id) {
        return RestResult.success(productBusiness.webInfo(id));
    }

    //@PreAuthorize("hasAuthority('admin:product:save')")
    @ApiOperation(value = "新增商品")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult<String> save(@RequestBody @Validated WebProductAddRequest request) {
        productBusiness.save(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public RestResult<String> delete(@PathVariable Integer id,
                                     @RequestParam(value = "type", required = false, defaultValue = "recycle") String type) {
        productBusiness.delete(id, type);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:restore')")
    @ApiOperation(value = "恢复商品")
    @RequestMapping(value = "/restore/{id}", method = RequestMethod.GET)
    public RestResult<String> reset(@RequestBody @PathVariable Integer id) {
        productBusiness.reset(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:update')")
    @ApiOperation(value = "商品修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RestResult<String> update(@RequestBody @Validated WebProductAddRequest request) {
        productBusiness.update(request);
        return RestResult.success();
    }
/*




    //@PreAuthorize("hasAuthority('admin:product:up')")
    @ApiOperation(value = "上架")
    @RequestMapping(value = "/putOnShell/{id}", method = RequestMethod.GET)
    public RestResult<String> putOn(@PathVariable Integer id) {
        productBusiness.putOnShelf(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:down')")
    @ApiOperation(value = "下架")
    @RequestMapping(value = "/offShell/{id}", method = RequestMethod.GET)
    public RestResult<String> offShell(@PathVariable Integer id) {
        if (productBusiness.offShelf(id)) {
            return RestResult.success();
        } else {
            return RestResult.failed();
        }
    }

    //@PreAuthorize("hasAuthority('admin:product:import:product')")
    @ApiOperation(value = "导入99Api商品")
    @RequestMapping(value = "/importProduct", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "form", value = "导入平台1=淘宝，2=京东，3=苏宁，4=拼多多, 5=天猫", dataType = "int", required = true),
            @ApiImplicitParam(name = "url", value = "URL", dataType = "String", required = true),
    })
    public RestResult<StoreProductRequest> importProduct(@RequestParam @Valid int form, @RequestParam @Valid String url) throws IOException, JSONException {
        return RestResult.success(productBusiness.importProductFromUrl(url, form));
    }

    //@PreAuthorize("hasAuthority('admin:product:copy:config')")
    @ApiOperation(value = "获取复制商品配置")
    @RequestMapping(value = "/copy/config", method = RequestMethod.POST)
    public RestResult<Map<String, Object>> copyConfig() {
        return RestResult.success(productBusiness.copyConfig());
    }

    //@PreAuthorize("hasAuthority('admin:product:copy:product')")
    @ApiOperation(value = "复制平台商品")
    @RequestMapping(value = "/copy/product", method = RequestMethod.POST)
    public RestResult<Map<String, Object>> copyProduct(@RequestBody @Valid StoreCopyProductRequest request) {
        return RestResult.success(productBusiness.copyProduct(request.getUrl()));
    }*/
}



