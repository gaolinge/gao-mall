package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempSearchRequest;
import com.gao.glg.mall.api.domain.EbShippingTempDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbShippingTempMapper;
import com.gao.glg.mall.api.service.EbShippingTempService;

import java.util.List;


@Service
public class EbShippingTempServiceImpl implements EbShippingTempService {
    @Resource
    private EbShippingTempMapper shippingTempMapper;

    @Override
    public EbShippingTempDomain getById(Integer id) {
        return shippingTempMapper.getById(id);
    }

    @Override
    public List<EbShippingTempDomain> page(WebLogisticsTempSearchRequest request) {
        return shippingTempMapper.page(request);
    }

    @Override
    public EbShippingTempDomain getByName(String name) {
        return shippingTempMapper.getByName(name);
    }

    @Override
    public void save(EbShippingTempDomain domain) {
        shippingTempMapper.save(domain);
    }

    @Override
    public void update(EbShippingTempDomain domain) {
        shippingTempMapper.update(domain);
    }

    @Override
    public void delete(Integer id) {
        shippingTempMapper.delete(id);
    }
}
