package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.bean.response.web.WebHomeRateResponse;
import com.gao.glg.mall.api.business.IHomeBusiness;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbOrderDomain;
import com.gao.glg.mall.api.service.EbOrderService;
import com.gao.glg.mall.api.service.EbUserService;
import com.gao.glg.mall.api.service.EbUserVisitRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class HomeBusiness implements IHomeBusiness {
    private static final String[] WEEK_LIST = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
    @Autowired
    private EbOrderService orderService;
    @Autowired
    private EbUserService userService;
    @Autowired
    private EbUserVisitRecordService userVisitRecordService;

    @Override
    public WebHomeRateResponse indexDate() {
        return null;
    }

    @Override
    public Map<Object, Object> chartUser() {
       return null;
    }

    @Override
    public Map<String, Integer> chartUserBuy() {
        /*Integer consumeZero = userService.getCountByPayCount(MallConstant.NUM_ZERO, MallConstant.NUM_ZERO);
        Integer consumeOne  = userService.getCountByPayCount(MallConstant.NUM_ONE, MallConstant.NUM_ONE);
        Integer consumeHis  = userService.getCountByPayCount(MallConstant.NUM_TWO, MallConstant.NUM_THREE);
        Integer consumeBack = userService.getCountByPayCount(MallConstant.NUM_ONE, MallConstant.EXPORT_MAX_LIMIT);

        Map<String, Integer> map = new HashMap<>();
        map.put("zero", consumeZero);
        map.put("one", consumeOne);
        map.put("history", consumeHis);
        map.put("back", consumeBack);
        return map;*/
        return null;
    }

    @Override
    public Map<String, Object> chartOrder() {
        /*List<EbOrderDomain> list = orderService.getOrderGroupByDate(MallConstant.SEARCH_DATE_LATELY_30, MallConstant.NUM_TEN);
        Map<Object, Object> count = dataFormat(getOrderCountGroupByDate(list), MallConstant.SEARCH_DATE_LATELY_30);
        Map<Object, Object> price = dataFormat(getOrderPriceGroupByDate(list), MallConstant.SEARCH_DATE_LATELY_30);

        Map<String, Object> map = new HashMap<>();
        map.put("quality", count);
        map.put("price",   price);
        return map;*/
        return null;
    }

    @Override
    public Map<String, Object> chartOrderInWeek() {
        return Collections.emptyMap();
    }

    @Override
    public Map<String, Object> chartOrderInMonth() {
        return Collections.emptyMap();
    }

    @Override
    public Map<String, Object> chartOrderInYear() {
        return Collections.emptyMap();
    }


    private Map<Object, Object> dataFormat(Map<Object, Object> countGroupDate, String dateLimit) {
        /*Map<Object, Object> map = new LinkedHashMap<>();
        List<String> listDate = DateUtil.getListDate(dateLimit);



        int i = 0;
        for (String date : listDate) {
            Object count = 0;
            if (countGroupDate.containsKey(date)) {
                count = countGroupDate.get(date);
            }

            String key;
            switch(dateLimit) {
                //格式化周
                case MallConstant.SEARCH_DATE_WEEK:
                case MallConstant.SEARCH_DATE_PRE_WEEK:
                    key = WEEK_LIST[i];
                    break;
                //格式化月
                case MallConstant.SEARCH_DATE_PRE_MONTH:
                case MallConstant.SEARCH_DATE_MONTH:
                    key = i + 1 + "";
                    break;
                //默认显示两位日期
                default:
                    key = date.substring(5, 10);
            }
            map.put(key, count);
            i++;
        }
        return map;*/
        return null;
    }
}
