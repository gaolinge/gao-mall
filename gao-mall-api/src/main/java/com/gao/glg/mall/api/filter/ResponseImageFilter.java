package com.gao.glg.mall.api.filter;

import com.gao.glg.mall.api.business.IFileBusiness;
import com.gao.glg.mall.api.constant.MallConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/23
 */
@Slf4j
@Component
public class ResponseImageFilter implements Filter{

    @Autowired
    private IFileBusiness fileBusiness;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        if (!isFilter(servletRequest)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        ResponseWrapper response = new ResponseWrapper((HttpServletResponse) servletResponse);
        filterChain.doFilter(servletRequest, response);

        byte[] content = response.getContent();
        if (content.length > 0) {
            String result = new String(content, StandardCharsets.UTF_8);

            try {
                result = addPrefix(result);
            } catch (Exception e) {
                log.error("ResponseFilter doFilter error", e);
            }

            ServletOutputStream outputStream = servletResponse.getOutputStream();
            if (!result.isEmpty()) {
                outputStream.write(result.getBytes());
                outputStream.flush();
                outputStream.close();
                servletResponse.flushBuffer();
            }
        }
    }

    /**
     * 是否拦截
     * <p/>
     *
     * @param servletRequest
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    private boolean isFilter(ServletRequest servletRequest) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String uri = request.getRequestURI();
        if (uri.startsWith("/api/v1/admin/") || uri.startsWith("/api/v1/app/")) {
            return true;
        }
        return false;
    }

    /**
     * 添加前缀
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    public String addPrefix(String data) {
        if (data.contains(MallConstant.UPLOAD_TYPE_IMAGE + "/")
                && !data.contains("data:image/png;base64")) {
            data = fileBusiness.prefixImage(data);
        }

        if (data.contains("file/")) {
            data = fileBusiness.prefixFile(data);
        }
        return data;
    }

}
