package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemStoreDomain;
import com.gao.glg.mall.api.mapper.EbSystemStoreMapper;
import com.gao.glg.mall.api.service.EbSystemStoreService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemStoreServiceImpl implements EbSystemStoreService  {
    @Resource
    private EbSystemStoreMapper systemStoreMapper;

    @Override
    public EbSystemStoreDomain getById(Integer id) {
        return systemStoreMapper.getById(id);
    }

    @Override
    public List<EbSystemStoreDomain> listByIds(List<Integer> ids) {
        return systemStoreMapper.listByIds(ids);
    }
}
