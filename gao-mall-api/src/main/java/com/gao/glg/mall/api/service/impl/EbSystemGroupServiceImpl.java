package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.bean.request.web.WebGroupSearchRequest;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemGroupDomain;
import com.gao.glg.mall.api.mapper.EbSystemGroupMapper;
import com.gao.glg.mall.api.service.EbSystemGroupService;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemGroupServiceImpl implements EbSystemGroupService  {
    @Resource
    private EbSystemGroupMapper systemGroupMapper;

    @Override
    public List<EbSystemGroupDomain> page(WebGroupSearchRequest request) {
        return systemGroupMapper.page(request);
    }

    @Override
    public void save(EbSystemGroupDomain domain) {
        systemGroupMapper.save(domain);
    }

    @Override
    public void delete(Integer id) {
        systemGroupMapper.delete(id);
    }

    @Override
    public void update(EbSystemGroupDomain domain) {
        systemGroupMapper.update(domain);
    }

    @Override
    public EbSystemGroupDomain getById(Integer id) {
        return systemGroupMapper.getById(id);
    }
}
