package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;

/**
 * EbUserExperienceRecordDomain
 * @author gaolinge
 */
@Data
public class EbUserExperienceRecordDomain {
    /** 
     * 记录id
     */
    private Integer id;

    /** 
     * 用户uid
     */
    private Integer uid;

    /** 
     * 关联id-orderNo,(sign,system默认为0）
     */
    private String linkId;

    /** 
     * 关联类型（order,sign,system）
     */
    private String linkType;

    /** 
     * 类型：1-增加，2-扣减
     */
    private Integer type;

    /** 
     * 标题
     */
    private String title;

    /** 
     * 经验
     */
    private Integer experience;

    /** 
     * 剩余
     */
    private Integer balance;

    /** 
     * 备注
     */
    private String mark;

    /** 
     * 状态：1-成功（保留字段）
     */
    private Integer status;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
