package com.gao.glg.mall.api.controller.web;

import com.gao.glg.mall.api.business.IAuthBusiness;
import com.gao.glg.mall.api.business.IRoleBusiness;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 角色中心
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin/auth")
@Api(tags = "角色中心")
public class WebAuthController {

    @Autowired
    private IAuthBusiness authBusiness;


}



