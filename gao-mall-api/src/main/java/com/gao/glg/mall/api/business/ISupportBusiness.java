package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.app.AppCityTreeResponse;

import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/6
 */
public interface ISupportBusiness {

    /**
     * 树
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    List<AppCityTreeResponse> tree();

    /**
     * 登入页图片
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/12
     * @version : 1.0.0
     */
    Map<String, Object> loginPic();

}
