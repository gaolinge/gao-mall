package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * 砍价商品header列表
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/22
 */
@Data
public class AppBargainHeaderResponse implements Serializable {

    @ApiModelProperty(value = "参与砍价总人数")
    private Integer bargainTotal;

    @ApiModelProperty(value = "砍价成功列表（默认7条）")
    private List<HashMap<String, Object>> bargainSuccessList;

}
