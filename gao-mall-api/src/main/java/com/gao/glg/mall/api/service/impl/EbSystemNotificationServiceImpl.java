package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemNotificationDomain;
import com.gao.glg.mall.api.mapper.EbSystemNotificationMapper;
import com.gao.glg.mall.api.service.EbSystemNotificationService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemNotificationServiceImpl implements EbSystemNotificationService  {
    @Resource
    private EbSystemNotificationMapper ebSystemNotificationMapper;
}
