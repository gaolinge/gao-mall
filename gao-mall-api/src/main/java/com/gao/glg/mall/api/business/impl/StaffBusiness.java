package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.bean.response.web.WebStaffResponse;
import com.gao.glg.mall.api.business.IStaffBusiness;
import com.gao.glg.mall.api.domain.EbSystemStoreDomain;
import com.gao.glg.mall.api.domain.EbSystemStoreStaffDomain;
import com.gao.glg.mall.api.domain.EbUserDomain;
import com.gao.glg.mall.api.service.EbSystemStoreService;
import com.gao.glg.mall.api.service.EbSystemStoreStaffService;
import com.gao.glg.mall.api.service.EbUserService;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.StreamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class StaffBusiness implements IStaffBusiness {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbSystemStoreService systemStoreService;
    @Autowired
    private EbSystemStoreStaffService systemStoreStaffService;

    @Override
    public PagerInfo<WebStaffResponse> list(Integer storeId, PagerDTO request) {
        PagerInfo pagerInfo = PagerUtil.page(request, () -> {
            List<EbSystemStoreStaffDomain> staffDomains = systemStoreStaffService.listByStoreId(storeId);
            if (CollectionUtils.isEmpty(staffDomains)) {
                return Collections.emptyList();
            }
            return BeanCopierUtil.copyList(staffDomains, WebStaffResponse.class);
        });
        List<WebStaffResponse> list = pagerInfo.getList();
        if (CollectionUtils.isEmpty(list)) {
            return pagerInfo;
        }

        List<Integer> uids = StreamUtil.map(list, WebStaffResponse::getUid);
        List<Integer> sids = StreamUtil.map(list, WebStaffResponse::getStoreId);
        List<EbUserDomain> userDomains = userService.listByIds(uids);
        List<EbSystemStoreDomain> storeDomains = systemStoreService.listByIds(sids);

        Map<Integer, EbUserDomain> userDomainMap = StreamUtil.listToMap(userDomains, EbUserDomain::getUid);
        Map<Integer, EbSystemStoreDomain> storeDomainMap = StreamUtil.listToMap(storeDomains, EbSystemStoreDomain::getId);

        list.forEach(item -> {
            item.setUser(userDomainMap.get(item.getUid()));
            item.setSystemStore(storeDomainMap.get(item.getStoreId()));
        });

        return pagerInfo;
    }
}
