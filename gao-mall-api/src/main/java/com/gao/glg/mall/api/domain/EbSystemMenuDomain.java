package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemMenuDomain
 * @author gaolinge
 */
@Data
public class EbSystemMenuDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 父级ID
     */
    private Integer pid;

    /** 
     * 名称
     */
    private String name;

    /** 
     * icon
     */
    private String icon;

    /** 
     * 权限标识
     */
    private String perms;

    /** 
     * 组件路径
     */
    private String component;

    /** 
     * 类型，M-目录，C-菜单，A-按钮
     */
    private String menuType;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 显示状态
     */
    private Boolean isShow;

    /** 
     * 是否删除
     */
    private Byte isDelte;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
