package com.gao.glg.mall.api.utils;


import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbOrderDomain;
import com.gao.glg.mall.api.service.EbOrderInfoService;
import com.gao.glg.mall.api.service.EbOrderService;
import com.gao.glg.mall.api.service.EbSystemConfigService;
import com.gao.glg.mall.api.service.EbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 订单工具类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Service
public class OrderUtil {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbSystemConfigService systemConfigService;
    @Autowired
    private EbOrderService orderService;
    @Autowired
    private EbOrderInfoService orderInfoService;

    /**
     * 检测支付渠道
     * @param payChannel 支付渠道
     */
    public static boolean checkPayChannel(String payChannel) {
       if (!payChannel.equals(MallConstant.PAY_CHANNEL_WE_CHAT_H5) &&
               !payChannel.equals(MallConstant.PAY_CHANNEL_WE_CHAT_PROGRAM) &&
               !payChannel.equals(MallConstant.PAY_CHANNEL_WE_CHAT_PUBLIC) &&
               !payChannel.equals(MallConstant.PAY_CHANNEL_WE_CHAT_APP_IOS) &&
               !payChannel.equals(MallConstant.PAY_CHANNEL_WE_CHAT_APP_ANDROID)) {
           return false;
       }
       return true;
    }

    /**
     * 校验支付类型
     * <p/>
     *
     * @param payType
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    public Boolean checkPayType(String payType){
        boolean result = false;
        payType = payType.toLowerCase();
        switch (payType){
            case MallConstant.PAY_TYPE_WE_CHAT:
                result = "1".equals(systemConfigService.getValueByKey(MallConstant.CONFIG_PAY_WEIXIN_OPEN));
                break;
            case MallConstant.PAY_TYPE_YUE:
                result = "1".equals(systemConfigService.getValueByKey(MallConstant.CONFIG_YUE_PAY_STATUS));
                break;
            case MallConstant.PAY_TYPE_ALI_PAY:
                result = "1".equals(systemConfigService.getValueByKey(MallConstant.CONFIG_ALI_PAY_STATUS));
                break;
        }
        return result;
    }


    /**
     * 获取支付名称
     * <p/>
     *
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    public String getPayName(String type){
        String payTypeStr = null;
        switch (type){
            case MallConstant.PAY_TYPE_WE_CHAT:
                payTypeStr = "微信支付";
                break;
            case MallConstant.PAY_TYPE_YUE:
                payTypeStr = "余额支付";
                break;
            case MallConstant.PAY_TYPE_OFFLINE:
                payTypeStr = "线下支付";
                break;
            case MallConstant.PAY_TYPE_ALI_PAY:
                payTypeStr = "支付宝支付";
                break;
            default:
                payTypeStr = "其他支付方式";
                break;
        }
        return payTypeStr;
    }


}
