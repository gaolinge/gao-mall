package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.web.WebExpressSearchRequest;
import com.gao.glg.mall.api.domain.EbExpressDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbExpressMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbExpressDomain domain);

    /**
     * 查询
     */
    List<EbExpressDomain> select();

    /**
     * 更新
     */
    int update(EbExpressDomain domain);

    /**
     * 查询
     */
    List<EbExpressDomain> page(@Param("request") WebExpressSearchRequest request);

    /**
     * 查询
     */
    List<EbExpressDomain> listByType(String type);

    /**
     * 查询
     */
    EbExpressDomain getById(Integer id);
}
