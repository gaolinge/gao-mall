package com.gao.glg.mall.api.constant;

/**
 * 支付相关常量类
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
public interface PayConstant {

    //支付方式
    String PAY_TYPE_WE_CHAT = "weixin"; //微信支付
    String PAY_TYPE_YUE = "yue"; //余额支付
    String PAY_TYPE_OFFLINE = "offline"; //线下支付
    String PAY_TYPE_ALI_PAY = "alipay"; //支付宝
    String PAY_TYPE_ZERO_PAY = "zeroPay"; // 零元付

    //支付渠道
    String PAY_CHANNEL_WE_CHAT_H5 = "weixinh5"; //H5唤起微信支付
    String PAY_CHANNEL_WE_CHAT_PUBLIC = "public"; //公众号
    String PAY_CHANNEL_WE_CHAT_PROGRAM = "routine"; //小程序
    String PAY_CHANNEL_WE_CHAT_APP_IOS = "weixinAppIos"; //微信App支付ios
    String PAY_CHANNEL_WE_CHAT_APP_ANDROID = "weixinAppAndroid"; //微信App支付android

    String PAY_CHANNEL_ALI_PAY = "alipay"; //支付宝支付
    String PAY_CHANNEL_ALI_APP_PAY = "appAliPay"; //支付宝App支付

    String WX_PAY_TRADE_TYPE_JS = "JSAPI";
    String WX_PAY_TRADE_TYPE_H5 = "MWEB";

    //微信支付接口请求地址
    String WX_PAY_API_URL = "https://api.mch.weixin.qq.com/";
    // 微信统一预下单
    String WX_PAY_API_URI = "pay/unifiedorder";
    // 微信查询订单
    String WX_PAY_ORDER_QUERY_API_URI = "pay/orderquery";
    // 微信支付回调地址
    String WX_PAY_NOTIFY_API_URI = "/api/admin/payment/callback/wechat";
    // 微信退款回调地址
    String WX_PAY_REFUND_NOTIFY_API_URI = "/api/admin/payment/callback/wechat/refund";

    String WX_PAY_SIGN_TYPE_MD5 = "MD5";
    String WX_PAY_SIGN_TYPE_SHA256 = "HMAC-SHA256";

    String PAY_BODY = "Crmeb支付中心-订单支付";
    String FIELD_SIGN = "sign";

    // 公共号退款
    String WX_PAY_REFUND_API_URI= "secapi/pay/refund";
}
