package com.gao.glg.mall.api.bean.request.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 预下单详情
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/10
 */
@Data
public class AppPreOrderItemRequest {

    @ApiModelProperty(value = "购物车编号，购物车预下单时必填")
    private Long shoppingCartId;

    @ApiModelProperty(value = "商品id（立即购买必填）")
    private Integer productId;

    @ApiModelProperty(value = "商品规格属性id（立即购买、活动购买必填）")
    private Integer attrValueId;

    @ApiModelProperty(value = "商品数量（立即购买、活动购买必填）")
    private Integer productNum;

    @ApiModelProperty(value = "订单编号（再次购买必填）")
    private String orderNo;

    @ApiModelProperty(value = "砍价商品id")
    private Integer bargainId;

    @ApiModelProperty(value = "用户砍价活动id")
    private Integer bargainUserId;

    @ApiModelProperty(value = "拼团商品id")
    private Integer combinationId;

    @ApiModelProperty(value = "拼团团长id")
    private Integer pinkId;

    @ApiModelProperty(value = "秒杀商品id")
    private Integer seckillId;

}
