package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.app.AppCartNumRequest;
import com.gao.glg.mall.api.bean.request.app.AppCartRequest;
import com.gao.glg.mall.api.bean.request.app.AppCartResetRequest;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
public interface ICartBusiness {

    /**
     * 分页
     * <p/>
     *
     * @param pagerDTO
     * @param isValid
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    PagerInfo page(PagerDTO pagerDTO, Boolean isValid);

    /**
     * 数量
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    Map<String, Integer> count(AppCartNumRequest request);

    /**
     * 加购
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    Map<String, String> save(AppCartRequest request);

    /**
     * 修改数量
     * <p/>
     *
     * @param id
     * @param number
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    void updateNum(Long id, Integer number);

    /**
     * 批量删除
     * <p/>
     *
     * @param ids
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    void delete(List<Long> ids);

    /**
     * 购物车重选提交
     * <p/>
     *
     * @param request
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    void reset(AppCartResetRequest request);
}
