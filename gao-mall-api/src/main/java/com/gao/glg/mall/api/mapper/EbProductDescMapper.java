package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbProductDescDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductDescMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductDescDomain domain);

    /**
     * 查询
     */
    List<EbProductDescDomain> select();

    /**
     * 更新
     */
    int update(EbProductDescDomain domain);

    /**
     * 查询
     */
    EbProductDescDomain geByProductId(@Param("pid") Integer pid, @Param("type") Integer type);

    /**
     * 查询
     */
    List<EbProductDescDomain> listByPidsAndType(@Param("pids") List<Integer> pids, @Param("type") Integer type);
}
