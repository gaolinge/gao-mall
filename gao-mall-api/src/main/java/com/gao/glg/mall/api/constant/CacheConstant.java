package com.gao.glg.mall.api.constant;

/**
 * 缓存常量
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
public interface CacheConstant {

    /** 用户注册信息缓存Key */
    String USER_REGISTER_KEY = "USER:REGISTER:";

    /** 商品浏览量（每日） */
    String PRO_PAGE_VIEW_KEY     = "statistics:product:page_view:";
    String PRO_PRO_PAGE_VIEW_KEY = "statistics:product:pro_page_view:{}:{}";

    /** 商品加购量（每日） */
    String PRO_ADD_CART_KEY     = "statistics:product:add_cart:";
    String PRO_PRO_ADD_CART_KEY = "statistics:product:pro_add_cart:{}:{}";

    /**
     * 用户登token redis存储前缀
     */
    String USER_APP_TOKEN_REDIS_KEY_PREFIX = "TOKEN:APP:USER:";
    String USER_WEB_TOKEN_REDIS_KEY_PREFIX = "TOKEN:WEB:USER:";
}
