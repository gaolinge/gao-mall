package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbWechatExceptionsDomain
 * @author gaolinge
 */
@Data
public class EbWechatExceptionsDomain {
    /** 
     * id
     */
    private Integer id;

    /** 
     * 错误码
     */
    private String errcode;

    /** 
     * 错误信息
     */
    private String errmsg;

    /** 
     * 回复数据
     */
    private String data;

    /** 
     * 备注
     */
    private String remark;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
