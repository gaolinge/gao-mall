package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserIntegralRecordDomain;
import com.gao.glg.mall.api.mapper.EbUserIntegralRecordMapper;
import com.gao.glg.mall.api.service.EbUserIntegralRecordService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserIntegralRecordServiceImpl implements EbUserIntegralRecordService  {
    @Resource
    private EbUserIntegralRecordMapper userIntegralRecordMapper;

    @Override
    public void save(EbUserIntegralRecordDomain domain) {
        userIntegralRecordMapper.save(domain);
    }
}
