package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemNotificationDomain
 * @author gaolinge
 */
@Data
public class EbSystemNotificationDomain {
    /** 
     * id
     */
    private Integer id;

    /** 
     * 标识
     */
    private String mark;

    /** 
     * 通知类型
     */
    private String type;

    /** 
     * 通知场景说明
     */
    private String description;

    /** 
     * 公众号模板消息（0：不存在，1：开启，2：关闭）
     */
    private Byte isWechat;

    /** 
     * 模板消息id
     */
    private Integer wechatId;

    /** 
     * 小程序订阅消息（0：不存在，1：开启，2：关闭）
     */
    private Byte isRoutine;

    /** 
     * 订阅消息id
     */
    private Integer routineId;

    /** 
     * 发送短信（0：不存在，1：开启，2：关闭）
     */
    private Byte isSms;

    /** 
     * 短信id
     */
    private Integer smsId;

    /** 
     * 发送类型（1：用户，2：管理员）
     */
    private Byte sendType;

    /** 
     * 创建时间
     */
    private Date createTime;


}
