package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempFreeRequest;
import com.gao.glg.mall.api.domain.EbShippingTempFreeDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbShippingTempFreeService {

    /**
     * 根据模板id和城市id查询包邮信息
     * <p/>
     *
     * @param tempId
     * @param cityId
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    EbShippingTempFreeDomain getByTempIdAndCityId(Integer tempId, Integer cityId);

    /**
     * 批量插入
     * <p/>
     *
     * @param list
     * @param type
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    void batchSave(List<WebLogisticsTempFreeRequest> list, Integer type, Integer id);

    /**
     * 根据模板id查询包邮信息
     * <p/>
     *
     * @param tempId
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    List<EbShippingTempFreeDomain> groupByUnqid(Integer tempId);
}
