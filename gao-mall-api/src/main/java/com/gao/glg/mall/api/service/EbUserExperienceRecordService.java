package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.response.app.AppUserExpResponse;
import com.gao.glg.mall.api.domain.EbUserExperienceRecordDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbUserExperienceRecordService {

    /**
     * 根据用户id查询
     * <p/>
     *
     * @param uid
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    List<AppUserExpResponse> listByUid(Integer uid);


    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/9
     * @version : 1.0.0
     */
    void save(EbUserExperienceRecordDomain domain);
}
