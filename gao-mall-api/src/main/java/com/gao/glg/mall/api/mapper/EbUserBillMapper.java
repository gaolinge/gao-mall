package com.gao.glg.mall.api.mapper;

import com.gao.glg.mall.api.domain.EbUserBillDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author gaolinge
 */
public interface EbUserBillMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserBillDomain domain);

    /**
     * 查询
     */
    List<EbUserBillDomain> select();

    /**
     * 更新
     */
    int update(EbUserBillDomain domain);

    /**
     * 查询
     */
    List<EbUserBillDomain> listByPmAndType(@Param("uid") Integer uid, @Param("pm") int pm, @Param("cate") String cate);
}
