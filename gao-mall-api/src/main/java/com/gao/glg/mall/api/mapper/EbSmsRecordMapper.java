package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSmsRecordDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSmsRecordMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSmsRecordDomain domain);

    /**
     * 查询
     */
    List<EbSmsRecordDomain> select();

    /**
     * 更新
     */
    int update(EbSmsRecordDomain domain);
}
