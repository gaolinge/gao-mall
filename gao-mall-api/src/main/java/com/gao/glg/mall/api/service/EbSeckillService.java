package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSeckillDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSeckillService {

    /**
     * 根据id查询
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    EbSeckillDomain getById(Integer id);

    /**
     * 根据timeId查询
     * <p/>
     *
     * @param timeId
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    List<EbSeckillDomain> getByTimeId(Integer timeId);

    /**
     *
     * <p/>
     *
     * @param productId
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbSeckillDomain> listByProductId(Integer productId);


}
