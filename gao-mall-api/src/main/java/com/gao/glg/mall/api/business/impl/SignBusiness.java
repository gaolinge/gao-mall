package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.response.app.AppSystemDataResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserSignInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserSignResponse;
import com.gao.glg.mall.api.business.ISignBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.*;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SignBusiness implements ISignBusiness {

    @Autowired
    private EbUserSignService userSignService;
    @Autowired
    private EbUserService userService;
    @Autowired
    private EbUserLevelService userLevelService;
    @Autowired
    private EbUserIntegralRecordService userIntegralRecordService;
    @Autowired
    private EbUserExperienceRecordService userExperienceRecordService;
    @Autowired
    private EbSystemGroupDataService systemGroupDataService;
    @Autowired
    private EbSystemUserLevelService systemUserLevelService;

    @Override
    public PagerInfo<AppUserSignResponse> list(PagerDTO request) {
        Integer uid = userService.getUserIdException();
        return PagerUtil.page(request, () -> {
            List<EbUserSignDomain> userSignDomains = userSignService.listByType(uid, 1);
            return BeanCopierUtil.copyList(userSignDomains, AppUserSignResponse.class);
        });
    }

    @Override
    public List<AppSystemDataResponse> signConfig() {
        List<Map<String, Object>> list = systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_SIGN);
        return BeanCopierUtil.copyList(list, AppSystemDataResponse.class);
    }

    @Override
    public AppUserSignInfoResponse info() {
        EbUserDomain user = userService.getUserInfo();
        String t1 = DateUtils.formatDate(new Date(), DateUtils.DATE_FORMAT);
        String t2 = DateUtils.formatDate(DateUtils.addDays(new Date(), -1), DateUtils.DATE_FORMAT);
        Integer sumNum = userSignService.countByType(user.getUid(), 1, null);
        Integer curNum = userSignService.countByType(user.getUid(), 1, t1);
        Integer yesNum = userSignService.countByType(user.getUid(), 1, t2);

        boolean isDaySign = curNum > 0;
        boolean isYesSign = yesNum > 0;

        Integer signNum = user.getSignNum();
        if (!isYesSign && !isDaySign) {
            signNum = 0;
        }
        if (signNum > 0 && signNum.equals(signConfig().size())) {
            signNum = 0;
            EbUserDomain userDomain = new EbUserDomain();
            userDomain.setUid(user.getUid());
            userDomain.setSignNum(0);
            userService.update(userDomain);
        }

        AppUserSignInfoResponse userSignInfoResponse = new AppUserSignInfoResponse();
        userSignInfoResponse.setSignNum(signNum);
        userSignInfoResponse.setSumSignDay(sumNum);
        userSignInfoResponse.setIsDaySign(isDaySign);
        userSignInfoResponse.setIsYesterdaySign(isYesSign);
        userSignInfoResponse.setIntegral(user.getIntegral());
        return userSignInfoResponse;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public AppSystemDataResponse sign() {
        EbUserDomain user = userService.getUserInfo();
        List<AppSystemDataResponse> configs = signConfig();
        if (CollUtil.isEmpty(configs)) {
            throw new BusinessException(ErrorConstant.NOT_CONFIG_SIGN);
        }

        int signNum = getSignNum(user, configs);
        AppSystemDataResponse signConfig = null;
        for (AppSystemDataResponse config : configs) {
            if (signNum == config.getDay()) {
                signConfig = config;
                break;
            }
        }

        if (ObjectUtil.isNull(signConfig)) {
            throw new BusinessException(ErrorConstant.NOT_CONFIG_SIGN);
        }

        List<EbSystemUserLevelDomain> select = systemUserLevelService.select();

        EbUserSignDomain userSignDomain = new EbUserSignDomain();
        userSignDomain.setUid(user.getUid());
        userSignDomain.setTitle(MallConstant.SIGN_TYPE_INTEGRAL_TITLE);
        userSignDomain.setNumber(signConfig.getIntegral());
        userSignDomain.setType(MallConstant.SIGN_TYPE_INTEGRAL);
        userSignDomain.setBalance(user.getIntegral() + signConfig.getIntegral());
        userSignDomain.setCreateDay(DateUtils.formatToDate(new Date(), DateUtils.DATE_FORMAT));

        EbUserIntegralRecordDomain pointRecordDomain = new EbUserIntegralRecordDomain();
        pointRecordDomain.setUid(user.getUid());
        pointRecordDomain.setLinkType(MallConstant.INTEGRAL_RECORD_LINK_TYPE_SIGN);
        pointRecordDomain.setType(MallConstant.INTEGRAL_RECORD_TYPE_ADD);
        pointRecordDomain.setTitle(MallConstant.BROKERAGE_RECORD_TITLE_SIGN);
        pointRecordDomain.setIntegral(signConfig.getIntegral());
        pointRecordDomain.setBalance(user.getIntegral() + signConfig.getIntegral());
        pointRecordDomain.setMark(StrUtil.format("签到积分奖励增加了{}积分", signConfig.getIntegral()));
        pointRecordDomain.setStatus(MallConstant.INTEGRAL_RECORD_STATUS_COMPLETE);

        EbUserExperienceRecordDomain expRecordDomain = new EbUserExperienceRecordDomain();
        expRecordDomain.setUid(user.getUid());
        expRecordDomain.setLinkType(MallConstant.EXPERIENCE_RECORD_LINK_TYPE_SIGN);
        expRecordDomain.setType(MallConstant.EXPERIENCE_RECORD_TYPE_ADD);
        expRecordDomain.setTitle(MallConstant.EXPERIENCE_RECORD_TITLE_SIGN);
        expRecordDomain.setExperience(signConfig.getExperience());
        expRecordDomain.setBalance(user.getExperience() + signConfig.getExperience());
        expRecordDomain.setMark(StrUtil.format("签到经验奖励增加了{}经验", signConfig.getExperience()));
        expRecordDomain.setStatus(MallConstant.EXPERIENCE_RECORD_STATUS_CREATE);

        EbUserDomain userDomain = new EbUserDomain();
        userDomain.setUid(user.getUid());
        userDomain.setSignNum(signNum);
        userDomain.setIntegral(signConfig.getIntegral());
        userDomain.setExperience(signConfig.getExperience());

        userService.update(userDomain);
        userLevelService.update(userDomain);
        userSignService.save(userSignDomain);
        userIntegralRecordService.save(pointRecordDomain);
        userExperienceRecordService.save(expRecordDomain);
        return signConfig;

    }

    private int getSignNum(EbUserDomain user, List<AppSystemDataResponse> config) {
        EbUserSignDomain signDomain = userSignService.getByLastSign(user.getUid());
        int signNum = user.getSignNum();
        if (signDomain == null) {
            // 没有签到过,重置签到次数
            signNum = 0;
        } else {
            Date date = DateUtils.formatToDate(new Date(), DateUtils.DATE_FORMAT);
            if (signDomain.getCreateDay().after(date)) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "今日已签到。不可重复签到");
            }
            Date currDate = DateUtils.addDays(signDomain.getCreateDay(), 1);
            Date date1 = DateUtils.formatToDate(currDate, DateUtils.DATE_FORMAT);
            if (!date.equals(date1)) {
                signNum = 0;
            }
        }

        if (user.getSignNum().equals(config.size())) {
            signNum = 0;
        }

        signNum = signNum + 1;
        return signNum;
    }
}
