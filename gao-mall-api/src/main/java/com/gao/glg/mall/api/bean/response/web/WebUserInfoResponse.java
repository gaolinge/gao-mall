package com.gao.glg.mall.api.bean.response.web;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 系统管理员
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/12
 */
@Data
public class WebUserInfoResponse implements Serializable {

    @ApiModelProperty(value = "手机号码")
    private String account;

    @ApiModelProperty(value = "手机号码")
    private String realName;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "角色")
    private String roles;

    @ApiModelProperty(value = "权限标识数组")
    private List<String> permissionsList;
}
