package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbBargainUserDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbBargainUserMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbBargainUserDomain domain);

    /**
     * 查询
     */
    List<EbBargainUserDomain> select();

    /**
     * 更新
     */
    int update(EbBargainUserDomain domain);
}
