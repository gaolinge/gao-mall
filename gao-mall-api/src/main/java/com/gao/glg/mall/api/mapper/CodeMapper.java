package com.gao.glg.mall.api.mapper;


import com.gao.glg.mall.api.bean.request.web.WebGenerateRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 *
 * @author gaolinge
 */
public interface CodeMapper {

    /**
     * 查询
     */
    List<Map<String, Object>> page(@Param("request") WebGenerateRequest request);
}
