package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.app.AppBargainRequest;
import com.gao.glg.mall.api.bean.response.app.*;
import com.gao.glg.mall.api.business.IBargainBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 砍价
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@Api(tags = "砍价商品")
@RestController
@RequestMapping("/app/bargain")
public class AppBargainController {

    @Autowired
    private IBargainBusiness bargainBusiness;

    @ApiOperation(value = "砍价首页信息")
    @GetMapping(value = "/index")
    public RestResult<List<AppBargainResponse>> index(){
        return RestResult.success(bargainBusiness.index());
    }

    @ApiOperation(value = "砍价商品列表header")
    @GetMapping(value = "/header")
    public RestResult<AppBargainHeaderResponse> header(){
        return RestResult.success(bargainBusiness.header());
    }

    @ApiOperation(value = "砍价商品列表")
    @GetMapping(value = "/list")
    public RestResult<PagerInfo<AppBargainInfoResponse>> list(@ModelAttribute PagerDTO request){
        return RestResult.success(bargainBusiness.list(request));
    }

    @ApiOperation(value = "获取用户砍价信息")
    @GetMapping(value = "/user")
    public RestResult<AppBargainUserResponse> info(@ModelAttribute @Validated AppBargainRequest request) {
        return RestResult.success(bargainBusiness.info(request));
    }

    @ApiOperation(value = "砍价商品详情")
    @GetMapping(value = "/detail/{id}")
    public RestResult<AppBargainDetailResponse> detail(@PathVariable(value = "id") Integer id) {
        return RestResult.success(bargainBusiness.detail(id));
    }

    @ApiOperation(value = "创建砍价活动")
    @PostMapping(value = "/start")
    public RestResult<Map<String, Object>> start(@RequestBody @Validated AppBargainRequest request) {
        return RestResult.success(bargainBusiness.start(request));
    }

    @ApiOperation(value = "砍价")
    @PostMapping(value = "/help")
    public RestResult<Map<String, Object>> help(@RequestBody @Validated AppBargainRequest request) {
        return RestResult.success(bargainBusiness.help(request));
    }

    @ApiOperation(value = "砍价记录")
    @GetMapping(value = "/record")
    public RestResult<PagerInfo<AppBargainRecordResponse>> records(@ModelAttribute PagerDTO request) {
        return RestResult.success(bargainBusiness.records(request));
    }

}
