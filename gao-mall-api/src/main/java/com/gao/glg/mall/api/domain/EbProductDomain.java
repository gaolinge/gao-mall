package com.gao.glg.mall.api.domain;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreProductDomain
 * @author gaolinge
 */
@Data
public class EbProductDomain {
    /** 
     * 商品id
     */
    private Integer id;

    /** 
     * 商户Id(0为总后台管理员创建,不为0的时候是商户后台创建)
     */
    private Integer merId;

    /** 
     * 商品图片
     */
    private String image;

    /** 
     * 轮播图
     */
    private String sliderImage;

    /** 
     * 商品名称
     */
    private String storeName;

    /** 
     * 商品简介
     */
    private String storeInfo;

    /** 
     * 关键字
     */
    private String keyword;

    /** 
     * 商品条码（一维码）
     */
    private String barCode;

    /** 
     * 分类id
     */
    private String cateId;

    /** 
     * 商品价格
     */
    private BigDecimal price;

    /** 
     * 会员价格
     */
    private BigDecimal vipPrice;

    /** 
     * 市场价
     */
    private BigDecimal otPrice;

    /** 
     * 邮费
     */
    private BigDecimal postage;

    /** 
     * 单位名
     */
    private String unitName;

    /** 
     * 排序
     */
    private Short sort;

    /** 
     * 销量
     */
    private Integer sales;

    /** 
     * 库存
     */
    private Integer stock;

    /** 
     * 状态（0：未上架，1：上架）
     */
    private Boolean isShow;

    /** 
     * 是否热卖
     */
    private Boolean isHot;

    /** 
     * 是否优惠
     */
    private Boolean isBenefit;

    /** 
     * 是否精品
     */
    private Boolean isBest;

    /** 
     * 是否新品
     */
    private Boolean isNew;

    /** 
     * 添加时间
     */
    private Integer addTime;

    /** 
     * 是否包邮
     */
    private Integer isPostage;

    /** 
     * 是否删除
     */
    private Integer isDel;

    /** 
     * 商户是否代理 0不可代理1可代理
     */
    private Integer merUse;

    /** 
     * 获得积分
     */
    private Integer giveIntegral;

    /** 
     * 成本价
     */
    private BigDecimal cost;

    /** 
     * 秒杀状态 0 未开启 1已开启
     */
    private Integer isSeckill;

    /** 
     * 砍价状态 0未开启 1开启
     */
    private Integer isBargain;

    /** 
     * 是否优品推荐
     */
    private Boolean isGood;

    /** 
     * 是否单独分佣
     */
    private Boolean isSub;

    /** 
     * 虚拟销量
     */
    private Integer ficti;

    /** 
     * 浏览量
     */
    private Integer browse;

    /** 
     * 商品二维码地址(用户小程序海报)
     */
    private String codePath;

    /** 
     * 淘宝京东1688类型
     */
    private String soureLink;

    /** 
     * 主图视频链接
     */
    private String videoLink;

    /** 
     * 运费模板ID
     */
    private Integer tempId;

    /** 
     * 规格 0单 1多
     */
    private Boolean specType;

    /** 
     * 活动显示排序0=默认, 1=秒杀，2=砍价，3=拼团
     */
    private String activity;

    /** 
     * 展示图
     */
    private String flatPattern;

    /** 
     * 是否回收站
     */
    private Boolean isRecycle;


}
