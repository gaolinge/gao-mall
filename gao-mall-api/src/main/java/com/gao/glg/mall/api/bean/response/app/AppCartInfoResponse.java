package com.gao.glg.mall.api.bean.response.app;

import com.gao.glg.serializer.FileUrlSerializerField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 购物车详情
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/4
 */
@Data
public class AppCartInfoResponse implements Serializable {

    @ApiModelProperty(value = "购物车表ID")
    private Long id;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @ApiModelProperty(value = "商品属性")
    private String productAttrUnique;

    @ApiModelProperty(value = "商品数量")
    private Integer cartNum;

    @FileUrlSerializerField
    @ApiModelProperty(value = "商品图片")
    private String image;

    @ApiModelProperty(value = "商品名称")
    private String storeName;

    @ApiModelProperty(value = "商品规格id")
    private Integer attrId;

    @ApiModelProperty(value = "商品属性索引值 (attr_value|attr_value[|....])")
    private String suk;

    @ApiModelProperty(value = "sku价格")
    private BigDecimal price;

    @ApiModelProperty(value = "商品是否有效")
    private Boolean attrStatus;

    @ApiModelProperty(value = "sku库存")
    private Integer stock;

    @ApiModelProperty(value = "sku会员价格")
    private BigDecimal vipPrice;
}
