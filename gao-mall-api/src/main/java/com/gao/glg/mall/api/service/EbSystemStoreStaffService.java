package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSystemStoreStaffDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemStoreStaffService {

    /**
     *
     * <p/>
     *
     * @param storeId
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    List<EbSystemStoreStaffDomain> listByStoreId(Integer storeId);
}
