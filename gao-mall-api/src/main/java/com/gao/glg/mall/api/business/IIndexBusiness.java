package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.app.AppIndexInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppSystemConfigResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/23
 */
public interface IIndexBusiness {

    /**
     * 首页信息
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    AppIndexInfoResponse info();

    /**
     * 分享信息
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    Map<String, String> share();

    /**
     * 颜色配置
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    AppSystemConfigResponse color();

    /**
     * 版本信息
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    Map<String, Object> version();

    /**
     * 商品列表
     * <p/>
     *
     * @param type
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    PagerInfo productList(Integer type, PagerDTO request);

    /**
     * 热搜词
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    List<Map<String, Object>> keywords();

    /**
     * 图片域名
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    String imageDomain();

}
