package com.gao.glg.mall.api;

import com.gao.glg.sentinel.annotation.EnableSentinel;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/7/3
 */
@Slf4j
//@EnableSentinel
@MapperScan(basePackages = "com.gao.glg.mall.api.mapper")
@SpringBootApplication(scanBasePackages = "com.gao.**", exclude = DataSourceAutoConfiguration.class)
public class MallApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(MallApiApplication.class, args);
        log.info("启动成功。。。");
    }
}