package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemStoreStaffDomain
 * @author gaolinge
 */
@Data
public class EbSystemStoreStaffDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 管理员id
     */
    private Integer uid;

    /** 
     * 店员头像
     */
    private String avatar;

    /** 
     * 门店id
     */
    private Integer storeId;

    /** 
     * 店员名称
     */
    private String staffName;

    /** 
     * 手机号码
     */
    private String phone;

    /** 
     * 核销开关
     */
    private Byte verifyStatus;

    /** 
     * 状态
     */
    private Byte status;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
