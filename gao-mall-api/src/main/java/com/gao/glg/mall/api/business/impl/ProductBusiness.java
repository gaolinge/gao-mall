package com.gao.glg.mall.api.business.impl;

import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.app.AppProductRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductAddRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppProductResponse;
import com.gao.glg.mall.api.bean.response.app.*;
import com.gao.glg.mall.api.bean.response.web.*;
import com.gao.glg.mall.api.business.IProductBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.*;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.mall.api.utils.CrmebUtil;
import com.gao.glg.mall.api.utils.MallUtil;
import com.gao.glg.mall.api.utils.StreamUtil;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.JSONUtils;
import com.gao.glg.utils.SplitUtil;
import com.github.houbb.heaven.util.lang.ObjectUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductBusiness implements IProductBusiness {

    @Autowired
    private EbSystemConfigService systemConfigService;
    @Autowired
    private EbUserService userService;
    @Autowired
    private EbSystemUserLevelService systemUserLevelService;
    @Autowired
    private EbUserVisitRecordService userVisitRecordService;
    @Autowired
    private EbProductRelationService productRelationService;
    @Autowired
    private EbProductService productService;
    @Autowired
    private EbProductDescService productDescService;
    @Autowired
    private EbProductReplyService productReplyService;
    @Autowired
    private EbProductAttrService productAttrService;
    @Autowired
    private EbProductAttrValueService productAttrValueService;
    @Autowired
    private EbProductCouponService productCouponService;
    @Autowired
    private EbSeckillService seckillService;
    @Autowired
    private EbBargainService bargainService;
    @Autowired
    private EbCombService combinationService;
    @Autowired
    private EbCategoryService categoryService;
    @Autowired
    private EbSeckillMangerService seckillMangerService;

    @Override
    public PagerInfo<AppIndexProductResponse> hotRecall(PagerDTO request) {
        return recommend(request, MallConstant.INDEX_HOT_BANNER);
    }

    @Override
    public PagerInfo<AppIndexProductResponse> recProduct() {
        PagerDTO request = new PagerDTO();
        request.setPageSize(9);
        return recommend(request, MallConstant.INDEX_RECOMMEND_BANNER);
    }

    @Override
    public PagerInfo<AppIndexProductResponse> appList(AppProductRequest request) {
        if (request.getCid() != null) {
            List<EbCategoryDomain> categoryDomains = categoryService.listByCid(request.getCid());
            List<Integer> cids = StreamUtil.map(categoryDomains, EbCategoryDomain::getId);
            cids.add(request.getCid());
            request.setCids(cids);
        }
        PagerInfo pageInfo = PagerUtil.page(request, () -> productService.appSearch(request));
        return recommend(pageInfo);
    }

    @Override
    public PagerInfo webList(WebProductSearchRequest request) {
        if (request.getType() == 4) {
            Integer stock = Integer.parseInt(systemConfigService.getValueByKey("store_stock"));
            request.setStock(stock);
        }
        PagerInfo pagerInfo = PagerUtil.page(request, () -> productService.webSearch(request));
        List<EbProductDomain> productDomains = pagerInfo.getList();
        if (CollectionUtils.isEmpty(productDomains)) {
            return pagerInfo;
        }
        List<Integer> pids = StreamUtil.map(productDomains, EbProductDomain::getId);
        List<EbProductDescDomain> productDescDomains = productDescService.listByPidsAndType(pids, MallConstant.PRODUCT_TYPE_NORMAL);
        List<EbProductRelationDomain> productRelationDomains = productRelationService.listByPidsAndType(pids, "collect");
        List<EbProductAttrDomain> productAttrDomains = productAttrService.listByPidsAndType(pids, MallConstant.PRODUCT_TYPE_NORMAL);
        List<EbProductAttrValueDomain> productAttrValueDomains = productAttrValueService.listByPidsAndType(pids, MallConstant.PRODUCT_TYPE_NORMAL);

        Map<Integer, EbProductDescDomain> descDomainMap = StreamUtil.listToMap(productDescDomains, EbProductDescDomain::getProductId);
        Map<Integer, List<EbProductRelationDomain>> relationDomainMap = StreamUtil.groupBy(productRelationDomains, EbProductRelationDomain::getProductId);
        Map<Integer, List<EbProductAttrDomain>> attrDomainMap = StreamUtil.groupBy(productAttrDomains, EbProductAttrDomain::getProductId);
        Map<Integer, List<EbProductAttrValueDomain>> attrValueDomainMap = StreamUtil.groupBy(productAttrValueDomains, EbProductAttrValueDomain::getProductId);

        List<WebProductResponse> responses =
                productDomains.stream().map(e -> {
                    EbProductDescDomain descDomain = descDomainMap.get(e.getId());
                    List<EbProductRelationDomain> relationDomains = relationDomainMap.get(e.getId());
                    List<EbProductAttrDomain> attrDomains = attrDomainMap.get(e.getId());
                    List<EbProductAttrValueDomain> attrValueDomains = attrValueDomainMap.get(e.getId());
                    List<WebProductAttrResponse> attrList = BeanCopierUtil.copyList(attrDomains, WebProductAttrResponse.class);
                    List<WebProductAttrValueResponse> attrValueList = BeanCopierUtil.copyList(attrValueDomains, WebProductAttrResponse.class);
                    Integer collectCount = relationDomains == null ? 0 : relationDomains.size();

                    WebProductResponse response = BeanCopierUtil.copy(e, WebProductResponse.class);
                    response.setCollectCount(collectCount);
                    response.setAttr(attrList);
                    response.setAttrValue(attrValueList);
                    response.setContent(descDomain.getDescription());
                    return response;
        }).collect(Collectors.toList());
        pagerInfo.setList(responses);
        return pagerInfo;
    }

    @Override
    public void save(WebProductAddRequest request) {

    }

    @Override
    public void update(WebProductAddRequest request) {

    }

    @Override
    public void delete(Integer id, String type) {

    }

    @Override
    public void reset(Integer id) {

    }

    @Override
    public WebProductInfoResponse webInfo(Integer id) {
        EbProductDomain productDomain = productService.getById(id);
        if (productDomain == null) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT);
        }
        EbProductDescDomain productDescDomain = productDescService.geByProductId(
                productDomain.getId(), MallConstant.PRODUCT_TYPE_NORMAL);
        List<EbProductAttrDomain> productAttrDomains = productAttrService.listByPidAndType(
                productDomain.getId(), MallConstant.PRODUCT_TYPE_NORMAL);
        List<EbProductAttrValueDomain> productAttrValueDomains = productAttrValueService.listByPidAndType(
                productDomain.getId(), MallConstant.PRODUCT_TYPE_NORMAL);
        List<EbProductCouponDomain> productCouponDomains = productCouponService.listByPid(productDomain.getId());

        List<String> activityName = getActivityName(productDomain.getActivity());
        List<String> sliderImage = JSONUtils.parseObject(productDomain.getSliderImage(), List.class);
        List<Integer> couponIds = StreamUtil.map(productCouponDomains, EbProductCouponDomain::getIssueCouponId);
        List<WebProductAttrResponse> attr = BeanCopierUtil.copyList(productAttrDomains, WebProductAttrResponse.class);
        List<WebProductAttrValueResponse> attrValue = BeanCopierUtil.copyList(productAttrValueDomains, WebProductAttrValueResponse.class);
        WebProductInfoResponse response = BeanCopierUtil.copy(productDomain, WebProductInfoResponse.class);
        response.setSliderImage(sliderImage);
        response.setCouponIds(couponIds);
        response.setContent(productDescDomain.getDescription());
        response.setActivity(activityName);
        response.setAttr(attr);
        response.setAttrValue(attrValue);
        return response;
    }

    @Override
    public List<WebProductTabsResponse> tabs() {
        List<WebProductTabsResponse> responses = new ArrayList<>();
        for (int i = 1; i < 6; i++) {
            Integer stock = i != 4 ? null : Integer.parseInt(systemConfigService.getValueByKey("store_stock"));
            Integer count = productService.countByType(i, stock);
            String name = null;
            switch (i) {
                case 1:
                    name = "出售中商品";
                    break;
                case 2:
                    name = "仓库中商品";
                    break;
                case 3:
                    name = "已经售馨商品";
                    break;
                case 4:
                    name = "警戒库存";
                    break;
                case 5:
                    name = "商品回收站";
                    break;
            }
            WebProductTabsResponse response = new WebProductTabsResponse();
            response.setName(name);
            response.setType(i);
            response.setCount(count == null ? 0 : count);
            responses.add(response);
        }
        return responses;
    }

    @Override
    public AppProductDetailResponse detail(Integer pid, String type) {
        EbProductDomain productDomain = productService.getById(pid);
        if (ObjectUtil.isNull(productDomain)) {
            throw new BusinessException(1);
        }
        EbProductDescDomain productDescDomain = productDescService.geByProductId(
                pid, MallConstant.PRODUCT_TYPE_NORMAL);
        AppProductResponse productInfo = BeanCopierUtil.copy(productDomain, AppProductResponse.class);
        if (productDescDomain != null) {
            productInfo.setContent(productDescDomain.getDescription());
        }

        // 会员价格
        EbUserDomain userDomain = userService.getUserInfo();
        EbSystemUserLevelDomain userLevelDomain = null;
        if (userDomain != null && userDomain.getLevel() != null) {
            userLevelDomain = systemUserLevelService.getById(userDomain.getLevel());
            if (userLevelDomain != null) {
                productDomain.setVipPrice(productDomain.getPrice());
            }
        }

        // 商品属性
        List<EbProductAttrDomain> attrList = productAttrService.listByPidAndType(
                productDomain.getId(), MallConstant.PRODUCT_TYPE_NORMAL);
        List<AppProductAttrResponse> productAttr = BeanCopierUtil.copyList(attrList, AppProductAttrResponse.class);

        // 商品属性值
        Map<String, Object> productValue = new HashMap<>();
        List<EbProductAttrValueDomain> productAttrValueDomains = productAttrValueService
                .listByPidAndType(productDomain.getId(), MallConstant.PRODUCT_TYPE_NORMAL);

        for (EbProductAttrValueDomain productAttrValueDomain : productAttrValueDomains) {
            AppProductAttrValueResponse productAttrValueResponse =
                    BeanCopierUtil.copy(productAttrValueDomain, AppProductAttrValueResponse.class);
            if (userLevelDomain != null) {
                productAttrValueResponse.setVipPrice(productAttrValueResponse.getPrice());
            }
            productValue.put(productAttrValueResponse.getSuk(), productAttrValueResponse);
        }

        // 商品活动
        List<AppProductActItemResponse> activityAllH5 = getProductAllActivity(productDomain);

        // 用户收藏、分销返佣
        AppProductDetailResponse productDetailResponse = new AppProductDetailResponse();
        if (userDomain != null) {
            productDetailResponse.setUserCollect(productRelationService.isLike(pid, false));
            String brokerageFuncStatus = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_BROKERAGE_FUNC_STATUS);
            if (brokerageFuncStatus.equals(MallConstant.COMMON_SWITCH_OPEN)) {
                String isBubble = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_STORE_BROKERAGE_IS_BUBBLE);
                if (isBubble.equals(MallConstant.COMMON_SWITCH_OPEN)) {
                    String priceRange = getPriceRange(productDomain.getIsSub(), productAttrValueDomains, userDomain.getIsPromoter());
                    productDetailResponse.setPriceName(priceRange);
                }
            }
        }

        productDetailResponse.setProductInfo(productInfo);
        productDetailResponse.setProductAttr(productAttr);
        productDetailResponse.setProductValue(productValue);
        productDetailResponse.setActivityAllH5(activityAllH5);

        productService.browseAdd(pid);
        userVisitRecordService.addRecord(2);
        return productDetailResponse;
    }

    @Override
    public AppProductReplayCountResponse replyCount(Integer pid) {
        Long totalNum  = productReplyService.replyCount(pid, null);
        Long goodNum   = productReplyService.replyCount(pid, 1);
        Long mediumNum = productReplyService.replyCount(pid, 2);
        Long lowerNum  = productReplyService.replyCount(pid, 3);
        EbProductReplyDomain productReplyDomain = productReplyService.sumScore(pid);

        String goodRate = calcGoodRate(totalNum, goodNum);
        Integer starLevel = calcStarLevel(productReplyDomain);

        AppProductReplayCountResponse response = new AppProductReplayCountResponse();
        response.setSumCount(totalNum);
        response.setGoodCount(goodNum);
        response.setInCount(mediumNum);
        response.setPoorCount(lowerNum);
        response.setReplyStar(starLevel);
        response.setReplyChance(goodRate);
        return response;
    }


    @Override
    public AppProductReplyDetailResponse productReply(Integer pid) {
        Long totalNum = productReplyService.replyCount(pid, null);
        if (totalNum == null || totalNum == 0) {
            AppProductReplyDetailResponse response = new AppProductReplyDetailResponse();
            response.setSumCount(0);
            response.setReplyChance("0");
            return response;
        }

        // 好评总数
        Long goodNum = productReplyService.replyCount(pid, 1);
        EbProductReplyDomain productReplyDomain = productReplyService.getByPidLimitOne(pid);

        String goodRate = calcGoodRate(totalNum, goodNum);
        Integer starLevel = calcStarLevel(productReplyDomain);
        String nickname = convertNickname(productReplyDomain);

        AppProductReplyRecordResponse replyRecordResponse = BeanCopierUtil.copy(
                productReplyDomain, AppProductReplyRecordResponse.class);
        replyRecordResponse.setPics(CrmebUtil.stringToArrayStr(productReplyDomain.getPics()));
        replyRecordResponse.setScore(starLevel);
        replyRecordResponse.setNickname(nickname);

        AppProductReplyDetailResponse response = new AppProductReplyDetailResponse();
        response.setSumCount(totalNum.intValue());
        response.setReplyChance(goodRate);
        response.setProductReply(replyRecordResponse);
        return response;
    }

    /**
     * 转换昵称
     * <p/>
     *
     * @param productReplyDomain
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    private String convertNickname(EbProductReplyDomain productReplyDomain) {
        String nickname = productReplyDomain.getNickname();
        if (nickname != null) {
            if (nickname.length() == 1) {
                nickname = nickname.concat("**");
            } else if (nickname.length() == 2) {
                nickname = nickname.substring(0, 1) + "**";
            } else {
                nickname = nickname.substring(0, 1) + "**" + nickname.substring(nickname.length() - 1);
            }
        }
        return nickname;
    }

    /**
     * 计算好评率
     * <p/>
     *
     * @param totalNum
     * @param goodNum
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    private String calcGoodRate(Long totalNum, Long goodNum) {
        return totalNum != null && totalNum > 0 && goodNum != null && goodNum > 0 ?
                String.format("%.2f", ((goodNum.doubleValue() / totalNum.doubleValue()))) : "0";
    }

    /**
     * 计算商品星级
     * <p/>
     *
     * @param productReplyDomain
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    private Integer calcStarLevel(EbProductReplyDomain productReplyDomain) {
        if (productReplyDomain != null
                && (productReplyDomain.getProductScore() != null
                ||  productReplyDomain.getServiceScore() != null)) {
            int totalScore = productReplyDomain.getProductScore() + productReplyDomain.getServiceScore();
            return new BigDecimal(totalScore).divide(BigDecimal.valueOf(2L), 0, BigDecimal.ROUND_DOWN).intValue();
        }
        return 0;
    }

    /**
     * 获取商品佣金区间
     * <p/>
     *
     * @param isSub
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    private String getPriceRange(Boolean isSub, List<EbProductAttrValueDomain> attrValueList, Boolean isPromoter) {
        if (!isPromoter) {
            return "0";
        }

        // 获取一级返佣比例
        String ratio = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_STORE_BROKERAGE_RATIO);
        BigDecimal BrokerRatio = new BigDecimal(ratio).divide(new BigDecimal("100"), 2, RoundingMode.DOWN);
        BigDecimal maxPrice;
        BigDecimal minPrice;
        if (isSub) {
            maxPrice = attrValueList.stream().map(EbProductAttrValueDomain::getBrokerage).reduce(BigDecimal.ZERO,BigDecimal::max);
            minPrice = attrValueList.stream().map(EbProductAttrValueDomain::getBrokerage).reduce(BigDecimal.ZERO,BigDecimal::min);
        } else {
            BigDecimal _maxPrice = attrValueList.stream().map(EbProductAttrValueDomain::getPrice).reduce(BigDecimal.ZERO,BigDecimal::max);
            BigDecimal _minPrice = attrValueList.stream().map(EbProductAttrValueDomain::getPrice).reduce(BigDecimal.ZERO,BigDecimal::min);
            maxPrice = BrokerRatio.multiply(_maxPrice).setScale(2, RoundingMode.HALF_UP);
            minPrice = BrokerRatio.multiply(_minPrice).setScale(2, RoundingMode.HALF_UP);
        }

        String priceName;
        if (minPrice.compareTo(BigDecimal.ZERO) == 0 && maxPrice.compareTo(BigDecimal.ZERO) == 0) {
            priceName = "0";
        } else if (minPrice.compareTo(BigDecimal.ZERO) == 0 && maxPrice.compareTo(BigDecimal.ZERO) > 0) {
            priceName = maxPrice.toString();
        } else if (minPrice.compareTo(BigDecimal.ZERO) > 0 && maxPrice.compareTo(BigDecimal.ZERO) > 0) {
            priceName = minPrice.toString();
        } else if (minPrice.compareTo(maxPrice) == 0) {
            priceName = minPrice.toString();
        } else {
            priceName = minPrice + "~" + maxPrice;
        }
        return priceName;
    }

    public List<AppProductActItemResponse> getProductAllActivity(EbProductDomain productDomain) {
        if(productDomain.getActivity() == null) {
            return new ArrayList<>();
        }

        Map<Integer, AppProductActItemResponse> activityMap = getProductActivity(
                productDomain.getId(), productDomain.getActivity());

        List<AppProductActItemResponse> actItemResponses = new ArrayList<>();
        List<Integer> activityList = CrmebUtil.stringToArrayInt(productDomain.getActivity());
        for (Integer code : activityList) {
            if(null != activityMap.get(code)){
                actItemResponses.add(activityMap.get(code));
            }
        }
        return actItemResponses;
    }


    /**
     * 获取活动名
     *
     * @param txt
     * @return
     */
    private List<String> getActivityName(String txt) {
        return SplitUtil.split(txt, ',').stream().map(e -> {
            Integer i = Integer.valueOf(e);
            if (i.equals(MallConstant.PRODUCT_TYPE_NORMAL)) {
                return MallConstant.PRODUCT_TYPE_NORMAL_STR;
            }
            if (i.equals(MallConstant.PRODUCT_TYPE_SECKILL)) {
                return MallConstant.PRODUCT_TYPE_SECKILL_STR;
            }
            if (i.equals(MallConstant.PRODUCT_TYPE_BARGAIN)) {
                return MallConstant.PRODUCT_TYPE_BARGAIN_STR;
            }
            if (i.equals(MallConstant.PRODUCT_TYPE_PINGTUAN)) {
                return MallConstant.PRODUCT_TYPE_PINGTUAN_STR;
            }
            return null;
        }).collect(Collectors.toList());
    }

    /**
     * 推荐商品
     * <p/>
     *
     * @param request
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    public PagerInfo<AppIndexProductResponse> recommend(PagerDTO request, int type) {
        PagerInfo pageInfo = PagerUtil.page(request, () -> productService.listByType(type));
        return recommend(pageInfo);
    }

    public PagerInfo<AppIndexProductResponse> recommend(PagerInfo pageInfo) {
        if (CollectionUtils.isEmpty(pageInfo.getList())) {
            pageInfo.setList(Collections.emptyList());
            return pageInfo;
        }

        List<AppIndexProductResponse> productResponses = new ArrayList<>();
        for (Object e : pageInfo.getList()) {
            EbProductDomain domain = (EbProductDomain) e;
            AppIndexProductResponse productResponse = new AppIndexProductResponse();
            BeanUtils.copyProperties(domain, productResponse);
            productResponses.add(productResponse);

            List<Integer> activityList = CrmebUtil.stringToArrayInt(domain.getActivity());
            if (activityList.get(0).equals(MallConstant.PRODUCT_TYPE_NORMAL)) {
                continue;
            }

            Map<Integer, AppProductActItemResponse> activityByProduct =
                    getProductActivity(domain.getId(), domain.getActivity());
            if (CollectionUtils.isEmpty(activityByProduct)) {
                continue;
            }

            for (Integer activity : activityList) {
                AppProductActItemResponse itemResponse = activityByProduct.get(activity);
                if (ObjectUtil.isNotNull(itemResponse)) {
                    productResponse.setActivityH5(itemResponse);
                    break;
                }
            }
        }
        pageInfo.setList(productResponses);
        return pageInfo;
    }

    /**
     * 获取商品活动
     * <p/>
     *
     * @param productId
     * @param activity
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    public Map<Integer, AppProductActItemResponse> getProductActivity(Integer productId, String activity) {
        if (StringUtils.isBlank(activity)) {
            return null;
        }
        Map<Integer, AppProductActItemResponse> result = new HashMap<>();
        List<Integer> activitys = CrmebUtil.stringToArrayInt(activity);
        for (Integer code : activitys) {
            switch (code) {
                case 1:
                    List<EbSeckillDomain> seckillDomains = seckillService.listByProductId(productId);
                    if (!CollectionUtils.isEmpty(seckillDomains)) {
                        EbSeckillDomain seckillDomain = seckillDomains.get(0);
                        EbSeckillMangerDomain seckillMangerDomain = seckillMangerService.getById(seckillDomain.getTimeId());
                        Long timestamp = MallUtil.timestamp(seckillMangerDomain.getEndTime());

                        AppProductActItemResponse response = new AppProductActItemResponse();
                        response.setId(seckillDomain.getId());
                        response.setTime(timestamp.intValue());
                        response.setType(MallConstant.PRODUCT_TYPE_SECKILL + "");
                        result.put(code, response);
                    }
                    break;
                case 2:
                    List<EbBargainDomain> bargainDomains = bargainService.listByProductId(productId);
                    if (!CollectionUtils.isEmpty(bargainDomains)) {
                        EbBargainDomain bargainDomain = bargainDomains.get(0);
                        long time = bargainDomain.getStopTime() / 1000;

                        AppProductActItemResponse response = new AppProductActItemResponse();
                        response.setId(bargainDomain.getId());
                        response.setTime((int) time);
                        response.setType(MallConstant.PRODUCT_TYPE_BARGAIN + "");
                        result.put(code, response);
                    }
                    break;
                case 3:
                    List<EbCombinationDomain> combinationDomains = combinationService.listByProductId(productId);
                    if (!CollectionUtils.isEmpty(combinationDomains)) {
                        EbCombinationDomain combinationDomain = combinationDomains.get(0);
                        long time = combinationDomain.getStopTime() / 1000;

                        AppProductActItemResponse response = new AppProductActItemResponse();
                        response.setId(combinationDomain.getId());
                        response.setTime((int) time);
                        response.setType(MallConstant.PRODUCT_TYPE_PINGTUAN + "");
                        result.put(code, response);
                    }
                    break;
            }
        }
        return result;
    }
}
