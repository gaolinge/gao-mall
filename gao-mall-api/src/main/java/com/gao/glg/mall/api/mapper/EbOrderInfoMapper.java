package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbOrderInfoDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbOrderInfoMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbOrderInfoDomain domain);

    /**
     * 查询
     */
    List<EbOrderInfoDomain> select();

    /**
     * 更新
     */
    int update(EbOrderInfoDomain domain);

    /**
     * 查询
     */
    List<EbOrderInfoDomain> listByOrderId(Integer orderId);
}
