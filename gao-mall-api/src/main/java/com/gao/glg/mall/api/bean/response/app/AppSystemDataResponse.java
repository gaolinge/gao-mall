package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/7
 */
@Data
public class AppSystemDataResponse implements Serializable {

    private Integer id;

    @ApiModelProperty(value = "显示文字")
    private String title;

    @ApiModelProperty(value = "第几天")
    private Integer day;

    @ApiModelProperty(value = "积分")
    private Integer integral;

    @ApiModelProperty(value = "经验")
    private Integer experience;
}
