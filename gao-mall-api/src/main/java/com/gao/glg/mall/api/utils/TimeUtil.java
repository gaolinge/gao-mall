package com.gao.glg.mall.api.utils;

import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.DateStartBean;
import com.gao.glg.mall.api.constant.MallConstant;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
public class TimeUtil {

    public static DateStartBean getDateLimit(String data){
        /*//时间计算
        String t1  = null;
        String t2  = DateUtil.nowDateTime(MallConstant.DATE_FORMAT);
        String day = DateUtil.nowDateTime(MallConstant.DATE_FORMAT_START);
        String end = DateUtil.nowDateTime(MallConstant.DATE_FORMAT_END);

        if(!StringUtils.isBlank(data)){
            switch (data){
                case MallConstant.SEARCH_DATE_DAY:
                    t1 = day;
                    break;
                case MallConstant.SEARCH_DATE_YESTERDAY:
                    t1 = DateUtil.addDay(day, -1, MallConstant.DATE_FORMAT_START);
                    t2 = DateUtil.addDay(end, -1, MallConstant.DATE_FORMAT_END);
                    break;
                case MallConstant.SEARCH_DATE_LATELY_7:
                    t1 = DateUtil.addDay(day, -6, MallConstant.DATE_FORMAT_START);
                    break;
                case MallConstant.SEARCH_DATE_WEEK:
                    t1 = getWeekStartDay();
                    t2 = getWeekEndDay();
                    break;
                case MallConstant.SEARCH_DATE_PRE_WEEK:
                    t1 = getLastWeekStartDay();
                    t2 = getLastWeekEndDay();
                    break;
                case MallConstant.SEARCH_DATE_LATELY_30:
                    t1 = DateUtil.addDay(day, -30, MallConstant.DATE_FORMAT_START);
                    break;
                case MallConstant.SEARCH_DATE_MONTH:
                    t1 = DateUtil.nowDateTime(MallConstant.DATE_FORMAT_MONTH_START);
                    t2 = getMonthEndDay();
                    break;
                case MallConstant.SEARCH_DATE_PRE_MONTH:
                    t1 = getLastMonthStartDay();
                    t2 = getLastMonthEndDay();
                    break;
                case MallConstant.SEARCH_DATE_YEAR:
                    t1 = DateUtil.nowDateTime(MallConstant.DATE_FORMAT_YEAR_START);
                    t2 = DateUtil.nowDateTime(MallConstant.DATE_FORMAT_YEAR_END);
                    break;
                case MallConstant.SEARCH_DATE_PRE_YEAR:
                    t1 = getLastYearStartDay();
                    t2 = getLastYearEndDay();
                    break;
                default:
                    List<String> list = CrmebUtil.stringToArrayStr(data);
                    if(list.size() == 1){
                        throw new IllegalArgumentException("选择时间参数格式错误，请在 " +
                                MallConstant.SEARCH_DATE_DAY + "|" +
                                MallConstant.SEARCH_DATE_YESTERDAY + "|" +
                                MallConstant.SEARCH_DATE_LATELY_7 + "|" +
                                MallConstant.SEARCH_DATE_LATELY_30 + "|" +
                                MallConstant.SEARCH_DATE_MONTH + "|" +
                                MallConstant.SEARCH_DATE_YEAR + "|自定义时间范围（格式：yyyy-MM-dd HH:mm:ss，两个时间范围用逗号分割）");
                    }
                    t1 = list.get(0);
                    t2 = list.get(1);

                    t1 = DateUtil.appointedDayStrToFormatStr(t1, MallConstant.DATE_FORMAT_DATE, MallConstant.DATE_FORMAT_START);
                    t2 = DateUtil.appointedDayStrToFormatStr(t2, MallConstant.DATE_FORMAT_DATE, MallConstant.DATE_FORMAT_END);
                    break;
            }
        }
        return new dateLimitUtilVo(t1, t2);*/
        return null;
    }
}
