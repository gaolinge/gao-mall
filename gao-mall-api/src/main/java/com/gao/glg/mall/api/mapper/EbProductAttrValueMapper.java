package com.gao.glg.mall.api.mapper;

import com.gao.glg.mall.api.domain.EbCartDomain;
import com.gao.glg.mall.api.domain.EbProductAttrValueDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author gaolinge
 */
public interface EbProductAttrValueMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductAttrValueDomain domain);

    /**
     * 查询
     */
    List<EbProductAttrValueDomain> select();

    /**
     * 更新
     */
    int update(EbProductAttrValueDomain domain);

    /**
     * 查询
     */
    List<EbProductAttrValueDomain> listByPidAndType(@Param("productId") Integer productId,
                                                    @Param("type") Integer type);

    /**
     * 查询
     */
    EbProductAttrValueDomain listByPidAndTypeAndAttrId(@Param("pid") Integer pid,
                                                             @Param("aid") String aid,
                                                             @Param("type") Integer type);

    /**
     * 查询
     */
    List<EbProductAttrValueDomain> listByCart(@Param("list") List<EbCartDomain> list, @Param("type") Integer type);

    /**
     * 查询
     */
    List<EbProductAttrValueDomain> listByPidsAndType(@Param("pids") List<Integer> pids, @Param("type") Integer type);
}
