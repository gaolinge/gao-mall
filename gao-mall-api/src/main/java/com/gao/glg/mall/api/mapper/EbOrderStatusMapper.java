package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbOrderStatusDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbOrderStatusMapper {



    /**
     * 保存
     */
    int save(EbOrderStatusDomain domain);

    /**
     * 查询
     */
    List<EbOrderStatusDomain> select();


}
