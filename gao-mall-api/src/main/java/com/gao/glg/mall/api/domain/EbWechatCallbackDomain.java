package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbWechatCallbackDomain
 * @author gaolinge
 */
@Data
public class EbWechatCallbackDomain {
    /** 
     * 主键ID
     */
    private Integer id;

    /** 
     * 商家小程序名称
     */
    private String toUserName;

    /** 
     * 微信团队的 OpenID(固定值)
     */
    private String fromUserName;

    /** 
     * 事件时间,Unix时间戳
     */
    private Long createTime;

    /** 
     * 消息类型
     */
    private String msgType;

    /** 
     * 事件类型
     */
    private String event;

    /** 
     * 内容
     */
    private String content;

    /** 
     * 创建时间
     */
    private Date addTime;


}
