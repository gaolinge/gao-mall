package com.gao.glg.mall.api.domain;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
/**
 * EbCategoryDomain
 * @author gaolinge
 */
@Data
public class EbCategoryDomain implements Serializable {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 父级ID
     */
    private Integer pid;

    /** 
     * 路径
     */
    private String path;

    /** 
     * 分类名称
     */
    private String name;

    /** 
     * 类型，1 产品分类，2 附件分类，3 文章分类， 4 设置分类， 5 菜单分类，6 配置分类， 7 秒杀配置
     */
    private Integer type;

    /** 
     * 地址
     */
    private String url;

    /** 
     * 扩展字段 Jsos格式
     */
    private String extra;

    /** 
     * 状态, 1正常，0失效
     */
    private Boolean status;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
