package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.web.WebGroupSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemGroupDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemGroupMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemGroupDomain domain);

    /**
     * 查询
     */
    List<EbSystemGroupDomain> select();

    /**
     * 更新
     */
    int update(EbSystemGroupDomain domain);

    /**
     * 查询
     */
    List<EbSystemGroupDomain> page(@Param("request") WebGroupSearchRequest request);

    /**
     * 查询
     */
    EbSystemGroupDomain getById(Integer id);
}
