package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebGroupRequest;
import com.gao.glg.mall.api.bean.request.web.WebGroupSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppCombinationIndexResponse;
import com.gao.glg.mall.api.bean.response.web.WebGroupResponse;
import com.gao.glg.page.PagerInfo;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/25
 */
public interface ICombBusiness {

    /**
     * 首页
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/25
     * @version : 1.0.0
     */
    AppCombinationIndexResponse index();

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    PagerInfo<WebGroupResponse> page(WebGroupSearchRequest request);

    /**
     * 新增
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void create(WebGroupRequest request);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 修改
     * <p/>
     *
     * @param id
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void update(Integer id, WebGroupRequest request);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    WebGroupResponse detail(Integer id);
}
