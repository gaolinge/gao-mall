package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.app.AppUserCollectAllRequest;
import com.gao.glg.mall.api.bean.request.app.AppUserCollectRequest;
import com.gao.glg.mall.api.bean.response.app.AppUserCollectResponse;
import com.gao.glg.mall.api.business.ICollectBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 商品点赞和收藏表
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/collect")
@Api(tags = "点赞/收藏")
public class AppCollectController {

    @Autowired
    private ICollectBusiness collectBusiness;

    @ApiOperation(value = "我的收藏列表")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public RestResult<PagerInfo<AppUserCollectResponse>> list(@Validated PagerDTO request) {
        return RestResult.success(collectBusiness.list(request));
    }

    @ApiOperation(value = "添加收藏产品")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public RestResult<String> save(@RequestBody @Validated AppUserCollectRequest request) {
        collectBusiness.add(request);
        return RestResult.success();
    }

    @ApiOperation(value = "取消收藏产品")
    @RequestMapping(value = "/cancel/{pid}", method = RequestMethod.POST)
    public RestResult<String> cancel(@PathVariable Integer pid) {
        collectBusiness.canal(pid);
        return RestResult.success();
    }

    @ApiOperation(value = "取消收藏产品")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public RestResult<String> delete(@RequestBody Map<String, String> map) {
        collectBusiness.delete(map);
        return RestResult.success();
    }

    @ApiOperation(value = "批量收藏")
    @RequestMapping(value = "/all", method = RequestMethod.POST)
    public RestResult<String> all(@RequestBody @Validated AppUserCollectAllRequest request) {
        collectBusiness.batchAdd(request);
        return RestResult.success();
    }
}



