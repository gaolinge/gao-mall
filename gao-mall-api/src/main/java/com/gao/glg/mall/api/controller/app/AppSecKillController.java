package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.app.AppSeckillDetailResponse;
import com.gao.glg.mall.api.bean.response.app.AppSeckillIndexResponse;
import com.gao.glg.mall.api.business.ISeckillBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 秒杀商品
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/seckill")
@Api(tags = "秒杀商品")
public class AppSecKillController {

    @Autowired
    private ISeckillBusiness seckillBusiness;

    @ApiOperation(value = "秒杀首页数据")
    @GetMapping(value = "/index")
    public RestResult<AppSeckillIndexResponse> index() {
        return RestResult.success(seckillBusiness.index());
    }

    @ApiOperation(value = "详情")
    @GetMapping(value = "/detail/{id}")
    public RestResult<AppSeckillDetailResponse> detail(
            @PathVariable(value = "id") Integer id) {
        return RestResult.success(seckillBusiness.detail(id));
    }

    /*@ApiOperation(value = "秒杀Header")
    @RequestMapping(value = "/header", method = RequestMethod.GET)
    public RestResult<List<SecKillResponse>> header() {
        return RestResult.success(storeSeckillService.getForH5Index());
    }

    @ApiOperation(value = "秒杀列表")
    @RequestMapping(value = "/list/{timeId}", method = RequestMethod.GET)
    public RestResult<PagerInfo<StoreSecKillH5Response>> list(@PathVariable("timeId") String timeId, @ModelAttribute PagerDTO request) {
        return RestResult.success(storeSeckillService.getKillListByTimeId(timeId, request));
    }

    @ApiOperation(value = "详情")
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public RestResult<StoreSeckillDetailResponse> info(@PathVariable(value = "id") Integer id) {
        return RestResult.success(storeSeckillService.getDetailH5(id));
    }*/
}
