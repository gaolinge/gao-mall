package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.CategoryTreeResponse;
import com.gao.glg.mall.api.bean.response.web.WebCateSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebCategoryRequest;
import com.gao.glg.mall.api.business.ICategoryBusiness;
import com.gao.glg.mall.api.domain.EbCategoryDomain;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 分类
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/16
 */
@Slf4j
@RestController
@RequestMapping("/admin/category")
@Api(tags = "分类服务")
public class WebCateController {

    @Autowired
    private ICategoryBusiness categoryBusiness;

    //@PreAuthorize("hasAuthority('admin:category:list:tree')")
    @ApiOperation(value = "获取tree结构的列表")
    @RequestMapping(value = "/list/tree", method = RequestMethod.GET)
    public RestResult<List<CategoryTreeResponse>> tree(@RequestParam(name = "type") Integer type,
                                                       @RequestParam(name = "status") Integer status,
                                                       @RequestParam(name = "name", required = false) String name) {
        return RestResult.success(categoryBusiness.tree(type, name, status));
    }

    @ApiOperation(value = "分页分类列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<EbCategoryDomain>> list(@ModelAttribute WebCateSearchRequest request) {
        return RestResult.success(categoryBusiness.list(request));
    }

    //@PreAuthorize("hasAuthority('admin:category:save')")
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult<String> save(@Validated WebCategoryRequest request) {
        categoryBusiness.save(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:category:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public RestResult<String> delete(@RequestParam(value = "id") Integer id) {
        categoryBusiness.delete(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:category:update')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiImplicitParam(name="id", value="分类ID")
    public RestResult<String> update(@RequestParam Integer id, @ModelAttribute WebCategoryRequest request) {
        categoryBusiness.update(id, request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:category:list:ids')")
    @ApiOperation(value = "根据id集合获取分类列表")
    @RequestMapping(value = "/list/ids", method = RequestMethod.GET)
    public RestResult<List<EbCategoryDomain>> getByIds(@Validated @RequestParam(name = "ids") String ids) {
        return RestResult.success(categoryBusiness.lisByIds(ids));
    }

    //@PreAuthorize("hasAuthority('admin:category:update:status')")
    @ApiOperation(value = "更改分类状态")
    @RequestMapping(value = "/updateStatus/{id}", method = RequestMethod.GET)
    public RestResult updateStatus(@Validated @PathVariable(name = "id") Integer id) {
        categoryBusiness.updateStatus(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:category:info')")
    @ApiOperation(value = "分类详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ApiImplicitParam(name="id", value="分类ID")
    public RestResult<EbCategoryDomain> info(@RequestParam(value = "id") Integer id) {
        return RestResult.success(categoryBusiness.info(id));
    }


}



