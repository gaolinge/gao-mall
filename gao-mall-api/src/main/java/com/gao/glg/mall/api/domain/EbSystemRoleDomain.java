package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemRoleDomain
 * @author gaolinge
 */
@Data
public class EbSystemRoleDomain {
    /** 
     * 身份管理id
     */
    private Integer id;

    /** 
     * 身份管理名称
     */
    private String roleName;

    /** 
     * 身份管理权限(menus_id)
     */
    private String rules;

    /** 
     * 
     */
    private Integer level;

    /** 
     * 状态
     */
    private Boolean status;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
