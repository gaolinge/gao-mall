package com.gao.glg.mall.api.domain;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreProductLogDomain
 * @author gaolinge
 */
@Data
public class EbProductLogDomain {
    /** 
     * 统计ID
     */
    private Integer id;

    /** 
     * 类型visit,cart,order,pay,collect,refund
     */
    private String type;

    /** 
     * 商品ID
     */
    private Integer productId;

    /** 
     * 用户ID
     */
    private Integer uid;

    /** 
     * 是否浏览
     */
    private Boolean visitNum;

    /** 
     * 加入购物车数量
     */
    private Integer cartNum;

    /** 
     * 下单数量
     */
    private Integer orderNum;

    /** 
     * 支付数量
     */
    private Integer payNum;

    /** 
     * 支付金额
     */
    private BigDecimal payPrice;

    /** 
     * 商品成本价
     */
    private BigDecimal costPrice;

    /** 
     * 支付用户ID
     */
    private Integer payUid;

    /** 
     * 退款数量
     */
    private Integer refundNum;

    /** 
     * 退款金额
     */
    private BigDecimal refundPrice;

    /** 
     * 收藏
     */
    private Boolean collectNum;

    /** 
     * 添加时间
     */
    private Long addTime;


}
