package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebGroupDataRequest;
import com.gao.glg.mall.api.bean.request.web.WebGroupDataSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebGroupDataResponse;
import com.gao.glg.page.PagerInfo;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
public interface ICombDataBusiness {

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    PagerInfo<WebGroupDataResponse> page(WebGroupDataSearchRequest request);

    /**
     * 新增
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void create(WebGroupDataRequest request);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 修改
     * <p/>
     *
     * @param id
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void update(Integer id, WebGroupDataRequest request);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    WebGroupDataResponse detail(Integer id);
}
