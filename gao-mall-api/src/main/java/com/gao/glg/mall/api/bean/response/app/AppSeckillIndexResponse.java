package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 秒杀首页
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/23
 */
@Data
public class AppSeckillIndexResponse implements Serializable {

    @ApiModelProperty(value = "秒杀时段信息")
    private AppSecKillResponse secKillResponse;

    @ApiModelProperty(value = "秒杀商品信息")
    private List<AppSeckillProductResponse> productList;
}
