package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.app.AppSeckillDetailResponse;
import com.gao.glg.mall.api.bean.response.app.AppSeckillIndexResponse;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/23
 */
public interface ISeckillBusiness {

    /**
     * 秒杀大首页
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    AppSeckillIndexResponse index();

    /**
     * 秒杀详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    AppSeckillDetailResponse detail(Integer id);
}
