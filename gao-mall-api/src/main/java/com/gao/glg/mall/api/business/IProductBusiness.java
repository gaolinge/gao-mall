package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.app.AppProductRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductAddRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppIndexProductResponse;
import com.gao.glg.mall.api.bean.response.app.AppProductDetailResponse;
import com.gao.glg.mall.api.bean.response.app.AppProductReplayCountResponse;
import com.gao.glg.mall.api.bean.response.app.AppProductReplyDetailResponse;
import com.gao.glg.mall.api.bean.response.web.WebProductInfoResponse;
import com.gao.glg.mall.api.bean.response.web.WebProductResponse;
import com.gao.glg.mall.api.bean.response.web.WebProductTabsResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/28
 */
public interface IProductBusiness {

    /**
     * 热门召回
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    PagerInfo<AppIndexProductResponse> hotRecall(PagerDTO request);


    /**
     * 详情
     * <p/>
     *
     * @param id
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    AppProductDetailResponse detail(Integer id, String type);

    /**
     * 评论数量和好评率
     * <p/>
     *
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    AppProductReplayCountResponse replyCount(Integer pid);

    /**
     * 评论详情
     * <p/>
     *
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    AppProductReplyDetailResponse productReply(Integer pid);

    /**
     * 推荐商品
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    PagerInfo<AppIndexProductResponse> recProduct();

    /**
     * 商品列表
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    PagerInfo<AppIndexProductResponse> appList(AppProductRequest request);

    /**
     * 商品列表
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    PagerInfo<WebProductResponse> webList(WebProductSearchRequest request);

    /**
     * 保存商品
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    void save(WebProductAddRequest request);

    /**
     * 修改商品
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    void update(WebProductAddRequest request);

    /**
     * 删除商品
     * <p/>
     *
     * @param id
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    void delete(Integer id, String type);

    /**
     * 恢复商品
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    void reset(Integer id);

    /**
     * 商品表头数量
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    List<WebProductTabsResponse> tabs();

    /**
     * 商品详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    WebProductInfoResponse webInfo(Integer id);
}
