package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbUserSignDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbUserSignService {

    /**
     * 列表
     * <p/>
     *
     * @param uid
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    List<EbUserSignDomain> listByType(Integer uid, int type);

    /**
     * 统计
     * <p/>
     *
     * @param uid
     * @param type
     * @param time
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    Integer countByType(Integer uid, int type, String time);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/9
     * @version : 1.0.0
     */
    void save(EbUserSignDomain domain);

    /**
     * 最后一条签到记录
     * <p/>
     *
     * @param uid
     * @return
     * @author : gaolinge
     * @date : 2024/12/9
     * @version : 1.0.0
     */
    EbUserSignDomain getByLastSign(Integer uid);
}
