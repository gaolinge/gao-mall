package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempSearchRequest;
import com.gao.glg.mall.api.domain.EbShippingTempDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbShippingTempService {

    /**
     * 根据id查询模板
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    EbShippingTempDomain getById(Integer id);

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    List<EbShippingTempDomain> page(WebLogisticsTempSearchRequest request);

    /**
     * 根据模板名称查询模板
     * <p/>
     *
     * @param name
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    EbShippingTempDomain getByName(String name);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    void save(EbShippingTempDomain domain);

    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    void update(EbShippingTempDomain domain);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    void delete(Integer id);
}
