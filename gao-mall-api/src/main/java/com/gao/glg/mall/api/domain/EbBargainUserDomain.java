package com.gao.glg.mall.api.domain;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreBargainUserDomain
 * @author gaolinge
 */
@Data
public class EbBargainUserDomain {
    /** 
     * 用户参与砍价表ID
     */
    private Integer id;

    /** 
     * 用户ID
     */
    private Integer uid;

    /** 
     * 砍价商品id
     */
    private Integer bargainId;

    /** 
     * 砍价的最低价
     */
    private BigDecimal bargainPriceMin;

    /** 
     * 砍价金额
     */
    private BigDecimal bargainPrice;

    /** 
     * 砍掉的价格
     */
    private BigDecimal price;

    /** 
     * 状态 1参与中 2 活动结束参与失败 3活动结束参与成功
     */
    private Byte status;

    /** 
     * 参与时间
     */
    private Long addTime;

    /** 
     * 是否取消
     */
    private Boolean isDel;


}
