package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/28
 */
@Data
public class AppProductDetailResponse implements Serializable {

    @ApiModelProperty(value = "商品信息")
    private AppProductResponse productInfo;

    @ApiModelProperty(value = "收藏标识")
    private Boolean userCollect;

    @ApiModelProperty(value = "产品属性")
    private List<AppProductAttrResponse> productAttr;

    @ApiModelProperty(value = "商品属性详情")
    private Map<String, Object> productValue;

    @ApiModelProperty(value = "返佣金额区间")
    private String priceName;

    @ApiModelProperty(value = "为移动端特定参数 所有参与的活动")
    private List<AppProductActItemResponse> activityAllH5;


}
