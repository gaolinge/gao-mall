package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbUserSignDomain
 * @author gaolinge
 */
@Data
public class EbUserSignDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 用户uid
     */
    private Integer uid;

    /** 
     * 签到说明
     */
    private String title;

    /** 
     * 获得
     */
    private Integer number;

    /** 
     * 剩余
     */
    private Integer balance;

    /** 
     * 类型，1积分，2经验
     */
    private Integer type;

    /** 
     * 签到日期
     */
    private Date createDay;

    /** 
     * 添加时间
     */
    private Date createTime;


}
