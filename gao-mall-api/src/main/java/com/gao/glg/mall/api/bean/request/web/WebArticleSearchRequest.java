package com.gao.glg.mall.api.bean.request.web;

import com.gao.glg.page.PagerDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class WebArticleSearchRequest extends PagerDTO {

    @ApiModelProperty(value = "分类id")
    private String cid;

    @ApiModelProperty(value = "搜索关键字")
    private String keywords;

}
