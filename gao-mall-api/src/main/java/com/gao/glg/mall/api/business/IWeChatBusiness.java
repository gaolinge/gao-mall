package com.gao.glg.mall.api.business;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/2
 */
public interface IWeChatBusiness {

    /**
     *
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/2
     * @version : 1.0.0
     */
    String logo();

}
