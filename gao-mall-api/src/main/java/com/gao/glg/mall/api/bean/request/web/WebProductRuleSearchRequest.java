package com.gao.glg.mall.api.bean.request.web;

import com.gao.glg.page.PagerDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 商品规则
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/27
 */
@Data
public class WebProductRuleSearchRequest extends PagerDTO {

    @ApiModelProperty(value = "搜索关键字")
    private String keywords;
}
