package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemAdminDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemAdminMapper {

    /**
     * 删除
     */
    int delete(Integer id);

    /**
     * 保存
     */
    int save(EbSystemAdminDomain domain);

    /**
     * 查询
     */
    List<EbSystemAdminDomain> select();

    /**
     * 更新
     */
    int update(EbSystemAdminDomain domain);

    /**
     * 查询
     */
    EbSystemAdminDomain getByAccount(String account);

    /**
     * 查询
     */
    List<EbSystemAdminDomain> page(@Param("request") WebEmployeeSearchRequest request);

    /**
     * 查询
     */
    EbSystemAdminDomain getById(Integer id);
}
