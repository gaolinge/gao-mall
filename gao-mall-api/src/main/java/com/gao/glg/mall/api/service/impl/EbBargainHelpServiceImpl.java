package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbBargainHelpMapper;
import com.gao.glg.mall.api.service.EbBargainHelpService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbBargainHelpServiceImpl implements EbBargainHelpService {
    @Resource
    private EbBargainHelpMapper ebBargainHelpMapper;
}
