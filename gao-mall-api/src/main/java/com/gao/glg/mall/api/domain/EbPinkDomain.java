package com.gao.glg.mall.api.domain;

import lombok.Data;

import java.math.BigDecimal;

/**
 * EbStorePinkDomain
 * @author gaolinge
 */
@Data
public class EbPinkDomain {
    /** 
     * 拼团ID
     */
    private Integer id;

    /** 
     * 用户id
     */
    private Integer uid;

    /** 
     * 订单id 生成
     */
    private String orderId;

    /** 
     * 订单id  数据库
     */
    private Integer orderIdKey;

    /** 
     * 购买商品个数
     */
    private Integer totalNum;

    /** 
     * 购买总金额
     */
    private BigDecimal totalPrice;

    /** 
     * 拼团商品id
     */
    private Integer cid;

    /** 
     * 商品id
     */
    private Integer pid;

    /** 
     * 拼图总人数
     */
    private Integer people;

    /** 
     * 拼团商品单价
     */
    private BigDecimal price;

    /** 
     * 开始时间
     */
    private Long addTime;

    /** 
     * 结束时间
     */
    private Long stopTime;

    /** 
     * 团长id 0为团长
     */
    private Integer kId;

    /** 
     * 是否发送模板消息0未发送1已发送
     */
    private Byte isTpl;

    /** 
     * 是否退款 0未退款 1已退款
     */
    private Byte isRefund;

    /** 
     * 状态1进行中2已完成3未完成
     */
    private Byte status;

    /** 
     * 是否虚拟拼团
     */
    private Boolean isVirtual;

    /** 
     * 用户昵称
     */
    private String nickname;

    /** 
     * 用户头像
     */
    private String avatar;


}
