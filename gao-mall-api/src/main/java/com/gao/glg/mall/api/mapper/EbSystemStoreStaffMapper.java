package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemStoreStaffDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemStoreStaffMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemStoreStaffDomain domain);

    /**
     * 查询
     */
    List<EbSystemStoreStaffDomain> select();

    /**
     * 更新
     */
    int update(EbSystemStoreStaffDomain domain);

    /**
     * 查询
     */
    List<EbSystemStoreStaffDomain> listByStoreId(Integer storeId);

}
