package com.gao.glg.mall.api.mapper;

import com.gao.glg.mall.api.domain.EbPinkDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbPinkMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbPinkDomain domain);

    /**
     * 查询
     */
    List<EbPinkDomain> select();

    /**
     * 更新
     */
    int update(EbPinkDomain domain);

    /**
     * 查询
     */
    List<EbPinkDomain> findSizePink(int limit);

    /**
     * 查询
     */
    Integer getTotalPeople();

}
