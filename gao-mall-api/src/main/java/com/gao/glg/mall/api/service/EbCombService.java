package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbCombinationDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbCombService {

    /**
     * 获取前n个拼团商品
     * <p/>
     *
     * @param limit
     * @return
     * @author : gaolinge
     * @date : 2024/11/25
     * @version : 1.0.0
     */
    List<EbCombinationDomain> listByStock(int limit);

    /**
     * 根据商品id查询
     * <p/>
     *
     * @param productId
     * @return
     * @author : gaolinge
     * @date : 2024/11/25
     * @version : 1.0.0
     */
    List<EbCombinationDomain> listByProductId(Integer productId);
}
