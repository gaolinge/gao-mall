package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.app.AppCartNumRequest;
import com.gao.glg.mall.api.bean.request.app.AppCartRequest;
import com.gao.glg.mall.api.bean.request.app.AppCartResetRequest;
import com.gao.glg.mall.api.bean.response.app.AppCartInfoResponse;
import com.gao.glg.mall.api.business.ICartBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 购物车
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@Api(tags = "购物车")
@RestController
@RequestMapping("/app/cart")
public class AppCartController {

    @Autowired
    private ICartBusiness cartBusiness;


    @ApiOperation(value = "分页列表") //配合swagger使用
    @GetMapping(value = "/list")
    public RestResult<PagerInfo<AppCartInfoResponse>> page(@RequestParam Boolean isValid, @Validated PagerDTO pagerDTO) {
        return RestResult.success(cartBusiness.page(pagerDTO, isValid));
    }

    @ApiOperation(value = "获取购物车数量")
    @GetMapping(value = "/count")
    public RestResult<Map<String, Integer>> count(@Validated AppCartNumRequest request) {
        return RestResult.success(cartBusiness.count(request));
    }

    @ApiOperation(value = "新增")
    @PostMapping(value = "/save")
    public RestResult<Map<String,String>> save(@RequestBody @Validated AppCartRequest request) {
        return RestResult.success(cartBusiness.save(request));
    }

    @ApiOperation(value = "修改")
    @PostMapping(value = "/num")
    public RestResult updateNum(@RequestParam Long id, @RequestParam Integer number) {
        cartBusiness.updateNum(id, number);
        return RestResult.success();
    }

    @ApiOperation(value = "删除")
    @PostMapping(value = "/delete")
    public RestResult delete(@RequestParam(value = "ids") List<Long> ids) {
        cartBusiness.delete(ids);
        return RestResult.success();
    }

    @ApiOperation(value = "购物车重选提交")
    @PostMapping(value = "/reset")
    public RestResult reset(@RequestBody @Validated AppCartResetRequest request){
        cartBusiness.reset(request);
        return RestResult.success();
    }
}



