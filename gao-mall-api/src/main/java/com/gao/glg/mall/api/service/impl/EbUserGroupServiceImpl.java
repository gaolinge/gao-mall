package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserGroupDomain;
import com.gao.glg.mall.api.mapper.EbUserGroupMapper;
import com.gao.glg.mall.api.service.EbUserGroupService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserGroupServiceImpl implements EbUserGroupService  {
    @Resource
    private EbUserGroupMapper ebUserGroupMapper;
}
