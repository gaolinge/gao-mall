package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemNotificationDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemNotificationMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemNotificationDomain domain);

    /**
     * 查询
     */
    List<EbSystemNotificationDomain> select();

    /**
     * 更新
     */
    int update(EbSystemNotificationDomain domain);
}
