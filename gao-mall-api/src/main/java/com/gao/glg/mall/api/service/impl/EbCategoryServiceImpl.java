package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.bean.response.web.WebCateSearchRequest;
import com.gao.glg.mall.api.domain.EbCategoryDomain;
import com.gao.glg.mall.api.mapper.EbCategoryMapper;
import com.gao.glg.mall.api.service.EbCategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbCategoryServiceImpl implements EbCategoryService  {

    @Resource
    private EbCategoryMapper categoryMapper;

    @Override
    public List<EbCategoryDomain> tree(Integer type, Integer status, String name, List<Integer> categoryIds) {
        return categoryMapper.listByTypeAndStatus(type, status,name, categoryIds);
    }

    @Override
    public List<EbCategoryDomain> listByCid(Integer cid) {
        return categoryMapper.listByCid("/"+cid+"/");
    }

    @Override
    public List<EbCategoryDomain> listByType(int type) {
        return categoryMapper.listByTypeAndStatus(type, 1, null, null);
    }

    @Override
    public List<EbCategoryDomain> listByCids(List<Integer> cids) {
        return categoryMapper.listByCids(cids);
    }

    @Override
    public List<EbCategoryDomain> search(WebCateSearchRequest request) {
        return categoryMapper.search(request);
    }

    @Override
    public void save(EbCategoryDomain category) {
        categoryMapper.save(category);
    }

    @Override
    public int queryNum(String name, Integer type) {
        return categoryMapper.queryNum(name, type);
    }

    @Override
    public EbCategoryDomain getById(Integer id) {
        return categoryMapper.getById(id);
    }

    @Override
    public int queryChildNum(Integer id) {
        return categoryMapper.queryChildNum("/"+id+"/");
    }

    @Override
    public void delete(Integer id) {
        categoryMapper.delete(id);
    }

    @Override
    public void updateStatus(Integer id) {
        categoryMapper.updateStatus(id);
    }
}
