package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.request.app.AppCartRequest;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbCartDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbCartMapper;
import com.gao.glg.mall.api.service.EbCartService;

import java.util.Date;
import java.util.List;


@Service
public class EbCartServiceImpl implements EbCartService {
    @Resource
    private EbCartMapper cartMapper;

    @Override
    public List<EbCartDomain> listByUserId(Integer userId, Boolean isValid) {
        return cartMapper.listByUserId(userId, isValid);
    }

    @Override
    public int countTotal(Integer userId, Boolean status) {
        return cartMapper.countTotal(userId, status);
    }

    @Override
    public int countNum(Integer userId, Boolean status) {
        Integer i = cartMapper.countNum(userId, status);
        return i == null ? 0 : i;
    }

    @Override
    public Long update(Long id, Integer num) {
        EbCartDomain cartDomain1 = new EbCartDomain();
        cartDomain1.setId(id);
        cartDomain1.setCartNum(num);
        cartDomain1.setUpdateTime(new Date());
        cartMapper.update(cartDomain1);
        return id;
    }

    @Override
    public Long save(Integer uid, AppCartRequest request) {
        EbCartDomain cartDomain = new EbCartDomain();
        cartDomain.setUid(uid);
        cartDomain.setType(MallConstant.STORE_REPLY_TYPE_PRODUCT);
        cartDomain.setProductId(request.getProductId());
        cartDomain.setProductAttrUnique(request.getProductAttrUnique());
        cartDomain.setCartNum(request.getCartNum());
        cartDomain.setIsNew(false);
        cartDomain.setCreateTime(new Date());
        cartDomain.setUpdateTime(new Date());
        cartDomain.setStatus(true);
        cartMapper.save(cartDomain);
        return cartDomain.getId();
    }

    @Override
    public List<EbCartDomain> listByUserIdAndAttrId(Integer uid, String attrId) {
        return cartMapper.listByUserIdAndAttrId(uid, attrId);
    }

    @Override
    public EbCartDomain getById(Long id) {
        return cartMapper.getById(id);
    }

    @Override
    public void batchDelete(List<Long> ids) {
        cartMapper.batchDelete(ids);
    }
}
