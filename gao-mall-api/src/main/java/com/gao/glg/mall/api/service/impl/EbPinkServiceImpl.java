package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbPinkDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbPinkMapper;
import com.gao.glg.mall.api.service.EbPinkService;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbPinkServiceImpl implements EbPinkService {
    @Resource
    private EbPinkMapper pinkMapper;

    @Override
    public List<EbPinkDomain> findSizePink(int limit) {
        return pinkMapper.findSizePink(limit);
    }

    @Override
    public Integer getTotalPeople() {
        return pinkMapper.getTotalPeople();
    }
}
