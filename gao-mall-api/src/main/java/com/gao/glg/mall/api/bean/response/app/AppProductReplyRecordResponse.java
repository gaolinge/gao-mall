package com.gao.glg.mall.api.bean.response.app;

import com.gao.glg.serializer.FileUrlSerializerField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 商品评论H5详情
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/29
 */
@Data
public class AppProductReplyRecordResponse implements Serializable {

    @ApiModelProperty(value = "评论ID")
    private Integer id;

    @ApiModelProperty(value = "用户ID")
    private Integer uid;

    @ApiModelProperty(value = "商品分数")
    private Integer score;

    @ApiModelProperty(value = "评论内容")
    private String comment;

    @FileUrlSerializerField
    @ApiModelProperty(value = "评论图片")
    private List<String> pics;

    @ApiModelProperty(value = "管理员回复内容")
    private String merchantReplyContent;

    @ApiModelProperty(value = "用户名称")
    private String nickname;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "评论时间")
    private Date createTime;

    @ApiModelProperty(value = "商品规格属性值")
    private String sku;
}
