package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductLogMapper;
import com.gao.glg.mall.api.service.EbProductLogService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbProductLogServiceImpl implements EbProductLogService {
    @Resource
    private EbProductLogMapper ebProductLogMapper;
}
