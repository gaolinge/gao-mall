package com.gao.glg.mall.api.bean.request.web;

import com.gao.glg.page.PagerDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class WebExpressSearchRequest extends PagerDTO {

    @ApiModelProperty(value = "搜索关键字", required = true)
    private String keywords;

}
