package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbProductCouponDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductCouponMapper;
import com.gao.glg.mall.api.service.EbProductCouponService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbProductCouponServiceImpl implements EbProductCouponService {
    @Resource
    private EbProductCouponMapper productCouponMapper;

    @Override
    public List<EbProductCouponDomain> listByPid(Integer id) {
        return productCouponMapper.listByPid(id);
    }
}
