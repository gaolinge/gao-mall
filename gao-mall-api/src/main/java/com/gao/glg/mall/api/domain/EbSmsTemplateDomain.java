package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSmsTemplateDomain
 * @author gaolinge
 */
@Data
public class EbSmsTemplateDomain {
    /** 
     * id
     */
    private Integer id;

    /** 
     * 短信模板id
     */
    private String tempId;

    /** 
     * 模板类型
     */
    private Byte tempType;

    /** 
     * 模板说明
     */
    private String title;

    /** 
     * 类型
     */
    private String type;

    /** 
     * 模板编号
     */
    private String tempKey;

    /** 
     * 状态
     */
    private Byte status;

    /** 
     * 短息内容
     */
    private String content;

    /** 
     * 添加时间
     */
    private Date createTime;


}
