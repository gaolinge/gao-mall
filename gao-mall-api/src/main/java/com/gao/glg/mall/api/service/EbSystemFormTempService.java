package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebFormCheckRequest;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemFormTempService {

    /**
     * 校验
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void check(WebFormCheckRequest request);
}
