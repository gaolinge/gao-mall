package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.bean.request.web.WebGenerateRequest;
import com.gao.glg.mall.api.business.ICodeBusiness;
import com.gao.glg.mall.api.service.CodeService;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CodeBusiness implements ICodeBusiness {

    @Autowired
    private CodeService codeService;

    @Override
    public PagerInfo<Map<String, Object>> page(WebGenerateRequest request) {
        return PagerUtil.page(request, () -> codeService.page(request));
    }

    @Override
    public byte[] generator(String[] tables) {
        return new byte[0];
    }
}
