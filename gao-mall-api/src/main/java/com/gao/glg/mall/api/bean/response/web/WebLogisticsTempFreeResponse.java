package com.gao.glg.mall.api.bean.response.web;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 免费模版
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class WebLogisticsTempFreeResponse implements Serializable {

    @ApiModelProperty(value = "城市ID, 多个逗号分割。 全国 all", required = true, example = "1,2,3,4")
    @NotNull(message = "请选择城市")
    private String cityId;

    @ApiModelProperty(value = "城市名称描述")
    private String title;

    @ApiModelProperty(value = "包邮件数", required = true, example = "1")
    @DecimalMin(value = "0.1", message = "包邮不能低于0.1")
    private BigDecimal number;

    @ApiModelProperty(value = "包邮金额", required = true, example = "0.1")
    @NotNull(message = "请填写包邮金额")
    @DecimalMin(value = "0", message = "包邮金额不能低于0")
    private BigDecimal price;
}
