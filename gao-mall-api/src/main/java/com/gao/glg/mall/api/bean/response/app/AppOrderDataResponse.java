package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单数量响应对象
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/4
 */
@Data
public class AppOrderDataResponse implements Serializable {

    @ApiModelProperty(value = "已完成订单数量")
    private Integer completeCount = 0;

    @ApiModelProperty(value = "待核销订单数量")
    private Integer evaluatedCount = 0;

    @ApiModelProperty(value = "支付订单总数")
    private Integer orderCount = 0;

    @ApiModelProperty(value = "待收货订单数量")
    private Integer receivedCount = 0;

    @ApiModelProperty(value = "退款订单数量")
    private Integer refundCount = 0;

    @ApiModelProperty(value = "总消费钱数")
    private BigDecimal sumPrice = BigDecimal.ZERO;

    @ApiModelProperty(value = "未支付订单数量")
    private Integer unPaidCount = 0;

    @ApiModelProperty(value = "待发货订单数量")
    private Integer unShippedCount = 0;
}
