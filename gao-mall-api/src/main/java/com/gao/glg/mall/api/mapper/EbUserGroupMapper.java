package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserGroupDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserGroupMapper {

    /**
     * 删除
     */
    int delete(java.lang.Short id);

    /**
     * 保存
     */
    int save(EbUserGroupDomain domain);

    /**
     * 查询
     */
    List<EbUserGroupDomain> select();

    /**
     * 更新
     */
    int update(EbUserGroupDomain domain);
}
