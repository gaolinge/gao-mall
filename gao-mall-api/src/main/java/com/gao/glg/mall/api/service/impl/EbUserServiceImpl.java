package com.gao.glg.mall.api.service.impl;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.cache.TokenCache;
import com.gao.glg.mall.api.constant.ErrorConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.domain.EbUserDomain;
import com.gao.glg.mall.api.mapper.EbUserMapper;
import com.gao.glg.mall.api.service.EbUserService;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserServiceImpl implements EbUserService  {

    @Autowired
    private TokenCache tokenCache;
    @Resource
    private EbUserMapper userMapper;


    @Override
    public EbUserDomain getUserInfo() {
        Integer userId = getUserId();
        if (userId == null) {
            return null;
        }
        return userMapper.getById(userId);
    }

    @Override
    public Integer getUserId() {
        return tokenCache.getUid();
    }

    @Override
    public Integer getUserIdException() {
        Integer userId = getUserId();
        if (userId == null) {
            throw new BusinessException(ErrorConstant.USER_LOGIN_EXPIRED);
        }
        return userId;
    }

    @Override
    public EbUserDomain getByAccount(String account) {
        return userMapper.getByAccount(account);
    }

    @Override
    public void updateLastLoginTime(Integer uid) {
        EbUserDomain domain = new EbUserDomain();
        domain.setUid(uid);
        domain.setLastLoginTime(new Date());
        userMapper.update(domain);
    }

    @Override
    public void update(EbUserDomain domain) {
        domain.setUpdateTime(new Date());
        userMapper.update(domain);
    }

    @Override
    public List<EbUserDomain> listByIds(List<Integer> uids) {
        return userMapper.listByIds(uids);
    }
}
