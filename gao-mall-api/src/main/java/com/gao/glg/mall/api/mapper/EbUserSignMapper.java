package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserSignDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserSignMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserSignDomain domain);

    /**
     * 查询
     */
    List<EbUserSignDomain> select();

    /**
     * 更新
     */
    int update(EbUserSignDomain domain);

    /**
     * 查询
     */
    List<EbUserSignDomain> listByType(Integer uid, int type);

    /**
     * 查询
     */
    Integer countByType(Integer uid, int type, String time);

    /**
     * 查询
     */
    EbUserSignDomain getByLastSign(Integer uid);
}
