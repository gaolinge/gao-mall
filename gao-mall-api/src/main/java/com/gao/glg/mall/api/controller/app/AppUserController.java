package com.gao.glg.mall.api.controller.app;


import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.app.AppUserCapitalResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserExpResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserLevelResponse;
import com.gao.glg.mall.api.business.IUserBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 用户中心
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app")
@Api(tags = "用户 -- 用户中心")
public class AppUserController {

    @Autowired
    private IUserBusiness userBusiness;


    @ApiOperation(value = "个人中心-用户信息")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public RestResult<AppUserInfoResponse> info() {
        return RestResult.success(userBusiness.appUserInfo());
    }

    @ApiOperation(value = "获取个人中心菜单")
    @RequestMapping(value = "/menu/user", method = RequestMethod.GET)
    public RestResult<Map<String, Object>> menus() {
        return RestResult.success(userBusiness.menus());
    }

    @ApiOperation(value = "会员等级列表")
    @RequestMapping(value = "/user/level/grade", method = RequestMethod.GET)
    public RestResult<List<AppUserLevelResponse>> level() {
        return RestResult.success(userBusiness.level());
    }

    @ApiOperation(value = "经验记录")
    @RequestMapping(value = "/user/expList", method = RequestMethod.GET)
    public RestResult<PagerInfo<AppUserExpResponse>> expRecord(@Validated PagerDTO request) {
        return RestResult.success(userBusiness.expRecord(request));
    }

    @ApiOperation(value = "用户资金统计")
    @RequestMapping(value = "/user/balance", method = RequestMethod.GET)
    public RestResult<AppUserCapitalResponse> capital() {
        return RestResult.success(userBusiness.capital());
    }


    /*@ApiOperation(value = "手机号修改密码")
    @RequestMapping(value = "/register/reset", method = RequestMethod.POST)
    public RestResult<Boolean> password(@RequestBody @Validated PasswordRequest request) {
        return RestResult.success(userService.password(request));
    }

    @ApiOperation(value = "修改个人资料")
    @RequestMapping(value = "/user/edit", method = RequestMethod.POST)
    public RestResult<Object> personInfo(@RequestBody @Validated UserEditRequest request) {
        if (userService.editUser(request)) {
            return RestResult.success();
        }
        return RestResult.failed();
    }



    @ApiOperation(value = "换绑手机号校验")
    @RequestMapping(value = "update/binding/verify", method = RequestMethod.POST)
    public RestResult<Boolean> updatePhoneVerify(@RequestBody @Validated UserBindingPhoneUpdateRequest request) {
        return RestResult.success(userService.updatePhoneVerify(request));
    }

    @ApiOperation(value = "换绑手机号")
    @RequestMapping(value = "update/binding", method = RequestMethod.POST)
    public RestResult<Boolean> updatePhone(@RequestBody @Validated UserBindingPhoneUpdateRequest request) {
        return RestResult.success(userService.updatePhone(request));
    }

    @ApiOperation(value = "获取个人中心菜单")
    @RequestMapping(value = "/menu/user", method = RequestMethod.GET)
    public RestResult<HashMap<String, Object>> getMenuUser() {
        return RestResult.success(systemGroupDataService.getMenuUser());
    }

    @ApiOperation(value = "推广数据接口(昨天的佣金 累计提现金额 当前佣金)")
    @RequestMapping(value = "/commission", method = RequestMethod.GET)
    public RestResult<UserCommissionResponse> getCommission() {
        return RestResult.success(userCenterService.getCommission());
    }

    @ApiOperation(value = "推广佣金明细")
    @RequestMapping(value = "/spread/commission/detail", method = RequestMethod.GET)
    public RestResult<PagerInfo<SpreadCommissionDetailResponse>> getSpreadCommissionDetail(@Validated PagerDTO PagerDTO) {
        PageInfo<SpreadCommissionDetailResponse> commissionDetail = userCenterService.getSpreadCommissionDetail(PagerDTO);
        return RestResult.success(PagerInfo.restPage(commissionDetail));
    }

    @ApiOperation(value = "推广佣金/提现总和")
    @RequestMapping(value = "/spread/count/{type}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "类型 佣金类型3=佣金,4=提现", allowableValues = "range[3,4]", dataType = "int")
    public RestResult<Map<String, BigDecimal>> getSpreadCountByType(@PathVariable Integer type) {
        Map<String, BigDecimal> map = new HashMap<>();
        map.put("count", userCenterService.getSpreadCountByType(type));
        return RestResult.success(map);
    }

    @ApiOperation(value = "提现申请")
    @RequestMapping(value = "/extract/cash", method = RequestMethod.POST)
    public RestResult<Boolean> extractCash(@RequestBody @Validated UserExtractRequest request) {
        return RestResult.success(userCenterService.extractCash(request));
    }

    @ApiOperation(value = "提现记录")
    @RequestMapping(value = "/extract/record", method = RequestMethod.GET)
    public RestResult<PagerInfo<UserExtractRecordResponse>> getExtractRecord(@Validated PagerDTO PagerDTO) {
        return RestResult.success(PagerInfo.restPage(userCenterService.getExtractRecord(PagerDTO)));
    }

    @ApiOperation(value = "提现用户信息")
    @RequestMapping(value = "/extract/user", method = RequestMethod.GET)
    public RestResult<UserExtractCashResponse> getExtractUser() {
        return RestResult.success(userCenterService.getExtractUser());
    }

    @ApiOperation(value = "提现银行/提现最低金额")
    @RequestMapping(value = "/extract/bank", method = RequestMethod.GET)
    public RestResult<List<String>> getExtractBank() {
        return RestResult.success(userCenterService.getExtractBank());
    }



    @ApiOperation(value = "推广人统计")
    @RequestMapping(value = "/spread/people/count", method = RequestMethod.GET)
    public RestResult<UserSpreadPeopleResponse>  getSpreadPeopleCount() {
        return RestResult.success(userCenterService.getSpreadPeopleCount());
    }

    @ApiOperation(value = "推广人列表")
    @RequestMapping(value = "/spread/people", method = RequestMethod.GET)
    public RestResult<PagerInfo<UserSpreadPeopleItemResponse>> getSpreadPeopleList(@Validated UserSpreadPeopleRequest request, @Validated PagerDTO PagerDTO) {
        List<UserSpreadPeopleItemResponse> spreadPeopleList = userCenterService.getSpreadPeopleList(request, PagerDTO);
        PagerInfo<UserSpreadPeopleItemResponse> PagerInfo = PagerInfo.restPage(spreadPeopleList);
        return RestResult.success(PagerInfo);
    }

    @ApiOperation(value = "用户积分信息")
    @RequestMapping(value = "/integral/user", method = RequestMethod.GET)
    public RestResult<IntegralUserResponse> getIntegralUser() {
        return RestResult.success(userCenterService.getIntegralUser());
    }

    @ApiOperation(value = "积分记录")
    @RequestMapping(value = "/integral/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<UserIntegralRecord>> getIntegralList(@Validated PagerDTO PagerDTO) {
        return RestResult.success(PagerInfo.restPage(userCenterService.getUserIntegralRecordList(PagerDTO)));
    }




    @ApiOperation(value = "推广订单")
    @RequestMapping(value = "/spread/order", method = RequestMethod.GET)
    public RestResult<UserSpreadOrderResponse> getSpreadOrder(@Validated PagerDTO PagerDTO) {
        return RestResult.success(userCenterService.getSpreadOrder(PagerDTO));
    }

    @ApiOperation(value = "推广人排行")
    @RequestMapping(value = "/rank", method = RequestMethod.GET)
    public RestResult<List<User>> getTopSpreadPeopleListByDate(@RequestParam(required = false) String type, @Validated PagerDTO PagerDTO) {
        return RestResult.success(userCenterService.getTopSpreadPeopleListByDate(type, PagerDTO));
    }


    @ApiOperation(value = "佣金排行")
    @RequestMapping(value = "/brokerage_rank", method = RequestMethod.GET)
    public RestResult<List<User>> getTopBrokerageListByDate(@RequestParam String type, @Validated PagerDTO PagerDTO) {
        return RestResult.success(userCenterService.getTopBrokerageListByDate(type, PagerDTO));
    }


    @ApiOperation(value = "当前用户在佣金排行第几名")
    @RequestMapping(value = "/user/brokerageRankNumber", method = RequestMethod.GET)
    public RestResult<Integer> getNumberByTop(@RequestParam String type) {
        return RestResult.success(userCenterService.getNumberByTop(type));
    }

    @ApiOperation(value = "推广海报图")
    @RequestMapping(value = "/user/spread/banner", method = RequestMethod.GET)
    public RestResult<List<UserSpreadBannerResponse>>  getSpreadBannerList() {
        return RestResult.success(userCenterService.getSpreadBannerList());
    }

    @ApiOperation(value = "绑定推广关系（登录状态）")
    @RequestMapping(value = "/user/bindSpread", method = RequestMethod.GET)
    public RestResult<Boolean> bindsSpread(Integer spreadPid) {
        userService.bindSpread(spreadPid);
        return RestResult.success();
    }*/
}



