package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.bean.request.web.WebArticleSearchRequest;
import com.gao.glg.mall.api.domain.EbArticleDomain;
import com.gao.glg.mall.api.mapper.EbArticleMapper;
import com.gao.glg.mall.api.service.EbArticleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Service
public class EbArticleServiceImpl implements EbArticleService  {

    @Resource
    private EbArticleMapper articleMapper;

    @Override
    public List<EbArticleDomain> listByCid(String cid) {
        return articleMapper.listByCid(cid);
    }

    @Override
    public List<EbArticleDomain> hotList() {
        return articleMapper.hotList();
    }

    @Override
    public List<EbArticleDomain> bannerList() {
        return articleMapper.bannerList();
    }

    @Override
    public void update(EbArticleDomain domain) {
        articleMapper.update(domain);
    }

    @Override
    public EbArticleDomain getById(Integer id) {
        return articleMapper.getById(id);
    }

    @Override
    public List<EbArticleDomain> page(WebArticleSearchRequest request) {
        return articleMapper.page(request);
    }

    @Override
    public void save(EbArticleDomain article) {
        articleMapper.save(article);
    }

    @Override
    public void delete(Integer id) {
        articleMapper.delete(id);
    }

}
