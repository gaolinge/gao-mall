package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserIntegralRecordDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserIntegralRecordMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserIntegralRecordDomain domain);

    /**
     * 查询
     */
    List<EbUserIntegralRecordDomain> select();

    /**
     * 更新
     */
    int update(EbUserIntegralRecordDomain domain);
}
