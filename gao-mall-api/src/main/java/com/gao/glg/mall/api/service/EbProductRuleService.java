package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebProductRuleSearchRequest;
import com.gao.glg.mall.api.domain.EbProductRuleDomain;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbProductRuleService {

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    List<EbProductRuleDomain> page(WebProductRuleSearchRequest request);

    /**
     * 根据规格名称获取
     * <p/>
     *
     * @param ruleName
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    EbProductRuleDomain getByRuleName(String ruleName);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void save(EbProductRuleDomain domain);
    /**
     * 删除
     * <p/>
     *
     * @param ids
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void deletes(List<Integer> ids);
    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void update(EbProductRuleDomain domain);
    /**
     * 根据主键获取
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    EbProductRuleDomain getById(Integer id);
}
