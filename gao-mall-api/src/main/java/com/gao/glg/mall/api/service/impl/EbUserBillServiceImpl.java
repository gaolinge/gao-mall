package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserBillDomain;
import com.gao.glg.mall.api.mapper.EbUserBillMapper;
import com.gao.glg.mall.api.service.EbUserBillService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserBillServiceImpl implements EbUserBillService  {
    @Resource
    private EbUserBillMapper userBillMapper;

    @Override
    public List<EbUserBillDomain> listByPmAndType(Integer uid, int pm, String cate) {
        return userBillMapper.listByPmAndType(uid, pm, cate);
    }
}
