package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbShippingTempFreeDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbShippingTempFreeMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 删除
     */
    void deleteByTempId(Integer tempId);

    /**
     * 保存
     */
    int save(EbShippingTempFreeDomain domain);

    /**
     * 查询
     */
    List<EbShippingTempFreeDomain> select();

    /**
     * 更新
     */
    int update(EbShippingTempFreeDomain domain);

    /**
     * 查询
     */
    EbShippingTempFreeDomain getByTempIdAndCityId(@Param("tempId") Integer tempId, @Param("cityId") Integer cityId);

    /**
     * 批量保存
     */
    void batchSave(@Param("list") List<EbShippingTempFreeDomain> domains);

    /**
     * 查询
     */
    List<EbShippingTempFreeDomain> groupByUniqid(Integer tempId);
}
