package com.gao.glg.mall.api.bean.request.web;

import com.gao.glg.page.PagerDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/28
 */
@Data
public class WebGenerateRequest extends PagerDTO {

    @ApiModelProperty("表名")
    private String tableName;
}