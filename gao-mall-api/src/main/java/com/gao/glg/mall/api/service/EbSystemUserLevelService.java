package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSystemUserLevelDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemUserLevelService {

    /**
     * 根据等级获取用户等级信息
     * <p/>
     *
     * @param level
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    EbSystemUserLevelDomain getById(Integer level);

    /**
     * 获取所有等级
     * <p/>
     *
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbSystemUserLevelDomain> select();

}
