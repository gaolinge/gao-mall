package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemUserLevelDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemUserLevelMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemUserLevelDomain domain);

    /**
     * 查询
     */
    List<EbSystemUserLevelDomain> select();

    /**
     * 更新
     */
    int update(EbSystemUserLevelDomain domain);

    /**
     * 查询
     */
    EbSystemUserLevelDomain getById(Integer levelId);
}
