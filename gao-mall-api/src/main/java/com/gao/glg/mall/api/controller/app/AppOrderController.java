package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.app.AppCalcOrderPriceRequest;
import com.gao.glg.mall.api.bean.request.app.AppCreateOrderRequest;
import com.gao.glg.mall.api.bean.request.app.AppPreOrderRequest;
import com.gao.glg.mall.api.bean.response.app.AppCalcOrderPriceResponse;
import com.gao.glg.mall.api.bean.response.app.AppOrderDataResponse;
import com.gao.glg.mall.api.bean.response.app.AppPreOrderResponse;
import com.gao.glg.mall.api.business.IOrderBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 订单
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/order")
@Api(tags = "订单")
public class AppOrderController {

    @Autowired
    private IOrderBusiness orderBusiness;

    @ApiOperation(value = "订单头部数量")
    @RequestMapping(value = "/data", method = RequestMethod.GET)
    public RestResult<AppOrderDataResponse> orderHead() {
        return RestResult.success(orderBusiness.orderDataNum());
    }

    @ApiOperation(value = "预下单")
    @RequestMapping(value = "/pre/order", method = RequestMethod.POST)
    public RestResult<Map<String, Object>> preOrder(@RequestBody @Validated AppPreOrderRequest request) {
        return RestResult.success(orderBusiness.preOrder(request));
    }

    @ApiOperation(value = "加载预下单")
    @RequestMapping(value = "load/pre/{preOrderNo}", method = RequestMethod.GET)
    public RestResult<AppPreOrderResponse> loadPreOrder(@PathVariable String preOrderNo) {
        return RestResult.success(orderBusiness.loadPreOrder(preOrderNo));
    }

    @ApiOperation(value = "计算订单价格")
    @RequestMapping(value = "/computed/price", method = RequestMethod.POST)
    public RestResult<AppCalcOrderPriceResponse> calcPrice(@Validated @RequestBody AppCalcOrderPriceRequest request) {
        return RestResult.success(orderBusiness.calcPrice(request));
    }

    @ApiOperation(value = "创建订单")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public RestResult<Map<String, Object>> createOrder(@Validated @RequestBody AppCreateOrderRequest request) {
        return RestResult.success(orderBusiness.createOrder(request));
    }

    /*

    @ApiOperation(value = "订单列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiImplicitParams ({
        @ApiImplicitParam(name = "type", value = "评价等级|0=未支付,1=待发货,2=待收货,3=待评价,4=已完成,-3=售后/退款", required = true)
    })
    public RestResult<PagerInfo<OrderDetailResponse>> orderList(@RequestParam(name = "type") Integer type,
                                                                 @ModelAttribute PagerDTO pageRequest) {
        return RestResult.success(orderService.list(type, pageRequest));
    }


    @ApiOperation(value = "订单详情")
    @RequestMapping(value = "/detail/{orderId}", method = RequestMethod.GET)
    public RestResult<StoreOrderDetailInfoResponse> orderDetail(@PathVariable String orderId) {
        return RestResult.success(orderService.detailOrder(orderId));
    }


    @ApiOperation(value = "删除订单")
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public RestResult<Boolean> delete(@RequestParam Integer id) {
        if( orderService.delete(id)) {
            return RestResult.success();
        }else{
            return RestResult.failed();
        }
    }

    @ApiOperation(value = "评价订单")
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public RestResult<Boolean> comment(@RequestBody @Validated StoreProductReplyAddRequest request) {
        if(orderService.reply(request)) {
            return RestResult.success();
        }else{
            return RestResult.failed();
        }
    }

    @ApiOperation(value = "订单收货")
    @RequestMapping(value = "/take", method = RequestMethod.POST)
    public RestResult<Boolean> take(@RequestParam(value = "id") Integer id) {
        if(orderService.take(id)) {
            return RestResult.success();
        }else{
            return RestResult.failed();
        }
    }

    @ApiOperation(value = "订单取消")
    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public RestResult<Boolean> cancel(@RequestParam(value = "id") Integer id) {
        if(orderService.cancel(id)) {
            return RestResult.success();
        }else{
            return RestResult.failed();
        }
    }


    @ApiOperation(value = "获取申请订单退款信息")
    @RequestMapping(value = "/apply/refund/{orderId}", method = RequestMethod.GET)
    public RestResult<ApplyRefundOrderInfoResponse> refundApplyOrder(@PathVariable String orderId) {
        return RestResult.success(orderService.applyRefundOrderInfo(orderId));
    }


    @ApiOperation(value = "订单退款申请")
    @RequestMapping(value = "/refund", method = RequestMethod.POST)
    public RestResult<Boolean> refundApply(@RequestBody @Validated OrderRefundApplyRequest request) {
        if(orderService.refundApply(request)) {
            return RestResult.success();
        }else{
            return RestResult.failed();
        }
    }


    @ApiOperation(value = "订单退款理由（商家提供）")
    @RequestMapping(value = "/refund/reason", method = RequestMethod.GET)
    public RestResult<List<String>> refundReason() {
        return RestResult.success(orderService.getRefundReason());
    }


    @ApiOperation(value = "物流信息查询")
    @RequestMapping(value = "/express/{orderId}", method = RequestMethod.GET)
    public RestResult<Object> getExpressInfo(@PathVariable String orderId) {
        return RestResult.success(orderService.expressOrder(orderId));
    }

    @ApiOperation(value = "待评价商品信息查询")
    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public RestResult<OrderProductReplyResponse> getOrderProductForReply(@Validated @RequestBody GetProductReply request) {
        return RestResult.success(orderService.getReplyProduct(request));
    }


    @ApiOperation(value = "获取支付配置")
    @RequestMapping(value = "get/pay/config", method = RequestMethod.GET)
    public RestResult<PreOrderResponse> getPayConfig() {
        return RestResult.success(orderService.getPayConfig());
    }*/
}
