package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbWechatPayInfoDomain;
import com.gao.glg.mall.api.mapper.EbWechatPayInfoMapper;
import com.gao.glg.mall.api.service.EbWechatPayInfoService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbWechatPayInfoServiceImpl implements EbWechatPayInfoService  {
    @Resource
    private EbWechatPayInfoMapper ebWechatPayInfoMapper;
}
