package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbBargainDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbBargainMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbBargainDomain domain);

    /**
     * 查询
     */
    List<EbBargainDomain> select();

    /**
     * 更新
     */
    int update(EbBargainDomain domain);

    /**
     * 查询
     */
    List<EbBargainDomain> listByInStock();

    /**
     * 查询
     */
    List<EbBargainDomain> listByProductId(Integer productId);
}
