package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.app.AppSystemDataResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserSignInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserSignResponse;
import com.gao.glg.mall.api.business.ISignBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 签到记录表
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/user/sign")
@Api(tags = "用户 -- 签到")
public class AppSignController {

    @Autowired
    private ISignBusiness signBusiness;

    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<AppUserSignResponse>> list(@Validated PagerDTO request) {
        return RestResult.success(signBusiness.list(request));
    }

    @ApiOperation(value = "配置")
    @RequestMapping(value = "/config", method = RequestMethod.GET)
    public RestResult<List<AppSystemDataResponse>> config() {
        return RestResult.success(signBusiness.signConfig());
    }

    @ApiOperation(value = "签到用户信息")
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public RestResult<AppUserSignInfoResponse> info() {
        return RestResult.success(signBusiness.info());
    }

    @ApiOperation(value = "签到")
    @RequestMapping(value = "/integral", method = RequestMethod.GET)
    public RestResult<AppSystemDataResponse> sign() {
        return RestResult.success(signBusiness.sign());
    }

    /*


    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/month", method = RequestMethod.GET)
    public RestResult<PagerInfo<UserSignMonthVo>>  getListGroupMonth(@Validated PagerDTO PagerDTO) {
        PagerInfo<UserSignMonthVo> userSignPagerInfo = PagerInfo.restPage(userSignService.getListGroupMonth(PagerDTO));
        return RestResult.success(userSignPagerInfo);
    }





    @ApiOperation(value = "今日记录详情")
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public RestResult<HashMap<String, Object>> get() {
        return RestResult.success(userSignService.get());
    }

    */
}



