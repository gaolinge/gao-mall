package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.web.WebSystemAttResponse;
import com.gao.glg.mall.api.business.IAuthBusiness;
import com.gao.glg.mall.api.business.IMaterialBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 角色中心
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin/material")
@Api(tags = "素材中心")
public class WebMaterialController {

    @Autowired
    private IMaterialBusiness materialBusiness;

    //@PreAuthorize("hasAuthority('admin:system:attachment:list')")
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebSystemAttResponse>> list(@RequestParam @Validated Integer pid,
            @RequestParam(value = "attType", defaultValue = "png,jpeg,jpg,audio/mpeg,text/plain,video/mp4,gif", required = false) String attType,
            @Validated PagerDTO request) {
        return RestResult.success(materialBusiness.list(pid, attType, request));
    }
}



