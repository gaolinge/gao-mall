package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbWechatCallbackDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbWechatCallbackMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbWechatCallbackDomain domain);

    /**
     * 查询
     */
    List<EbWechatCallbackDomain> select();

    /**
     * 更新
     */
    int update(EbWechatCallbackDomain domain);
}
