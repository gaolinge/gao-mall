package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemConfigDomain;
import com.gao.glg.mall.api.mapper.EbSystemConfigMapper;
import com.gao.glg.mall.api.service.EbSystemConfigService;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemConfigServiceImpl implements EbSystemConfigService  {

    @Resource
    private EbSystemConfigMapper systemConfigMapper;

    @Override
    public EbSystemConfigDomain getByKey(String key) {
        return systemConfigMapper.getByKey(key);
    }

    @Override
    public String getValueByKey(String key) {
        EbSystemConfigDomain domain = systemConfigMapper.getByKey(key);
        if(domain == null){
            return null;
        }
        return domain.getValue();
    }

    @Override
    public Map<String, String> info(int formId) {
        List<EbSystemConfigDomain> domain = systemConfigMapper.getByFormId(formId);
        if (CollectionUtils.isEmpty(domain)) {
            return null;
        }
        HashMap<String, String> map = new HashMap<>();
        for (EbSystemConfigDomain systemConfig : domain) {
            map.put(systemConfig.getName(), systemConfig.getValue());
        }
        map.put("id", formId + "");
        return map;
    }
}
