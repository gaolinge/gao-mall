package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemRoleMenuDomain;
import com.gao.glg.mall.api.mapper.EbSystemRoleMenuMapper;
import com.gao.glg.mall.api.service.EbSystemRoleMenuService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemRoleMenuServiceImpl implements EbSystemRoleMenuService  {
    @Resource
    private EbSystemRoleMenuMapper systemRoleMenuMapper;

    @Override
    public void batchSave(List<EbSystemRoleMenuDomain> domains) {
        systemRoleMenuMapper.batchSave(domains);
    }

    @Override
    public void delete(Integer rid) {
        systemRoleMenuMapper.delete(rid);
    }

    @Override
    public List<EbSystemRoleMenuDomain> listByRid(Integer id) {
        return systemRoleMenuMapper.listByRid(id);
    }
}
