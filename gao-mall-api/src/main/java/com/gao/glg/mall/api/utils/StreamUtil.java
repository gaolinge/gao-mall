package com.gao.glg.mall.api.utils;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
public class StreamUtil {

    /**
     * list转map
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <K, V> List<K> map(List<V> list, Function<V, K> keyMapper) {
        if (list == null) {
            return null;
        }
        return list.stream().map(keyMapper).collect(Collectors.toList());
    }

    /**
     * list转map
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <K, V> Map<K, V> listToMap(List<V> list, Function<V, K> keyMapper) {
        return list.stream().collect(Collectors.toMap(
                keyMapper, each -> each, (value1, value2) -> value1));
    }

    /**
     * 分组
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <K, V> Map<K, List<V>> groupBy(List<V> list, Function<V, K> keyMapper) {
        return list.stream().collect(Collectors.groupingBy(keyMapper));
    }

    /**
     * 升序
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <K extends Comparable<? super K>, V> List<V> acs(List<V> list, Function<V, K> keyMapper) {
        return list.stream().sorted(Comparator.comparing(keyMapper)).collect(Collectors.toList());
    }

    /**
     * 降序
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <K extends Comparable<? super K>, V> List<V> desc(List<V> list, Function<V, K> keyMapper) {
        return list.stream().sorted(Comparator.comparing(keyMapper).reversed()).collect(Collectors.toList());
    }

    /**
     * 求和
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <V> Double sumDouble(List<V> list, Function<V, Double> keyMapper) {
        return list.stream().mapToDouble((e) -> keyMapper.apply(e)).sum();
    }

    /**
     * 求和
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <V> Long sumLong(List<V> list, Function<V, Long> keyMapper) {
        return list.stream().mapToLong((e) -> keyMapper.apply(e)).sum();
    }

    /**
     * 求和
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <V> Integer sumInt(List<V> list, Function<V, Integer> keyMapper) {
        return list.stream().mapToInt((e) -> keyMapper.apply(e)).sum();
    }

    /**
     * 求和
     * <p/>
     *
     * @param list
     * @param keyMapper
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    public static <V> BigDecimal sumBigDecimal(List<V> list, Function<V, BigDecimal> keyMapper) {
        return list.stream().map(keyMapper).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
