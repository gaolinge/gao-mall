package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserSignDomain;
import com.gao.glg.mall.api.mapper.EbUserSignMapper;
import com.gao.glg.mall.api.service.EbUserSignService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserSignServiceImpl implements EbUserSignService  {
    @Resource
    private EbUserSignMapper userSignMapper;

    @Override
    public List<EbUserSignDomain> listByType(Integer uid, int type) {
        return userSignMapper.listByType(uid,type);
    }

    @Override
    public Integer countByType(Integer uid, int type, String time) {
        return userSignMapper.countByType(uid,type,time);
    }

    @Override
    public void save(EbUserSignDomain domain) {
        userSignMapper.save(domain);
    }

    @Override
    public EbUserSignDomain getByLastSign(Integer uid) {
        return userSignMapper.getByLastSign(uid);
    }
}
