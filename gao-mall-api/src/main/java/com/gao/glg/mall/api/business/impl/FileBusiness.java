package com.gao.glg.mall.api.business.impl;

import com.gao.glg.constant.CommonConstant;
import com.gao.glg.mall.api.bean.response.ImageUploadResponse;
import com.gao.glg.mall.api.bean.response.app.AppFileResultResponse;
import com.gao.glg.mall.api.business.ICdnBusiness;
import com.gao.glg.mall.api.business.IFileBusiness;
import com.gao.glg.mall.api.service.EbSystemConfigService;
import com.gao.glg.utils.SplitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Service
public class FileBusiness implements IFileBusiness {

    private String[] suffixs = new String[]{"jpg", "jpeg", "png", "gif", "bmp", "tiff", "webp"};
    @Value("${spring.profiles.active}")
    private String env;
    @Value("${container.file.directory}")
    private String directory;
    @Value("${image.path.prefix}")
    private String imgPrefix;
    @Autowired
    private ICdnBusiness cdnBusiness;
    @Autowired
    private EbSystemConfigService systemConfigService;

    @Override
    public AppFileResultResponse imageUpload(MultipartFile multipart, String model, Integer pid) {
        return null;
    }

    @Override
    public ImageUploadResponse uploadImage(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        String suffix = SplitUtil.lastSection(fileName, CommonConstant.SPOT_SYMBOL);
        fileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + suffix;
        String filePath = directory + fileName;
        file.transferTo(new File(filePath));

        // 添加文件权限
        Path path = Paths.get(filePath);
        Set<PosixFilePermission> filePermissions = PosixFilePermissions.fromString("rw-r--r--");
        Files.setPosixFilePermissions(path, filePermissions);

        String url = buildPath(fileName);
        Map<String, Object> data = new HashMap<>();
        data.put("url", url);
        data.put("alt", fileName);
        data.put("href", url);
        ImageUploadResponse response = new ImageUploadResponse();
        response.setErrno(0);
        response.setData(data);
        return response;
    }

    @Override
    public String uploadImag(MultipartFile file) throws IOException {
        String fileName = file.getOriginalFilename();
        String suffix = SplitUtil.lastSection(fileName, CommonConstant.SPOT_SYMBOL);
        fileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + suffix;
        String filePath = directory + fileName;
        file.transferTo(new File(filePath));

        if (!env.equals("local")) {
            // 添加文件权限
            Path path = Paths.get(filePath);
            Set<PosixFilePermission> filePermissions = PosixFilePermissions.fromString("rw-r--r--");
            Files.setPosixFilePermissions(path, filePermissions);
        }
        return buildPath(fileName);
    }

    @Override
    public void deleteFile(String fileName) throws IOException {
        String filePath = directory + fileName;
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }


    @Override
    public String buildPath(String fileName) {
        return imgPrefix + fileName;
    }


    @Override
    public String buildPath(String fileName, String suffix) {
        for (String e : suffixs) {
            if (e.equals(suffix)) {
                return buildPath(fileName);
            }
        }
        return fileName;
    }


    @Override
    public String prefixImage(String data) {
        return data.replace("crmebimage/public/maintain/2021/12/25/",  "http://47.113.114.15/img/")
                   .replace("crmebimage/public/maintain/2023/02/23/",  "http://47.113.114.15/img/")
                   .replace("crmebimage/public/operation/2021/12/25/", "http://47.113.114.15/img/")
                   .replace("crmebimage/public/store/2021/12/25/",     "http://47.113.114.15/img/");
    }

    @Override
    public String prefixFile(String path) {
        if (path.contains("file/excel")) {
            String cdnUrl = systemConfigService.getValueByKey("local" + "UploadUrl");
            return path.replace("file/", cdnUrl + "/file/");
        }
        return path.replace("file/", cdnBusiness.getUrl() + "/file/");
    }
}
