package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbProductDescDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductDescMapper;
import com.gao.glg.mall.api.service.EbProductDescService;

import java.util.Collections;
import java.util.List;

@Service
public class EbProductDescServiceImpl implements EbProductDescService {
    @Resource
    private EbProductDescMapper productDescMapper;

    @Override
    public EbProductDescDomain geByProductId(Integer pid, Integer type) {
        return productDescMapper.geByProductId(pid, type);
    }

    @Override
    public List<EbProductDescDomain> listByPidsAndType(List<Integer> pids, Integer type) {
        return productDescMapper.listByPidsAndType(pids, type);
    }
}
