package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbStoreProductRelationDomain
 * @author gaolinge
 */
@Data
public class EbProductRelationDomain {
    /** 
     * id
     */
    private Integer id;

    /** 
     * 用户ID
     */
    private Integer uid;

    /** 
     * 商品ID
     */
    private Integer productId;

    /** 
     * 类型(收藏(collect）、点赞(like))
     */
    private String type;

    /** 
     * 某种类型的商品(普通商品、秒杀商品)
     */
    private String category;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
