package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.app.AppUserAddressRequest;
import com.gao.glg.mall.api.bean.response.app.AppUserAddressResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
public interface IAddressBusiness {

    /**
     * 地址列表
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    PagerInfo<AppUserAddressResponse> list(PagerDTO request);

    /**
     * 保存地址
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    AppUserAddressResponse create(AppUserAddressRequest request);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    AppUserAddressResponse detail(Integer id);

    /**
     * 设置默认
     * <p/>
     *
     * @param id
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    void setDefault(Object id);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    void delete(Object id);

    /**
     * 获取默认地址
     * <p/>
     *
     * @param
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    AppUserAddressResponse getDefault();

}
