package com.gao.glg.mall.api.business.impl;

import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.web.WebArticleRequest;
import com.gao.glg.mall.api.bean.request.web.WebArticleSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppArticleInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppArticleResponse;
import com.gao.glg.mall.api.bean.response.app.AppCategoryResponse;
import com.gao.glg.mall.api.bean.response.web.WebArticleResponse;
import com.gao.glg.mall.api.business.IArticleBusiness;
import com.gao.glg.mall.api.business.ICdnBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbArticleDomain;
import com.gao.glg.mall.api.domain.EbCategoryDomain;
import com.gao.glg.mall.api.service.EbArticleService;
import com.gao.glg.mall.api.service.EbCategoryService;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class ArticleBusiness implements IArticleBusiness {

    @Autowired
    private ICdnBusiness cdnBusiness;
    @Autowired
    private EbArticleService articleService;
    @Autowired
    private EbCategoryService categoryService;
   
    @Override
    public PagerInfo<AppArticleResponse> page(String cid, PagerDTO request) {
        return PagerUtil.page(request, () -> {
            List<EbArticleDomain> articleDomains = articleService.listByCid(cid);
            return BeanCopierUtil.copyList(articleDomains, AppArticleResponse.class);
        });
    }

    @Override
    public List<AppArticleResponse> hotList() {
        List<EbArticleDomain> articleDomains = articleService.hotList();
        if (articleDomains == null) {
            return Collections.emptyList();
        }
        return BeanCopierUtil.copyList(articleDomains, AppArticleResponse.class);
    }

    @Override
    public List<AppArticleInfoResponse> bannerList() {
        List<EbArticleDomain> articleDomains = articleService.bannerList();
        if (articleDomains == null) {
            return Collections.emptyList();
        }
        return BeanCopierUtil.copyList(articleDomains, AppArticleInfoResponse.class);
    }

    @Override
    public PagerInfo<AppCategoryResponse> categoryPage() {
        return PagerUtil.page(new PagerDTO(), () -> {
            List<EbCategoryDomain> domains = categoryService.listByType(MallConstant.CATEGORY_TYPE_ARTICLE);
            if (domains == null) { return Collections.emptyList(); }
            return BeanCopierUtil.copyList(domains, AppCategoryResponse.class);
        });
    }

    @Override
    public AppArticleResponse info(Integer id) {
        EbArticleDomain articleDomain = articleService.getById(id);
        if (articleDomain == null) {
            throw new BusinessException(ErrorConstant.ARTICLE_NOT_EXIST);
        }

        EbArticleDomain domain = new EbArticleDomain();
        domain.setId(id);
        domain.setVisit("1");
        articleService.update(domain);
        return BeanCopierUtil.copy(articleDomain, AppArticleResponse.class);
    }

    @Override
    public PagerInfo<WebArticleResponse> page(WebArticleSearchRequest request) {
        return PagerUtil.page(request, () -> {
            List<EbArticleDomain> domains = articleService.page(request);
            return BeanCopierUtil.copyList(domains, WebArticleResponse.class);
        });
    }

    @Override
    public void create(WebArticleRequest request) {
        EbArticleDomain domain = new EbArticleDomain();
        BeanUtils.copyProperties(request, domain);
        domain.setImageInput(cdnBusiness.clearPrefix(request.getImageInput()));
        domain.setContent(cdnBusiness.clearPrefix(request.getContent()));
        articleService.save(domain);
    }

    @Override
    public void delete(Integer id) {
        articleService.delete(id);
    }

    @Override
    public void update(Integer id, WebArticleRequest request) {
        EbArticleDomain domain = new EbArticleDomain();
        BeanUtils.copyProperties(request, domain);
        domain.setId(id);
        domain.setImageInput(cdnBusiness.clearPrefix(request.getImageInput()));
        domain.setContent(cdnBusiness.clearPrefix(request.getContent()));
        articleService.update(domain);
    }

    @Override
    public WebArticleResponse detail(Integer id) {
        EbArticleDomain domain = articleService.getById(id);
        if (domain == null) {
            throw new BusinessException(ErrorConstant.ARTICLE_NOT_EXIST);
        }
        return BeanCopierUtil.copy(domain, WebArticleResponse.class);
    }
}
