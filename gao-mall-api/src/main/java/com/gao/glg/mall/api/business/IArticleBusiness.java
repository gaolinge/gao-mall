package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebArticleRequest;
import com.gao.glg.mall.api.bean.request.web.WebArticleSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppArticleInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppArticleResponse;
import com.gao.glg.mall.api.bean.response.app.AppCategoryResponse;
import com.gao.glg.mall.api.bean.response.web.WebArticleResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.List;

/**
 * 文章
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
public interface IArticleBusiness {

    /**
     *
     * <p/>
     *
     * @param cid
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    PagerInfo<AppArticleResponse> page(String cid, PagerDTO request);

    /**
     *
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    List<AppArticleResponse> hotList();

    /**
     * 轮播列表
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    List<AppArticleInfoResponse> bannerList();

    /**
     * 文章分类
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    PagerInfo<AppCategoryResponse> categoryPage();

    /**
     * 文章详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    AppArticleResponse info(Integer id);

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    PagerInfo<WebArticleResponse> page(WebArticleSearchRequest request);

    /**
     * 新建
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    void create(WebArticleRequest request);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 更新
     * <p/>
     *
     * @param id
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    void update(Integer id, WebArticleRequest request);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/21
     * @version : 1.0.0
     */
    WebArticleResponse detail(Integer id);
}
