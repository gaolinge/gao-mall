package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebRoleRequest;
import com.gao.glg.mall.api.bean.request.web.WebRoleSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebRoleResponse;
import com.gao.glg.mall.api.bean.response.web.WebSystemRoleResponse;
import com.gao.glg.page.PagerInfo;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/19
 */
public interface IRoleBusiness {

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    PagerInfo<WebSystemRoleResponse> page(WebRoleSearchRequest request);

    /**
     * 新建
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void create(WebRoleRequest request);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 修改
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void update(WebRoleRequest request);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    WebRoleResponse detail(Integer id);

    /**
     * 修改状态
     * <p/>
     *
     * @param id
     * @param status
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void updateStatus(Integer id, Boolean status);
}
