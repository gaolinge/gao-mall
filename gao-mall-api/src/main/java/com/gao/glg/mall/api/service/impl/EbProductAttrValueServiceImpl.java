package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbCartDomain;
import com.gao.glg.mall.api.domain.EbProductAttrValueDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductAttrValueMapper;
import com.gao.glg.mall.api.service.EbProductAttrValueService;

import java.util.Collections;
import java.util.List;


@Service
public class EbProductAttrValueServiceImpl implements EbProductAttrValueService {

    @Resource
    private EbProductAttrValueMapper productAttrValueMapper;

    @Override
    public List<EbProductAttrValueDomain> listByPidAndType(Integer pid, Integer type) {
        return productAttrValueMapper.listByPidAndType(pid, type);
    }

    @Override
    public EbProductAttrValueDomain getByPidAndTypeAndAttrId(Integer pid, String aid, Integer type) {
        return productAttrValueMapper.listByPidAndTypeAndAttrId(pid, aid, type);
    }

    @Override
    public List<EbProductAttrValueDomain> listByCart(List<EbCartDomain> list, Integer type) {
        return productAttrValueMapper.listByCart(list, type);
    }

    @Override
    public List<EbProductAttrValueDomain> listByPidsAndType(List<Integer> pids, Integer type) {
        return productAttrValueMapper.listByPidsAndType(pids, type);
    }
}
