package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempRequest;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempFreeResponse;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempRegionResponse;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempResponse;
import com.gao.glg.page.PagerInfo;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
public interface ILogisticsBusiness {

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    PagerInfo<WebLogisticsTempResponse> page(WebLogisticsTempSearchRequest request);

    /**
     * 创建
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void create(WebLogisticsTempRequest request);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 更新
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void update(Integer id, WebLogisticsTempRequest request);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    WebLogisticsTempResponse detail(Integer id);

    /**
     * 分页
     * <p/>
     *
     * @param tempId
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    List<WebLogisticsTempFreeResponse> freePage(Integer tempId);

    /**
     * 分页
     * <p/>
     *
     * @param tempId
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    List<WebLogisticsTempRegionResponse> regionPage(Integer tempId);
}
