package com.gao.glg.mall.api.domain;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreBargainUserHelpDomain
 * @author gaolinge
 */
@Data
public class EbBargainHelpDomain {
    /** 
     * 砍价用户帮助表ID
     */
    private Integer id;

    /** 
     * 帮助的用户id
     */
    private Integer uid;

    /** 
     * 砍价商品ID
     */
    private Integer bargainId;

    /** 
     * 用户参与砍价表id
     */
    private Integer bargainUserId;

    /** 
     * 帮助砍价多少金额
     */
    private BigDecimal price;

    /** 
     * 添加时间
     */
    private Long addTime;


}
