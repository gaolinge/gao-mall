package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.bean.response.app.AppCityTreeResponse;
import com.gao.glg.mall.api.business.ISupportBusiness;
import com.gao.glg.mall.api.cache.CityCache;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbSystemCityDomain;
import com.gao.glg.mall.api.service.EbSystemCityService;
import com.gao.glg.mall.api.service.EbSystemConfigService;
import com.gao.glg.mall.api.service.EbSystemGroupDataService;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.TreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupportBusiness implements ISupportBusiness {

    @Autowired
    private CityCache cityCache;
    @Autowired
    private EbSystemCityService systemCityService;
    @Autowired
    private EbSystemConfigService systemConfigService;
    @Autowired
    private EbSystemGroupDataService systemGroupDataService;

    @Override
    public List<AppCityTreeResponse> tree() {
        List<AppCityTreeResponse> responses = null;
        if (CollectionUtils.isEmpty(responses)) {
            List<EbSystemCityDomain> systemCityDomains = systemCityService.select();
            responses = TreeUtil.buildTree(systemCityDomains, EbSystemCityDomain::getParentId, EbSystemCityDomain::getCityId, 0,
                    (list, dto) -> {
                        AppCityTreeResponse response = BeanCopierUtil.copy(dto, AppCityTreeResponse.class);
                        response.setChild(list);
                        return response;
            });
        }
        return responses;
    }

    @Override
    public Map<String, Object> loginPic() {
        String value1 = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_ADMIN_LOGIN_BACKGROUND_IMAGE);
        String value2 = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_ADMIN_LOGIN_LOGO_LEFT_TOP);
        String value3 = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_ADMIN_LOGIN_LOGO_LOGIN);
        List<Map<String, Object>> bannerList = systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_ADMIN_LOGIN_BANNER_IMAGE_LIST);

        Map<String, Object> map = new HashMap<>();
        map.put("backgroundImage", value1);
        map.put("logo",            value2);
        map.put("loginLogo",       value3);
        map.put("banner",          bannerList);
        return map;
    }



}
