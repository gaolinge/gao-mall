package com.gao.glg.mall.api.bean.request.web;

import com.gao.glg.page.PagerDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 组合数据详情表
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class WebGroupDataSearchRequest extends PagerDTO {

    @ApiModelProperty(value = "关键字")
    private String keywords;

    @ApiModelProperty(value = "分组id")
    private Integer gid;

    @ApiModelProperty(value = "状态（1：开启；2：关闭；）")
    private Boolean status;
}
