package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSystemAttachmentDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemAttachmentService {

    /**
     * 根据父id和类型查询
     * <p/>
     *
     * @param pid
     * @param types
     * @return
     * @author : gaolinge
     * @date : 2024/12/19
     * @version : 1.0.0
     */
    List<EbSystemAttachmentDomain> listByPidAndTypes(Integer pid, List<String> types);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/19
     * @version : 1.0.0
     */
    void save(EbSystemAttachmentDomain domain);

}
