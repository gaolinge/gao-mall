package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 商品详情评论
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/29
 */
@Data
public class AppProductReplyDetailResponse implements Serializable {

    @ApiModelProperty(value = "评论总数")
    private Integer sumCount;

    @ApiModelProperty(value = "好评率")
    private String replyChance;

    @ApiModelProperty(value = "最后一条评论信息")
    private AppProductReplyRecordResponse productReply;
}
