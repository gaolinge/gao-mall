package com.gao.glg.mall.api.domain;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreProductAttrValueDomain
 * @author gaolinge
 */
@Data
public class EbProductAttrValueDomain {
    /** 
     * 主键
     */
    private Integer id;

    /** 
     * 商品ID
     */
    private Integer productId;

    /** 
     * 商品属性索引值 (attr_value|attr_value[|....])
     */
    private String suk;

    /** 
     * 属性对应的库存
     */
    private Integer stock;

    /** 
     * 销量
     */
    private Integer sales;

    /** 
     * 属性金额
     */
    private BigDecimal price;

    /** 
     * 图片
     */
    private String image;

    /** 
     * 唯一值
     */
    private String unique;

    /** 
     * 成本价
     */
    private BigDecimal cost;

    /** 
     * 商品条码
     */
    private String barCode;

    /** 
     * 原价
     */
    private BigDecimal otPrice;

    /** 
     * 重量
     */
    private BigDecimal weight;

    /** 
     * 体积
     */
    private BigDecimal volume;

    /** 
     * 一级返佣
     */
    private BigDecimal brokerage;

    /** 
     * 二级返佣
     */
    private BigDecimal brokerageTwo;

    /** 
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    private Boolean type;

    /** 
     * 活动限购数量
     */
    private Integer quota;

    /** 
     * 活动限购数量显示
     */
    private Integer quotaShow;

    /** 
     * attr_values 创建更新时的属性对应
     */
    private String attrValue;

    /** 
     * 是否删除,0-否，1-是
     */
    private Boolean isDel;


}
