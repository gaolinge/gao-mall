package com.gao.glg.mall.api.bean.response.web;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 主页统计数据
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class WebHomeRateResponse implements Serializable {

    @ApiModelProperty(value = "今日销售额")
    private Object sales;

    @ApiModelProperty(value = "昨日销售额")
    private Object yesterdaySales;

    @ApiModelProperty(value = "今日访问量")
    private Object pageviews;

    @ApiModelProperty(value = "昨日访问量")
    private Object yesterdayPageviews;

    @ApiModelProperty(value = "今日订单量")
    private Object orderNum;

    @ApiModelProperty(value = "昨日订单量")
    private Object yesterdayOrderNum;

    @ApiModelProperty(value = "今日新增用户")
    private Object newUserNum;

    @ApiModelProperty(value = "昨日新增用户")
    private Object yesterdayNewUserNum;

}
