package com.gao.glg.mall.api.cache;

import com.gao.glg.mall.api.constant.CacheConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/12
 */
@Service
public class ValidateCodeCache {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 设置过期时间
     * <p/>
     *
     * @param key
     * @return
     * @author : gaolinge
     * @date : 2024/12/12
     * @version : 1.0.0
     */
    public void setTimeout(String key, String value) {
        key = CacheConstant.PRO_ADD_CART_KEY + key;
        stringRedisTemplate.opsForValue().set(key, value, 5, TimeUnit.MINUTES);
    }

    public String get(String key) {
        key = CacheConstant.PRO_ADD_CART_KEY + key;
        return stringRedisTemplate.opsForValue().get(key);
    }


}
