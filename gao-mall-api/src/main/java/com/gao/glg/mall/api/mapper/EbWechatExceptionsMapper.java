package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbWechatExceptionsDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbWechatExceptionsMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbWechatExceptionsDomain domain);

    /**
     * 查询
     */
    List<EbWechatExceptionsDomain> select();

    /**
     * 更新
     */
    int update(EbWechatExceptionsDomain domain);
}
