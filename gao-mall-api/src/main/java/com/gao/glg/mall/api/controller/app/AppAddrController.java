package com.gao.glg.mall.api.controller.app;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.app.AppUserAddressRequest;
import com.gao.glg.mall.api.bean.response.app.AppUserAddressResponse;
import com.gao.glg.mall.api.business.IAddressBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 地址
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/address")
@Api(tags = "地址")
public class AppAddrController {

    @Autowired
    private IAddressBusiness addressBusiness;

    @ApiOperation(value = "列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<AppUserAddressResponse>> list(PagerDTO request) {
        return RestResult.success(addressBusiness.list(request));
    }

    @ApiOperation(value = "保存")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public RestResult<AppUserAddressResponse> create(@RequestBody @Validated AppUserAddressRequest request) {
        return RestResult.success(addressBusiness.create(request));
    }

    @ApiOperation(value = "地址详情")
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public RestResult<AppUserAddressResponse> detail(@PathVariable("id") Integer id) {
        return RestResult.success(addressBusiness.detail(id));
    }

    @ApiOperation(value = "设置默认地址")
    @RequestMapping(value = "/default/set", method = RequestMethod.POST)
    public RestResult<AppUserAddressResponse> def(@RequestBody JSONObject request) {
        addressBusiness.setDefault(request.get("id"));
        return RestResult.success();
    }


    @ApiOperation(value = "删除")
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    public RestResult<String> delete(@RequestBody JSONObject request) {
        addressBusiness.delete(request.get("id"));
        return RestResult.success();
    }

    @ApiOperation(value = "获取默认地址")
    @RequestMapping(value = "/default", method = RequestMethod.GET)
    public RestResult<AppUserAddressResponse> getDefault() {
        return RestResult.success(addressBusiness.getDefault());

    }
}



