package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbProductAttrDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductAttrMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductAttrDomain domain);

    /**
     * 查询
     */
    List<EbProductAttrDomain> select();

    /**
     * 更新
     */
    int update(EbProductAttrDomain domain);

    /**
     * 查询
     */
    List<EbProductAttrDomain> listByPidAndType(@Param("productId") Integer productId,
                                               @Param("type") Integer type);

    /**
     * 查询
     */
    List<EbProductAttrDomain> listByPidsAndType(@Param("pids") List<Integer> pids, @Param("type") Integer type);
}
