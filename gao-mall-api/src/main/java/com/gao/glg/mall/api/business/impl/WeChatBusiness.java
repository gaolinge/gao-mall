package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.business.IWeChatBusiness;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.service.EbSystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeChatBusiness implements IWeChatBusiness {

    @Autowired
    private EbSystemConfigService systemConfigService;

    @Override
    public String logo() {
        return systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_MOBILE_LOGIN_LOGO);
    }
}
