package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.bean.response.web.WebSystemAttResponse;
import com.gao.glg.mall.api.business.IMaterialBusiness;
import com.gao.glg.mall.api.domain.EbSystemAttachmentDomain;
import com.gao.glg.mall.api.service.EbSystemAttachmentService;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.SplitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialBusiness implements IMaterialBusiness {

    @Autowired
    private EbSystemAttachmentService systemAttachmentService;

    @Override
    public PagerInfo<WebSystemAttResponse> list(Integer pid, String attType, PagerDTO request) {
        return PagerUtil.page(request, () -> {
            List<String> types = null;
            if (attType != null) {
                types = SplitUtil.split(attType, ',');
            }
            List<EbSystemAttachmentDomain> attachmentDomains = systemAttachmentService.listByPidAndTypes(pid, types);
            return BeanCopierUtil.copyList(attachmentDomains, WebSystemAttResponse.class);
        });
    }
}
