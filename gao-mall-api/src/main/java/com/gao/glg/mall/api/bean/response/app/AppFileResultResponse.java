package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 文件信息
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
@Data
public class AppFileResultResponse implements Serializable {

    @ApiModelProperty(value = "文件名")
    private String fileName;

    @ApiModelProperty(value = "扩展名")
    private String extName;

    @ApiModelProperty(value = "文件大小")
    private Long fileSize;

    @ApiModelProperty(value = "可供访问的url")
    private String url;

    @ApiModelProperty(value = "类型")
    private String type;
}
