package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbArticleDomain
 * @author gaolinge
 */
@Data
public class EbArticleDomain {
    /** 
     * 文章管理ID
     */
    private Integer id;

    /** 
     * 分类id
     */
    private String cid;

    /** 
     * 文章标题
     */
    private String title;

    /** 
     * 文章作者
     */
    private String author;

    /** 
     * 文章图片
     */
    private String imageInput;

    /** 
     * 文章简介
     */
    private String synopsis;

    /** 
     * 文章分享标题
     */
    private String shareTitle;

    /** 
     * 文章分享简介
     */
    private String shareSynopsis;

    /** 
     * 浏览次数
     */
    private String visit;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 原文链接
     */
    private String url;

    /** 
     * 微信素材id
     */
    private String mediaId;

    /** 
     * 状态
     */
    private Byte status;

    /** 
     * 是否隐藏
     */
    private Byte hide;

    /** 
     * 管理员id
     */
    private Integer adminId;

    /** 
     * 商户id
     */
    private Integer merId;

    /** 
     * 商品关联id
     */
    private Integer productId;

    /** 
     * 是否热门(小程序)
     */
    private Byte isHot;

    /** 
     * 是否轮播图(小程序)
     */
    private Byte isBanner;

    /** 
     * 文章内容
     */
    private String content;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
