package com.gao.glg.mall.api.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * 城市缓存
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/6
 */
@Service
public class CityCache {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

}
