package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebEmployeeSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemAdminDomain;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemAdminService {

    /**
     * 根据账号获取用户信息
     * <p/>
     *
     * @param account
     * @return
     * @author : gaolinge
     * @date : 2024/12/13
     * @version : 1.0.0
     */
    EbSystemAdminDomain getByAccount(String account);

    /**
     * 修改用户信息
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/13
     * @version : 1.0.0
     */
    void update(EbSystemAdminDomain domain);

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/13
     * @version : 1.0.0
     */
    List<EbSystemAdminDomain> page(WebEmployeeSearchRequest request);

    /**
     * 新增
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/13
     * @version : 1.0.0
     */
    void save(EbSystemAdminDomain domain);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/13
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 根据id查询
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/13
     * @version : 1.0.0
     */
    EbSystemAdminDomain getById(Integer id);
}
