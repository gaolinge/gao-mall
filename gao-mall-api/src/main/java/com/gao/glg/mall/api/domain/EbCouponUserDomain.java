package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreCouponUserDomain
 * @author gaolinge
 */
@Data
public class EbCouponUserDomain {
    /** 
     * id
     */
    private Integer id;

    /** 
     * 优惠券发布id
     */
    private Integer couponId;

    /** 
     * 兑换的项目id
     */
    private Integer cid;

    /** 
     * 领取人id
     */
    private Integer uid;

    /** 
     * 优惠券名称
     */
    private String name;

    /** 
     * 优惠券的面值
     */
    private BigDecimal money;

    /** 
     * 最低消费多少金额可用优惠券
     */
    private BigDecimal minPrice;

    /** 
     * 获取方式，send后台发放, 用户领取 get
     */
    private String type;

    /** 
     * 状态（0：未使用，1：已使用, 2:已失效）
     */
    private Integer status;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 开始使用时间
     */
    private Date startTime;

    /** 
     * 过期时间
     */
    private Date endTime;

    /** 
     * 使用时间
     */
    private Date useTime;

    /** 
     * 使用类型 1 全场通用, 2 商品券, 3 品类券
     */
    private Integer useType;

    /** 
     * 所属商品id / 分类id
     */
    private String primaryKey;


}
