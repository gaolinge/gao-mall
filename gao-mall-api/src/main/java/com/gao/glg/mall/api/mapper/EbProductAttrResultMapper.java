package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbProductAttrResultDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductAttrResultMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductAttrResultDomain domain);

    /**
     * 查询
     */
    List<EbProductAttrResultDomain> select();

    /**
     * 更新
     */
    int update(EbProductAttrResultDomain domain);
}
