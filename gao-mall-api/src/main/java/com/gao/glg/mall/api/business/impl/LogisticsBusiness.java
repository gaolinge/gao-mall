package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.collection.CollUtil;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempRequest;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempFreeResponse;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempRegionResponse;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempResponse;
import com.gao.glg.mall.api.business.ILogisticsBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.domain.EbShippingTempDomain;
import com.gao.glg.mall.api.domain.EbShippingTempFreeDomain;
import com.gao.glg.mall.api.domain.EbShippingTempRegionDomain;
import com.gao.glg.mall.api.service.EbShippingTempFreeService;
import com.gao.glg.mall.api.service.EbShippingTempRegionService;
import com.gao.glg.mall.api.service.EbShippingTempService;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class LogisticsBusiness implements ILogisticsBusiness {

    @Autowired
    private EbShippingTempService shippingTempService;
    @Autowired
    private EbShippingTempFreeService shippingTempFreeService;
    @Autowired
    private EbShippingTempRegionService shippingTempRegionService;

    @Override
    public PagerInfo<WebLogisticsTempResponse> page(WebLogisticsTempSearchRequest request) {
        return PagerUtil.page(request, () -> {
            List<EbShippingTempDomain> domains = shippingTempService.page(request);
            return BeanCopierUtil.copyList(domains, WebLogisticsTempResponse.class);
        });
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void create(WebLogisticsTempRequest request) {
        if (CollUtil.isEmpty(request.getShippingTemplatesRegionRequestList())) {
            throw new BusinessException(ErrorConstant.AT_LEAST_ONE_DEFAULT_NATIONAL_REGION);
        }
        EbShippingTempDomain domain = shippingTempService.getByName(request.getName());
        if (domain != null) {
            throw new BusinessException(ErrorConstant.TEMPLATE_NAME_ALREADY_EXISTS);
        }

        domain = new EbShippingTempDomain();
        domain.setName(request.getName());
        domain.setSort(request.getSort());
        domain.setType(request.getType());
        domain.setAppoint(request.getAppoint());
        shippingTempService.save(domain);
        shippingTempRegionService.batchSave(request.getShippingTemplatesRegionRequestList(), request.getType(), domain.getId());
        if(!CollectionUtils.isEmpty(request.getShippingTemplatesFreeRequestList()) && request.getAppoint()){
            shippingTempFreeService.batchSave(request.getShippingTemplatesFreeRequestList(), request.getType(), domain.getId());
        }
    }

    @Override
    public void delete(Integer id) {
        shippingTempService.delete(id);
    }

    @Override
    public void update(Integer id, WebLogisticsTempRequest request) {
        if (CollUtil.isEmpty(request.getShippingTemplatesRegionRequestList())) {
            throw new BusinessException(ErrorConstant.AT_LEAST_ONE_DEFAULT_NATIONAL_REGION);
        }

        EbShippingTempDomain domain = new EbShippingTempDomain();
        domain.setId(id);
        domain.setName(request.getName());
        domain.setSort(request.getSort());
        domain.setType(request.getType());
        domain.setAppoint(request.getAppoint());
        shippingTempService.update(domain);

        shippingTempRegionService.batchSave(request.getShippingTemplatesRegionRequestList(), request.getType(), domain.getId());
        if(!CollectionUtils.isEmpty(request.getShippingTemplatesFreeRequestList()) && request.getAppoint()){
            shippingTempFreeService.batchSave(request.getShippingTemplatesFreeRequestList(), request.getType(), domain.getId());
        }
    }

    @Override
    public WebLogisticsTempResponse detail(Integer id) {
        EbShippingTempDomain domain = shippingTempService.getById(id);
        return BeanCopierUtil.copy(domain, WebLogisticsTempResponse.class);
    }

    @Override
    public List<WebLogisticsTempFreeResponse> freePage(Integer tempId) {
        List<EbShippingTempFreeDomain> domains = shippingTempFreeService.groupByUnqid(tempId);
        return BeanCopierUtil.copyList(domains, WebLogisticsTempFreeResponse.class);
    }

    @Override
    public List<WebLogisticsTempRegionResponse> regionPage(Integer tempId) {
        List<EbShippingTempRegionDomain> domains = shippingTempRegionService.groupByUnqid(tempId);
        return BeanCopierUtil.copyList(domains, WebLogisticsTempRegionResponse.class);
    }
}
