package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.domain.EbSeckillDomain;
import com.gao.glg.mall.api.domain.EbSeckillMangerDomain;
import com.gao.glg.mall.api.mapper.EbSeckillMapper;
import com.gao.glg.mall.api.service.EbSeckillMangerService;
import com.gao.glg.mall.api.service.EbSeckillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EbSeckillServiceImpl implements EbSeckillService {

    @Resource
    private EbSeckillMapper seckillMapper;
    @Autowired
    private EbSeckillMangerService seckillMangerService;

    @Override
    public EbSeckillDomain getById(Integer id) {
        return seckillMapper.getById(id);
    }

    @Override
    public List<EbSeckillDomain> getByTimeId(Integer timeId) {
        return seckillMapper.getByTimeId(timeId);
    }

    @Override
    public List<EbSeckillDomain> listByProductId(Integer productId) {
        List<EbSeckillMangerDomain> seckillMangerDomains = seckillMangerService.select(20);
        seckillMangerDomains = seckillMangerDomains.stream().filter(e -> getSeckillStatus(e) == 2).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(seckillMangerDomains)) {
            return new ArrayList<>();
        }

        List<Integer> timeIds = seckillMangerDomains.stream().map(
                EbSeckillMangerDomain::getId).collect(Collectors.toList());
        return seckillMapper.listByTimeIds(productId, timeIds);
    }

    public Integer getSeckillStatus(EbSeckillMangerDomain domain) {
        Integer status = null;
        if(domain.getStatus() == 0) {
            status = 0;
        } else if(domain.getStatus() == 1 && getCurrentHour() < domain.getStartTime()){
            status = 1;
        } else if(domain.getStatus() == 1 && getCurrentHour()>= domain.getStartTime() && getCurrentHour() < domain.getEndTime()) {
            status = 2;
        } else if(domain.getStatus() == 1 && getCurrentHour()>= domain.getEndTime()) {
            status =-1;
        }
        return status;
    }

    public static int getCurrentHour(){
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        return hour;
    }
}
