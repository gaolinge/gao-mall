package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbUserAddressDomain
 * @author gaolinge
 */
@Data
public class EbUserAddressDomain {
    /** 
     * 用户地址id
     */
    private Integer id;

    /** 
     * 用户id
     */
    private Integer uid;

    /** 
     * 收货人姓名
     */
    private String realName;

    /** 
     * 收货人电话
     */
    private String phone;

    /** 
     * 收货人所在省
     */
    private String province;

    /** 
     * 收货人所在市
     */
    private String city;

    /** 
     * 城市id
     */
    private Integer cityId;

    /** 
     * 收货人所在区
     */
    private String district;

    /** 
     * 收货人详细地址
     */
    private String detail;

    /** 
     * 邮编
     */
    private Integer postCode;

    /** 
     * 经度
     */
    private String longitude;

    /** 
     * 纬度
     */
    private String latitude;

    /** 
     * 是否默认
     */
    private Integer isDefault;

    /** 
     * 是否删除
     */
    private Integer isDel;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
