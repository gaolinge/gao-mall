package com.gao.glg.mall.api.bean.response.app;

import com.gao.glg.serializer.FileUrlSerializerField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 秒杀Header时间header响应对象
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/23
 */
@Data
public class AppSecKillResponse implements Serializable {

    @ApiModelProperty(value = "秒杀时段id")
    private Integer id;

    @FileUrlSerializerField
    @ApiModelProperty(value = "秒杀时段轮播图")
    private String slide;

    @ApiModelProperty(value = "秒杀时段状态名称")
    private String statusName;

    @ApiModelProperty(value = "秒杀时段状态")
    private int status;

    @ApiModelProperty(value = "秒杀时段时间信息")
    private String time;

    @ApiModelProperty(value = "秒杀时段结束时间")
    private String timeSwap;

    @ApiModelProperty(value = "是否选中")
    private Boolean isCheck = false;
}
