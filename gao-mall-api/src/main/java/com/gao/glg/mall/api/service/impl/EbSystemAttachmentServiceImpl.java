package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.domain.EbSystemAttachmentDomain;
import com.gao.glg.mall.api.mapper.EbSystemAttachmentMapper;
import com.gao.glg.mall.api.service.EbSystemAttachmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemAttachmentServiceImpl implements EbSystemAttachmentService {

    @Resource
    private EbSystemAttachmentMapper systemAttachmentMapper;


    @Override
    public List<EbSystemAttachmentDomain> listByPidAndTypes(Integer pid, List<String> types) {
        return systemAttachmentMapper.listByPidAndTypes(pid, types);
    }

    @Override
    public void save(EbSystemAttachmentDomain domain) {
        systemAttachmentMapper.save(domain);
    }
}
