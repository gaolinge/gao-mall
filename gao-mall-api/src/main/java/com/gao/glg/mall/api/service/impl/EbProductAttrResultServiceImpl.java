package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductAttrResultMapper;
import com.gao.glg.mall.api.service.EbProductAttrResultService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbProductAttrResultServiceImpl implements EbProductAttrResultService {
    @Resource
    private EbProductAttrResultMapper ebProductAttrResultMapper;
}
