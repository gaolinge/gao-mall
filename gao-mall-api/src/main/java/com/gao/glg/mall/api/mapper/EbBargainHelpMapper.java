package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbBargainHelpDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbBargainHelpMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbBargainHelpDomain domain);

    /**
     * 查询
     */
    List<EbBargainHelpDomain> select();

    /**
     * 更新
     */
    int update(EbBargainHelpDomain domain);
}
