package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.app.AppFileResultResponse;
import com.gao.glg.mall.api.bean.response.ImageUploadResponse;
import com.gao.glg.mall.api.business.IFileBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


/**
 * 上传文件
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/upload")
@Api(tags = "上传文件")
public class AppFileController {

    @Autowired
    private IFileBusiness fileService;


    @ApiOperation(value = "图片上传")
    @PostMapping(value = "/image")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "model", value = "模块 用户user,商品product,微信wechat,news文章"),
            @ApiImplicitParam(name = "pid", value = "分类ID 0编辑器,1商品图片,2拼团图片,3砍价图片,4秒杀图片" +
                    ",5文章图片,6组合数据图,7前台用户,8微信系列 ", allowableValues = "range[0,1,2,3,4,5,6,7,8]")
    })
    public RestResult<AppFileResultResponse> image(MultipartFile multipart,
                                                   @RequestParam(value = "model") String model,
                                                   @RequestParam(value = "pid") Integer pid) throws IOException {
        return RestResult.success(fileService.imageUpload(multipart, model, pid));
    }


    @PostMapping(value = "/upload-image")
    @ApiOperation(value = "上传图片")
    public ImageUploadResponse uploadImage(@RequestParam("file") MultipartFile file) throws Exception {
        return fileService.uploadImage(file);
    }

    @PostMapping(value = "/upload-imag")
    @ApiOperation(value = "上传图片")
    public RestResult<String> uploadImag(@RequestParam("file") MultipartFile file) throws Exception {
        return RestResult.success(fileService.uploadImag(file));
    }

    @GetMapping(value = "/delete/image/{fileName}")
    @ApiOperation(value = "删除图片")
    public RestResult<String> uploadImag(@PathVariable String fileName) throws Exception {
        fileService.deleteFile(fileName);
        return RestResult.success();
    }
}



