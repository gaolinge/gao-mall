package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebRoleSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemRoleDomain;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemRoleService {

    /**
     * 获取所有
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    List<EbSystemRoleDomain> selectAll();

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    List<EbSystemRoleDomain> page(WebRoleSearchRequest request);

    /**
     * 根据角色名称获取
     * <p/>
     *
     * @param roleName
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    EbSystemRoleDomain getByRoleName(String roleName);

    /**
     * 新建
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void save(EbSystemRoleDomain domain);

    /**
     * 根据id获取
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    EbSystemRoleDomain getById(Integer id);

    /**
     * 修改
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void update(EbSystemRoleDomain domain);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void delete(Integer id);
}
