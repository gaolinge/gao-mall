package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebCouponRequest;
import com.gao.glg.mall.api.bean.request.web.WebCouponSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebCouponResponse;
import com.gao.glg.mall.api.business.ICouponBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 优惠劵中心
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin/coupon")
@Api(tags = "优惠劵中心")
public class WebCouponController {

    @Autowired
    private ICouponBusiness couponBusiness;

    //@PreAuthorize("hasAuthority('admin:coupon:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebCouponResponse>> page(@Validated WebCouponSearchRequest request) {
        return RestResult.success(couponBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:coupon:save')")
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult save(@RequestBody @Validated WebCouponRequest request) {
        couponBusiness.create(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:coupon:update:status')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update/status", method = RequestMethod.POST)
    public RestResult<String> updateStatus(@RequestParam Integer id, @RequestParam Integer status) {
        couponBusiness.updateStatus(id, status);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:coupon:info')")
    /*@ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.POST)
    @ApiImplicitParam(name="id", value="优惠券ID", required = true)
    public RestResult<StoreCouponInfoResponse> info(@RequestParam Integer id) {
        return RestResult.success(couponBusiness.info(id));
    }*/

    //@PreAuthorize("hasAuthority('admin:coupon:send:list')")
    /*@ApiOperation(value = "发送优惠券列表")
    @RequestMapping(value = "/send/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebCouponResponse>> getSendList(@Validated SearchAndPageRequest searchAndPageRequest) {
        return RestResult.success(couponBusiness.getSendList(searchAndPageRequest));
    }*/

    //@PreAuthorize("hasAuthority('admin:coupon:delete')")
    @ApiOperation(value = "删除优惠券")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public RestResult delete(@RequestParam Integer id) {
        couponBusiness.delete(id);
        return RestResult.success();
    }
}



