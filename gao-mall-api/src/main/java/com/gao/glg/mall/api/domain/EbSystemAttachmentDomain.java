package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemAttachmentDomain
 * @author gaolinge
 */
@Data
public class EbSystemAttachmentDomain {
    /** 
     * 
     */
    private Integer attId;

    /** 
     * 附件名称
     */
    private String name;

    /** 
     * 附件路径
     */
    private String attDir;

    /** 
     * 压缩图片路径
     */
    private String sattDir;

    /** 
     * 附件大小
     */
    private String attSize;

    /** 
     * 附件类型
     */
    private String attType;

    /** 
     * 分类ID0编辑器,1商品图片,2拼团图片,3砍价图片,4秒杀图片,5文章图片,6组合数据图， 7前台用户
     */
    private Integer pid;

    /** 
     * 图片上传类型 1本地 2七牛云 3OSS 4COS 
     */
    private Integer imageType;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
