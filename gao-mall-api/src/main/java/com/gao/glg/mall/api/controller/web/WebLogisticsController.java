package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempRequest;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempFreeResponse;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempRegionResponse;
import com.gao.glg.mall.api.bean.response.web.WebLogisticsTempResponse;
import com.gao.glg.mall.api.business.ILogisticsBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Slf4j
@RestController
@RequestMapping("/admin/logistics")
@Api(tags = "物流")
public class WebLogisticsController {

    @Autowired
    private ILogisticsBusiness shippingTemplatesService;

    //@PreAuthorize("hasAuthority('admin:shipping:templates:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/templates/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebLogisticsTempResponse>> page(@Validated WebLogisticsTempSearchRequest request){
        return RestResult.success(shippingTemplatesService.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:shipping:templates:free:list')")
    @ApiOperation(value = "根据模板id查询数据")
    @RequestMapping(value = "/free/list", method = RequestMethod.GET)
    public RestResult<List<WebLogisticsTempFreeResponse>> freePage(@RequestParam Integer tempId){
        return RestResult.success(shippingTemplatesService.freePage(tempId));
    }

    //@PreAuthorize("hasAuthority('admin:shipping:templates:region:list')")
    @ApiOperation(value = "根据模板id查询数据")
    @RequestMapping(value = "/region/list", method = RequestMethod.GET)
    public RestResult<List<WebLogisticsTempRegionResponse>> regionPage(@RequestParam Integer tempId){
        return RestResult.success(shippingTemplatesService.regionPage(tempId));
    }

    //@PreAuthorize("hasAuthority('admin:shipping:templates:save')")
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/templates/save", method = RequestMethod.POST)
    public RestResult<String> save(@RequestBody @Validated WebLogisticsTempRequest request){
        shippingTemplatesService.create(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:shipping:templates:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/templates/delete", method = RequestMethod.GET)
    @ApiImplicitParam(name="id", value="模板ID", required = true)
    public RestResult<String> delete(@RequestParam(value = "id") Integer id){
        shippingTemplatesService.delete(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:shipping:templates:update')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/templates/update", method = RequestMethod.POST)
    public RestResult<String> update(@RequestParam Integer id, @RequestBody @Validated WebLogisticsTempRequest request){
        shippingTemplatesService.update(id, request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:shipping:templates:info')")
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/templates/info", method = RequestMethod.GET)
    @ApiImplicitParam(name="id", value="模板ID", required = true)
    public RestResult<WebLogisticsTempResponse> detail(@RequestParam(value = "id") Integer id){
        return RestResult.success(shippingTemplatesService.detail(id));
    }
}