package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeSearchRequest;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemAdminDomain;
import com.gao.glg.mall.api.mapper.EbSystemAdminMapper;
import com.gao.glg.mall.api.service.EbSystemAdminService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemAdminServiceImpl implements EbSystemAdminService  {
    @Resource
    private EbSystemAdminMapper systemAdminMapper;

    @Override
    public EbSystemAdminDomain getByAccount(String account) {
        return systemAdminMapper.getByAccount(account);
    }

    @Override
    public void update(EbSystemAdminDomain domain) {
        systemAdminMapper.update(domain);
    }

    @Override
    public List<EbSystemAdminDomain> page(WebEmployeeSearchRequest request) {
        return systemAdminMapper.page(request);
    }

    @Override
    public void save(EbSystemAdminDomain domain) {
        systemAdminMapper.save(domain);
    }

    @Override
    public void delete(Integer id) {
        systemAdminMapper.delete(id);
    }

    @Override
    public EbSystemAdminDomain getById(Integer id) {
        return systemAdminMapper.getById(id);
    }
}
