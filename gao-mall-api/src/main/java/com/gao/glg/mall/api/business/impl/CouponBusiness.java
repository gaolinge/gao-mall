package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.util.StrUtil;
import com.gao.glg.enums.YesNoEnum;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.dto.AppOrderInfoDTO;
import com.gao.glg.mall.api.bean.dto.AppOrderItemDTO;
import com.gao.glg.mall.api.bean.request.app.AppCouponReceiveRequest;
import com.gao.glg.mall.api.bean.request.web.WebCouponRequest;
import com.gao.glg.mall.api.bean.request.web.WebCouponSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppCouponOrderResponse;
import com.gao.glg.mall.api.bean.response.app.AppCouponResponse;
import com.gao.glg.mall.api.bean.response.app.AppCouponUserResponse;
import com.gao.glg.mall.api.bean.response.web.WebCouponResponse;
import com.gao.glg.mall.api.business.ICouponBusiness;
import com.gao.glg.mall.api.cache.OrderCache;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbCouponDomain;
import com.gao.glg.mall.api.domain.EbCouponUserDomain;
import com.gao.glg.mall.api.service.EbCouponService;
import com.gao.glg.mall.api.service.EbCouponUserService;
import com.gao.glg.mall.api.service.EbProductService;
import com.gao.glg.mall.api.service.EbUserService;
import com.gao.glg.mall.api.utils.StreamUtil;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.DateUtils;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CouponBusiness implements ICouponBusiness {

    @Autowired
    private OrderCache orderCache;
    @Autowired
    private EbUserService userService;
    @Autowired
    private EbProductService productService;
    @Autowired
    private EbCouponService couponService;
    @Autowired
    private EbCouponUserService couponUserService;

    @Override
    public List<AppCouponResponse> list(int type, int productId, PagerDTO request) {
        List<EbCouponDomain> couponDomains = couponService.listByReceive(type, productId, request);
        if (CollectionUtils.isEmpty(couponDomains)) {
            return Lists.newArrayList();
        }

        List<EbCouponUserDomain> couponUserDomains = couponUserService.listByUid(userService.getUserId());
        Map<Integer, EbCouponUserDomain> couponUserMap = StreamUtil.listToMap(couponUserDomains, EbCouponUserDomain::getCouponId);

        List<AppCouponResponse> responses = new ArrayList<>();
        for (EbCouponDomain couponDomain : couponDomains) {
            AppCouponResponse response = BeanCopierUtil.copy(couponDomain, AppCouponResponse.class);
            if (couponUserMap.containsKey(couponDomain.getId())) {
                response.setIsUse(true);
            }
            if (response.getReceiveEndTime() == null) {
                response.setReceiveStartTime(null);
            }
            String t1 = DateUtils.formatDate(couponDomain.getReceiveStartTime(), DateUtils.DATE_FORMAT);
            String t2 = DateUtils.formatDate(couponDomain.getUseEndTime(),       DateUtils.DATE_FORMAT);
            response.setUseStartTimeStr(t1);
            response.setUseEndTimeStr(t2);
            responses.add(response);
        }
        return responses;
    }

    @Override
    public PagerInfo myList(String type, PagerDTO request) {
        Integer userId = userService.getUserIdException();
        return PagerUtil.page(request, () -> {
            List<EbCouponUserDomain> couponUserDomains = couponUserService.listByUidAndType(userId, type);
            if (CollectionUtils.isEmpty(couponUserDomains)) {
                return null;
            }
            return couponUserDomains.stream().map(domain -> {
                AppCouponUserResponse response =
                        BeanCopierUtil.copy(domain, AppCouponUserResponse.class);
                String status = buildStatus(domain);
                String t1 = DateUtils.formatDate(domain.getStartTime(), DateUtils.DATE_FORMAT);
                String t2 = DateUtils.formatDate(domain.getEndTime(),   DateUtils.DATE_FORMAT);
                response.setValidStr(status);
                response.setUseStartTimeStr(t1);
                response.setUseEndTimeStr(t2);
                return response;
            }).collect(Collectors.toList());
        });
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void receive(AppCouponReceiveRequest request) {
        EbCouponDomain couponDomain = couponService.getById(request.getCouponId());
        if (YesNoEnum.YES.getValue() == couponDomain.getIsLimited()
                && couponDomain.getLastTotal() < 1) {
            throw new BusinessException(ErrorConstant.COUPON_NUM_INSUFFICIENT);
        }

        Integer userId = userService.getUserIdException();
        List<EbCouponUserDomain> couponUserDomains = couponUserService
                .listByUidAndCouponId(userId, request.getCouponId());
        if (!CollectionUtils.isEmpty(couponUserDomains)) {
            throw new BusinessException(ErrorConstant.ALREADY_RECEIVED_COUPON);
        }

        EbCouponUserDomain couponUserDomain = new EbCouponUserDomain();
        couponUserDomain.setUid(userId);
        couponUserDomain.setCouponId(couponDomain.getId());
        couponUserDomain.setName(couponDomain.getName());
        couponUserDomain.setMoney(couponDomain.getMoney());
        couponUserDomain.setMinPrice(couponDomain.getMinPrice());
        couponUserDomain.setUseType(couponDomain.getUseType());
        couponUserDomain.setType(MallConstant.STORE_COUPON_USER_TYPE_GET);
        couponUserDomain.setStatus(YesNoEnum.NO.getValue());
        if (couponDomain.getUseType() > YesNoEnum.YES.getValue()) {
            couponUserDomain.setPrimaryKey(couponDomain.getPrimaryKey());
        }
        if (YesNoEnum.YES.getValue() == couponDomain.getIsFixedTime()) {
            Date endTime = DateUtils.addDays(new Date(), couponDomain.getDay());
            couponUserDomain.setStartTime(new Date());
            couponUserDomain.setEndTime(endTime);
        }

        couponUserService.save(couponUserDomain);
        couponService.deduct(couponDomain.getId(), couponDomain.getIsLimited(), 1);
    }

    @Override
    public List<AppCouponOrderResponse> canCoupon(String preOrderNo) {
        AppOrderInfoDTO infoDTO = orderCache.getPreOrder(preOrderNo);
        if (infoDTO == null) {
            throw new BusinessException(ErrorConstant.PRE_ORDER_NOT_EXIST);
        }

        List<Integer> pids = infoDTO.getOrderDetailList().stream()
                .map(AppOrderItemDTO::getProductId).distinct().collect(Collectors.toList());
        BigDecimal maxPrice = infoDTO.getProTotalFee();
        List<EbCouponUserDomain> couponUserDomains = couponUserService.listByCanUse(pids, maxPrice);
        return BeanCopierUtil.copyList(couponUserDomains, AppCouponOrderResponse.class);
    }

    @Override
    public PagerInfo<WebCouponResponse> page(WebCouponSearchRequest request) {
        return PagerUtil.page(request, ()-> {
            List<EbCouponDomain> domains = couponService.page(request);
            return BeanCopierUtil.copyList(domains, WebCouponResponse.class);
        });
    }

    @Override
    public void create(WebCouponRequest request) {
        if (request.getIsLimited() && (request.getTotal() == null || request.getTotal() == 0)) {
            throw new BusinessException(ErrorConstant.FORM_TEMPLATE_FORMAT_ERROR, "请输入数量！");
        }

        if (request.getUseType() > 1 && (StrUtil.isBlank(request.getPrimaryKey()))) {
            throw new BusinessException(ErrorConstant.FORM_TEMPLATE_FORMAT_ERROR, "请选择商品/分类！");
        }

        if (!request.getIsFixedTime() && (request.getDay() == null || request.getDay() == 0)) {
            throw new BusinessException(ErrorConstant.FORM_TEMPLATE_FORMAT_ERROR, "请输入天数！");
        }

        if (request.getIsForever()) {
            if (request.getReceiveStartTime() == null || request.getReceiveEndTime() == null) {
                throw new BusinessException(ErrorConstant.FORM_TEMPLATE_FORMAT_ERROR, "请选择领取时间范围！");
            }
            if (request.getReceiveStartTime().compareTo(request.getReceiveEndTime()) > 0) {
                throw new BusinessException(ErrorConstant.FORM_TEMPLATE_FORMAT_ERROR, "请选择正确的领取时间范围！");
            }
        }

        EbCouponDomain domain = new EbCouponDomain();
        BeanUtils.copyProperties(request, domain);
        domain.setLastTotal(request.getTotal());
        if (!request.getIsFixedTime()) {
            domain.setUseStartTime(null);
            domain.setUseEndTime(null);
        }
        if (!request.getIsForever()) {
            domain.setReceiveStartTime(DateUtils.getCurrentDate());
        }
        couponService.save(domain);
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        EbCouponDomain coupon = couponService.getById(id);
        if (coupon == null) {
            throw new BusinessException(ErrorConstant.COUPON_DOES_NOT_EXIST);
        }
        if (coupon.getStatus().equals(status)) {
            return;
        }

        EbCouponDomain domain = new EbCouponDomain();
        domain.setId(id);
        domain.setStatus(status);
        couponService.update(domain);
    }

    @Override
    public void delete(Integer id) {
        EbCouponDomain coupon = couponService.getById(id);
        if (coupon == null) {
            throw new BusinessException(ErrorConstant.COUPON_DOES_NOT_EXIST);
        }
        couponService.delete(id);
    }

    /**
     * 构建状态
     *
     * @param domain
     * @return
     */
    private String buildStatus(EbCouponUserDomain domain) {
        String status = "usable";
        switch (domain.getStatus()) {
            case 1:
                status = "unusable";
                break;
            case 2:
                status = "overdue";
                break;
        }

        Date date = new Date();
        if (domain.getStartTime() != null &&
                domain.getEndTime() != null) {
            if (domain.getStartTime().compareTo(date) > 0) {
                status = "notStart";
            }
            if (date.compareTo(domain.getEndTime()) >= 0) {
                status = "overdue";
            }
        }
        return status;
    }


}
