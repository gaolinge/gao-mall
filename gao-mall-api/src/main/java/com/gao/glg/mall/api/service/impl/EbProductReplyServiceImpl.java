package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.request.web.WebProductReplySearchRequest;
import com.gao.glg.mall.api.domain.EbProductReplyDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductReplyMapper;
import com.gao.glg.mall.api.service.EbProductReplyService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbProductReplyServiceImpl implements EbProductReplyService {
    @Resource
    private EbProductReplyMapper productReplyMapper;

    @Override
    public Long replyCount(Integer pid, Integer type) {
        return productReplyMapper.replyCount(pid, type);
    }

    @Override
    public EbProductReplyDomain sumScore(Integer pid) {
        return productReplyMapper.sumScore(pid);
    }

    @Override
    public EbProductReplyDomain getByPidLimitOne(Integer pid) {
        return productReplyMapper.getByPidLimitOne(pid);
    }

    @Override
    public List<EbProductReplyDomain> page(WebProductReplySearchRequest request) {
        return productReplyMapper.page(request);
    }

    @Override
    public void save(EbProductReplyDomain domain) {
        productReplyMapper.save(domain);
    }

    @Override
    public void update(EbProductReplyDomain domain) {
        productReplyMapper.update(domain);
    }

    @Override
    public EbProductReplyDomain getById(Integer id) {
        return productReplyMapper.getById(id);
    }

    @Override
    public Integer countByOidAndPidAndUid(Integer oid, Integer pid, Integer uid) {
        return productReplyMapper.countByOidAndPidAndUid(oid, pid, uid);
    }
}
