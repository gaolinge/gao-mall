package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbUserBillDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbUserBillService {

    /**
     * 更新pm和类型查询
     * <p/>
     *
     * @param uid
     * @param pm
     * @param cate
     * @return
     * @author : gaolinge
     * @date : 2024/12/10
     * @version : 1.0.0
     */
    List<EbUserBillDomain> listByPmAndType(Integer uid, int pm, String cate);
}
