package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbCartDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbCartMapper {

    /**
     * 删除
     */
    int delete(java.lang.Long id);

    /**
     * 保存
     */
    int save(EbCartDomain domain);

    /**
     * 查询
     */
    List<EbCartDomain> select();

    /**
     * 更新
     */
    int update(EbCartDomain domain);

    /**
     * 查询
     */
    List<EbCartDomain> listByUserId(@Param("userId") Integer userId,
                                    @Param("isValid") Boolean isValid);

    /**
     * 查询
     */
    Integer countTotal(@Param("userId") Integer userId, @Param("status") Boolean status);

    /**
     * 查询
     */
    Integer countNum(@Param("userId") Integer userId, @Param("status")  Boolean status);

    /**
     * 查询
     */
    List<EbCartDomain> listByUserIdAndAttrId(@Param("userId") Integer uid, @Param("attrId") String attrId);

    /**
     * 查询
     */
    EbCartDomain getById(Long id);

    /**
     * 更新
     */
    void batchDelete(@Param("ids") List<Long> ids);
}
