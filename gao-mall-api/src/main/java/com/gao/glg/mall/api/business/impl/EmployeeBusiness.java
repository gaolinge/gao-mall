package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.util.ReUtil;
import com.gao.glg.enums.YesNoEnum;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeRequest;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeSearchRequest;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeUpdateRequest;
import com.gao.glg.mall.api.bean.response.web.WebAdminResponse;
import com.gao.glg.mall.api.bean.response.web.WebEmployeeResponse;
import com.gao.glg.mall.api.bean.response.web.WebMenuResponse;
import com.gao.glg.mall.api.bean.response.web.WebUserInfoResponse;
import com.gao.glg.mall.api.business.IEmployeeBusiness;
import com.gao.glg.mall.api.cache.TokenCache;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.RegularConstants;
import com.gao.glg.mall.api.domain.EbSystemAdminDomain;
import com.gao.glg.mall.api.domain.EbSystemMenuDomain;
import com.gao.glg.mall.api.domain.EbSystemRoleDomain;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.mall.api.utils.MallUtil;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.SplitUtil;
import com.gao.glg.utils.StreamUtil;
import com.gao.glg.utils.TreeUtil;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EmployeeBusiness implements IEmployeeBusiness {

    @Autowired
    private TokenCache tokenCache;
    @Autowired
    private EbSystemRoleService systemRoleService;
    @Autowired
    private EbSystemMenuService systemMenuService;
    @Autowired
    private EbSystemAdminService systemAdminService;
    @Autowired
    private EbSystemGroupDataService systemGroupDataService;

    @Override
    public WebUserInfoResponse info(String token) {
        String account = tokenCache.getWebValue(token, String.class);
        if (account == null) {
            throw new BusinessException(ErrorConstant.USER_LOGIN_EXPIRED);
        }

        EbSystemAdminDomain adminDomain = systemAdminService.getByAccount(account);
        List<String> roles = SplitUtil.split(adminDomain.getRoles(), ',');
        List<String> permissions = roles.contains("1") ?
                Lists.newArrayList("*:*:*") : systemMenuService.selectPerms(roles);

        WebUserInfoResponse response = new WebUserInfoResponse();
        response.setAccount(adminDomain.getAccount());
        response.setRealName(adminDomain.getRealName());
        response.setPhone(adminDomain.getPhone());
        response.setRoles(adminDomain.getRoles());
        response.setPermissionsList(permissions);
        return response;
    }

    @Override
    public List<WebMenuResponse> menus() {
        String token = tokenCache.getToken();
        String account = tokenCache.getWebValue(token, String.class);
        if (account == null) {
            throw new BusinessException(ErrorConstant.USER_LOGIN_EXPIRED);
        }

        EbSystemAdminDomain adminDomain = systemAdminService.getByAccount(account);
        List<String> roles = SplitUtil.split(adminDomain.getRoles(), ',');
        List<EbSystemMenuDomain> menuDomains = roles.contains(YesNoEnum.YES.getText()) ?
                systemMenuService.selectAll() : systemMenuService.listByRoles(roles);

        return TreeUtil.buildTree(menuDomains, EbSystemMenuDomain::getPid, EbSystemMenuDomain::getId, 0,
                (list, dto) -> {
                    WebMenuResponse response = BeanCopierUtil.copy(dto, WebMenuResponse.class);
                    response.setChildList(list);
                    return response;
                }, true);

    }

    @Override
    public PagerInfo<WebAdminResponse> page(WebEmployeeSearchRequest request) {
        PagerInfo<WebAdminResponse> page = PagerUtil.page(request, () -> {
            List<EbSystemAdminDomain> domains = systemAdminService.page(request);
            return domains.stream().map(domain -> {
                WebAdminResponse response = BeanCopierUtil.copy(domain, WebAdminResponse.class);
                response.setStatus(Objects.equals(YesNoEnum.YES.getValue(), domain.getStatus()));
                response.setLastTime(domain.getUpdateTime());
                return response;
            }).collect(Collectors.toList());
        });

        if (!CollectionUtils.isEmpty(page.getList())) {
            List<EbSystemRoleDomain> systemRoleDomains = systemRoleService.selectAll();
            Map<Integer, EbSystemRoleDomain> roleMap = StreamUtil.listToMap(systemRoleDomains, EbSystemRoleDomain::getId);

            page.getList().forEach(e -> {
                List<String> roleNames = new ArrayList<>();
                for (String roleId : e.getRoles().split(",")) {
                    EbSystemRoleDomain roleDomain = roleMap.get(Integer.valueOf(roleId));
                    roleNames.add(roleDomain.getRoleName());
                }
                String roleName = String.join(",", roleNames);
                e.setRoleNames(roleName);
            });
        }

        return  page;
    }

    @Override
    public void create(WebEmployeeRequest request) {
        if (request.getPhone() != null) {
            if (!ReUtil.isMatch(RegularConstants.PHONE_TWO, request.getPhone())) {
                throw new BusinessException(ErrorConstant.PHONE_NUMBER_FORMAT_ERROR);
            }
        }
        EbSystemAdminDomain domain = systemAdminService.getByAccount(request.getAccount());
        if (domain != null) {
            throw new BusinessException(ErrorConstant.USER_ACCOUNT_EXIST);
        }

        String pwd = MallUtil.encrypt(request.getPwd(), request.getAccount());
        domain = BeanCopierUtil.copy(request, EbSystemAdminDomain.class);
        domain.setPwd(pwd);
        systemAdminService.save(domain);
    }

    @Override
    public void delete(Integer id) {
        systemAdminService.delete(id);
    }

    @Override
    public void update(WebEmployeeUpdateRequest request) {
        if (request.getPhone() != null) {
            if (!ReUtil.isMatch(RegularConstants.PHONE_TWO, request.getPhone())) {
                throw new BusinessException(ErrorConstant.PHONE_NUMBER_FORMAT_ERROR);
            }
        }
        EbSystemAdminDomain domain = systemAdminService.getById(request.getId());
        if (domain == null) {
            throw new BusinessException(ErrorConstant.USER_ACCOUNT_NOT_EXIST);
        }
        domain = systemAdminService.getByAccount(request.getAccount());
        if (domain != null && !domain.getId().equals(request.getId())) {
            throw new BusinessException(ErrorConstant.USER_ACCOUNT_EXIST);
        }

        domain = BeanCopierUtil.copy(request, EbSystemAdminDomain.class);
        String pwd = request.getPwd() == null ? null :
                MallUtil.encrypt(request.getPwd(), request.getAccount());
        domain.setPwd(pwd);
        systemAdminService.update(domain);
    }

    @Override
    public WebEmployeeResponse detail(Integer id) {
        EbSystemAdminDomain domain = systemAdminService.getById(id);
        if (domain == null) {
            throw new BusinessException(ErrorConstant.USER_ACCOUNT_NOT_EXIST);
        }
        return BeanCopierUtil.copy(domain, WebEmployeeResponse.class);
    }

    @Override
    public void updateStatus(Integer id, Boolean status) {
        EbSystemAdminDomain domain = systemAdminService.getById(id);
        if (domain == null) {
            throw new BusinessException(ErrorConstant.USER_ACCOUNT_NOT_EXIST);
        }
        if (Objects.equals(status, domain.getStatus())) {
            return;
        }
        domain = new EbSystemAdminDomain();
        domain.setId(id);
        domain.setStatus(status ? YesNoEnum.YES.getValue() : YesNoEnum.NO.getValue());
        systemAdminService.update(domain);
    }

    @Override
    public void updateSms(Integer id) {
        EbSystemAdminDomain domain = systemAdminService.getById(id);
        if (domain == null) {
            throw new BusinessException(ErrorConstant.USER_ACCOUNT_NOT_EXIST);
        }
        if (domain.getPhone() == null) {
            throw new BusinessException(ErrorConstant.NO_PHONE_NUMBER_CONFIGURED);
        }

        EbSystemAdminDomain domain1 = new EbSystemAdminDomain();
        domain1.setId(id);
        domain1.setIsSms(!domain.getIsSms());
        systemAdminService.update(domain1);
    }
}
