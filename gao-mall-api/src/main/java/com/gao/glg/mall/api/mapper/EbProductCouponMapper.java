package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbProductCouponDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductCouponMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductCouponDomain domain);

    /**
     * 查询
     */
    List<EbProductCouponDomain> select();

    /**
     * 更新
     */
    int update(EbProductCouponDomain domain);

    /**
     * 查询
     */
    List<EbProductCouponDomain> listByPid(Integer id);
}
