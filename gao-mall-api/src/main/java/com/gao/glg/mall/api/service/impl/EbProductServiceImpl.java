package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.request.app.AppProductRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductSearchRequest;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbCategoryDomain;
import com.gao.glg.mall.api.domain.EbProductDomain;
import com.gao.glg.mall.api.service.EbCategoryService;
import com.gao.glg.utils.SplitUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductMapper;
import com.gao.glg.mall.api.service.EbProductService;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EbProductServiceImpl implements EbProductService {

    @Resource
    private EbProductMapper productMapper;
    @Autowired
    private EbCategoryService categoryService;

    @Override
    public List<EbProductDomain> listByType(Integer type) {
        return productMapper.listByType(type);
    }

    @Override
    public void browseAdd(Integer id) {
        EbProductDomain domain = new EbProductDomain();
        domain.setId(id);
        domain.setBrowse(1);
        productMapper.update(domain);
    }

    @Override
    public EbProductDomain getById(Integer pid) {
        return productMapper.getById(pid);
    }

    @Override
    public List<EbProductDomain> appSearch(AppProductRequest request) {
        // 防止sql注入
        if (!StringUtils.isEmpty(request.getPriceOrder())) {
            if (!request.getPriceOrder().equals(MallConstant.DESC) &&
                !request.getPriceOrder().equals(MallConstant.ASC)) {
                request.setPriceOrder(null);
            }
        }
        if (!StringUtils.isEmpty(request.getSalesOrder())) {
            if (!request.getSalesOrder().equals(MallConstant.DESC) &&
                !request.getSalesOrder().equals(MallConstant.ASC)) {
                request.setSalesOrder(null);
            }
        }
        if (StringUtils.isEmpty(request.getSalesOrder())) {
            request.setSalesOrder(null);
            if (StringUtils.isEmpty(request.getPriceOrder())) {
                request.setPriceOrder(MallConstant.ASC);
            }
        }
        return productMapper.appSearch(request);
    }

    @Override
    public List<EbProductDomain> webSearch(WebProductSearchRequest request) {
        return productMapper.webSearch(request);
    }

    @Override
    public Integer countByType(int type, Integer stock) {
        return productMapper.countByType(type, stock);
    }

    @Override
    public List<EbProductDomain> listByIds(List<Integer> pids) {
        return productMapper.listByIds(pids);
    }

    @Override
    public List<Integer> queryCategory2(List<Integer> pids) {
        List<EbProductDomain> productDomains = productMapper.listByIds(pids);
        if (CollectionUtils.isEmpty(productDomains)) {
            return Collections.emptyList();
        }

        List<Integer> category2 = new ArrayList<>();
        for (EbProductDomain productDomain : productDomains) {
            String cateId = productDomain.getCateId();
            for (String s : SplitUtil.split(cateId, ',')) {
                category2.add(Integer.parseInt(s));
            }
        };

        category2 = category2.stream().distinct().collect(Collectors.toList());
        List<EbCategoryDomain> categoryDomains = categoryService.listByCids(category2);

        category2 = new ArrayList<>();
        for (EbCategoryDomain categoryDomain : categoryDomains) {
            List<String> split = SplitUtil.split(categoryDomain.getPath(), '/');
            if (split.size() > 2) {
                String s = split.get(2);
                int cate2 = Integer.parseInt(s);
                if (cate2 > 0) {
                    category2.add(cate2);
                }
            }
        }
        return category2;
    }
}
