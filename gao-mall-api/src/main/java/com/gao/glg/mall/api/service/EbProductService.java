package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.app.AppProductRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductSearchRequest;
import com.gao.glg.mall.api.domain.EbProductDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbProductService {

    /**
     *
     * <p/>
     *
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    List<EbProductDomain> listByType(Integer type);

    /**
     * 浏览 +1
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void browseAdd(Integer id);

    /**
     * 根据id获取商品
     * <p/>
     *
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    EbProductDomain getById(Integer pid);

    /**
     * 搜索
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductDomain> appSearch(AppProductRequest request);

    /**
     * 搜索
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductDomain> webSearch(WebProductSearchRequest request);

    /**
     * 根据id集合获取商品
     * <p/>
     *
     * @param pids
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductDomain> listByIds(List<Integer> pids);

    /**
     * 查询二级分类
     * <p/>
     *
     * @param pids
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<Integer> queryCategory2(List<Integer> pids);


    /**
     * 根据类型查询数量
     * <p/>
     *
     * @param type
     * @param stock
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    Integer countByType(int type, Integer stock);
}
