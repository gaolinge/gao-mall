package com.gao.glg.mall.api.controller;

import com.gao.glg.bean.RestResult;
import com.gao.glg.jasypt.AESJasyptEncryptor;
import com.gao.glg.sentinel.annotation.NoGather;
import com.gao.glg.utils.StringLengthCompressUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/7/3
 */
@Api(tags = {"工具"})
@NoGather
@RestController
@RequestMapping("/tool")
public class ToolController {
    private static final String SIT_SECRET = "N1cnI4GNDuBL5gju";
    private static final String PRO_SECRET = "IjlKn3sWK8qFltXN";

    @PostMapping(value = "/aes/decrypt/{env}")
    @ApiOperation(value = "aes 解密", httpMethod = "POST")
    public RestResult<String> aesDecrypt(@PathVariable String env, @RequestBody String txt) {
        String password = Objects.equals(env, "prod") ? PRO_SECRET : SIT_SECRET;
        AESJasyptEncryptor encryptor = new AESJasyptEncryptor(password);
        return RestResult.success(encryptor.decrypt(txt));
    }

    @PostMapping(value = "/aes/encrypt/{env}")
    @ApiOperation(value = "aes 加密", httpMethod = "POST")
    public RestResult<String> aesEncrypt(@PathVariable String env, @RequestBody String txt) {
        String password = Objects.equals(env, "prod") ? PRO_SECRET : SIT_SECRET;
        AESJasyptEncryptor encryptor = new AESJasyptEncryptor(password);
        return RestResult.success(encryptor.encrypt(txt));
    }

    @PostMapping(value = "/decompression")
    @ApiOperation(value = "解压", httpMethod = "POST")
    public RestResult<String> decompression(@RequestBody String txt) {
        return RestResult.success(StringLengthCompressUtil.combinationRestore(txt));
    }



}