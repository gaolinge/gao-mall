package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbProductCouponDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbProductCouponService {

    /**
     * 根据商品id查询商品优惠券
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/19
     * @version : 1.0.0
     */
    List<EbProductCouponDomain> listByPid(Integer id);
}
