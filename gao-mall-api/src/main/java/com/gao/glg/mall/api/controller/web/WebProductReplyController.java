package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebProductReplyCommentRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductReplyRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductReplySearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebProductReplyResponse;
import com.gao.glg.mall.api.business.IProductReplyBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 评论
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/27
 */
@Slf4j
@RestController
@RequestMapping("/admin/product/reply")
@Api(tags = "商品评论")
public class WebProductReplyController {

    @Autowired
    private IProductReplyBusiness productReplyBusiness;

    //@PreAuthorize("hasAuthority('admin:product:reply:list')")
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebProductReplyResponse>> page(@Validated WebProductReplySearchRequest request) {
        return RestResult.success(productReplyBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:product:reply:save')")
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult<String> create(@RequestBody @Validated WebProductReplyRequest request) {
        productReplyBusiness.create(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:reply:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public RestResult<String> delete(@PathVariable Integer id) {
        productReplyBusiness.delete(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:reply:info')")
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public RestResult<WebProductReplyResponse> detail(@PathVariable Integer id) {
        return RestResult.success(productReplyBusiness.detail(id));
    }

    //@PreAuthorize("hasAuthority('admin:product:reply:comment')")
    @ApiOperation(value = "回复")
    @RequestMapping(value = "/comment", method = RequestMethod.POST)
    public RestResult<String> comment(@RequestBody WebProductReplyCommentRequest request) {
        productReplyBusiness.comment(request);
        return RestResult.success();
    }
}



