package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.web.WebRoleSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemRoleDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemRoleMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemRoleDomain domain);

    /**
     * 查询
     */
    List<EbSystemRoleDomain> select();

    /**
     * 更新
     */
    int update(EbSystemRoleDomain domain);

    /**
     * 查询
     */
    List<EbSystemRoleDomain> page(@Param("request") WebRoleSearchRequest request);

    /**
     * 查询
     */
    EbSystemRoleDomain getByRoleName(String roleName);

    /**
     * 查询
     */
    EbSystemRoleDomain getById(Integer id);
}
