package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebEmployeeRequest;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeSearchRequest;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeUpdateRequest;
import com.gao.glg.mall.api.bean.response.web.WebAdminResponse;
import com.gao.glg.mall.api.bean.response.web.WebEmployeeResponse;
import com.gao.glg.mall.api.bean.response.web.WebMenuResponse;
import com.gao.glg.mall.api.bean.response.web.WebUserInfoResponse;
import com.gao.glg.page.PagerInfo;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/19
 */
public interface IEmployeeBusiness {


    /**
     * 用户信息
     * <p/>
     *
     * @param
     * @param token
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    WebUserInfoResponse info(String token);

    /**
     * 菜单列表
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    List<WebMenuResponse> menus();

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    PagerInfo<WebAdminResponse> page(WebEmployeeSearchRequest request);

    /**
     * 新建
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    void create(WebEmployeeRequest request);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 更新
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    void update(WebEmployeeUpdateRequest request);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    WebEmployeeResponse detail(Integer id);

    /**
     * 更新状态
     * <p/>
     *
     * @param id
     * @param status
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    void updateStatus(Integer id, Boolean status);

    /**
     * 更新短信
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    void updateSms(Integer id);
}
