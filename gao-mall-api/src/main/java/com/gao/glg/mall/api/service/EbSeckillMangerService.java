package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSeckillMangerDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSeckillMangerService {

    /**
     * 查询当前时段秒杀
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    List<EbSeckillMangerDomain> listByCurrent();

    /**
     * 查询
     * <p/>
     *
     * @param num
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbSeckillMangerDomain> select(int num);

    /**
     * 根据timeId查询
     * <p/>
     *
     * @param timeId
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    EbSeckillMangerDomain getById(Integer timeId);
}
