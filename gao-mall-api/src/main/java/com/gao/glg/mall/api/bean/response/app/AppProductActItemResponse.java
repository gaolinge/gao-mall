package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 商品所参与的活动类型
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/23
 */
@Data
public class AppProductActItemResponse implements Serializable {

    @ApiModelProperty(value = "参与活动id")
    private Integer id;

    @ApiModelProperty(value = "活动结束时间")
    private Integer time;

    @ApiModelProperty(value = "活动参与类型:1=秒杀，2=砍价，3=拼团")
    private String type;
}
