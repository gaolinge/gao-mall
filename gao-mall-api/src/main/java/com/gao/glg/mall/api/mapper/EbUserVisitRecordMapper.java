package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserVisitRecordDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserVisitRecordMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserVisitRecordDomain domain);

    /**
     * 查询
     */
    List<EbUserVisitRecordDomain> select();

    /**
     * 更新
     */
    int update(EbUserVisitRecordDomain domain);
}
