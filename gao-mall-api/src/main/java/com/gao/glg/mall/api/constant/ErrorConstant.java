package com.gao.glg.mall.api.constant;

/**
 * 错误码常量类
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
public interface ErrorConstant {

    /**
     * 登录信息已过期，请重新登录！
     **/
    long USER_LOGIN_EXPIRED = 100001L;
    /**
     * 账号已存在
     **/
    long USER_ACCOUNT_EXIST = 100001L;
    /**
     * 账号不存在
     **/
    long USER_ACCOUNT_NOT_EXIST = 100001L;
    /**
     * 角色名已存在
     **/
    long ROLE_NAME_ALREADY_EXIST = 100001L;
    /**
     * 角色不存在
     **/
    long ROLE_NOT_EXIST = 100001L;
    /**
     * 账号密码错误
     **/
    long ACCOUNT_PASSWORD_INCORRECT = 100002L;
    /**
     * 未找到商品
     **/
    long NOT_FOUND_PRODUCT = 100003L;
    /**
     * 未找到商品SKU
     **/
    long NOT_FOUND_PRODUCT_SKU = 100004L;
    /**
     * 商品数量不合法
     **/
    long PRODUCT_NUM_NOT_LEGITIMATE = 100005L;
    /**
     * 商品数量不能小于1大于99
     **/
    long PRODUCT_NUM_1_99 = 100006L;
    /**
     * 请配置公众号分享信息！
     **/
    long PLEASE_CONFIG_OFFICIAL_ACCOUNT = 100007L;
    /**
     * 生成二维码参数不合法！
     **/
    long QR_CODE_PARAM_NOT_LEGITIMATE = 100008L;
    /**
     * 优惠劵数量不足
     **/
    long COUPON_NUM_INSUFFICIENT = 100009L;
    /**
     * 已经领取过该优惠劵
     **/
    long ALREADY_RECEIVED_COUPON = 100011L;
    /**
     * 请选择正确城市数据
     **/
    long NO_CITY_DATA = 100012L;
    /**
     * 当前城市未找到！
     **/
    long NOT_FOUND_CITY = 100013L;
    /**
     * 未配置签到
     **/
    long NOT_CONFIG_SIGN = 100014L;
    /**
     * 预下单订单不存在
     **/
    long PRE_ORDER_NOT_EXIST = 100015L;
    /**
     * 营销活动商品无法使用优惠券
     **/
    long MARKETING_ACTIVITIES_NO_USE_COUPON = 100016L;
    /**
     * 视频号商品无法使用优惠券
     **/
    long VIDEO_NUMBER_NO_USE_COUPON = 100017L;
    /**
     * 优惠券领取记录不存在！
     **/
    long COUPON_USER_RECORD_NOT_EXIST = 100018L;
    /**
     * 此优惠券已使用！
     **/
    long COUPON_USED_ALREADY = 100019L;
    /**
     * 此优惠券已失效！
     **/
    long COUPON_INVALID_ALREADY = 100020L;
    /**
     * 此优惠券还未到达使用时间范围之内！
     **/
    long COUPON_NOT_ARRIVED_YET_TIME = 100021L;
    /**
     * 总金额小于优惠券最小使用金额
     **/
    long COUPON_NOT_ARRIVED_YET_AMOUNT = 100022L;
    /**
     * 此优惠券为商品券，请购买相关商品之后再使用！
     **/
    long PRODUCT_NOT_SATISFY_USE_CONDITION = 100022L;
    /**
     * 此优惠券为分类券，请购买相关分类下的商品之后再使用！
     **/
    long CATEGORY_NOT_SATISFY_USE_CONDITION = 100022L;
    /**
     * 手机号格式不正确
     **/
    long PHONE_NUMBER_FORMAT_ERROR = 100022L;
    /**
     * 未配置手机号
     **/
    long NO_PHONE_NUMBER_CONFIGURED = 100022L;


    /**
     * 文章不存在
     **/
    long ARTICLE_NOT_EXIST = 101001L;
    /**
     * 验证码错误
     **/
    long VERIFICATION_CODE_ERROR = 101002L;
    /**
     * 此分类已存在
     **/
    long CATEGORY_ALREADY_EXISTS = 101003L;
    /**
     * 存在子类
     **/
    long EXISTS_SUB_CATEGORY = 101004L;
    /**
     * 模板表单 【" + formTemp.getName() + "】 的内容不是正确的JSON格式！
     **/
    long FORM_TEMPLATE_FORMAT_ERROR = 101005L;
    /**
     * 区域运费最少需要一条默认的全国区域
     **/
    long AT_LEAST_ONE_DEFAULT_NATIONAL_REGION = 101006L;
    /**
     * 模板名称已存在,请更换模板名称!
     **/
    long TEMPLATE_NAME_ALREADY_EXISTS = 101007L;
    /**
     * 优惠劵不存在
     **/
    long COUPON_DOES_NOT_EXIST = 101008L;
    /**
     * 快递公司不存在
     **/
    long EXPRESS_DOES_NOT_EXIST = 101009L;
    /**
     * 商品规则名称已存在,请更换规则名称!
     **/
    long PRODUCT_RULE_NAME_ALREADY_EXISTS = 101010L;
    /**
     * 订单不存在
     **/
    long ORDER_DOES_NOT_EXIST = 101010L;
    /**
     * 订单已经评论
     **/
    long ORDER_ALREADY_COMMENT = 101011L;
}
