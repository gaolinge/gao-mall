package com.gao.glg.mall.api.mapper;

import com.gao.glg.mall.api.domain.EbCouponUserDomain;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author gaolinge
 */
public interface EbCouponUserMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbCouponUserDomain domain);

    /**
     * 查询
     */
    List<EbCouponUserDomain> select();

    /**
     * 更新
     */
    int update(EbCouponUserDomain domain);

    /**
     * 查询
     */
    List<EbCouponUserDomain> listByUid(Integer userId);

    /**
     * 查询
     */
    List<EbCouponUserDomain> listByUidAndType(@Param("uid") Integer uid, @Param("type") String type);

    /**
     * 查询
     */
    List<EbCouponUserDomain> listByUidAndCouponId(@Param("uid") Integer uid, @Param("couponId") Integer couponId);

    /**
     * 查询
     */
    List<EbCouponUserDomain> listByCanUse(@Param("uid") Integer uid,
                                          @Param("pids") List<Integer> pids,
                                          @Param("cids") List<Integer> cids,
                                          @Param("maxPrice") BigDecimal maxPrice);

    /**
     * 查询
     */
    EbCouponUserDomain getById(Integer id);
}
