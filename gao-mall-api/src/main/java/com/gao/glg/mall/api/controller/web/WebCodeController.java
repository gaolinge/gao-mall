package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebGenerateRequest;
import com.gao.glg.mall.api.business.ICodeBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin/code")
@Api(tags = "代码服务")
public class WebCodeController {

    @Autowired
    private ICodeBusiness codeBusiness;

    @ApiOperation(value="代码生成-新列表")
    @GetMapping("/list")
    public RestResult<PagerInfo<Map<String, Object>>> page(WebGenerateRequest request){
        return RestResult.success(codeBusiness.page(request));
    }

    @GetMapping("/generator")
    public void generator(@RequestParam String tables, HttpServletResponse response) throws IOException {
        byte[] data = codeBusiness.generator(tables.split(","));

//        String contentLength = "Content-Length";
//        String contentType = "application/octet-stream; charset=UTF-8;";
//        String contentDisposition = "Content-Disposition";
//        String attachment = "attachment; filename=\"CRMEB-Java-Code-"+ DateUtil.dateToStr(new Date(), Constants.DATE_FORMAT_HHMM) +".zip\"";
//
//        response.reset();
//        response.addHeader(contentLength, data.length +"");
//        response.setContentType(contentType);
//        response.setHeader(contentDisposition, attachment);
//        IOUtils.write(data, response.getOutputStream());
    }



}



