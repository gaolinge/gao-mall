package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebLoginRequest;
import com.gao.glg.mall.api.bean.response.web.WebLoginResponse;
import com.gao.glg.mall.api.business.ILoginBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户登陆
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin")
@Api(tags = "登录注册")
public class WebLoginController {

    @Autowired
    private ILoginBusiness loginBusiness;

    @ApiOperation(value="PC登录")
    @PostMapping(value = "/login", produces = "application/json")
    public RestResult<WebLoginResponse> webLogin(@RequestBody @Validated WebLoginRequest request) {
        return RestResult.success(loginBusiness.webLogin(request));
    }

    //@PreAuthorize("hasAuthority('admin:logout')")
    @ApiOperation(value="PC登出")
    @GetMapping(value = "/logout")
    public RestResult logout(HttpServletRequest request) {
        loginBusiness.logout(2);
        return RestResult.success();
    }

    /*@ApiOperation(value = "手机号登录接口")
    @RequestMapping(value = "/login/mobile", method = RequestMethod.POST)
    public RestResult<LoginResponse> phoneLogin(@RequestBody @Validated LoginMobileRequest loginRequest) {
        return RestResult.success(loginService.phoneLogin(loginRequest));
    }






    @ApiOperation(value = "发送短信登录验证码")
    @RequestMapping(value = "/sendCode", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="phone", value="手机号码", required = true)
    })
    public RestResult<Object> sendCode(@RequestParam String phone){
        if(smsService.sendCommonCode(phone)){
            return RestResult.success("发送成功");
        }else{
            return RestResult.failed("发送失败");
        }
    }*/
}



