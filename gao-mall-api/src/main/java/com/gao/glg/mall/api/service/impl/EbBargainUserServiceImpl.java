package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.mapper.EbBargainUserMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.service.EbBargainUserService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbBargainUserServiceImpl implements EbBargainUserService {

    @Resource
    private EbBargainUserMapper storeBargainUserMapper;
}
