package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSystemStoreDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemStoreService {

    /**
     * 根据id查询
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    EbSystemStoreDomain getById(Integer id);

    /**
     * 根据id查询
     * <p/>
     *
     * @param ids
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    List<EbSystemStoreDomain> listByIds(List<Integer> ids);
}
