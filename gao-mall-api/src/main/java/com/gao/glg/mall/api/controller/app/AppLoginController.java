package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.app.AppLoginRequest;
import com.gao.glg.mall.api.bean.response.app.AppLoginResponse;
import com.gao.glg.mall.api.business.ILoginBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户登陆
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app")
@Api(tags = "登录注册")
public class AppLoginController {

    @Autowired
    private ILoginBusiness loginBusiness;

    @ApiOperation(value = "账号密码登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public RestResult<AppLoginResponse> login(@RequestBody @Validated AppLoginRequest loginRequest) {
        return RestResult.success(loginBusiness.appLogin(loginRequest));
    }

    @ApiOperation(value = "退出")
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public RestResult<String> loginOut(HttpServletRequest request){
        loginBusiness.logout(1);
        return RestResult.success();
    }

    /*@ApiOperation(value = "手机号登录接口")
    @RequestMapping(value = "/login/mobile", method = RequestMethod.POST)
    public RestResult<LoginResponse> phoneLogin(@RequestBody @Validated LoginMobileRequest loginRequest) {
        return RestResult.success(loginService.phoneLogin(loginRequest));
    }



    @ApiOperation(value = "发送短信登录验证码")
    @RequestMapping(value = "/sendCode", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name="phone", value="手机号码", required = true)
    })
    public RestResult<Object> sendCode(@RequestParam String phone){
        if(smsService.sendCommonCode(phone)){
            return RestResult.success("发送成功");
        }else{
            return RestResult.failed("发送失败");
        }
    }*/
}



