package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.web.WebValidateCodeResponse;

/**
 * 验证码
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/12
 */
public interface IValidateBusiness {

    /**
     * 获取
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/12
     * @version : 1.0.0
     */
    WebValidateCodeResponse get();
}
