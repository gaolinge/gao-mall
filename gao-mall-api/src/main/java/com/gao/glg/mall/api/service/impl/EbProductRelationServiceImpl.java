package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.response.app.AppUserCollectResponse;
import com.gao.glg.mall.api.domain.EbProductRelationDomain;
import com.gao.glg.mall.api.service.EbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductRelationMapper;
import com.gao.glg.mall.api.service.EbProductRelationService;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;

@Service
public class EbProductRelationServiceImpl implements EbProductRelationService {
    @Autowired
    private EbUserService userService;
    @Resource
    private EbProductRelationMapper productRelationMapper;

    @Override
    public boolean isLike(Integer id, boolean b) {
        return !CollectionUtils.isEmpty(likeOrCollect(userService.getUserId(), id, b));
    }

    @Override
    public Integer collectNum(Integer uid) {
        return productRelationMapper.collectNum(uid);
    }

    @Override
    public List<EbProductRelationDomain> likeOrCollect(Integer uid, Integer id, boolean b) {
        String type = b ? "like":"collect";
        return productRelationMapper.listByUidAndPidAndType(uid, id, type);
    }

    @Override
    public void save(EbProductRelationDomain domain) {
        productRelationMapper.save(domain);
    }

    @Override
    public List<AppUserCollectResponse> list(Integer uid) {
        return productRelationMapper.list(uid);
    }

    @Override
    public void delete(Integer uid, Integer pid) {
        productRelationMapper.delete(uid, pid);
    }

    @Override
    public void batchDelete(Integer uid, List<String> ids, int type) {
        productRelationMapper.batchDelete(uid, ids, type);
    }

    @Override
    public void batchSave(List<EbProductRelationDomain> domains) {
        productRelationMapper.batchSave(domains);
    }

    @Override
    public List<EbProductRelationDomain> listByPidsAndType(List<Integer> pids, String type) {
        return productRelationMapper.listByPidsAndType(pids, type);
    }
}
