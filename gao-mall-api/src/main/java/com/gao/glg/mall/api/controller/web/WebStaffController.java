package com.gao.glg.mall.api.controller.web;


import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.web.WebStaffResponse;
import com.gao.glg.mall.api.business.IStaffBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户中心
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin/system/store/staff")
@Api(tags = "用户 -- 用户中心")
public class WebStaffController {

    @Autowired
    private IStaffBusiness staffBusiness;


    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebStaffResponse>> getList(Integer storeId, @ModelAttribute PagerDTO request) {
        return RestResult.success(staffBusiness.list(storeId, request));
    }

 /*
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@RequestBody @ModelAttribute SystemStoreStaffRequest systemStoreStaffRequest) {
        if (systemStoreStaffService.saveUnique(systemStoreStaffRequest)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }

    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public CommonResult<String> delete(@RequestParam(value = "id") Integer id) {
        if (systemStoreStaffService.removeById(id)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }


    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestParam Integer id, @RequestBody @ModelAttribute SystemStoreStaffRequest systemStoreStaffRequest) {
        if (systemStoreStaffService.edit(id, systemStoreStaffRequest)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }

    @ApiOperation(value = "修改状态")
    @RequestMapping(value = "/update/status", method = RequestMethod.GET)
    public CommonResult<String> updateStatus(@RequestParam Integer id, @RequestParam Integer status) {
        if (systemStoreStaffService.updateStatus(id, status)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }

    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public CommonResult<SystemStoreStaff> info(@RequestParam(value = "id") Integer id) {
        return CommonResult.success(systemStoreStaffService.getById(id));
    }*/
}



