package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.bean.request.app.AppBargainRequest;
import com.gao.glg.mall.api.bean.response.app.*;
import com.gao.glg.mall.api.business.IBargainBusiness;
import com.gao.glg.mall.api.domain.EbBargainDomain;
import com.gao.glg.mall.api.service.EbBargainHelpService;
import com.gao.glg.mall.api.service.EbBargainService;
import com.gao.glg.mall.api.service.EbBargainUserService;
import com.gao.glg.mall.api.service.EbUserService;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class BargainBusiness implements IBargainBusiness {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbBargainService bargainService;
    @Autowired
    private EbBargainHelpService bargainHelpService;
    @Autowired
    private EbBargainUserService bargainUserService;

    @Override
    public List<AppBargainResponse> index() {
        List<EbBargainDomain> storeBargainDomains = bargainService.listByInStock();
        return BeanCopierUtil.copyList(storeBargainDomains, AppBargainResponse.class);
    }

    @Override
    public AppBargainHeaderResponse header() {
        /*AppBargainHeaderResponse headerResponse = new AppBargainHeaderResponse();
        // 获取参与砍价总人数
        Integer bargainTotal = bargainHelpService.getCount();
        headerResponse.setBargainTotal(bargainTotal);
        if (bargainTotal <= 0) {
            return headerResponse;
        }
        // 获取砍价成功列表
        List<EbBargainUserDomain> bargainUserDomains = bargainUserService.getHeaderList();
        if (CollectionUtils.isEmpty(bargainUserDomains)) {
            headerResponse.setBargainSuccessList(CollUtil.newArrayList());
            return headerResponse;
        }


        List<Integer> uidList = bargainUserDomains.stream().map(EbBargainUserDomain::getUid).distinct().collect(Collectors.toList());
        HashMap<Integer, User> userMap = userService.getMapListInUid(uidList);
        List<Integer> bargainIdList = bargainUserList.stream().map(StoreBargainUser::getBargainId).distinct().collect(Collectors.toList());
        HashMap<Integer, String> bargainMap = getStoreNameMapInId(bargainIdList);
        List<HashMap<String, Object>> mapList = bargainUserList.stream().map(e -> {
            // 获取用户昵称头像
            User user = userMap.get(e.getUid());
            HashMap<String, Object> map = CollUtil.newHashMap();
            map.put("nickName", user.getNickname());
            map.put("avatar", user.getAvatar());
            map.put("price", e.getBargainPriceMin());
            map.put("title", bargainMap.get(e.getBargainId()));
            return map;
        }).collect(Collectors.toList());
        headerResponse.setBargainSuccessList(mapList);
        return headerResponse;*/
        return null;
    }

    @Override
    public PagerInfo<AppBargainInfoResponse> list(PagerDTO request) {
        return PagerUtil.page(request, () -> {
            List<EbBargainDomain> list = bargainService.listByInStock();
            return BeanCopierUtil.copyList(list, AppBargainInfoResponse.class);
        });
    }

    @Override
    public Map<String, Object> start(AppBargainRequest request) {
        return Collections.emptyMap();
    }

    @Override
    public AppBargainUserResponse info(AppBargainRequest request) {
        return null;
    }

    @Override
    public AppBargainDetailResponse detail(Integer id) {
        return null;
    }

    @Override
    public Map<String, Object> help(AppBargainRequest request) {
        return Collections.emptyMap();
    }

    @Override
    public PagerInfo<AppBargainRecordResponse> records(PagerDTO request) {
        return null;
    }
}
