package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 首页信息
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/23
 */
@Data
public class AppIndexInfoResponse implements Serializable {

    @ApiModelProperty(value = "首页banner滚动图")
    private List<Map<String, Object>> banner;

    @ApiModelProperty(value = "导航模块")
    private List<Map<String, Object>> menus;

    @ApiModelProperty(value = "新闻简报消息滚动")
    private List<Map<String, Object>> roll;

    @ApiModelProperty(value = "企业logo")
    private String logoUrl;

    @ApiModelProperty(value = "是否关注公众号")
    private boolean subscribe;

    @ApiModelProperty(value = "首页超值爆款")
    private List<Map<String, Object>> explosiveMoney;

    @ApiModelProperty(value = "首页精品推荐图片")
    private List<Map<String, Object>> bastBanner;

    @ApiModelProperty(value = "云智服H5 url")
    private String yzfUrl;

    @ApiModelProperty(value = "商品分类页配置")
    private String categoryPageConfig;

    @ApiModelProperty(value = "是否隐藏一级分类")
    private String isShowCategory;

    @ApiModelProperty(value = "客服电话")
    private String consumerHotline;

    @ApiModelProperty(value = "客服电话服务开关")
    private String telephoneServiceSwitch;

    @ApiModelProperty(value = "首页商品列表模板配置")
    private String homePageSaleListStyle;
}
