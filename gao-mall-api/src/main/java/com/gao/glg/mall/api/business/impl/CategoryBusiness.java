package com.gao.glg.mall.api.business.impl;

import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.response.CategoryTreeResponse;
import com.gao.glg.mall.api.bean.response.web.WebCateSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebCategoryRequest;
import com.gao.glg.mall.api.business.ICategoryBusiness;
import com.gao.glg.mall.api.business.ICdnBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbCategoryDomain;
import com.gao.glg.mall.api.service.EbCategoryService;
import com.gao.glg.mall.api.service.EbSystemAttachmentService;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.SplitUtil;
import com.gao.glg.utils.StreamUtil;
import com.gao.glg.utils.TreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class CategoryBusiness implements ICategoryBusiness {

    @Autowired
    private ICdnBusiness cdnBusiness;
    @Autowired
    private EbCategoryService categoryService;
    @Autowired
    private EbSystemAttachmentService systemAttachmentService;

    @Override
    public List<CategoryTreeResponse> tree() {
        List<EbCategoryDomain> categoryDomains = categoryService.tree(
                MallConstant.CATEGORY_TYPE_PRODUCT, 1, null, null);

        return TreeUtil.buildTree(categoryDomains, EbCategoryDomain::getPid, EbCategoryDomain::getId, 0,
                (list, dto) -> {
                    CategoryTreeResponse response = BeanCopierUtil.copy(dto, CategoryTreeResponse.class);
                    response.setChild(list);
                    return response;
        });
    }

    @Override
    public List<CategoryTreeResponse> tree(Integer type, String name, Integer status) {
        List<EbCategoryDomain> categoryDomains = categoryService.tree(
                type, status, name, null);

        return TreeUtil.buildTree(categoryDomains, EbCategoryDomain::getPid, EbCategoryDomain::getId, 0,
                (list, dto) -> {
                    CategoryTreeResponse response = BeanCopierUtil.copy(dto, CategoryTreeResponse.class);
                    response.setChild(list);
                    return response;
                });
    }

    @Override
    public PagerInfo<EbCategoryDomain> list(WebCateSearchRequest request) {
        return PagerUtil.page(request, () -> categoryService.search(request));
    }

    @Override
    public void save(WebCategoryRequest request) {
        if(categoryService.queryNum(request.getName(), request.getType()) > 0){
            throw new BusinessException(ErrorConstant.CATEGORY_ALREADY_EXISTS);
        }
        EbCategoryDomain categoryDomain = categoryService.getById(request.getPid());
        String path = categoryDomain == null ? null :
                categoryDomain.getPath() + request.getPid() + "/";
        String extra = cdnBusiness.clearPrefix(request.getExtra());
        EbCategoryDomain domain = BeanCopierUtil.copy(request, EbCategoryDomain.class);
        domain.setPath(path);
        domain.setExtra(extra);
        categoryService.save(domain);
    }

    @Override
    public void delete(Integer id) {
        if (categoryService.queryChildNum(id) > 0) {
            throw new BusinessException(ErrorConstant.EXISTS_SUB_CATEGORY);
        }
        categoryService.delete(id);
    }

    @Override
    public void update(Integer id, WebCategoryRequest request) {
        // TODO
    }

    @Override
    public List<EbCategoryDomain> lisByIds(String ids) {
        List<String> cid = SplitUtil.split(ids, ',');
        List<Integer> cids = StreamUtil.map(cid, Integer::parseInt);
        return categoryService.listByCids(cids);
    }

    @Override
    public void updateStatus(Integer id) {
        categoryService.updateStatus(id);
    }

    @Override
    public EbCategoryDomain info(Integer id) {
        return categoryService.getById(id);
    }
}
