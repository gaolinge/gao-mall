package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.business.IRechargeBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 充值
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/recharge")
@Api(tags = "用户 -- 充值")
public class AppRechargeController {

    @Autowired
    private IRechargeBusiness rechargeBusiness;


    /*@ApiOperation(value = "充值额度选择")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public RestResult<UserRechargeFrontResponse> getRechargeConfig() {
        return RestResult.success(userCenterService.getRechargeConfig());
    }


    @ApiOperation(value = "小程序充值")
    @RequestMapping(value = "/routine", method = RequestMethod.POST)
    public RestResult<Map<String, Object>> routineRecharge(HttpServletRequest httpServletRequest, @RequestBody @Validated UserRechargeRequest request) {
        request.setFromType(Constants.PAY_TYPE_WE_CHAT_FROM_PROGRAM);
        request.setClientIp(CrmebUtil.getClientIp(httpServletRequest));
        OrderPayResultResponse recharge = userCenterService.recharge(request);
        Map<String, Object> map = new HashMap<>();
        map.put("data", recharge);
        map.put("type", request.getFromType());
        return RestResult.success(map);
    }


    @ApiOperation(value = "公众号充值")
    @RequestMapping(value = "/wechat", method = RequestMethod.POST)
    public RestResult<OrderPayResultResponse> weChatRecharge(HttpServletRequest httpServletRequest, @RequestBody @Validated UserRechargeRequest request) {
        request.setClientIp(CrmebUtil.getClientIp(httpServletRequest));
        return RestResult.success(userCenterService.recharge(request));
    }


    @ApiOperation(value = "佣金转入余额")
    @RequestMapping(value = "/transferIn", method = RequestMethod.POST)
    public RestResult<Boolean> transferIn(@RequestParam(name = "price") BigDecimal price) {
        return RestResult.success(userCenterService.transferIn(price));
    }

    @ApiOperation(value = "用户账单记录")
    @RequestMapping(value = "/bill/record", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "记录类型：all-全部，expenditure-支出，income-收入", required = true)
    public RestResult<PagerInfo<UserRechargeBillRecordResponse>> billRecord(@RequestParam(name = "type") String type, @ModelAttribute PagerDTO pageRequest) {
        return RestResult.success(userCenterService.nowMoneyBillRecord(type, pageRequest));
    }*/
}



