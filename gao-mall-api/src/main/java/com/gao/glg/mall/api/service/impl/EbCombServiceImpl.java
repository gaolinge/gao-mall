package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbCombinationDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbCombinationMapper;
import com.gao.glg.mall.api.service.EbCombService;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbCombServiceImpl implements EbCombService {
    @Resource
    private EbCombinationMapper combinationMapper;

    @Override
    public List<EbCombinationDomain> listByStock(int limit) {
        return combinationMapper.listByStock(limit);
    }

    @Override
    public List<EbCombinationDomain> listByProductId(Integer productId) {
        return combinationMapper.listByProductId(productId);
    }
}
