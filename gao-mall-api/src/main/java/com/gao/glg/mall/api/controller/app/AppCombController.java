package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.app.AppCombinationIndexResponse;
import com.gao.glg.mall.api.business.ICombBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 拼团商品
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/combination")
@Api(tags = "拼团商品")
public class AppCombController {

    @Autowired
    private ICombBusiness combBusiness;

    @ApiOperation(value = "拼团首页数据")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public RestResult<AppCombinationIndexResponse> index() {
        return RestResult.success(combBusiness.index());
    }

    /*@ApiOperation(value = "拼团商品列表header")
    @RequestMapping(value = "/header", method = RequestMethod.GET)
    public RestResult<CombinationHeaderResponse> header() {
        return RestResult.success(storeCombinationService.getHeader());
    }

    @ApiOperation(value = "拼团商品列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<StoreCombinationH5Response>> list(@ModelAttribute PagerDTO PagerDTO) {
        return RestResult.success(PagerInfo.restPage(storeCombinationService.getH5List(PagerDTO)));
    }

    @ApiOperation(value = "拼团商品详情")
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public RestResult<CombinationDetailResponse> detail(@PathVariable(value = "id") Integer id) {
        CombinationDetailResponse h5Detail = storeCombinationService.getH5Detail(id);
        return RestResult.success(h5Detail);
    }

    @ApiOperation(value = "去拼团")
    @RequestMapping(value = "/pink/{pinkId}", method = RequestMethod.GET)
    public RestResult<GoPinkResponse> goPink(@PathVariable(value = "pinkId") Integer pinkId) {
        GoPinkResponse goPinkResponse = storeCombinationService.goPink(pinkId);
        return RestResult.success(goPinkResponse);
    }

    @ApiOperation(value = "更多拼团")
    @RequestMapping(value = "/more", method = RequestMethod.GET)
    public RestResult<PageInfo<StoreCombination>> getMore(@RequestParam Integer comId, @Validated PagerDTO PagerDTO) {
        PageInfo<StoreCombination> more = storeCombinationService.getMore(PagerDTO, comId);
        return RestResult.success(more);
    }

    @ApiOperation(value = "取消拼团")
    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public RestResult<Object> remove(@RequestBody @Validated StorePinkRequest storePinkRequest) {
        if (storeCombinationService.removePink(storePinkRequest)) {
            return RestResult.success("取消成功");
        } else {
            return RestResult.failed("取消失败");
        }
    }*/

}
