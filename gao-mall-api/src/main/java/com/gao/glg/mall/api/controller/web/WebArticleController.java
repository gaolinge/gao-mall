package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebArticleRequest;
import com.gao.glg.mall.api.bean.request.web.WebArticleSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebArticleResponse;
import com.gao.glg.mall.api.business.IArticleBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("/admin/article")
@Api(tags = "文章管理")
public class WebArticleController {

    @Autowired
    private IArticleBusiness articleBusiness;

    //@PreAuthorize("hasAuthority('admin:article:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiImplicitParam(name="keywords", value="搜索关键字")
    public RestResult<PagerInfo<WebArticleResponse>> page(@Validated WebArticleSearchRequest request) {
        return RestResult.success(articleBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:article:save')")
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult<String> create(@RequestBody @Validated WebArticleRequest request) {
        articleBusiness.create(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:article:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ApiImplicitParam(name="id", value="文章ID")
    public RestResult<String> delete(@RequestParam(value = "id") Integer id) {
        articleBusiness.delete(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:article:update')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiImplicitParam(name="id", value="文章ID")
    public RestResult<String> update(@RequestParam Integer id, @RequestBody @Validated WebArticleRequest request) {
        articleBusiness.update(id, request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:article:info')")
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ApiImplicitParam(name="id", value="文章ID")
    public RestResult<WebArticleResponse> detail(@RequestParam(value = "id") Integer id) {
        return RestResult.success(articleBusiness.detail(id));
   }
}



