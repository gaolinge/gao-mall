package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserRechargeDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserRechargeMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserRechargeDomain domain);

    /**
     * 查询
     */
    List<EbUserRechargeDomain> select();

    /**
     * 更新
     */
    int update(EbUserRechargeDomain domain);
}
