package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemGroupDataDomain
 * @author gaolinge
 */
@Data
public class EbSystemGroupDataDomain {
    /** 
     * 组合数据详情ID
     */
    private Integer id;

    /** 
     * 对应的数据组id
     */
    private Integer gid;

    /** 
     * 数据组对应的数据值（json数据）
     */
    private String value;

    /** 
     * 数据排序
     */
    private Integer sort;

    /** 
     * 状态（1：开启；2：关闭；）
     */
    private Boolean status;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
