package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbSystemRoleMenuDomain
 * @author gaolinge
 */
@Data
public class EbSystemRoleMenuDomain {
    /** 
     * 角色id
     */
    private Integer rid;

    /** 
     * 权限id
     */
    private Integer menuId;


}
