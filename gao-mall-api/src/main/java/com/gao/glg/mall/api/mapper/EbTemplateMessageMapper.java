package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbTemplateMessageDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbTemplateMessageMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbTemplateMessageDomain domain);

    /**
     * 查询
     */
    List<EbTemplateMessageDomain> select();

    /**
     * 更新
     */
    int update(EbTemplateMessageDomain domain);
}
