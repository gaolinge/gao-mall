package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebCouponSearchRequest;
import com.gao.glg.mall.api.domain.EbCouponDomain;
import com.gao.glg.page.PagerDTO;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbCouponService {

    /**
     * 可用的优惠券列表
     * <p/>
     *
     * @param type
     * @param productId
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    List<EbCouponDomain> listByReceive(int type, int productId, PagerDTO request);

    /**
     * 根据id获取
     * <p/>
     *
     * @param couponId
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    EbCouponDomain getById(Integer couponId);

    /**
     * 扣减数量
     * <p/>
     *
     * @param id
     * @param isLimited
     * @param num
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    void deduct(Integer id, Integer isLimited, int num);

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    List<EbCouponDomain> page(WebCouponSearchRequest request);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    void save(EbCouponDomain domain);

    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    void update(EbCouponDomain domain);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    void delete(Integer id);
}
