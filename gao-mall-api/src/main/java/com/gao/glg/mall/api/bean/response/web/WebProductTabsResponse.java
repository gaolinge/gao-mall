package com.gao.glg.mall.api.bean.response.web;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/18
 */
@Data
public class WebProductTabsResponse implements Serializable {

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "类型：1=出售中商品 2=仓库中商品 4=已经售罄商品 5=警戒库存 6=商品回收站")
    private Integer type;

    @ApiModelProperty(value = "数量")
    private Integer count;
}
