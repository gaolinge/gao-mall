package com.gao.glg.mall.api.controller.web;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebExpressSearchRequest;
import com.gao.glg.mall.api.bean.request.web.WebExpressUpdateRequest;
import com.gao.glg.mall.api.bean.response.web.WebExpressResponse;
import com.gao.glg.mall.api.business.IExpressBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Slf4j
@RestController
@RequestMapping("/admin/express")
@Api(tags = "设置 -- 物流 -- 公司")
public class WebExpressController {

    @Autowired
    private IExpressBusiness expressBusiness;

    //@PreAuthorize("hasAuthority('admin:express:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ApiImplicitParam(name="keywords", value="搜索关键字")
    public RestResult<PagerInfo<WebExpressResponse>> page(@Validated WebExpressSearchRequest request) {
        return RestResult.success(expressBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:express:update')")
    @ApiOperation(value = "编辑")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RestResult update(@RequestBody @Validated WebExpressUpdateRequest request) {
        expressBusiness.update(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:express:update:show')")
    @ApiOperation(value = "修改显示状态")
    @RequestMapping(value = "/update/show", method = RequestMethod.POST)
    public RestResult updateShow(@RequestBody @Validated WebExpressUpdateRequest request) {
        expressBusiness.updateShow(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:express:sync')")
    @ApiOperation(value = "同步物流公司")
    @RequestMapping(value = "/sync/express", method = RequestMethod.POST)
    public RestResult<String> syncExpress() {
        expressBusiness.syncExpress();
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:express:info')")
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ApiImplicitParam(name="id", value="快递公司ID", required = true)
    public RestResult<WebExpressResponse> detail(@RequestParam(value = "id") Integer id) {
        return RestResult.success(expressBusiness.detail(id));
   }

    //@PreAuthorize("hasAuthority('admin:express:all')")
    @ApiOperation(value = "查询全部物流公司")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ApiImplicitParam(name="type", value="类型：normal-普通，elec-电子面单")
    public RestResult<List<WebExpressResponse>> list(@RequestParam(value = "type") String type) {
        return RestResult.success(expressBusiness.all(type));
    }


    //@PreAuthorize("hasAuthority('admin:express:template')")
    @ApiOperation(value = "查询物流公司面单模板")
    @RequestMapping(value = "/template", method = RequestMethod.GET)
    @ApiImplicitParam(name="com", value="快递公司编号", required = true)
    public RestResult<JSONObject> template(@RequestParam(value = "com") String no) {
        return RestResult.success(expressBusiness.template(no));
    }
}



