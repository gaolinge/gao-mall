package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.app.AppSystemDataResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserSignInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserSignResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
public interface ISignBusiness {

    /**
     * 列表
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    PagerInfo<AppUserSignResponse> list(PagerDTO request);

    /**
     * 配置
     * <p/>
     *
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    List<AppSystemDataResponse> signConfig();

    /**
     * 签到信息
     * <p/>
     *
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    AppUserSignInfoResponse info();

    /**
     * 签到
     * <p/>
     *
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    AppSystemDataResponse sign();

}
