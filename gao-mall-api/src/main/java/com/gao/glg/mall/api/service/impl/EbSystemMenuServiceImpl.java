package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemMenuDomain;
import com.gao.glg.mall.api.mapper.EbSystemMenuMapper;
import com.gao.glg.mall.api.service.EbSystemMenuService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemMenuServiceImpl implements EbSystemMenuService  {
    @Resource
    private EbSystemMenuMapper systemMenuMapper;

    @Override
    public List<String> selectPerms(List<String> roles) {
        return systemMenuMapper.selectPerms(roles);
    }

    @Override
    public List<EbSystemMenuDomain> selectAll() {
        return systemMenuMapper.selectAll();
    }

    @Override
    public List<EbSystemMenuDomain> listByRoles(List<String> roles) {
        return systemMenuMapper.listByRoles(roles);
    }

    @Override
    public List<EbSystemMenuDomain> all() {
        return systemMenuMapper.all();
    }
}
