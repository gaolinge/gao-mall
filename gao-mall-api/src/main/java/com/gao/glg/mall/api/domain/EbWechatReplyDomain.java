package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbWechatReplyDomain
 * @author gaolinge
 */
@Data
public class EbWechatReplyDomain {
    /** 
     * 微信关键字回复id
     */
    private Integer id;

    /** 
     * 关键字
     */
    private String keywords;

    /** 
     * 回复类型
     */
    private String type;

    /** 
     * 回复数据
     */
    private String data;

    /** 
     * 回复状态 0=不可用  1 =可用
     */
    private Byte status;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
