package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/28
 */
@Data
public class AppProductAttrResponse {

    @ApiModelProperty(value = "attrId")
    private Integer id;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @ApiModelProperty(value = "属性名")
    private String attrName;

    @ApiModelProperty(value = "属性值")
    private String attrValues;

    @ApiModelProperty(value = "活动类型 0=商品，1=秒杀，2=砍价，3=拼团")
    private Integer type;

    @ApiModelProperty(value = "是否删除,0-否，1-是")
    private Boolean isDel;
}
