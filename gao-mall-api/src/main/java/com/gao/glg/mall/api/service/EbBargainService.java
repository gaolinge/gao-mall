package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbBargainDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbBargainService {

    /**
     * 查询有货的砍价商品
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    List<EbBargainDomain> listByInStock();

    /**
     *
     * <p/>
     *
     * @param productId
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    List<EbBargainDomain> listByProductId(Integer productId);
}
