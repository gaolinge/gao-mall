package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.bean.request.web.WebGenerateRequest;
import com.gao.glg.mall.api.mapper.CodeMapper;
import com.gao.glg.mall.api.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class CodeServiceImpl implements CodeService {

    @Autowired
    private CodeMapper codeMapper;

    @Override
    public List<Map<String, Object>> page(WebGenerateRequest request) {
        return codeMapper.page(request);
    }
}
