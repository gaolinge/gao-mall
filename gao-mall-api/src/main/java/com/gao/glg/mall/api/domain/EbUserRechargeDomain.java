package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbUserRechargeDomain
 * @author gaolinge
 */
@Data
public class EbUserRechargeDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 充值用户UID
     */
    private Integer uid;

    /** 
     * 订单号
     */
    private String orderId;

    /** 
     * 充值金额
     */
    private BigDecimal price;

    /** 
     * 购买赠送金额
     */
    private BigDecimal givePrice;

    /** 
     * 充值类型
     */
    private String rechargeType;

    /** 
     * 是否充值
     */
    private Boolean paid;

    /** 
     * 充值支付时间
     */
    private Date payTime;

    /** 
     * 充值时间
     */
    private Date createTime;

    /** 
     * 退款金额
     */
    private BigDecimal refundPrice;


}
