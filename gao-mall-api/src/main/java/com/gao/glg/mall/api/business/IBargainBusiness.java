package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.app.AppBargainRequest;
import com.gao.glg.mall.api.bean.response.app.*;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/22
 */
public interface IBargainBusiness {

    /**
     * 首页
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    List<AppBargainResponse> index();

    /**
     * 砍价商品详情
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    AppBargainHeaderResponse header();

    /**
     * 砍价商品列表header
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    PagerInfo<AppBargainInfoResponse> list(PagerDTO request);

    /**
     * 砍价商品列表header
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    Map<String, Object> start(AppBargainRequest request);

    /**
     * 砍价商品列表header
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    AppBargainUserResponse info(AppBargainRequest request);

    /**
     * 砍价商品列表header
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    AppBargainDetailResponse detail(Integer id);

    /**
     * 砍价商品列表header
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    Map<String, Object> help(AppBargainRequest request);


    /**
     * 砍价记录
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/22
     * @version : 1.0.0
     */
    PagerInfo<AppBargainRecordResponse> records(PagerDTO request);



}
