package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.gao.glg.enums.YesNoEnum;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.dto.AppOrderInfoDTO;
import com.gao.glg.mall.api.bean.dto.AppOrderItemDTO;
import com.gao.glg.mall.api.bean.request.app.AppCalcOrderPriceRequest;
import com.gao.glg.mall.api.bean.request.app.AppCreateOrderRequest;
import com.gao.glg.mall.api.bean.request.app.AppPreOrderItemRequest;
import com.gao.glg.mall.api.bean.request.app.AppPreOrderRequest;
import com.gao.glg.mall.api.bean.response.app.AppCalcOrderPriceResponse;
import com.gao.glg.mall.api.bean.response.app.AppOrderDataResponse;
import com.gao.glg.mall.api.bean.response.app.AppPreOrderResponse;
import com.gao.glg.mall.api.business.IOrderBusiness;
import com.gao.glg.mall.api.cache.OrderCache;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.*;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.mall.api.utils.OrderUtil;
import com.gao.glg.mall.api.utils.StreamUtil;
import com.gao.glg.utils.SplitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderBusiness implements IOrderBusiness {

    @Autowired
    private OrderCache orderCache;
    @Autowired
    private OrderUtil orderUtil;
    @Autowired
    private EbUserService userService;
    @Autowired
    private EbOrderService orderService;
    @Autowired
    private EbProductService productService;
    @Autowired
    private EbBargainService bargainService;
    @Autowired
    private EbSystemStoreService systemStoreService;
    @Autowired
    private EbCouponUserService couponUserService;
    @Autowired
    private EbSeckillService seckillService;
    @Autowired
    private EbProductAttrValueService productAttrValueService;
    @Autowired
    private EbUserAddressService userAddressService;
    @Autowired
    private EbSystemUserLevelService systemUserLevelService;
    @Autowired
    private EbSystemConfigService systemConfigService;
    @Autowired
    private EbShippingTempService shippingTempService;
    @Autowired
    private EbShippingTempFreeService shippingTempFreeService;
    @Autowired
    private EbShippingTempRegionService shippingTempRegionService;

    @Override
    public AppOrderDataResponse orderDataNum() {
        Integer userId = userService.getUserIdException();
        List<EbOrderDomain> orderDomains = orderService.listByUserId(userId);
        AppOrderDataResponse response = new AppOrderDataResponse();
        if (CollectionUtils.isEmpty(orderDomains)) {
            return response;
        }

        Integer unPaidCount   = orderService.orderNum(userId, MallConstant.ORDER_STATUS_H5_UNPAID);
        Integer unShippedCount= orderService.orderNum(userId, MallConstant.ORDER_STATUS_H5_NOT_SHIPPED);
        Integer receivedCount = orderService.orderNum(userId, MallConstant.ORDER_STATUS_H5_SPIKE);
        Integer evaluatedCount= orderService.orderNum(userId, MallConstant.ORDER_STATUS_H5_JUDGE);
        Integer completeCount = orderService.orderNum(userId, MallConstant.ORDER_STATUS_H5_COMPLETE);
        Integer refundCount   = orderService.orderNum(userId, MallConstant.ORDER_STATUS_H5_REFUNDING);
        BigDecimal totalPayAmount = StreamUtil.sumBigDecimal(orderDomains, EbOrderDomain::getPayPrice);

        response.setOrderCount(orderDomains.size());
        response.setSumPrice(totalPayAmount);
        response.setUnPaidCount(unPaidCount);
        response.setUnShippedCount(unShippedCount);
        response.setReceivedCount(receivedCount);
        response.setEvaluatedCount(evaluatedCount);
        response.setCompleteCount(completeCount);
        response.setRefundCount(refundCount);
        return response;
    }

    @Override
    public Map<String, Object> preOrder(AppPreOrderRequest request) {
        if (CollUtil.isEmpty(request.getOrderDetails())) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "预下单订单详情列表不能为空");
        }
        EbUserDomain userDomain = userService.getUserInfo();
        EbUserAddressDomain userAddress = userAddressService.getByDefault(userDomain.getUid());

        AppOrderInfoDTO orderDTO = validatePreOrderRequest(request, userDomain);
        BigDecimal totalPrice = orderDTO.getOrderDetailList().get(0).getProductType().equals(MallConstant.PRODUCT_TYPE_NORMAL) ?
                orderDTO.getOrderDetailList().stream().map(e -> e.getVipPrice().multiply(new BigDecimal(e.getPayNum()))).reduce(BigDecimal.ZERO, BigDecimal::add):
                orderDTO.getOrderDetailList().stream().map(e -> e.getPrice().multiply(new BigDecimal(e.getPayNum()))).reduce(BigDecimal.ZERO, BigDecimal::add);

        int orderProNum = StreamUtil.sumInt(orderDTO.getOrderDetailList(), AppOrderItemDTO::getPayNum);
        orderDTO.setProTotalFee(totalPrice);
        orderDTO.setOrderProNum(orderProNum);
        if (userAddress != null) {
            getFreightFee(orderDTO, userAddress);
            orderDTO.setAddressId(userAddress.getId());
            orderDTO.setRealName(userAddress.getRealName());
            orderDTO.setPhone(userAddress.getPhone());
            orderDTO.setProvince(userAddress.getProvince());
            orderDTO.setCity(userAddress.getCity());
            orderDTO.setDistrict(userAddress.getDistrict());
            orderDTO.setDetail(userAddress.getDetail());
        } else {
            orderDTO.setFreightFee(BigDecimal.ZERO);
        }
        // 实际支付金额
        orderDTO.setPayFee(orderDTO.getProTotalFee().add(orderDTO.getFreightFee()));
        orderDTO.setUserIntegral(userDomain.getIntegral());
        orderDTO.setUserBalance(userDomain.getNowMoney());

        String key = orderCache.addPreOrder(userDomain.getUid(), orderDTO);
        Map<String, Object> map = new HashMap<>();
        map.put("preOrderNo", key);
        return map;
    }

    @Override
    public AppPreOrderResponse loadPreOrder(String preOrderNo) {
        AppOrderInfoDTO infoDTO = orderCache.getPreOrder(preOrderNo);
        if (infoDTO == null) {
            throw new BusinessException(ErrorConstant.PRE_ORDER_NOT_EXIST);
        }

        AppPreOrderResponse preOrderResponse = new AppPreOrderResponse();
        preOrderResponse.setOrderInfoVo(infoDTO);
        String payWeixinOpen = systemConfigService.getValueByKey(MallConstant.CONFIG_PAY_WEIXIN_OPEN);
        if (infoDTO.getIsVideo()) {
            // 关闭余额支付和到店自提
            preOrderResponse.setYuePayStatus("0");
            preOrderResponse.setPayWeixinOpen(payWeixinOpen);
            preOrderResponse.setStoreSelfMention("false");
            preOrderResponse.setAliPayStatus("0");
            return preOrderResponse;
        }

        String yuePayStatus = systemConfigService.getValueByKey(MallConstant.CONFIG_YUE_PAY_STATUS);// 1开启 2关闭
        String storeSelfMention = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_STORE_SELF_MENTION);
        String aliPayStatus = systemConfigService.getValueByKey(MallConstant.CONFIG_ALI_PAY_STATUS);// 1开启
        preOrderResponse.setYuePayStatus(yuePayStatus);
        preOrderResponse.setPayWeixinOpen(payWeixinOpen);
        preOrderResponse.setStoreSelfMention(storeSelfMention);
        preOrderResponse.setAliPayStatus(aliPayStatus);
        return preOrderResponse;
    }

    @Override
    public AppCalcOrderPriceResponse calcPrice(AppCalcOrderPriceRequest request) {
        AppOrderInfoDTO infoDTO = orderCache.getPreOrder(request.getPreOrderNo());
        if (infoDTO == null) {
            throw new BusinessException(ErrorConstant.PRE_ORDER_NOT_EXIST);
        }
        EbUserDomain userDomain = userService.getUserInfo();
        return calculatePrice(request, infoDTO, userDomain);
    }

    private AppCalcOrderPriceResponse calculatePrice(AppCalcOrderPriceRequest request,
                                                     AppOrderInfoDTO infoDTO,
                                                     EbUserDomain userDomain) {
        AppCalcOrderPriceResponse priceResponse = new AppCalcOrderPriceResponse();
        calculateFright(request, infoDTO, userDomain, priceResponse);
        calculateCoupon(request, infoDTO, userDomain, priceResponse);
        calculatePoints(request, infoDTO, userDomain, priceResponse);
        return priceResponse;
    }


    @Override
    public Map<String, Object> createOrder(AppCreateOrderRequest request) {
        /*EbUserDomain userDomain = userService.getUserInfo();
        AppOrderInfoDTO infoDTO = orderCache.getPreOrder(request.getPreOrderNo());
        if (infoDTO == null) {
            throw new BusinessException(ErrorConstant.PRE_ORDER_NOT_EXIST);
        }

        if (!orderUtil.checkPayType(request.getPayType())) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "暂不支持该支付方式，请刷新页面或者联系管理员");
        }

        if (request.getPayType().equals(MallConstant.PAY_TYPE_WE_CHAT)) {
            if (request.getPayChannel() == null) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "支付渠道不能为空!");
            }
            if (!OrderUtil.checkPayChannel(request.getPayChannel())) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "支付渠道不存在!");
            }
        }

        // 校验商品库存
        List<Map<String, Object>> skuRecordList = validateProductStock(infoDTO, userDomain);

        // 校验收货信息
        String verifyCode = "";
        String userAddressStr = "";
        if (request.getShippingType() == 1) { // 快递配送
            if (request.getAddressId() <= 0) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "请选择收货地址");
            }
            EbUserAddressDomain userAddress = userAddressService.getById(request.getAddressId());
            if (userAddress == null || YesNoEnum.YES.getValue() == userAddress.getIsDel()) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "收货地址有误");
            }
            request.setRealName(userAddress.getRealName());
            request.setPhone(userAddress.getPhone());
            userAddressStr = userAddress.getProvince() + userAddress.getCity() + userAddress.getDistrict() + userAddress.getDetail();
        } else if (request.getShippingType() == 2) { // 到店自提
            if (StringUtils.isBlank(request.getRealName()) || StringUtils.isBlank(request.getPhone())) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "请填写姓名和电话");
            }
            // 自提开关是否打开
            String storeSelfMention = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_STORE_SELF_MENTION);
            if ("false".equals(storeSelfMention)) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "请先联系管理员开启门店自提");
            }
            EbSystemStoreDomain systemStore = systemStoreService.getById(request.getStoreId());
            if (ObjectUtil.isNull(systemStore) || systemStore.getIsDel() || !systemStore.getIsShow()) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "暂无门店无法选择门店自提");
            }
            verifyCode = CrmebUtil.randomCount(1111111111,999999999)+"";
            userAddressStr = systemStore.getName();
        }

        // 活动商品校验
        // 秒杀
        if (ObjectUtil.isNotNull(infoDTO.getSeckillId()) && infoDTO.getSeckillId() > 0) {
            EbSeckillDomain storeSeckill = seckillService.getById(infoDTO.getSeckillId());
            if (storeSeckill.getStatus().equals(0)) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "秒杀商品已关闭");
            }
            AppOrderItemDTO detailVo = infoDTO.getOrderDetailList().get(0);
            EbProductAttrValueDomain attrValueDomain = productAttrValueService.getByPidAndTypeAndAttrId(
                    detailVo.getAttrValueId(), infoDTO.getSeckillId(), MallConstant.PRODUCT_TYPE_SECKILL);
            if (ObjectUtil.isNull(attrValueDomain)) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "秒杀商品规格不存在");
            }
            commonValidateSeckill(storeSeckill, attrValueDomain, userDomain, detailVo.getPayNum());
        }

        // 计算订单各种价格
        AppCalcOrderPriceRequest orderComputedPriceRequest = new AppCalcOrderPriceRequest();
        orderComputedPriceRequest.setShippingType(request.getShippingType());
        orderComputedPriceRequest.setAddressId(request.getAddressId());
        orderComputedPriceRequest.setCouponId(request.getCouponId());
        orderComputedPriceRequest.setUseIntegral(request.getUseIntegral());
        AppCalcOrderPriceResponse computedOrderPriceResponse = calculatePrice(orderComputedPriceRequest, infoDTO, userDomain);

        // 生成订单号
        String orderNo = CrmebUtil.getOrderNo("order");

        // 购买赠送的积分
        int gainIntegral = 0;
        List<EbOrderInfoDomain> storeOrderInfos = new ArrayList<>();
        for (AppOrderItemDTO detailVo : infoDTO.getOrderDetailList()) {
            // 赠送积分
            if (ObjectUtil.isNotNull(detailVo.getGiveIntegral()) && detailVo.getGiveIntegral() > 0) {
                gainIntegral += detailVo.getGiveIntegral() * detailVo.getPayNum();
            }
            // 订单详情
            EbOrderInfoDomain soInfo = new EbOrderInfoDomain();
            soInfo.setProductId(detailVo.getProductId());
            soInfo.setInfo(JSON.toJSON(detailVo).toString());
            soInfo.setUnique(detailVo.getAttrValueId().toString());
            soInfo.setOrderNo(orderNo);
            soInfo.setProductName(detailVo.getProductName());
            soInfo.setAttrValueId(detailVo.getAttrValueId());
            soInfo.setImage(detailVo.getImage());
            soInfo.setSku(detailVo.getSku());
            soInfo.setPrice(detailVo.getPrice());
            soInfo.setPayNum(detailVo.getPayNum());
            soInfo.setWeight(detailVo.getWeight());
            soInfo.setVolume(detailVo.getVolume());
            if (ObjectUtil.isNotNull(detailVo.getGiveIntegral()) && detailVo.getGiveIntegral() > 0) {
                soInfo.setGiveIntegral(detailVo.getGiveIntegral());
            } else {
                soInfo.setGiveIntegral(0);
            }
            soInfo.setIsReply(false);
            soInfo.setIsSub(detailVo.getIsSub());
            soInfo.setProductType(detailVo.getProductType());
            if (ObjectUtil.isNotNull(detailVo.getVipPrice())) {
                soInfo.setVipPrice(detailVo.getVipPrice());
            } else {
                soInfo.setVipPrice(detailVo.getPrice());
            }

            storeOrderInfos.add(soInfo);
        }

        // 下单赠送积分
        if (computedOrderPriceResponse.getPayFee().compareTo(BigDecimal.ZERO) > 0) {
            // 赠送积分比例
            String integralStr = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_INTEGRAL_RATE_ORDER_GIVE);
            if (StrUtil.isNotBlank(integralStr)) {
                BigDecimal integralBig = new BigDecimal(integralStr);
                int integral = integralBig.multiply(computedOrderPriceResponse.getPayFee()).setScale(0, BigDecimal.ROUND_DOWN).intValue();
                if (integral > 0) {
                    // 添加积分
                    gainIntegral += integral;
                }
            }
        }

        // 支付渠道 默认：余额支付
        int isChannel = 3;
        if (request.getPayType().equals(MallConstant.PAY_TYPE_WE_CHAT)) {
            switch (request.getPayChannel()) {
                case MallConstant.PAY_CHANNEL_WE_CHAT_H5:// H5
                    isChannel = 2;
                    break;
                case MallConstant.PAY_CHANNEL_WE_CHAT_PUBLIC:// 公众号
                    isChannel = 0;
                    break;
                case MallConstant.PAY_CHANNEL_WE_CHAT_PROGRAM:// 小程序
                    isChannel = 1;
                    break;
                case MallConstant.PAY_CHANNEL_WE_CHAT_APP_IOS:// app ios
                    isChannel = 4;
                    break;
                case MallConstant.PAY_CHANNEL_WE_CHAT_APP_ANDROID:// app android
                    isChannel = 5;
                    break;
            }
        }
        if (request.getPayType().equals(MallConstant.PAY_TYPE_ALI_PAY)) {
            isChannel = 6;
            if (request.getPayChannel().equals(MallConstant.PAY_CHANNEL_ALI_APP_PAY)) {
                isChannel = 7;
            }
        }

        EbOrderDomain orderDomain = new EbOrderDomain();
        orderDomain.setUid(userDomain.getUid());
        orderDomain.setOrderId(orderNo);
        orderDomain.setRealName(request.getRealName());
        orderDomain.setUserPhone(request.getPhone());
        orderDomain.setUserAddress(userAddressStr);
        // 如果是自提
        if (request.getShippingType() == 2) {
            orderDomain.setVerifyCode(verifyCode);
            orderDomain.setStoreId(request.getStoreId());
        }
        orderDomain.setTotalNum(infoDTO.getOrderProNum());
        orderDomain.setCouponId(Optional.ofNullable(request.getCouponId()).orElse(0));

        // 订单总价
        BigDecimal totalPrice = computedOrderPriceResponse.getProTotalFee().add(computedOrderPriceResponse.getFreightFee());

        orderDomain.setTotalPrice(totalPrice);
        orderDomain.setProTotalPrice(computedOrderPriceResponse.getProTotalFee());
        orderDomain.setTotalPostage(computedOrderPriceResponse.getFreightFee());
        orderDomain.setCouponPrice(computedOrderPriceResponse.getCouponFee());
        orderDomain.setPayPrice(computedOrderPriceResponse.getPayFee());
        orderDomain.setPayPostage(computedOrderPriceResponse.getFreightFee());
        orderDomain.setDeductionPrice(computedOrderPriceResponse.getDeductionPrice());
        orderDomain.setPayType(request.getPayType());
        orderDomain.setUseIntegral(computedOrderPriceResponse.getUsedIntegral());
        orderDomain.setGainIntegral(gainIntegral);
        orderDomain.setMark(StringEscapeUtils.escapeHtml4(request.getMark()));
        orderDomain.setCombinationId(infoDTO.getCombinationId());
        orderDomain.setPinkId(infoDTO.getPinkId());
        orderDomain.setSeckillId(infoDTO.getSeckillId());
        orderDomain.setBargainId(infoDTO.getBargainId());
        orderDomain.setBargainUserId(infoDTO.getBargainUserId());
        orderDomain.setCreateTime(new Date());
        orderDomain.setShippingType(request.getShippingType());
        orderDomain.setIsChannel(isChannel);
        orderDomain.setCost(BigDecimal.ZERO);
        orderDomain.setPaid(0);
        orderDomain.setType(infoDTO.getIsVideo() ? 1 : 0);

        EbCouponUserDomain storeCouponUser = new EbCouponUserDomain();
        if (orderDomain.getCouponId() > 0) {
            storeCouponUser = couponUserService.getById(orderDomain.getCouponId());
            storeCouponUser.setStatus(1);
        }
        EbCouponUserDomain finalStoreCouponUser = storeCouponUser;

        Boolean execute = transactionTemplate.execute(e -> {
            // 扣减库存
            // 需要根据是否活动商品，扣减不同的库存
            if (orderDomain.getSeckillId() > 0) {// 秒杀扣库存
                Map<String, Object> skuRecord = skuRecordList.get(0);
                // 秒杀商品扣库存
                seckillService.operationStock(skuRecord.getInt("activityId"), skuRecord.getInt("num"), "sub");
                // 秒杀商品规格扣库存
                productAttrValueService.operationStock(skuRecord.getInt("activityAttrValueId"), skuRecord.getInt("num"), "sub", MallConstant.PRODUCT_TYPE_SECKILL);
                // 普通商品口库存
                productService.operationStock(skuRecord.getInt("productId"), skuRecord.getInt("num"), "sub");
                // 普通商品规格扣库存
                productAttrValueService.operationStock(skuRecord.getInt("attrValueId"), skuRecord.getInt("num"), "sub", MallConstant.PRODUCT_TYPE_NORMAL);
            } else if (orderDomain.getBargainId() > 0) {// 砍价扣库存
                Map<String, Object> skuRecord = skuRecordList.get(0);
                // 砍价商品扣库存
                bargainService.operationStock(skuRecord.getInt("activityId"), skuRecord.getInt("num"), "sub");
                // 砍价商品规格扣库存
                productAttrValueService.operationStock(skuRecord.getInt("activityAttrValueId"), skuRecord.getInt("num"), "sub", MallConstant.PRODUCT_TYPE_BARGAIN);
                // 普通商品口库存
                productService.operationStock(skuRecord.getInt("productId"), skuRecord.getInt("num"), "sub");
                // 普通商品规格扣库存
                productAttrValueService.operationStock(skuRecord.getInt("attrValueId"), skuRecord.getInt("num"), "sub", MallConstant.PRODUCT_TYPE_NORMAL);
            } else if (orderDomain.getCombinationId() > 0) {// 拼团扣库存
                Map<String, Object> skuRecord = skuRecordList.get(0);
                // 拼团商品扣库存
                Boolean operationStock = storeCombinationService.operationStock(skuRecord.getInt("activityId"), skuRecord.getInt("num"), "sub");
                System.out.println("拼团商品扣库存operationStock " + operationStock);
                // 拼团商品规格扣库存
                productAttrValueService.operationStock(skuRecord.getInt("activityAttrValueId"), skuRecord.getInt("num"), "sub", MallConstant.PRODUCT_TYPE_PINGTUAN);
                // 普通商品口库存
                productService.operationStock(skuRecord.getInt("productId"), skuRecord.getInt("num"), "sub");
                // 普通商品规格扣库存
                productAttrValueService.operationStock(skuRecord.getInt("attrValueId"), skuRecord.getInt("num"), "sub", MallConstant.PRODUCT_TYPE_NORMAL);
            } else { // 普通商品
                for (MyRecord skuRecord : skuRecordList) {
                    // 普通商品口库存
                    storeProductService.operationStock(skuRecord.getInt("productId"), skuRecord.getInt("num"), "sub");
                    // 普通商品规格扣库存
                    storeProductAttrValueService.operationStock(skuRecord.getInt("attrValueId"), skuRecord.getInt("num"), "sub", MallConstant.PRODUCT_TYPE_NORMAL);
                }
            }

            orderService.create(orderDomain);
            storeOrderInfos.forEach(info -> info.setOrderId(orderDomain.getId()));
            // 优惠券修改
            if (orderDomain.getCouponId() > 0) {
                couponUserService.update(finalStoreCouponUser);
            }
            // 保存购物车商品详情
            storeOrderInfoService.saveOrderInfos(storeOrderInfos);
            // 生成订单日志
            storeOrderStatusService.createLog(orderDomain.getId(), MallConstant.ORDER_STATUS_CACHE_CREATE_ORDER, "订单生成");

            // 清除购物车数据
            if (CollUtil.isNotEmpty(infoDTO.getCartIdList())) {
                cartService.deleteCartByIds(infoDTO.getCartIdList());
            }
            return Boolean.TRUE;
        });
        if (!execute) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "订单生成失败");
        }

        orderCache.delete(request.getPreOrderNo());
        orderCache.addTimeoutCancel(orderDomain.getOrderId());
        // 发送后台管理员下单提醒通知短信
        sendAdminOrderNotice(orderDomain.getOrderId());

        Map<String, Object> record = new HashMap<>();
        record.put("orderNo", orderDomain.getOrderId());
        return record;*/
        return null;
    }


    /**
     * 计算积分
     * <p/>
     *
     * @param request
     * @param infoDTO
     * @param userDomain
     * @param priceResponse
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    private void calculatePoints(AppCalcOrderPriceRequest request, AppOrderInfoDTO infoDTO,
                                 EbUserDomain userDomain, AppCalcOrderPriceResponse priceResponse) {
        // 总金额 - 运费 - 优惠金额
        BigDecimal totalAmount = infoDTO.getProTotalFee().add(priceResponse.getFreightFee()).subtract(priceResponse.getCouponFee());
        priceResponse.setUseIntegral(request.getUseIntegral());
        priceResponse.setProTotalFee(infoDTO.getProTotalFee());
        // 不允许使用积分、积分不足
        if (!request.getUseIntegral() ||
                userDomain.getIntegral() <= 0) {
            priceResponse.setDeductionPrice(BigDecimal.ZERO);
            priceResponse.setSurplusIntegral(userDomain.getIntegral());
            priceResponse.setPayFee(totalAmount);
            priceResponse.setUsedIntegral(0);
            return;
        }

        String pointRatio = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_INTEGRAL_RATE);
        BigDecimal canDeductAmount = new BigDecimal(userDomain.getIntegral()).multiply(new BigDecimal(pointRatio));

        if (canDeductAmount.compareTo(totalAmount) < 0) {
            totalAmount = totalAmount.subtract(canDeductAmount);
            priceResponse.setSurplusIntegral(0);
            priceResponse.setUsedIntegral(userDomain.getIntegral());
        } else {
            canDeductAmount = totalAmount;
            totalAmount = BigDecimal.ZERO;
            if (canDeductAmount.compareTo(BigDecimal.ZERO) > 0) {
                // 计算使用的积分
                int usePoint = canDeductAmount.divide(new BigDecimal(pointRatio), 0, BigDecimal.ROUND_UP).intValue();
                priceResponse.setSurplusIntegral(userDomain.getIntegral() - usePoint);
                priceResponse.setUsedIntegral(usePoint);
            }
        }
        priceResponse.setPayFee(totalAmount);
        priceResponse.setDeductionPrice(canDeductAmount);
    }

    /**
     * 计算优惠劵
     * <p/>
     *
     * @param request
     * @param infoDTO
     * @param userDomain
     * @param priceResponse
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    private void calculateCoupon(AppCalcOrderPriceRequest request, AppOrderInfoDTO infoDTO,
                                 EbUserDomain userDomain, AppCalcOrderPriceResponse priceResponse) {

        if (infoDTO.getSeckillId() > 0 || infoDTO.getBargainId() > 0 || infoDTO.getCombinationId() > 0) {
            throw new BusinessException(ErrorConstant.MARKETING_ACTIVITIES_NO_USE_COUPON);
        }
        if (infoDTO.getIsVideo()) {
            throw new BusinessException(ErrorConstant.VIDEO_NUMBER_NO_USE_COUPON);
        }

        if (ObjectUtil.isNull(request.getCouponId()) || request.getCouponId() <= 0) {
            priceResponse.setCouponFee(BigDecimal.ZERO);
            return;
        }

        EbCouponUserDomain couponUserDomain = couponUserService.getById(request.getCouponId());
        if (couponUserDomain == null || !couponUserDomain.getUid().equals(userDomain.getUid())) {
            throw new BusinessException(ErrorConstant.COUPON_USER_RECORD_NOT_EXIST);
        }
        if (couponUserDomain.getStatus() == 1) {
            throw new BusinessException(ErrorConstant.COUPON_USED_ALREADY);
        }
        if (couponUserDomain.getStatus() == 2) {
            throw new BusinessException(ErrorConstant.COUPON_INVALID_ALREADY);
        }

        Date date = new Date();
        if (couponUserDomain.getStartTime().compareTo(date) > 0) {
            throw new BusinessException(ErrorConstant.COUPON_NOT_ARRIVED_YET_TIME);
        }
        if (date.compareTo(couponUserDomain.getEndTime()) > 0) {
            throw new BusinessException(ErrorConstant.COUPON_INVALID_ALREADY);
        }
        if (couponUserDomain.getMinPrice().compareTo(infoDTO.getProTotalFee()) > 0) {
            throw new BusinessException(ErrorConstant.COUPON_NOT_ARRIVED_YET_AMOUNT);
        }

        // 使用类型 1 通用券 2 商品券 3 分类券
        if (couponUserDomain.getUseType() > 1) {
            List<Integer> pids = StreamUtil.map(infoDTO.getOrderDetailList(), AppOrderItemDTO::getProductId);
            if (CollectionUtils.isEmpty(pids)) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT);
            }

            List<Integer> ids = SplitUtil.split(couponUserDomain.getPrimaryKey(), ',')
                    .stream().map(Integer::parseInt).collect(Collectors.toList());
            if (couponUserDomain.getUseType() == 2) {
                ids.retainAll(pids);
                if (CollUtil.isEmpty(ids)) {
                    throw new BusinessException(ErrorConstant.PRODUCT_NOT_SATISFY_USE_CONDITION);
                }
            }

            if (couponUserDomain.getUseType() == 3) {
                List<Integer> cids = productService.queryCategory2(pids);
                ids.retainAll(cids);
                if (CollUtil.isEmpty(ids)) {
                    throw new BusinessException(ErrorConstant.CATEGORY_NOT_SATISFY_USE_CONDITION);
                }
            }
        }

        BigDecimal couponAmount = infoDTO.getProTotalFee().compareTo(couponUserDomain.getMoney()) <= 0 ?
                infoDTO.getProTotalFee() : couponUserDomain.getMoney();
        priceResponse.setCouponFee(couponAmount);
    }


    /**
     * 计算运费
     * <p/>
     *
     * @param request
     * @param infoDTO
     * @param userDomain
     * @param priceResponse
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    private void calculateFright(AppCalcOrderPriceRequest request, AppOrderInfoDTO infoDTO,
                                 EbUserDomain userDomain, AppCalcOrderPriceResponse priceResponse) {

        if (request.getAddressId() == null || Objects.equals(request.getShippingType(), 2)) {
            priceResponse.setFreightFee(BigDecimal.ZERO);
            return;
        }

        EbUserAddressDomain userAddress = userAddressService.getById(request.getAddressId());
        if (userAddress == null) {
            priceResponse.setFreightFee(BigDecimal.ZERO);
            return;
        }

        getFreightFee(infoDTO, userAddress);
        priceResponse.setFreightFee(infoDTO.getFreightFee());
    }

    /**
     * 校验预下单商品信息
     * @param request 预下单请求参数
     * @return OrderInfoVo
     */
    private AppOrderInfoDTO validatePreOrderRequest(AppPreOrderRequest request, EbUserDomain user) {
        List<AppOrderItemDTO> orderItemDTOS = new ArrayList<>();
        AppOrderInfoDTO infoDTO = new AppOrderInfoDTO();
        infoDTO.setOrderDetailList(orderItemDTOS);

        switch (request.getPreOrderType()) {
            case "shoppingCart":
                //orderItemDTOS = validatePreOrderShopping(request, user);
                List<Long> cartIdList = request.getOrderDetails().stream().map(
                        AppPreOrderItemRequest::getShoppingCartId).distinct().collect(Collectors.toList());
                infoDTO.setCartIdList(cartIdList);
                break;

            case "again":
                AppPreOrderItemRequest detailRequest = request.getOrderDetails().get(0);
                //orderItemDTOS = validatePreOrderAgain(detailRequest, user);
                break;

            case "buyNow":
                AppPreOrderItemRequest itemRequest = request.getOrderDetails().get(0);
                // 秒杀
                if (itemRequest.getSeckillId() != null) {
                    //orderItemDTOS.add(validatePreOrderSeckill(detailRequest, user));
                    infoDTO.setSeckillId(itemRequest.getSeckillId());
                    break;
                }
                // 砍价
                if (itemRequest.getBargainId() != null) {
                    //orderItemDTOS.add(validatePreOrderBargain(detailRequest, user));
                    infoDTO.setBargainId(itemRequest.getBargainId());
                    infoDTO.setBargainUserId(itemRequest.getBargainUserId());
                    break;
                }
                // 拼团
                if (itemRequest.getCombinationId() != null) {
                    //orderItemDTOS.add(validatePreOrderCombination(detailRequest, user));
                    infoDTO.setCombinationId(itemRequest.getCombinationId());
                    infoDTO.setPinkId(itemRequest.getPinkId());
                    break;
                }

                // 普通商品
                if (ObjectUtil.isNull(itemRequest.getProductId())) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "商品编号不能为空");
                }
                if (ObjectUtil.isNull(itemRequest.getAttrValueId())) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "商品规格属性值不能为空");
                }
                if (ObjectUtil.isNull(itemRequest.getProductNum()) || itemRequest.getProductNum() < 0) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "购买数量必须大于0");
                }
                // 查询商品信息
                EbProductDomain storeProduct = productService.getById(itemRequest.getProductId());
                if (ObjectUtil.isNull(storeProduct)) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "商品信息不存在，请刷新后重新选择");
                }
                if (YesNoEnum.YES.getValue() == storeProduct.getIsDel()) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "商品已删除，请刷新后重新选择");
                }
                if (!storeProduct.getIsShow()) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "商品已下架，请刷新后重新选择");
                }
                if (storeProduct.getStock() < itemRequest.getProductNum()) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "商品库存不足，请刷新后重新选择");
                }
                // 查询商品规格属性值信息
                EbProductAttrValueDomain attrValue = productAttrValueService.getByPidAndTypeAndAttrId(
                        itemRequest.getProductId(), itemRequest.getAttrValueId().toString(), MallConstant.PRODUCT_TYPE_NORMAL);
                if (ObjectUtil.isNull(attrValue)) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "商品规格信息不存在，请刷新后重新选择");
                }
                if (attrValue.getStock() < itemRequest.getProductNum()) {
                    throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "商品规格库存不足，请刷新后重新选择");
                }
                EbSystemUserLevelDomain userLevel = null;
                if (user.getLevel() != null) {
                    userLevel = systemUserLevelService.getById(user.getLevel());
                }
                AppOrderItemDTO detailVo = new AppOrderItemDTO();
                detailVo.setProductId(storeProduct.getId());
                detailVo.setProductName(storeProduct.getStoreName());
                detailVo.setAttrValueId(attrValue.getId());
                detailVo.setSku(attrValue.getSuk());
                detailVo.setPrice(attrValue.getPrice());
                detailVo.setPayNum(itemRequest.getProductNum());
                detailVo.setImage(StrUtil.isNotBlank(attrValue.getImage()) ? attrValue.getImage() : storeProduct.getImage());
                detailVo.setVolume(attrValue.getVolume());
                detailVo.setWeight(attrValue.getWeight());
                detailVo.setTempId(storeProduct.getTempId());
                detailVo.setIsSub(storeProduct.getIsSub());
                detailVo.setProductType(MallConstant.PRODUCT_TYPE_NORMAL);
                detailVo.setVipPrice(detailVo.getPrice());
                detailVo.setGiveIntegral(storeProduct.getGiveIntegral());
                if (userLevel != null) {
                    detailVo.setVipPrice(detailVo.getPrice());
                }
                orderItemDTOS.add(detailVo);
                break;

        }
        return infoDTO;
    }


    /**
     *
     * <p/>
     *
     * @param infoDTO
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    private void getFreightFee(AppOrderInfoDTO infoDTO, EbUserAddressDomain userAddress) {
        String freightSwitch = systemConfigService.getValueByKey(MallConstant.STORE_FEE_POSTAGE_SWITCH);
        String freeShipping  = systemConfigService.getValueByKey(MallConstant.STORE_FEE_POSTAGE);

        if (userAddress == null || userAddress.getCityId() == null) {
            infoDTO.setFreightFee(BigDecimal.ZERO);
            return;
        }

        if ("true".equals(freightSwitch) &&
                ("0".equals(freeShipping) || infoDTO.getProTotalFee().compareTo(new BigDecimal(freeShipping)) >= 0)) {
            infoDTO.setFreightFee(BigDecimal.ZERO);
            return;
        }

        Map<Integer, Map<String, Object>> productMap = new HashMap<>();
        infoDTO.getOrderDetailList().forEach(e -> {
            Integer pid = e.getProductId();
            if (productMap.containsKey(pid)) {
                BigDecimal weight = e.getWeight().multiply(BigDecimal.valueOf(e.getPayNum()));
                BigDecimal volume = e.getVolume().multiply(BigDecimal.valueOf(e.getPayNum()));
                Map<String, Object> record = productMap.get(pid);
                BigDecimal totalPrice  = (BigDecimal) record.get("totalPrice");
                BigDecimal totalWeight = (BigDecimal) record.get("weight");
                BigDecimal totalVolume = (BigDecimal) record.get("volume");
                Integer totalNum       = (Integer) record.get("totalNum");
                record.put("totalPrice", totalPrice.add(e.getPrice().multiply(BigDecimal.valueOf(e.getPayNum()))));
                record.put("totalNum",   totalNum + e.getPayNum());
                record.put("weight",     totalWeight.add(weight));
                record.put("volume",     totalVolume.add(volume));
            } else {
                Map<String, Object> record = new HashMap<>();
                BigDecimal weight = e.getWeight().multiply(BigDecimal.valueOf(e.getPayNum()));
                BigDecimal volume = e.getVolume().multiply(BigDecimal.valueOf(e.getPayNum()));
                record.put("totalPrice", e.getPrice().multiply(BigDecimal.valueOf(e.getPayNum())));
                record.put("totalNum",   e.getPayNum());
                record.put("tempId",     e.getTempId());
                record.put("proId",      pid);
                record.put("weight",     weight);
                record.put("volume",     volume);

                productMap.put(pid, record);
            }
        });

        // 指定包邮（单品运费模板）> 指定区域配送（单品运费模板）
        BigDecimal storePostage = BigDecimal.ZERO;
        for (Map.Entry<Integer, Map<String, Object>> m : productMap.entrySet()) {
            Map<String, Object> value = m.getValue();
            Integer tempId = (Integer) value.get("tempId");
            EbShippingTempDomain shippingTemplate = shippingTempService.getById(tempId);
            if (shippingTemplate.getAppoint()) {// 指定包邮
                // 判断是否在指定包邮区域内
                // 必须满足件数 + 金额 才能包邮
                EbShippingTempFreeDomain shippingTemplatesFree = shippingTempFreeService
                        .getByTempIdAndCityId(tempId, userAddress.getCityId());
                if (shippingTemplatesFree != null) {
                    BigDecimal totalAmount = (BigDecimal) value.get("totalPrice");
                    Integer totalNum = (Integer) value.get("totalNum");

                    if (new BigDecimal(totalNum).compareTo(shippingTemplatesFree.getNumber()) >= 0
                            && totalAmount.compareTo(shippingTemplatesFree.getPrice()) >= 0) {
                        continue;
                    }
                }
            }

            // 不满足指定包邮条件，走指定区域配送
            EbShippingTempRegionDomain shippingTempRegionDomain = shippingTempRegionService
                    .getByTempIdAndCityId(tempId, userAddress.getCityId());
            if (ObjectUtil.isNull(shippingTempRegionDomain)) {
                throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT, "计算运费时，未找到全国运费配置");
            }


            // 判断计费方式：件数、重量、体积
            switch (shippingTemplate.getType()) {
                case 1: // 件数
                    // 判断件数是否超过首件
                    Integer num = (Integer) value.get("totalNum");
                    if (num <= shippingTempRegionDomain.getFirst().intValue()) {
                        storePostage = storePostage.add(shippingTempRegionDomain.getFirstPrice());
                    } else {// 超过首件的需要计算续件
                        int renewalNum = num - shippingTempRegionDomain.getFirst().intValue();
                        // 剩余件数/续件 = 需要计算的续件费用的次数
                        BigDecimal divide = new BigDecimal(renewalNum).divide(shippingTempRegionDomain.getRenewal(), 0, BigDecimal.ROUND_UP);
                        BigDecimal renewalPrice = shippingTempRegionDomain.getRenewalPrice().multiply(divide);
                        storePostage = storePostage.add(shippingTempRegionDomain.getFirstPrice()).add(renewalPrice);
                    }
                    break;
                case 2: // 重量
                    BigDecimal weight = (BigDecimal) value.get("weight");
                    if (weight.compareTo(shippingTempRegionDomain.getFirst()) <= 0) {
                        storePostage = storePostage.add(shippingTempRegionDomain.getFirstPrice());
                    } else {// 超过首件的需要计算续件
                        BigDecimal renewalNum = weight.subtract(shippingTempRegionDomain.getFirst());
                        // 剩余件数/续件 = 需要计算的续件费用的次数
                        BigDecimal divide = renewalNum.divide(shippingTempRegionDomain.getRenewal(), 0, BigDecimal.ROUND_UP);
                        BigDecimal renewalPrice = shippingTempRegionDomain.getRenewalPrice().multiply(divide);
                        storePostage = storePostage.add(shippingTempRegionDomain.getFirstPrice()).add(renewalPrice);
                    }
                    break;
                case 3: // 体积
                    BigDecimal volume = (BigDecimal) value.get("volume");
                    if (volume.compareTo(shippingTempRegionDomain.getFirst()) <= 0) {
                        storePostage = storePostage.add(shippingTempRegionDomain.getFirstPrice());
                    } else {// 超过首件的需要计算续件
                        BigDecimal renewalNum = volume.subtract(shippingTempRegionDomain.getFirst());
                        // 剩余件数/续件 = 需要计算的续件费用的次数
                        BigDecimal divide = renewalNum.divide(shippingTempRegionDomain.getRenewal(), 0, BigDecimal.ROUND_UP);
                        BigDecimal renewalPrice = shippingTempRegionDomain.getRenewalPrice().multiply(divide);
                        storePostage = storePostage.add(shippingTempRegionDomain.getFirstPrice()).add(renewalPrice);
                    }
                    break;
            }
        }
        infoDTO.setFreightFee(storePostage);
    }



}
