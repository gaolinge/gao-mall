package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.domain.EbUserAddressDomain;
import com.gao.glg.mall.api.mapper.EbUserAddressMapper;
import com.gao.glg.mall.api.service.EbUserAddressService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class EbUserAddressServiceImpl implements EbUserAddressService  {
    @Resource
    private EbUserAddressMapper userAddressMapper;

    @Override
    public List<EbUserAddressDomain> listByUid(Integer uid) {
        return userAddressMapper.listByUid(uid);
    }

    @Override
    public void canalDefault(Integer uid) {
        userAddressMapper.canalDefault(uid);
    }

    @Override
    public void save(EbUserAddressDomain domain) {
        domain.setCreateTime(new Date());
        domain.setUpdateTime(new Date());
        userAddressMapper.save(domain);
    }

    @Override
    public void update(EbUserAddressDomain domain) {
        domain.setUpdateTime(new Date());
        userAddressMapper.update(domain);
    }

    @Override
    public EbUserAddressDomain getByUidAndId(Integer uid, Integer id) {
        return userAddressMapper.getByUidAndId(uid, id);
    }

    @Override
    public EbUserAddressDomain getByDefault(Integer uid) {
        return userAddressMapper.getByDefault(uid);
    }

    @Override
    public EbUserAddressDomain getById(Integer id) {
        return userAddressMapper.getById(id);
    }
}
