package com.gao.glg.mall.api.bean.response.app;

import com.gao.glg.mall.api.bean.dto.AppOrderInfoDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 预下单
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/10
 */
@Data
public class AppPreOrderResponse implements Serializable {

    @ApiModelProperty(value = "订单详情对象")
    private AppOrderInfoDTO orderInfoVo;

    @ApiModelProperty(value = "预下单订单号")
    private String preOrderNo;

    @ApiModelProperty(value = "门店自提是否开启")
    private String storeSelfMention;

    @ApiModelProperty(value = "余额支付 1 开启 2 关闭")
    private String yuePayStatus;

    @ApiModelProperty(value = "微信支付 1 开启 0 关闭")
    private String payWeixinOpen;

    @ApiModelProperty(value = "支付宝支付 1 开启 0 关闭")
    private String aliPayStatus;

}
