package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.collection.CollUtil;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.response.app.*;
import com.gao.glg.mall.api.business.ISeckillBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.*;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.mall.api.utils.MallUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SeckillBusiness implements ISeckillBusiness {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbUserVisitRecordService userVisitRecordService;
    @Autowired
    private EbProductService productService;
    @Autowired
    private EbProductDescService productDescService;
    @Autowired
    private EbProductRelationService productRelationService;
    @Autowired
    private EbProductAttrService productAttrService;
    @Autowired
    private EbProductAttrValueService productAttrValueService;
    @Autowired
    private EbSeckillService seckillService;
    @Autowired
    private EbSeckillMangerService seckillMangerService;


    @Override
    public AppSeckillIndexResponse index() {
        List<EbSeckillMangerDomain> seckillMangerDomains = seckillMangerService.listByCurrent();
        if (CollectionUtils.isEmpty(seckillMangerDomains)) {
            return null;
        }

        EbSeckillMangerDomain seckillMangerDomain = seckillMangerDomains.get(0);
        List<EbSeckillDomain> seckillDomains = seckillService.getByTimeId(seckillMangerDomain.getId());
        if (CollectionUtils.isEmpty(seckillDomains)) {
            return null;
        }

        String statusName = getStatusName(seckillMangerDomain.getStatus(),
                seckillMangerDomain.getStartTime(), seckillMangerDomain.getEndTime());
        Integer killStatus = getKillStatus(seckillMangerDomain.getStatus(),
                seckillMangerDomain.getStartTime(), seckillMangerDomain.getEndTime());
        String t1 = seckillMangerDomain.getStartTime().toString();
        String t2 = seckillMangerDomain.getEndTime().toString();
        String p1 = t1.length() == 1 ? "0" + t1 : t1;
        String p2 = t2.length() == 1 ? "0" + t2 : t2;
        long timestamp = MallUtil.timestamp(seckillMangerDomain.getEndTime());
        AppSecKillResponse secKillResponse = new AppSecKillResponse();
        secKillResponse.setId(seckillMangerDomain.getId());
        secKillResponse.setSlide(seckillMangerDomain.getSilderImgs());
        secKillResponse.setStatusName(statusName);
        secKillResponse.setStatus(killStatus);
        secKillResponse.setTime(p1 + ":00," + p2 + ":00");
        secKillResponse.setTimeSwap(timestamp + "");

        List<AppSeckillProductResponse> productResponse =
                BeanCopierUtil.copyList(seckillDomains, AppSeckillProductResponse.class);

        AppSeckillIndexResponse response = new AppSeckillIndexResponse();
        response.setSecKillResponse(secKillResponse);
        response.setProductList(productResponse);
        return response;
    }

    @Override
    public AppSeckillDetailResponse detail(Integer id) {
        // 获取秒杀商品信息
        EbSeckillDomain seckillDomain = seckillService.getById(id);
        if (seckillDomain == null || seckillDomain.getIsDel() == 1) {
            throw new BusinessException(ErrorConstant.ARTICLE_NOT_EXIST, "未找到对应秒杀商品信息");
        }
        if (seckillDomain.getStatus().equals(0)) {
            throw new BusinessException(ErrorConstant.ARTICLE_NOT_EXIST, "秒杀商品已下架");
        }

        EbProductDomain storeProduct = productService.getById(seckillDomain.getProductId());
        EbProductDescDomain productDescDomain = productDescService.geByProductId(id, MallConstant.PRODUCT_TYPE_SECKILL);
        EbSeckillMangerDomain seckillMangerDomain = seckillMangerService.getById(seckillDomain.getTimeId());
        List<EbProductAttrDomain> productAttrDomains = productAttrService.listByPidAndType(id, MallConstant.PRODUCT_TYPE_SECKILL);
        List<EbProductAttrValueDomain> productAttrValueDomains = productAttrValueService.listByPidAndType(seckillDomain.getProductId(), MallConstant.PRODUCT_TYPE_NORMAL);
        List<EbProductAttrValueDomain> seckillAttrValueDomains = productAttrValueService.listByPidAndType(seckillDomain.getId(), MallConstant.PRODUCT_TYPE_SECKILL);

        Integer seckillStatus = getKillStatus(seckillDomain.getStatus(), seckillMangerDomain.getStartTime(), seckillMangerDomain.getEndTime());
        AppSecKillItemResponse secKillItemResponse = BeanCopierUtil.copy(seckillDomain, AppSecKillItemResponse.class);
        secKillItemResponse.setStoreName(seckillDomain.getTitle());
        secKillItemResponse.setSliderImage(seckillDomain.getImages());
        secKillItemResponse.setStoreInfo(seckillDomain.getInfo());
        secKillItemResponse.setSeckillStatus(seckillStatus);
        secKillItemResponse.setContent(productDescDomain == null || productDescDomain.getDescription() == null ? "" : productDescDomain.getDescription());
        secKillItemResponse.setSales(storeProduct.getSales());
        secKillItemResponse.setFicti(storeProduct.getFicti());
        if (seckillMangerDomain != null) {
            long timestamp = MallUtil.timestamp(seckillMangerDomain.getEndTime());
            secKillItemResponse.setTimeSwap(timestamp + "");
        }

        AppSeckillDetailResponse productDetailResponse = new AppSeckillDetailResponse();
        productDetailResponse.setStoreSeckill(secKillItemResponse);

        if (storeProduct.getIsDel() == 1) {
            productDetailResponse.setMasterStatus("delete");
        } else if (!storeProduct.getIsShow()) {
            productDetailResponse.setMasterStatus("soldOut");
        } else if (storeProduct.getStock() <= 0) {
            productDetailResponse.setMasterStatus("sellOut");
        } else {
            productDetailResponse.setMasterStatus("normal");
        }

        List<AppProductAttrResponse> productAttr = BeanCopierUtil.copyList(
                productAttrDomains, AppProductAttrResponse.class);
        productDetailResponse.setProductAttr(productAttr);

        Map<String, Object> skuMap = new HashMap<>();
        for (int i = 0; i < productAttrValueDomains.size(); i++) {
            EbProductAttrValueDomain productAttrValueDomain = productAttrValueDomains.get(i);
            List<EbProductAttrValueDomain> attrValueDomains = seckillAttrValueDomains.stream().filter(
                    e -> productAttrValueDomain.getSuk().equals(e.getSuk())).collect(Collectors.toList());

            AppProductAttrValueResponse atr = CollUtil.isEmpty(attrValueDomains) ?
                    BeanCopierUtil.copy(productAttrValueDomain, AppProductAttrValueResponse.class):
                    BeanCopierUtil.copy(attrValueDomains.get(0), AppProductAttrValueResponse.class);
            skuMap.put(atr.getSuk(), atr);
        }
        productDetailResponse.setProductValue(skuMap);
        productDetailResponse.setUserCollect(productRelationService.isLike(
                secKillItemResponse.getProductId(), false));
        userVisitRecordService.addRecord(3);
        return productDetailResponse;
    }

    /**
     * 获取状态名
     *
     * @param status
     * @param startTime
     * @param endTime
     * @return
     */
    public String getStatusName(int status, int startTime, int endTime) {
        String statusName = null;
        int currentHour = DateUtils.getCurrentDate().getHours();
        if (status == 1 && currentHour < startTime) {
            statusName = "即将开始";
        } else if (status == 0) {
            statusName = "关闭";
        } else if (status == 1 && currentHour < endTime) {
            statusName = "进行中";
        } else if (status == 1 && currentHour >= endTime) {
            statusName = "已结束";
        }
        return statusName;
    }

    /**
     * 获取秒杀状态
     *
     * @param status
     * @param startTime
     * @param endTime
     * @return
     */
    public Integer getKillStatus(int status, int startTime, int endTime) {
        int hours = DateUtils.getCurrentDate().getHours();
        Integer killStatus = null;
        if (status == 1 && hours < startTime) {
            killStatus = 1;
        } else if (status == 0) {
            killStatus = 0;
        } else if (status == 1 && hours >= startTime
                && hours < endTime) {
            killStatus = 2;
        } else if (status == 1 && hours >= endTime) {
            killStatus = -1;
        }
        return killStatus;
    }
}
