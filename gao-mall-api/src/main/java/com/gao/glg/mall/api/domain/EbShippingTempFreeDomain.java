package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbShippingTemplatesFreeDomain
 * @author gaolinge
 */
@Data
public class EbShippingTempFreeDomain {
    /** 
     * 编号
     */
    private Integer id;

    /** 
     * 模板ID
     */
    private Integer tempId;

    /** 
     * 城市ID
     */
    private Integer cityId;

    /** 
     * 描述
     */
    private String title;

    /** 
     * 包邮件数
     */
    private BigDecimal number;

    /** 
     * 包邮金额
     */
    private BigDecimal price;

    /** 
     * 计费方式
     */
    private Integer type;

    /** 
     * 分组唯一值
     */
    private String uniqid;

    /** 
     * 是否无效
     */
    private Integer status;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
