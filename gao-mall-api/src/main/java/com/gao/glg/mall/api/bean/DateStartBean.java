package com.gao.glg.mall.api.bean;

import lombok.Data;

/**
 * 时间起始对象
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class DateStartBean {

    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
}
