package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempRegionRequest;
import com.gao.glg.mall.api.domain.EbShippingTempRegionDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbShippingTempRegionService {

    /**
     * 根据模板id和城市id查询模板区域
     * <p/>
     *
     * @param tempId
     * @param cityId
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    EbShippingTempRegionDomain getByTempIdAndCityId(Integer tempId, Integer cityId);

    /**
     * 批量插入
     * <p/>
     *
     * @param list
     * @param type
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    void batchSave(List<WebLogisticsTempRegionRequest> list, Integer type, Integer id);

    /**
     * 根据模板id查询模板区域
     * <p/>
     *
     * @param tempId
     * @return
     * @author : gaolinge
     * @date : 2024/12/11
     * @version : 1.0.0
     */
    List<EbShippingTempRegionDomain> groupByUnqid(Integer tempId);

}
