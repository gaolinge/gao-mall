package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.app.AppCouponReceiveRequest;
import com.gao.glg.mall.api.bean.request.web.WebCouponRequest;
import com.gao.glg.mall.api.bean.request.web.WebCouponSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppCouponOrderResponse;
import com.gao.glg.mall.api.bean.response.app.AppCouponResponse;
import com.gao.glg.mall.api.bean.response.web.WebCouponResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
public interface ICouponBusiness {

    /**
     * 优惠劵列表
     * <p/>
     *
     * @param type
     * @param productId
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    List<AppCouponResponse> list(int type, int productId, PagerDTO request);

    /**
     * 我的优惠劵
     * <p/>
     *
     * @param type
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    PagerInfo myList(String type, PagerDTO request);

    /**
     * 领取优惠劵
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    void receive(AppCouponReceiveRequest request);

    /**
     * 可用优惠劵
     * <p/>
     *
     * @param preOrderNo
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    List<AppCouponOrderResponse> canCoupon(String preOrderNo);

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    PagerInfo<WebCouponResponse> page(WebCouponSearchRequest request);

    /**
     * 新建
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    void create(WebCouponRequest request);

    /**
     * 更新状态
     * <p/>
     *
     * @param id
     * @param status
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    void updateStatus(Integer id, Integer status);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    void delete(Integer id);
}
