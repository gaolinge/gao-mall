package com.gao.glg.mall.api.bean.response.web;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/12
 */
@Data
public class WebValidateCodeResponse implements Serializable {

    @ApiModelProperty(value = "key")
    private String key;

    @ApiModelProperty(value = "code")
    private String code;
}
