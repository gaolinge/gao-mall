package com.gao.glg.mall.api.bean.request.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 砍价公共请求对象
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/22
 */
@Data
public class AppBargainRequest implements Serializable {

    @ApiModelProperty(value = "砍价商品ID", required = true)
    @NotNull(message = "砍价商品编号不能为空")
    private Integer bargainId;

    @ApiModelProperty(value = "用户砍价活动ID")
    private Integer bargainUserId;

    @ApiModelProperty(value = "用户砍价活动Uid")
    private Integer bargainUserUid;
}
