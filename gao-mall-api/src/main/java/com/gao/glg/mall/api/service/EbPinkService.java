package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbPinkDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbPinkService {

    /**
     * 获取前n个拼团商品
     * <p/>
     *
     * @param limit
     * @return
     * @author : gaolinge
     * @date : 2024/11/25
     * @version : 1.0.0
     */
    List<EbPinkDomain> findSizePink(int limit);

    /**
     * 获取总人数
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/25
     * @version : 1.0.0
     */
    Integer getTotalPeople();

}
