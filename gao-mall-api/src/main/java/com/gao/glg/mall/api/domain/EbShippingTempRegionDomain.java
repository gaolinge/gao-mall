package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbShippingTemplatesRegionDomain
 * @author gaolinge
 */
@Data
public class EbShippingTempRegionDomain {
    /** 
     * 编号
     */
    private Integer id;

    /** 
     * 模板ID
     */
    private Integer tempId;

    /** 
     * 城市ID
     */
    private Integer cityId;

    /** 
     * 描述
     */
    private String title;

    /** 
     * 首件
     */
    private BigDecimal first;

    /** 
     * 首件运费
     */
    private BigDecimal firstPrice;

    /** 
     * 续件
     */
    private BigDecimal renewal;

    /** 
     * 续件运费
     */
    private BigDecimal renewalPrice;

    /** 
     * 计费方式 1按件数 2按重量 3按体积
     */
    private Integer type;

    /** 
     * 分组唯一值
     */
    private String uniqid;

    /** 
     * 是否无效
     */
    private Integer status;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
