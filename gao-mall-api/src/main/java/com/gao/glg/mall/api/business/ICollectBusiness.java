package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.app.AppUserCollectAllRequest;
import com.gao.glg.mall.api.bean.request.app.AppUserCollectRequest;
import com.gao.glg.mall.api.bean.response.app.AppUserCollectResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
public interface ICollectBusiness {

    /**
     * 添加收藏
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void add(AppUserCollectRequest request);

    /**
     * 我的收藏列表
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    PagerInfo<AppUserCollectResponse> list(PagerDTO request);

    /**
     * 取消收藏
     * <p/>
     *
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void canal(Integer pid);

    /**
     * 批量删除
     * <p/>
     *
     * @param map
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void delete(Map<String, String> map);

    /**
     * 批量添加
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void batchAdd(AppUserCollectAllRequest request);

}
