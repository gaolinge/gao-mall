package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbStoreCartDomain
 * @author gaolinge
 */
@Data
public class EbCartDomain {
    /** 
     * 购物车表ID
     */
    private Long id;

    /** 
     * 用户ID
     */
    private Integer uid;

    /** 
     * 类型
     */
    private String type;

    /** 
     * 商品ID
     */
    private Integer productId;

    /** 
     * 商品属性
     */
    private String productAttrUnique;

    /** 
     * 商品数量
     */
    private Integer cartNum;

    /** 
     * 是否为立即购买
     */
    private Boolean isNew;

    /** 
     * 拼团id
     */
    private Integer combinationId;

    /** 
     * 秒杀商品ID
     */
    private Integer seckillId;

    /** 
     * 砍价id
     */
    private Integer bargainId;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * g
     */
    private Date updateTime;

    /** 
     * 购物车状态
     */
    private Boolean status;


}
