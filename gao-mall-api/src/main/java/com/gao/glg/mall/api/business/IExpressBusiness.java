package com.gao.glg.mall.api.business;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.mall.api.bean.request.web.WebExpressSearchRequest;
import com.gao.glg.mall.api.bean.request.web.WebExpressUpdateRequest;
import com.gao.glg.mall.api.bean.response.web.WebExpressResponse;
import com.gao.glg.page.PagerInfo;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
public interface IExpressBusiness {

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    PagerInfo<WebExpressResponse> page(WebExpressSearchRequest request);

    /**
     * 同步快递
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void syncExpress();

    /**
     *
     * <p/>
     *
     * @param no
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    JSONObject template(String no);

    /**
     * 全部
     * <p/>
     *
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    List<WebExpressResponse> all(String type);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    WebExpressResponse detail(Integer id);

    /**
     * 更新
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void update(WebExpressUpdateRequest request);

    /**
     * 更新
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void updateShow(WebExpressUpdateRequest request);
}
