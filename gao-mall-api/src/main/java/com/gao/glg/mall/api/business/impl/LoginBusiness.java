package com.gao.glg.mall.api.business.impl;

import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.app.AppLoginRequest;
import com.gao.glg.mall.api.bean.request.web.WebLoginRequest;
import com.gao.glg.mall.api.bean.response.app.AppLoginResponse;
import com.gao.glg.mall.api.bean.response.web.WebLoginResponse;
import com.gao.glg.mall.api.business.ILoginBusiness;
import com.gao.glg.mall.api.cache.TokenCache;
import com.gao.glg.mall.api.cache.ValidateCodeCache;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.domain.EbSystemAdminDomain;
import com.gao.glg.mall.api.domain.EbUserDomain;
import com.gao.glg.mall.api.service.EbSystemAdminService;
import com.gao.glg.mall.api.service.EbUserService;
import com.gao.glg.mall.api.utils.CrmebUtil;
import com.gao.glg.mall.api.utils.MallUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;


@Service
public class LoginBusiness implements ILoginBusiness {

    @Autowired
    private ValidateCodeCache validateCodeCache;
    @Autowired
    private TokenCache tokenCache;
    @Autowired
    private EbUserService userService;
    @Autowired
    private EbSystemAdminService systemAdminService;


    @Override
    public AppLoginResponse appLogin(AppLoginRequest request) {
        EbUserDomain userDomain = userService.getByAccount(request.getAccount());
        if (userDomain == null || userDomain.getStatus() == 0 ||
                MallUtil.checkPassword(request, userDomain)) {
            throw new BusinessException(ErrorConstant.ACCOUNT_PASSWORD_INCORRECT);
        }

        //绑定推广关系
        /*if (request.getSpreadPid() > 0) {
            bindSpread(userDomain, request.getSpreadPid());
        }*/
        userService.updateLastLoginTime(userDomain.getUid());
        String token = tokenCache.createAppToken(userDomain);
        AppLoginResponse response = new AppLoginResponse();
        response.setToken(token);
        response.setUid(userDomain.getUid());
        response.setNikeName(userDomain.getNickname());
        response.setPhone(userDomain.getPhone());
        return response;
    }

    @Override
    public void logout(int type) {
        tokenCache.deleteToken(type);
    }

    @Override
    public WebLoginResponse webLogin(WebLoginRequest request) {
        String value = validateCodeCache.get(request.getKey());
        if (value == null || !value.toLowerCase().equals(request.getCode())) {
            throw new BusinessException(ErrorConstant.VERIFICATION_CODE_ERROR);
        }

        EbSystemAdminDomain domain = systemAdminService.getByAccount(request.getAccount());
        if (domain == null || domain.getStatus() == 0) {
            throw new BusinessException(ErrorConstant.ACCOUNT_PASSWORD_INCORRECT);
        }

        HttpServletRequest servletRequest = ((ServletRequestAttributes)
                RequestContextHolder.getRequestAttributes()).getRequest();
        String ip = CrmebUtil.getClientIp(servletRequest);
        String token = tokenCache.createWebToken(domain.getAccount());

        EbSystemAdminDomain adminDomain = new EbSystemAdminDomain();
        adminDomain.setId(domain.getId());
        adminDomain.setLastIp(ip);
        systemAdminService.update(adminDomain);

        WebLoginResponse response = new WebLoginResponse();
        response.setAccount(domain.getAccount());
        response.setRealName(domain.getRealName());
        response.setToken(token);
        return response;
    }

}
