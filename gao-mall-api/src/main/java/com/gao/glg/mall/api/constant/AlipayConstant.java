package com.gao.glg.mall.api.constant;

/**
 * 支付宝配置
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
public interface AlipayConstant {

    // 商户appid
    String APPID = "ali_pay_appid";

    // 私钥 pkcs8格式的
    String RSA_PRIVATE_KEY = "ali_pay_private_key";

    // 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    String notify_url = "ali_pay_notifu_url";

    // 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
    String return_url = "ali_pay_return_url";

    // 用户付款中途退出返回商户网站的地址
    String quit_url = "ali_pay_quit_url";

    // 请求网关地址
    String URL = "https://openapi.alipay.com/gateway.do";

    // 编码
    String CHARSET = "UTF-8";

    // 返回格式
    String FORMAT = "json";

    // 支付宝公钥
    String ALIPAY_PUBLIC_KEY_2 = "ali_pay_public_key2";
    String ALIPAY_PUBLIC_KEY = "ali_pay_public_key";
    // 日志记录目录
    String LOG_PATH = "/log";
    // RSA2
    String SIGNTYPE = "RSA2";

    // 是否开启支付宝支付
    String ALIPAY_IS_OPEN = "ali_pay_is_open";
}
