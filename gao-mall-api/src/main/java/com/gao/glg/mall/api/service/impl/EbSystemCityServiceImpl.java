package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemCityDomain;
import com.gao.glg.mall.api.mapper.EbSystemCityMapper;
import com.gao.glg.mall.api.service.EbSystemCityService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemCityServiceImpl implements EbSystemCityService  {
    @Resource
    private EbSystemCityMapper systemCityMapper;

    @Override
    public List<EbSystemCityDomain> select() {
        return systemCityMapper.select();
    }

    @Override
    public EbSystemCityDomain getByCityName(String cityName) {
        return systemCityMapper.getByCityName(cityName);
    }

    @Override
    public EbSystemCityDomain getByCityId(Integer cityId) {
        return systemCityMapper.getByCityId(cityId);
    }

    @Override
    public List<Integer> selectCityIds() {
        return systemCityMapper.selectCityIds();
    }
}
