package com.gao.glg.mall.api.business.impl;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.mall.api.bean.request.web.WebGroupDataRequest;
import com.gao.glg.mall.api.bean.request.web.WebGroupDataSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebGroupDataResponse;
import com.gao.glg.mall.api.business.ICdnBusiness;
import com.gao.glg.mall.api.business.ICombDataBusiness;
import com.gao.glg.mall.api.domain.EbSystemGroupDataDomain;
import com.gao.glg.mall.api.service.EbSystemFormTempService;
import com.gao.glg.mall.api.service.EbSystemGroupDataService;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CombDataBusiness implements ICombDataBusiness {

    @Autowired
    private ICdnBusiness cdnBusiness;
    @Autowired
    private EbSystemGroupDataService systemGroupDataService;
    @Autowired
    private EbSystemFormTempService systemFormTempService;

    @Override
    public PagerInfo<WebGroupDataResponse> page(WebGroupDataSearchRequest request) {
        return PagerUtil.page(request, () -> {
            List<EbSystemGroupDataDomain> domains = systemGroupDataService.page(request);
            return BeanCopierUtil.copyList(domains, WebGroupDataResponse.class);
        });
    }

    @Override
    public void create(WebGroupDataRequest request) {
        systemFormTempService.check(request.getForm());
        String value = JSONObject.toJSONString(request.getForm());
        value = cdnBusiness.clearPrefix(value);

        EbSystemGroupDataDomain domain = new EbSystemGroupDataDomain();
        domain.setGid(request.getGid());
        domain.setValue(value);
        domain.setSort(request.getForm().getSort());
        domain.setStatus(request.getForm().getStatus());
        systemGroupDataService.save(domain);
    }

    @Override
    public void delete(Integer id) {
        systemGroupDataService.delete(id);
    }

    @Override
    public void update(Integer id, WebGroupDataRequest request) {
        systemFormTempService.check(request.getForm());
        String value = JSONObject.toJSONString(request.getForm());
        value = cdnBusiness.clearPrefix(value);

        EbSystemGroupDataDomain domain = new EbSystemGroupDataDomain();
        domain.setId(id);
        domain.setGid(request.getGid());
        domain.setValue(value);
        domain.setSort(request.getForm().getSort());
        domain.setStatus(request.getForm().getStatus());
        systemGroupDataService.update(domain);
    }

    @Override
    public WebGroupDataResponse detail(Integer id) {
        EbSystemGroupDataDomain domain = systemGroupDataService.getById(id);
        return BeanCopierUtil.copy(domain, WebGroupDataResponse.class);
    }
}