package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbBargainDomain;
import com.gao.glg.mall.api.mapper.EbBargainMapper;
import com.gao.glg.mall.api.service.EbBargainService;

import java.util.Collections;
import java.util.List;

@Service
public class EbBargainServiceImpl implements EbBargainService {

    @Resource
    private EbBargainMapper bargainMapper;

    @Override
    public List<EbBargainDomain> listByInStock() {
        return bargainMapper.listByInStock();
    }

    @Override
    public List<EbBargainDomain> listByProductId(Integer productId) {
        return bargainMapper.listByProductId(productId);
    }
}
