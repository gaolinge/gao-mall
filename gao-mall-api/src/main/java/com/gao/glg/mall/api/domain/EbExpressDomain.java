package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbExpressDomain
 * @author gaolinge
 */
@Data
public class EbExpressDomain {
    /** 
     * 快递公司id
     */
    private Integer id;

    /** 
     * 快递公司简称
     */
    private String code;

    /** 
     * 快递公司全称
     */
    private String name;

    /** 
     * 是否需要月结账号
     */
    private Boolean partnerId;

    /** 
     * 是否需要月结密码
     */
    private Boolean partnerKey;

    /** 
     * 是否需要取件网店
     */
    private Boolean net;

    /** 
     * 账号
     */
    private String account;

    /** 
     * 密码
     */
    private String password;

    /** 
     * 网点名称
     */
    private String netName;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 是否显示
     */
    private Boolean isShow;

    /** 
     * 是否可用
     */
    private Boolean status;


}
