package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbUserVisitRecordDomain
 * @author gaolinge
 */
@Data
public class EbUserVisitRecordDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 日期
     */
    private String date;

    /** 
     * 用户uid
     */
    private Integer uid;

    /** 
     * 访问类型
     */
    private Integer visitType;


}
