package com.gao.glg.mall.api.business;


import com.gao.glg.mall.api.bean.response.app.AppFileResultResponse;
import com.gao.glg.mall.api.bean.response.ImageUploadResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/7/3
 */
public interface IFileBusiness {

    /**
     * 上传
     * <p/>
     *
     * @param file
     * @return
     * @author : gaolinge
     * @date : 2024/7/3
     * @version : 1.0.0
     */
    ImageUploadResponse uploadImage(MultipartFile file) throws IOException;

    /**
     * 上传
     * <p/>
     *
     * @param file
     * @return
     * @author : gaolinge
     * @date : 2024/7/3
     * @version : 1.0.0
     */
    String uploadImag(MultipartFile file) throws IOException;

    /**
     * 删除
     * <p/>
     *
     * @param fileName
     * @return
     * @author : gaolinge
     * @date : 2024/7/3
     * @version : 1.0.0
     */
    void deleteFile(String fileName) throws IOException;

    /**
     * 构建路径
     * <p/>
     *
     * @param fileName
     * @return
     * @author : gaolinge
     * @date : 2024/7/6
     * @version : 1.0.0
     */
    String buildPath(String fileName);

    /**
     * 构建路径
     * <p/>
     *
     * @param fileName
     * @return
     * @author : gaolinge
     * @date : 2024/7/6
     * @version : 1.0.0
     */
    String buildPath(String fileName, String suffix);

    /**
     *
     * <p/>
     *
     * @param path
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    String prefixFile(String path);

    /**
     *
     * <p/>
     *
     * @param data
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    String prefixImage(String data);

    /**
     * 图片上传
     * <p/>
     *
     * @param multipart
     * @param model
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    AppFileResultResponse imageUpload(MultipartFile multipart,
                                      String model, Integer pid);
}
