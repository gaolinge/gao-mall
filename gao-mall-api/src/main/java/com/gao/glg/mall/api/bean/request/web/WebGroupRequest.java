package com.gao.glg.mall.api.bean.request.web;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

/**
 * 组合
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class WebGroupRequest implements Serializable {

    @ApiModelProperty(value = "数据组名称")
    @Length(max = 50, message = "数据组名称长度不能超过50个字符")
    private String name;

    @ApiModelProperty(value = "简介")
    @Length(max = 256, message = "数据组名称长度不能超过256个字符")
    private String info;

    @ApiModelProperty(value = "form 表单 id")
    private Integer formId;

}
