package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbShippingTempRegionDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbShippingTempRegionMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 删除
     */
    void deleteByTempId(Integer tempId);

    /**
     * 保存
     */
    int save(EbShippingTempRegionDomain domain);

    /**
     * 查询
     */
    List<EbShippingTempRegionDomain> select();

    /**
     * 更新
     */
    int update(EbShippingTempRegionDomain domain);

    /**
     * 查询
     */
    EbShippingTempRegionDomain getByTempIdAndCityId(@Param("tempId") Integer tempId, @Param("cityId") Integer cityId);

    /**
     * 批量保存
     */
    void batchSave(@Param("list") List<EbShippingTempRegionDomain> domains);

    /**
     * 查询
     */
    List<EbShippingTempRegionDomain> groupByUniqid(Integer tempId);
}
