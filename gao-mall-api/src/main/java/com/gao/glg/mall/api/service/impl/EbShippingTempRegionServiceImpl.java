package com.gao.glg.mall.api.service.impl;
import com.gao.glg.enums.YesNoEnum;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempRegionRequest;
import com.gao.glg.mall.api.domain.EbShippingTempRegionDomain;
import com.gao.glg.mall.api.service.EbSystemCityService;
import com.gao.glg.mall.api.utils.CrmebUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbShippingTempRegionMapper;
import com.gao.glg.mall.api.service.EbShippingTempRegionService;

import java.util.ArrayList;
import java.util.List;


@Service
public class EbShippingTempRegionServiceImpl implements EbShippingTempRegionService {
    @Autowired
    private EbSystemCityService systemCityService;
    @Resource
    private EbShippingTempRegionMapper shippingTempRegionMapper;

    @Override
    public EbShippingTempRegionDomain getByTempIdAndCityId(Integer tempId, Integer cityId) {
        return shippingTempRegionMapper.getByTempIdAndCityId(tempId, cityId);
    }

    @Override
    public void batchSave(List<WebLogisticsTempRegionRequest> list, Integer type, Integer tempId) {
        List<EbShippingTempRegionDomain> domains = new ArrayList<>();
        for (WebLogisticsTempRegionRequest regionRequest : list) {
            List<Integer> cityIdList =
                    "all".equals(regionRequest.getCityId()) || "0".equals(regionRequest.getCityId()) ?
                    systemCityService.selectCityIds() : CrmebUtil.stringToArray(regionRequest.getCityId());

            String uniqueKey = DigestUtils.md5Hex(regionRequest.toString());
            cityIdList.forEach(cityId -> {
                EbShippingTempRegionDomain domain = new EbShippingTempRegionDomain();
                domain.setCityId(cityId);
                domain.setTitle(regionRequest.getTitle());
                domain.setUniqid(uniqueKey);
                domain.setRenewal(regionRequest.getRenewal());
                domain.setRenewalPrice(regionRequest.getRenewalPrice());
                domain.setFirst(regionRequest.getFirst());
                domain.setFirstPrice(regionRequest.getFirstPrice());
                domain.setTempId(tempId);
                domain.setType(type);
                domain.setStatus(YesNoEnum.YES.getValue());
                domains.add(domain);
            });
        }
        shippingTempRegionMapper.deleteByTempId(tempId);
        shippingTempRegionMapper.batchSave(domains);
    }

    @Override
    public List<EbShippingTempRegionDomain> groupByUnqid(Integer tempId) {
        return shippingTempRegionMapper.groupByUniqid(tempId);
    }
}
