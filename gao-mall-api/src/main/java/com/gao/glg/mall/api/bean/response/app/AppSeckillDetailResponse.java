package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 商品秒杀详情
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/3
 */
@Data
public class AppSeckillDetailResponse implements Serializable {

    @ApiModelProperty(value = "产品属性")
    private List<AppProductAttrResponse> productAttr;

    @ApiModelProperty(value = "商品属性详情")
    private Map<String,Object> productValue;

    @ApiModelProperty(value = "返佣金额区间")
    private String priceName;

    @ApiModelProperty(value = "收藏标识")
    private Boolean userCollect;

    @ApiModelProperty(value = "秒杀商品信息")
    private AppSecKillItemResponse storeSeckill;

    @ApiModelProperty(value = "主商品状态:normal-正常，sellOut-售罄，soldOut-下架,delete-删除")
    private String masterStatus;
}
