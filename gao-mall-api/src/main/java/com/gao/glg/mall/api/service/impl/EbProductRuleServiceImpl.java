package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.request.web.WebProductRuleSearchRequest;
import com.gao.glg.mall.api.domain.EbProductRuleDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductRuleMapper;
import com.gao.glg.mall.api.service.EbProductRuleService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbProductRuleServiceImpl implements EbProductRuleService {
    @Resource
    private EbProductRuleMapper productRuleMapper;

    @Override
    public List<EbProductRuleDomain> page(WebProductRuleSearchRequest request) {
        return productRuleMapper.page(request);
    }

    @Override
    public EbProductRuleDomain getByRuleName(String ruleName) {
        return productRuleMapper.getByRuleName(ruleName);
    }

    @Override
    public void save(EbProductRuleDomain domain) {
        productRuleMapper.save(domain);
    }

    @Override
    public void deletes(List<Integer> ids) {
        productRuleMapper.deletes(ids);
    }

    @Override
    public void update(EbProductRuleDomain domain) {
        productRuleMapper.update(domain);
    }

    @Override
    public EbProductRuleDomain getById(Integer id) {
        return productRuleMapper.getById(id);
    }
}
