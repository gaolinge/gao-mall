package com.gao.glg.mall.api.bean.request.web;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 运费模版
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class WebLogisticsTempRequest implements Serializable {

    @ApiModelProperty(value = "模板名称", required = true)
    @NotBlank(message = "模板名称必须填写")
    @Length(max = 200, message = "模板名称不能超过200个字符")
    private String name;

    @ApiModelProperty(value = "计费方式 1(按件数), 2(按重量)，3(按体积)", example = "1", required = true)
    @NotNull(message = "计费方式必须选择")
    @Range(min = 1, max = 3, message = "计费方式选择区间 1(按件数), 2(按重量)，3(按体积)")
    private Integer type;

    @ApiModelProperty(value = "指定包邮", example = "1", required = true)
    @NotNull(message = "指定包邮必须选择")
    private Boolean appoint;

    @ApiModelProperty(value = "排序", example = "0")
    @NotNull(message = "排序数字必须填写")
    @Min(value = 0, message = "排序最小为0")
    private Integer sort;

    @ApiModelProperty(value = "配送区域及运费", required = true)
    private List<WebLogisticsTempRegionRequest> shippingTemplatesRegionRequestList;

    @ApiModelProperty(value = "指定包邮设置", required = true)
    private List<WebLogisticsTempFreeRequest> shippingTemplatesFreeRequestList;

}
