package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbOrderInfoDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbOrderInfoService {

    /**
     * 根据订单id查询订单信息
     * <p/>
     *
     * @param orderId
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    List<EbOrderInfoDomain> listByOrderId(Integer orderId);
}
