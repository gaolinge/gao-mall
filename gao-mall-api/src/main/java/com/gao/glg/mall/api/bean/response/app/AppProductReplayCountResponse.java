package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 商品评价数量和好评度
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/29
 */
@Data
public class AppProductReplayCountResponse implements Serializable {

    @ApiModelProperty(value = "评论总数")
    private Long sumCount;

    @ApiModelProperty(value = "好评总数")
    private Long goodCount;

    @ApiModelProperty(value = "中评总数")
    private Long inCount;

    @ApiModelProperty(value = "差评总数")
    private Long poorCount;

    @ApiModelProperty(value = "好评率")
    private String replyChance;

    @ApiModelProperty(value = "评分星数")
    private Integer replyStar;
}
