package com.gao.glg.mall.api.bean.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
@Data
public class SystemFormDTO implements Serializable {

    @ApiModelProperty(value = "表单名称")
    private String formRef;

    @ApiModelProperty(value = "form对象")
    private String formModel;

    @ApiModelProperty(value = "大小")
    private String size;

    @ApiModelProperty(value = "label位置")
    private String labelPosition;

    @ApiModelProperty(value = "label宽度")
    private String labelWidth;

    @ApiModelProperty(value = "form规则")
    private String formRules;

    @ApiModelProperty(value = "")
    private String gutter;

    @ApiModelProperty(value = "是否禁用")
    private String disabled;

    @ApiModelProperty(value = "span")
    private String span;

    @ApiModelProperty(value = "button")
    private String formBtns;

    @ApiModelProperty(value = "字段值列表")
    private List<String> fields;
}
