package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbStoreProductCouponDomain
 * @author gaolinge
 */
@Data
public class EbProductCouponDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 商品id
     */
    private Integer productId;

    /** 
     * 优惠劵id
     */
    private Integer issueCouponId;

    /** 
     * 添加时间
     */
    private Integer addTime;


}
