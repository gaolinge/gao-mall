package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.service.EbUserService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserVisitRecordDomain;
import com.gao.glg.mall.api.mapper.EbUserVisitRecordMapper;
import com.gao.glg.mall.api.service.EbUserVisitRecordService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserVisitRecordServiceImpl implements EbUserVisitRecordService  {
    @Autowired
    private EbUserService userService;
    @Resource
    private EbUserVisitRecordMapper userVisitRecordMapper;

    @Override
    public void addRecord(int type) {
        if (userService.getUserId() !=null) {
            String time = new DateTime().toString("yyyy-MM-dd");
            EbUserVisitRecordDomain domain = new EbUserVisitRecordDomain();
            domain.setDate(time);
            domain.setUid(userService.getUserId());
            domain.setVisitType(type);
            userVisitRecordMapper.save(domain);
        }
    }
}
