package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbUserAddressDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbUserAddressService {

    /**
     * 地址列表
     * <p/>
     *
     * @param uid
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    List<EbUserAddressDomain> listByUid(Integer uid);

    /**
     * 取消默认
     * <p/>
     *
     * @param uid
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    void canalDefault(Integer uid);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    void save(EbUserAddressDomain domain);

    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    void update(EbUserAddressDomain domain);

    /**
     * 根据id获取
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    EbUserAddressDomain getByUidAndId(Integer uid, Integer id);

    /**
     * 获取默认地址
     * <p/>
     *
     * @param uid
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    EbUserAddressDomain getByDefault(Integer uid);

    /**
     * 根据id获取
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    EbUserAddressDomain getById(Integer id);

}
