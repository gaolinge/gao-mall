package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.CategoryTreeResponse;
import com.gao.glg.mall.api.bean.response.web.WebCateSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebCategoryRequest;
import com.gao.glg.mall.api.domain.EbCategoryDomain;
import com.gao.glg.page.PagerInfo;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/6
 */
public interface ICategoryBusiness {

    /**
     * 分类树
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    List<CategoryTreeResponse> tree();
    /**
     * 分类树
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    List<CategoryTreeResponse> tree(Integer type, String name, Integer status);

    /**
     * 分页分类列表
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    PagerInfo<EbCategoryDomain> list(WebCateSearchRequest request);

    /**
     * 新增
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    void save(WebCategoryRequest request);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 修改
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    void update(Integer id, WebCategoryRequest request);

    /**
     * 根据id集合获取分类列表
     * <p/>
     *
     * @param ids
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    List<EbCategoryDomain> lisByIds(String ids);

    /**
     * 修改状态
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    void updateStatus(Integer id);

    /**
     * 根据id获取分类
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    EbCategoryDomain info(Integer id);
}
