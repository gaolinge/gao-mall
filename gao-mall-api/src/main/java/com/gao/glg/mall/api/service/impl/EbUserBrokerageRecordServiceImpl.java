package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserBrokerageRecordDomain;
import com.gao.glg.mall.api.mapper.EbUserBrokerageRecordMapper;
import com.gao.glg.mall.api.service.EbUserBrokerageRecordService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserBrokerageRecordServiceImpl implements EbUserBrokerageRecordService  {
    @Resource
    private EbUserBrokerageRecordMapper ebUserBrokerageRecordMapper;
}
