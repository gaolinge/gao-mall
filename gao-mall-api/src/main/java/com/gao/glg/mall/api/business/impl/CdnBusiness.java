package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.business.ICdnBusiness;
import com.gao.glg.mall.api.service.EbSystemConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CdnBusiness implements ICdnBusiness {

    @Autowired
    private EbSystemConfigService systemConfigService;

    @Override
    public String clearPrefix(String path) {
        if(StringUtils.isBlank(path)){
            return path;
        }
        String url = getUrl() + "/";
        if(path.contains(url)){
            return path.replace(url, "");
        }
        return path;
    }

    @Override
    public String getUrl() {
        String uploadType = systemConfigService.getValueByKey("uploadType");
        int type = Integer.parseInt(uploadType);
        String pre = "local";
        switch (type) {
            case 2:
                pre = "qn";
                break;
            case 3:
                pre = "al";
                break;
            case 4:
                pre = "tx";
                break;
            default:
                break;
        }
        return systemConfigService.getValueByKey(pre + "UploadUrl");
    }
}
