package com.gao.glg.mall.api.domain;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreCombinationDomain
 * @author gaolinge
 */
@Data
public class EbCombinationDomain {
    /** 
     * 拼团商品ID
     */
    private Integer id;

    /** 
     * 商品id
     */
    private Integer productId;

    /** 
     * 商户id
     */
    private Integer merId;

    /** 
     * 推荐图
     */
    private String image;

    /** 
     * 轮播图
     */
    private String images;

    /** 
     * 活动标题
     */
    private String title;

    /** 
     * 活动属性
     */
    private String attr;

    /** 
     * 参团人数
     */
    private Integer people;

    /** 
     * 简介
     */
    private String info;

    /** 
     * 价格
     */
    private BigDecimal price;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 销量
     */
    private Integer sales;

    /** 
     * 库存
     */
    private Integer stock;

    /** 
     * 添加时间
     */
    private Long addTime;

    /** 
     * 推荐
     */
    private Byte isHost;

    /** 
     * 商品状态
     */
    private Byte isShow;

    /** 
     * 
     */
    private Byte isDel;

    /** 
     * 
     */
    private Byte combination;

    /** 
     * 商户是否可用1可用0不可用
     */
    private Byte merUse;

    /** 
     * 是否包邮1是0否
     */
    private Byte isPostage;

    /** 
     * 邮费
     */
    private BigDecimal postage;

    /** 
     * 拼团开始时间
     */
    private Long startTime;

    /** 
     * 拼团结束时间
     */
    private Long stopTime;

    /** 
     * 拼团订单有效时间(小时)
     */
    private Integer effectiveTime;

    /** 
     * 拼图商品成本
     */
    private BigDecimal cost;

    /** 
     * 浏览量
     */
    private Integer browse;

    /** 
     * 单位名
     */
    private String unitName;

    /** 
     * 运费模板ID
     */
    private Integer tempId;

    /** 
     * 重量
     */
    private BigDecimal weight;

    /** 
     * 体积
     */
    private BigDecimal volume;

    /** 
     * 单次购买数量
     */
    private Integer num;

    /** 
     * 限购总数
     */
    private Integer quota;

    /** 
     * 限量总数显示
     */
    private Integer quotaShow;

    /** 
     * 原价
     */
    private BigDecimal otPrice;

    /** 
     * 每个订单可购买数量
     */
    private Integer onceNum;

    /** 
     * 虚拟成团百分比
     */
    private Integer virtualRation;


}
