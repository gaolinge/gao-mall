package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbUserIntegralRecordDomain;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbUserIntegralRecordService {

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/9
     * @version : 1.0.0
     */
    void save(EbUserIntegralRecordDomain domain);

}
