package com.gao.glg.mall.api.utils;

import java.util.function.Supplier;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/7
 */
public class Expression {


    /**
     * 有结果的表达式
     * <p/>
     *
     * @param supplier
     * @param consumer1
     * @param consumer2
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    public static Object result(boolean flag, Supplier consumer1, Supplier consumer2){
        if(flag){
            return consumer1.get();
        }else{
            return consumer2.get();
        }
    }

    /**
     * 无结果的表达式
     * <p/>
     *
     * @param flag
     * @param consumer1
     * @param consumer2
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    public static void noResult(boolean flag, Consumer consumer1, Consumer consumer2){
        if(flag){
            consumer1.get();
        }else{
            consumer2.get();
        }
    }

    @FunctionalInterface
    public interface Consumer  {

        /**
         * Gets a result.
         *
         * @return a result
         */
        void get();
    }
}
