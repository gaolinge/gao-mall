package com.gao.glg.mall.api.utils;

import cn.hutool.crypto.symmetric.DES;
import com.gao.glg.mall.api.bean.request.app.AppLoginRequest;
import com.gao.glg.mall.api.domain.EbUserDomain;
import com.gao.glg.utils.DateUtils;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/4
 */
public class MallUtil {

    public static Long timestamp(){
        return new Date().getTime() / 1000;
    }

    public static Long timestamp(Integer time){
        return DateUtils.formatToDate(new Date(), "yyyy-MM-dd " + time + ":00:00").getTime() / 1000;
    }

    public static String encrypt(String pwd, String key) {
        byte[] secretKey = secretKey(key);
        DES des = new DES(secretKey);
        byte[] result = des.encrypt(pwd);
        return cn.hutool.core.codec.Base64.encode(result);
    }

    public static String decrypt(String pwd, String key) {
        byte[] secretKey = secretKey(key);
        DES des = new DES(secretKey);
        return des.decryptStr(pwd);
    }

    public static boolean checkPassword(AppLoginRequest request, EbUserDomain domain) {
        byte[] secretKey = secretKey(domain.getAccount());
        DES des = new DES(secretKey);
        byte[] result = des.encrypt(request.getPassword());
        return Base64.encode(result).equals(request.getPassword());
    }

    public static void main(String[] args) {
        byte[] secretKey = secretKey("18292417675");
        DES des = new DES(secretKey);
        byte[] result = des.decrypt("f6mcpGQ8NEmwbab2TlkpUg==");
        System.out.println(new String(result, StandardCharsets.UTF_8));
    }

    /**
     * 获得DES加密秘钥
     * @param key
     * @return
     */
    public static byte[] secretKey(String key) {
        byte[] result = new byte[8];
        byte[] keys = null;
        keys = key.getBytes(StandardCharsets.UTF_8);
        for(int i = 0; i<8;i++){
            if(i < keys.length){
                result[i] = keys[i];
            }else{
                result[i] = 0x01;
            }
        }
        return result;
    }

}
