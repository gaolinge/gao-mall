package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.app.AppProductRequest;
import com.gao.glg.mall.api.bean.response.app.AppIndexProductResponse;
import com.gao.glg.mall.api.bean.response.app.AppProductDetailResponse;
import com.gao.glg.mall.api.bean.response.app.AppProductReplayCountResponse;
import com.gao.glg.mall.api.bean.response.app.AppProductReplyDetailResponse;
import com.gao.glg.mall.api.business.IProductBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 商品
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController("ProductController")
@RequestMapping("/app")
@Api(tags = "商品")
public class AppProductController {

    @Autowired
    private IProductBusiness productBusiness;

    @ApiOperation(value = "商品列表")
    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public RestResult<PagerInfo<AppIndexProductResponse>> list(@Validated AppProductRequest request) {
        return RestResult.success(productBusiness.appList(request));
    }

    @ApiOperation(value = "优选商品推荐")
    @RequestMapping(value = "/product/good", method = RequestMethod.GET)
    public RestResult<PagerInfo<AppIndexProductResponse>> getGoodProductList() {
        return RestResult.success(productBusiness.recProduct());
    }

    @ApiOperation(value = "热门商品推荐")
    @RequestMapping(value = "/product/hot", method = RequestMethod.GET)
    public RestResult<PagerInfo<AppIndexProductResponse>> getHotProductList(@Validated PagerDTO request) {
        return RestResult.success(productBusiness.hotRecall(request));
    }

    @ApiOperation(value = "商品详情")
    @RequestMapping(value = "/product/detail/{id}", method = RequestMethod.GET)
    public RestResult<AppProductDetailResponse> detail(@PathVariable Integer id,
                                                       @RequestParam(value = "type", defaultValue = "normal") String type) {
        return RestResult.success(productBusiness.detail(id, type));
    }

    @ApiOperation(value = "商品评论数量")
    @RequestMapping(value = "/reply/config/{id}", method = RequestMethod.GET)
    public RestResult<AppProductReplayCountResponse> replyCount(@PathVariable Integer id) {
        return RestResult.success(productBusiness.replyCount(id));
    }

    @ApiOperation(value = "商品详情评论")
    @RequestMapping(value = "/reply/product/{id}", method = RequestMethod.GET)
    public RestResult<AppProductReplyDetailResponse> productReply(@PathVariable Integer id) {
        return RestResult.success(productBusiness.productReply(id));
    }



    /*







    @ApiOperation(value = "商品评论列表")
    @RequestMapping(value = "/reply/list/{id}", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "评价等级|0=全部,1=好评,2=中评,3=差评", allowableValues = "range[0,1,2,3]")
    public RestResult<PagerInfo<ProductReplyResponse>> getReplyList(@PathVariable Integer id,
                                                                       @RequestParam(value = "type") Integer type, @Validated PagerDTO PagerDTO) {
        return RestResult.success(productBusiness.restPage(pro.getReplyList(id, type, PagerDTO)));
    }

    @ApiOperation(value = "商品列表(个别分类模型使用)")
    @RequestMapping(value = "/product/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<IndexProductResponse>> getProductList(@Validated ProductListRequest request, @Validated PagerDTO PagerDTO) {
        return RestResult.success(productBusiness.getCategoryProductList(request, PagerDTO));
    }

    @ApiOperation(value = "商品规格详情")
    @RequestMapping(value = "/product/sku/detail/{id}", method = RequestMethod.GET)
    public RestResult<ProductDetailResponse> getSkuDetail(@PathVariable Integer id) {
        return RestResult.success(productBusiness.getSkuDetail(id));
    }


    @ApiOperation(value = "商品排行榜")
    @RequestMapping(value = "/product/leaderboard", method = RequestMethod.GET)
    public RestResult<List<StoreProduct>> getLeaderboard() {
        return RestResult.success(productBusiness.getLeaderboard());
    }*/
}



