package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbUserGroupDomain
 * @author gaolinge
 */
@Data
public class EbUserGroupDomain {
    /** 
     * 
     */
    private Short id;

    /** 
     * 用户分组名称
     */
    private String groupName;


}
