package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemFormTempDomain;

import javax.validation.constraints.Min;
import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemFormTempMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemFormTempDomain domain);

    /**
     * 查询
     */
    List<EbSystemFormTempDomain> select();

    /**
     * 更新
     */
    int update(EbSystemFormTempDomain domain);

    /**
     * 查询
     */
    EbSystemFormTempDomain getById(Integer id);
}
