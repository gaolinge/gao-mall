package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSystemRoleMenuDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemRoleMenuService {

    /**
     * 批量保存
     * <p/>
     *
     * @param domains
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void batchSave(List<EbSystemRoleMenuDomain> domains);

    /**
     * 根据角色id删除
     * <p/>
     *
     * @param rid
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    void delete(Integer rid);

    /**
     * 根据角色id查询
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    List<EbSystemRoleMenuDomain> listByRid(Integer id);
}
