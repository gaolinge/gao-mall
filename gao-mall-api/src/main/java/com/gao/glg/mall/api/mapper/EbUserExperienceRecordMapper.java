package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.response.app.AppUserExpResponse;
import com.gao.glg.mall.api.domain.EbUserExperienceRecordDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserExperienceRecordMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserExperienceRecordDomain domain);

    /**
     * 查询
     */
    List<EbUserExperienceRecordDomain> select();

    /**
     * 更新
     */
    int update(EbUserExperienceRecordDomain domain);

    /**
     * 查询
     */
    List<AppUserExpResponse> listByUid(Integer uid);
}
