package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer uid);

    /**
     * 保存
     */
    int save(EbUserDomain domain);

    /**
     * 查询
     */
    List<EbUserDomain> select();

    /**
     * 更新
     */
    int update(EbUserDomain domain);

    /**
     * 查询
     */
    EbUserDomain getById(int uid);

    /**
     * 查询
     */
    EbUserDomain getByAccount(String account);

    /**
     * 查询
     */
    List<EbUserDomain> listByIds(List<Integer> uids);
}
