package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbTemplateMessageDomain;
import com.gao.glg.mall.api.mapper.EbTemplateMessageMapper;
import com.gao.glg.mall.api.service.EbTemplateMessageService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbTemplateMessageServiceImpl implements EbTemplateMessageService  {
    @Resource
    private EbTemplateMessageMapper ebTemplateMessageMapper;
}
