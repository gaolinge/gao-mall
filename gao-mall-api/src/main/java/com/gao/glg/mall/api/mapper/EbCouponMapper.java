package com.gao.glg.mall.api.mapper;

import com.gao.glg.mall.api.bean.request.web.WebCouponSearchRequest;
import com.gao.glg.mall.api.domain.EbCouponDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author gaolinge
 */
public interface EbCouponMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbCouponDomain domain);

    /**
     * 查询
     */
    List<EbCouponDomain> select();

    /**
     * 更新
     */
    int update(EbCouponDomain domain);

    /**
     * 查询
     */
    List<EbCouponDomain> listByReceive(@Param("type") int type,
                                       @Param("pids") List<Integer> pids,
                                       @Param("cids") List<Integer> cids,
                                       @Param("start") int start, @Param("size") int size);

    /**
     * 查询
     */
    EbCouponDomain getById(Integer couponId);

    /**
     * 更新
     */
    void deduct(@Param("id") Integer id, @Param("isLimited") Integer isLimited, @Param("num") int num);

    /**
     * 分页
     */
    List<EbCouponDomain> page(@Param("request") WebCouponSearchRequest request);
}
