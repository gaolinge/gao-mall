package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbUserDomain;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbUserLevelService {
    void update(EbUserDomain domain);
}
