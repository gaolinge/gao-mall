package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebGroupRequest;
import com.gao.glg.mall.api.bean.request.web.WebGroupSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebGroupResponse;
import com.gao.glg.mall.api.business.ICombBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/admin/comb")
@Api(tags = "设置 -- 组合数据")
public class WebCombController {

    @Autowired
    private ICombBusiness combBusiness;

    //@PreAuthorize("hasAuthority('admin:system:group:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebGroupResponse>> page(@Validated WebGroupSearchRequest request) {
        return RestResult.success(combBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:system:group:save')")
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult<String> create(@Validated WebGroupRequest request) {
        combBusiness.create(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:group:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public RestResult<String> delete(@RequestParam(value = "id") Integer id) {
        combBusiness.delete(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:group:update')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RestResult<String> update(@RequestParam Integer id, @Validated WebGroupRequest request) {
        combBusiness.update(id, request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:group:info')")
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public RestResult<WebGroupResponse> detail(@RequestParam(value = "id") Integer id) {
        return RestResult.success(combBusiness.detail(id));
   }
}



