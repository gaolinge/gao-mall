package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbOrderInfoDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbOrderInfoMapper;
import com.gao.glg.mall.api.service.EbOrderInfoService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbOrderInfoServiceImpl implements EbOrderInfoService {
    @Resource
    private EbOrderInfoMapper orderInfoMapper;

    @Override
    public List<EbOrderInfoDomain> listByOrderId(Integer orderId) {
        return orderInfoMapper.listByOrderId(orderId);
    }
}
