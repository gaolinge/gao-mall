package com.gao.glg.mall.api.service.impl;

import com.gao.glg.enums.YesNoEnum;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempFreeRequest;
import com.gao.glg.mall.api.domain.EbShippingTempFreeDomain;
import com.gao.glg.mall.api.mapper.EbShippingTempFreeMapper;
import com.gao.glg.mall.api.service.EbShippingTempFreeService;
import com.gao.glg.mall.api.service.EbSystemCityService;
import com.gao.glg.mall.api.utils.CrmebUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@Service
public class EbShippingTempFreeServiceImpl implements EbShippingTempFreeService {

    @Autowired
    private EbSystemCityService systemCityService;
    @Resource
    private EbShippingTempFreeMapper shippingTempFreeMapper;

    @Override
    public EbShippingTempFreeDomain getByTempIdAndCityId(Integer tempId, Integer cityId) {
        return shippingTempFreeMapper.getByTempIdAndCityId(tempId, cityId);
    }

    @Override
    public void batchSave(List<WebLogisticsTempFreeRequest> list, Integer type, Integer tempId) {
        List<EbShippingTempFreeDomain> domains = new ArrayList<>();
        for (WebLogisticsTempFreeRequest request : list) {
            List<Integer> cityIdList =
                    "all".equals(request.getCityId()) || "0".equals(request.getCityId()) ?
                    systemCityService.selectCityIds() : CrmebUtil.stringToArray(request.getCityId());

            String uniqueKey = DigestUtils.md5Hex(request.toString());
            cityIdList.forEach(cityId -> {
                EbShippingTempFreeDomain domain = new EbShippingTempFreeDomain();
                domain.setCityId(cityId);
                domain.setTitle(request.getTitle());
                domain.setUniqid(uniqueKey);
                domain.setTempId(tempId);
                domain.setType(type);
                domain.setNumber(request.getNumber());
                domain.setPrice(request.getPrice());
                domain.setStatus(YesNoEnum.YES.getValue());
                domains.add(domain);
            });
        }
        shippingTempFreeMapper.deleteByTempId(tempId);
        shippingTempFreeMapper.batchSave(domains);
    }

    @Override
    public List<EbShippingTempFreeDomain> groupByUnqid(Integer tempId) {
        return shippingTempFreeMapper.groupByUniqid(tempId);
    }
}
