package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserTokenDomain;
import com.gao.glg.mall.api.mapper.EbUserTokenMapper;
import com.gao.glg.mall.api.service.EbUserTokenService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserTokenServiceImpl implements EbUserTokenService  {
    @Resource
    private EbUserTokenMapper ebUserTokenMapper;
}
