package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserBrokerageRecordDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserBrokerageRecordMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserBrokerageRecordDomain domain);

    /**
     * 查询
     */
    List<EbUserBrokerageRecordDomain> select();

    /**
     * 更新
     */
    int update(EbUserBrokerageRecordDomain domain);
}
