package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductCateMapper;
import com.gao.glg.mall.api.service.EbProductCateService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbProductCateServiceImpl implements EbProductCateService {
    @Resource
    private EbProductCateMapper ebProductCateMapper;
}
