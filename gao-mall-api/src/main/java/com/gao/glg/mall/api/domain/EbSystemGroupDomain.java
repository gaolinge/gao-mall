package com.gao.glg.mall.api.domain;

import lombok.Data;

import java.util.Date;


/**
 * EbSystemGroupDomain
 * @author gaolinge
 */
@Data
public class EbSystemGroupDomain {
    /** 
     * 组合数据ID
     */
    private Integer id;

    /** 
     * 数据组名称
     */
    private String name;

    /** 
     * 简介
     */
    private String info;

    /** 
     * form 表单 id
     */
    private Integer formId;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

}
