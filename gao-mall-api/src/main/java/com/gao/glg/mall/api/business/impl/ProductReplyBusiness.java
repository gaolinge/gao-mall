package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gao.glg.enums.YesNoEnum;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.web.WebProductReplyCommentRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductReplyRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductReplySearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebProductReplyResponse;
import com.gao.glg.mall.api.bean.response.web.WebProductResponse;
import com.gao.glg.mall.api.business.ICdnBusiness;
import com.gao.glg.mall.api.business.IProductReplyBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.*;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.mall.api.utils.CrmebUtil;
import com.gao.glg.mall.api.utils.MallUtil;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.StreamUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProductReplyBusiness implements IProductReplyBusiness {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbOrderService orderService;
    @Autowired
    private EbOrderInfoService orderInfoService;
    @Autowired
    private ICdnBusiness cdnBusiness;
    @Autowired
    private EbProductService productService;
    @Autowired
    private EbProductReplyService productReplyService;

    @Override
    public PagerInfo<WebProductReplyResponse> page(WebProductReplySearchRequest request) {
        PagerInfo<WebProductReplyResponse> page = PagerUtil.page(request, () -> {
            List<EbProductReplyDomain> domains = productReplyService.page(request);
            return domains.stream().map(e -> {
                WebProductReplyResponse response = new WebProductReplyResponse();
                BeanCopierUtil.copy(e, response);
                response.setPics(CrmebUtil.stringToArrayStr(e.getPics()));
                return response;
            }).collect(Collectors.toList());

        });
        if (!CollectionUtils.isEmpty(page.getList())) {
            List<Integer> pids = StreamUtil.map(page.getList(), WebProductReplyResponse::getProductId);
            List<EbProductDomain> productDomains = productService.listByIds(pids);
            Map<Integer, EbProductDomain> proudctMap = StreamUtil.listToMap(productDomains, EbProductDomain::getId);
            page.getList().forEach(item -> {
                EbProductDomain productDomain = proudctMap.get(item.getProductId());
                if (productDomain != null) {
                    item.setStoreProduct(BeanCopierUtil.copy(productDomain, WebProductResponse.class));
                }
            });
        }
        return page;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void create(WebProductReplyRequest request) {
        EbUserDomain userDomain = userService.getUserInfo();
        EbOrderDomain orderDomain = orderService.getByOderId(request.getOrderNo());
        if (ObjectUtil.isNull(orderDomain) || !orderDomain.getUid().equals(userDomain.getUid())) {
            throw new BusinessException(ErrorConstant.ORDER_DOES_NOT_EXIST);
        }
        List<EbOrderInfoDomain> infoDomains = orderInfoService.listByOrderId(orderDomain.getId());
        if (CollectionUtils.isEmpty(infoDomains)) {
            throw new BusinessException(ErrorConstant.ORDER_DOES_NOT_EXIST);
        }
        boolean flag = false;
        for (EbOrderInfoDomain infoDomain : infoDomains) {
            if (Objects.equals(request.getProductId(), infoDomain.getProductId())) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT);
        }


        Integer count = productReplyService.countByOidAndPidAndUid(
                orderDomain.getId(), request.getProductId(), userDomain.getUid());
        if (count > 0) {
            throw new BusinessException(ErrorConstant.ORDER_ALREADY_COMMENT);
        }
        count = productReplyService.countByOidAndPidAndUid(
                orderDomain.getId(), null, userDomain.getUid());
        if (infoDomains.size() == count) {
            EbOrderDomain updateOrderDomain = new EbOrderDomain();
            updateOrderDomain.setId(orderDomain.getId());
            updateOrderDomain.setStatus(MallConstant.ORDER_STATUS_INT_COMPLETE);
            orderService.update(updateOrderDomain);
        }

        EbProductReplyDomain domain = new EbProductReplyDomain();
        BeanUtils.copyProperties(request, domain);
        domain.setOid(orderDomain.getId());
        domain.setAvatar(cdnBusiness.clearPrefix(userDomain.getAvatar()));
        domain.setNickname(userDomain.getNickname());
        if (request.getPics() != null) {
            String pics = request.getPics()
                    .replace("[\"","")
                    .replace("\"]","")
                    .replace("\"", "");
            domain.setPics(cdnBusiness.clearPrefix(ArrayUtils.toString(pics)));
        }
        productReplyService.save(domain);
    }

    @Override
    public void delete(Integer id) {
        EbProductReplyDomain domain = new EbProductReplyDomain();
        domain.setId(id);
        domain.setIsDel(YesNoEnum.YES.getValue());
        productReplyService.update(domain);
    }

    @Override
    public void comment(WebProductReplyCommentRequest request) {
        EbProductReplyDomain domain = new EbProductReplyDomain();
        domain.setId(request.getIds());
        domain.setMerchantReplyContent(request.getMerchantReplyContent());
        domain.setMerchantReplyTime(MallUtil.timestamp().intValue());
        productReplyService.update(domain);
    }

    @Override
    public WebProductReplyResponse detail(Integer id) {
        EbProductReplyDomain domain = productReplyService.getById(id);
        return BeanCopierUtil.copy(domain, WebProductReplyResponse.class);
    }
}
