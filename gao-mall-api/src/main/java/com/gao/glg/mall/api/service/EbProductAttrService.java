package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbProductAttrDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbProductAttrService {

    /**
     * 根据商品id和类型获取商品属性
     * <p/>
     *
     * @param id
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductAttrDomain> listByPidAndType(Integer id, Integer type);

    /**
     * 根据商品id和类型
     * <p/>
     *
     * @param pids
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductAttrDomain> listByPidsAndType(List<Integer> pids, Integer type);
}
