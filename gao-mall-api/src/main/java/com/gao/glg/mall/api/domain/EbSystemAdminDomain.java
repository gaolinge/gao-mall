package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemAdminDomain
 * @author gaolinge
 */
@Data
public class EbSystemAdminDomain {
    /** 
     * 后台管理员表ID
     */
    private Integer id;

    /** 
     * 后台管理员账号
     */
    private String account;

    /** 
     * 后台管理员密码
     */
    private String pwd;

    /** 
     * 后台管理员姓名
     */
    private String realName;

    /** 
     * 后台管理员权限(menus_id)
     */
    private String roles;

    /** 
     * 后台管理员最后一次登录ip
     */
    private String lastIp;

    /** 
     * 后台管理员最后一次登录时间
     */
    private Date updateTime;

    /** 
     * 后台管理员添加时间
     */
    private Date createTime;

    /** 
     * 登录次数
     */
    private Integer loginCount;

    /** 
     * 后台管理员级别
     */
    private Integer level;

    /** 
     * 后台管理员状态 1有效0无效
     */
    private Integer status;

    /** 
     * 
     */
    private Integer isDel;

    /** 
     * 手机号码
     */
    private String phone;

    /** 
     * 是否接收短信
     */
    private Boolean isSms;


}
