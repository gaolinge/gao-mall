package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbCartDomain;
import com.gao.glg.mall.api.domain.EbProductAttrValueDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbProductAttrValueService {

    /**
     * 根据商品id和类型获取商品属性值
     * <p/>
     *
     * @param pid
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductAttrValueDomain> listByPidAndType(Integer pid, Integer type);

    /**
     * 根据商品id和类型和属性id获取商品属性值
     * <p/>
     *
     * @param pid
     * @param aid
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    EbProductAttrValueDomain getByPidAndTypeAndAttrId(Integer pid, String aid, Integer type);

    /**
     * 根据购物车获取商品属性值
     * <p/>
     *
     * @param list
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductAttrValueDomain> listByCart(List<EbCartDomain> list, Integer type);

    /**
     * 根据商品id和类型
     * <p/>
     *
     * @param pids
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductAttrValueDomain> listByPidsAndType(List<Integer> pids, Integer type);
}
