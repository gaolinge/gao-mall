package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbStoreOrderStatusDomain
 * @author gaolinge
 */
@Data
public class EbOrderStatusDomain {
    /** 
     * 订单id
     */
    private Integer oid;

    /** 
     * 操作类型
     */
    private String changeType;

    /** 
     * 操作备注
     */
    private String changeMessage;

    /** 
     * 操作时间
     */
    private Date createTime;


}
