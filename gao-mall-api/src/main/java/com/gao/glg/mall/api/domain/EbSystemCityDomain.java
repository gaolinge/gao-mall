package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemCityDomain
 * @author gaolinge
 */
@Data
public class EbSystemCityDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 城市id
     */
    private Integer cityId;

    /** 
     * 省市级别
     */
    private Integer level;

    /** 
     * 父级id
     */
    private Integer parentId;

    /** 
     * 区号
     */
    private String areaCode;

    /** 
     * 名称
     */
    private String name;

    /** 
     * 合并名称
     */
    private String mergerName;

    /** 
     * 经度
     */
    private String lng;

    /** 
     * 纬度
     */
    private String lat;

    /** 
     * 是否展示
     */
    private Boolean isShow;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
