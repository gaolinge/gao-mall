package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbWechatExceptionsDomain;
import com.gao.glg.mall.api.mapper.EbWechatExceptionsMapper;
import com.gao.glg.mall.api.service.EbWechatExceptionsService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbWechatExceptionsServiceImpl implements EbWechatExceptionsService  {
    @Resource
    private EbWechatExceptionsMapper ebWechatExceptionsMapper;
}
