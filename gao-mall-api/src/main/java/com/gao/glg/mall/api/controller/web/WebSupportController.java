package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.web.WebValidateCodeResponse;
import com.gao.glg.mall.api.business.ISupportBusiness;
import com.gao.glg.mall.api.business.IValidateBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * 支持
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin")
@Api(tags = "支持")
public class WebSupportController {

    @Autowired
    private ISupportBusiness supportBusiness;
    @Autowired
    private IValidateBusiness validateBusiness;


    @ApiOperation(value = "获取登录页图片")
    @RequestMapping(value = "/getLoginPic", method = RequestMethod.GET)
    public RestResult<Map<String, Object>> loginPic() {
        return RestResult.success(supportBusiness.loginPic());
    }

    @ApiOperation(value="获取验证码")
    @GetMapping(value = "/validate/code/get")
    public RestResult<WebValidateCodeResponse> get() {
        return RestResult.success(validateBusiness.get());
    }
}



