package com.gao.glg.mall.api.bean.request.app;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 商品点赞和收藏
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
@Data
public class AppUserCollectRequest implements Serializable {

    @ApiModelProperty(value = "商品ID")
    @JsonProperty("id")
    @Min(value = 1, message = "请选择产品")
    private Integer productId;

    @ApiModelProperty(value = "产品类型|store=普通产品,product_seckill=秒杀产品(默认 普通产品 store)")
    @JsonProperty("category")
    @NotBlank(message = "请选择产品类型")
    private String category;
}
