package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbUserExtractDomain
 * @author gaolinge
 */
@Data
public class EbUserExtractDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 
     */
    private Integer uid;

    /** 
     * 名称
     */
    private String realName;

    /** 
     * bank = 银行卡 alipay = 支付宝 weixin=微信
     */
    private String extractType;

    /** 
     * 银行卡
     */
    private String bankCode;

    /** 
     * 开户地址
     */
    private String bankAddress;

    /** 
     * 支付宝账号
     */
    private String alipayCode;

    /** 
     * 提现金额
     */
    private BigDecimal extractPrice;

    /** 
     * 
     */
    private String mark;

    /** 
     * 
     */
    private BigDecimal balance;

    /** 
     * 无效原因
     */
    private String failMsg;

    /** 
     * -1 未通过 0 审核中 1 已提现
     */
    private Byte status;

    /** 
     * 微信号
     */
    private String wechat;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 失败时间
     */
    private Date failTime;

    /** 
     * 银行名称
     */
    private String bankName;

    /** 
     * 微信收款二维码
     */
    private String qrcodeUrl;


}
