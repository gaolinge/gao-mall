package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.web.WebProductRuleSearchRequest;
import com.gao.glg.mall.api.domain.EbProductRuleDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductRuleMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductRuleDomain domain);

    /**
     * 查询
     */
    List<EbProductRuleDomain> select();

    /**
     * 更新
     */
    int update(EbProductRuleDomain domain);

    /**
     * 查询
     */
    List<EbProductRuleDomain> page(@Param("request") WebProductRuleSearchRequest request);

    /**
     * 查询
     */
    EbProductRuleDomain getByRuleName(String ruleName);

    /**
     * 查询
     */
    void deletes(@Param("list") List<Integer> ids);

    /**
     * 查询
     */
    EbProductRuleDomain getById(Integer id);
}
