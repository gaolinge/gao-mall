package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbWechatPayInfoDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbWechatPayInfoMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbWechatPayInfoDomain domain);

    /**
     * 查询
     */
    List<EbWechatPayInfoDomain> select();

    /**
     * 更新
     */
    int update(EbWechatPayInfoDomain domain);
}
