package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSeckillDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSeckillMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSeckillDomain domain);

    /**
     * 查询
     */
    List<EbSeckillDomain> select();

    /**
     * 更新
     */
    int update(EbSeckillDomain domain);

    /**
     * 查询
     */
    List<EbSeckillDomain> getByTimeId(Integer timeId);

    /**
     * 查询
     */
    List<EbSeckillDomain> listByTimeIds(@Param("productId") Integer productId,
                                        @Param("timeIds") List<Integer> timeIds);
    /**
     * 查询
     */
    EbSeckillDomain getById(Integer id);
}
