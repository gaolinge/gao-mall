package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.app.AppIndexInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppSystemConfigResponse;
import com.gao.glg.mall.api.business.IIndexBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 首页
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@Api(tags = "首页")
@RestController
@RequestMapping("/app/index")
public class AppIndexController {

    @Autowired
    private IIndexBusiness indexBusiness;

    @ApiOperation(value = "首页数据")
    @GetMapping(value = "/info")
    public RestResult<AppIndexInfoResponse> info() {
        return RestResult.success(indexBusiness.info());
    }

    @ApiOperation(value = "首页商品列表")
    @GetMapping(value = "/product/{type}")
    @ApiImplicitParam(
            name = "type", value = "类型 【1 精品推荐 2 热门榜单 3首发新品 4促销单品】", dataType = "int", required = true)
    public RestResult<PagerInfo> productList(
            @PathVariable(value = "type") Integer type, PagerDTO request) {
        return RestResult.success(indexBusiness.productList(type, request));
    }

    @ApiOperation(value = "热门搜索")
    @GetMapping(value = "/search/keyword")
    public RestResult<List<Map<String, Object>>> keyword() {
        return RestResult.success(indexBusiness.keywords());
    }

    @ApiOperation(value = "分享配置")
    @GetMapping(value = "/share")
    public RestResult<Map<String, String>> share() {
        return RestResult.success(indexBusiness.share());
    }

    @ApiOperation(value = "颜色配置")
    @GetMapping(value = "/color/config")
    public RestResult<AppSystemConfigResponse> getColorConfig() {
        return RestResult.success(indexBusiness.color());
    }

    @ApiOperation(value = "获取版本信息")
    @GetMapping(value = "/get/version")
    public RestResult<Map<String, Object>> version() {
        return RestResult.success(indexBusiness.version());
    }

    @ApiOperation(value = "全局本地图片域名")
    @GetMapping(value = "/image/domain")
    public RestResult<String> imageDomain() {
        return RestResult.success(indexBusiness.imageDomain());
    }
}



