package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户经验
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/7
 */
@Data
public class AppUserExpResponse implements Serializable {

    @ApiModelProperty(value = "记录id")
    private Integer id;

    @ApiModelProperty(value = "类型：1-增加，2-扣减")
    private Integer type;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "经验")
    private Integer experience;

    @ApiModelProperty(value = "添加时间")
    private Date createTime;

}
