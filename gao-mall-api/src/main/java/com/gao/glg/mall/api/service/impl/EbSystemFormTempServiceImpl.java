package com.gao.glg.mall.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.dto.SystemFormDTO;
import com.gao.glg.mall.api.bean.request.web.WebFormCheckRequest;
import com.gao.glg.mall.api.bean.request.web.WebFormItemCheckRequest;
import com.gao.glg.mall.api.constant.ErrorConstant;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemFormTempDomain;
import com.gao.glg.mall.api.mapper.EbSystemFormTempMapper;
import com.gao.glg.mall.api.service.EbSystemFormTempService;

import java.util.HashMap;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemFormTempServiceImpl implements EbSystemFormTempService {

    @Resource
    private EbSystemFormTempMapper systemFormTempMapper;

    @Override
    public void check(WebFormCheckRequest request) {
        HashMap<String, String> map = new HashMap<>();
        for (WebFormItemCheckRequest item : request.getFields()) {
            map.put(item.getName(), item.getValue());
        }

        EbSystemFormTempDomain domain = systemFormTempMapper.getById(request.getId());
        SystemFormDTO formDTO;
        try {
            formDTO = JSONObject.parseObject(domain.getContent(), SystemFormDTO.class);
        } catch (Exception e) {
            throw new BusinessException(ErrorConstant.FORM_TEMPLATE_FORMAT_ERROR, domain.getName());
        }


    }



}
