package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreOrderInfoDomain
 * @author gaolinge
 */
@Data
public class EbOrderInfoDomain {
    /** 
     * 主键
     */
    private Integer id;

    /** 
     * 订单id
     */
    private Integer orderId;

    /** 
     * 商品ID
     */
    private Integer productId;

    /** 
     * 购买东西的详细信息
     */
    private String info;

    /** 
     * 唯一id
     */
    private String unique;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 订单号
     */
    private String orderNo;

    /** 
     * 商品名称
     */
    private String productName;

    /** 
     * 规格属性值id
     */
    private Integer attrValueId;

    /** 
     * 商品图片
     */
    private String image;

    /** 
     * 商品sku
     */
    private String sku;

    /** 
     * 商品价格
     */
    private BigDecimal price;

    /** 
     * 购买数量
     */
    private Integer payNum;

    /** 
     * 重量
     */
    private BigDecimal weight;

    /** 
     * 体积
     */
    private BigDecimal volume;

    /** 
     * 赠送积分
     */
    private Integer giveIntegral;

    /** 
     * 是否评价，0-未评价，1-已评价
     */
    private Boolean isReply;

    /** 
     * 是否单独分佣,0-否，1-是
     */
    private Boolean isSub;

    /** 
     * 会员价
     */
    private BigDecimal vipPrice;

    /** 
     * 商品类型:0-普通，1-秒杀，2-砍价，3-拼团，4-视频号
     */
    private Integer productType;


}
