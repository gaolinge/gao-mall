package com.gao.glg.mall.api.business.impl;

import com.gao.glg.constant.CommonConstant;
import com.gao.glg.mall.api.bean.response.web.WebUploadResponse;
import com.gao.glg.mall.api.business.ICdnBusiness;
import com.gao.glg.mall.api.business.IUploadBusiness;
import com.gao.glg.mall.api.domain.EbSystemAttachmentDomain;
import com.gao.glg.mall.api.service.EbSystemAttachmentService;
import com.gao.glg.utils.SplitUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;
import java.util.UUID;

@Service
public class UploadBusiness implements IUploadBusiness {
    private String[] suffixs = new String[]{"jpg", "jpeg", "png", "gif", "bmp", "tiff", "webp"};
    @Value("${spring.profiles.active}")
    private String env;
    @Value("${container.file.directory}")
    private String directory;
    @Value("${image.path.prefix}")
    private String imgPrefix;
    @Autowired
    private ICdnBusiness cdnBusiness;
    @Autowired
    private EbSystemAttachmentService systemAttachmentService;

    @Override
    public WebUploadResponse imageUpload(MultipartFile file, String model, Integer pid) throws IOException {
        String fileName = file.getOriginalFilename();
        String suffix = SplitUtil.lastSection(fileName, CommonConstant.SPOT_SYMBOL);
        fileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + suffix;
        String filePath = directory + fileName;
        file.transferTo(new File(filePath));

        // 添加文件权限
        Path path = Paths.get(filePath);
        Set<PosixFilePermission> filePermissions = PosixFilePermissions.fromString("rw-r--r--");
        Files.setPosixFilePermissions(path, filePermissions);
        String url = imgPrefix + fileName;

        EbSystemAttachmentDomain attachmentDomain = new EbSystemAttachmentDomain();
        attachmentDomain.setName(fileName);
        attachmentDomain.setSattDir(url);
        attachmentDomain.setAttSize(file.getSize() + "");
        attachmentDomain.setAttType(file.getContentType());
        attachmentDomain.setPid(pid);
        attachmentDomain.setImageType(1);
        systemAttachmentService.save(attachmentDomain);

        WebUploadResponse response = new WebUploadResponse();
        response.setUrl(url);
        response.setFileName(fileName);
        response.setExtName(fileName);
        response.setFileSize(file.getSize());
        response.setType(file.getContentType());
        return response;
    }

    @Override
    public WebUploadResponse fileUpload(MultipartFile file, String model, Integer pid) {
        return null;
    }
}
