package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.CategoryTreeResponse;
import com.gao.glg.mall.api.bean.response.app.AppCityTreeResponse;
import com.gao.glg.mall.api.business.ICategoryBusiness;
import com.gao.glg.mall.api.business.ISupportBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 支持
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app")
@Api(tags = "支持")
public class AppSupportController {

    @Autowired
    private ISupportBusiness supportBusiness;
    @Autowired
    private ICategoryBusiness categoryBusiness;

    @ApiOperation(value = "树形结构")
    @RequestMapping(value = "/city/list", method = RequestMethod.GET)
    public RestResult<List<AppCityTreeResponse>> cityTree(){
        return RestResult.success(supportBusiness.tree());
    }

    @ApiOperation(value = "获取分类")
    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public RestResult<List<CategoryTreeResponse>> categoryTree() {
        return RestResult.success(categoryBusiness.tree());
    }
}



