package com.gao.glg.mall.api.business;


/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/7/3
 */
public interface ICdnBusiness {

    /**
     * 上传
     * <p/>
     *
     * @param path
     * @return
     * @author : gaolinge
     * @date : 2024/7/3
     * @version : 1.0.0
     */
    String clearPrefix(String path);

    /**
     * 获取url
     * <p/>
     *
     * @return
     * @author : gaolinge
     * @date : 2024/7/3
     * @version : 1.0.0
     */
    String getUrl();
}
