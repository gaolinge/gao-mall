package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbStoreProductReplyDomain
 * @author gaolinge
 */
@Data
public class EbProductReplyDomain {
    /** 
     * 评论ID
     */
    private Integer id;

    /** 
     * 用户ID
     */
    private Integer uid;

    /** 
     * 订单ID
     */
    private Integer oid;

    /** 
     * 商品唯一id
     */
    private String unique;

    /** 
     * 商品id
     */
    private Integer productId;

    /** 
     * 某种商品类型(普通商品、秒杀商品）
     */
    private String replyType;

    /** 
     * 商品分数
     */
    private Integer productScore;

    /** 
     * 服务分数
     */
    private Integer serviceScore;

    /** 
     * 评论内容
     */
    private String comment;

    /** 
     * 评论图片
     */
    private String pics;

    /** 
     * 管理员回复内容
     */
    private String merchantReplyContent;

    /** 
     * 管理员回复时间
     */
    private Integer merchantReplyTime;

    /** 
     * 0未删除1已删除
     */
    private Integer isDel;

    /** 
     * 0未回复1已回复
     */
    private Integer isReply;

    /** 
     * 用户名称
     */
    private String nickname;

    /** 
     * 用户头像
     */
    private String avatar;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 商品规格属性值,多个,号隔开
     */
    private String sku;


}
