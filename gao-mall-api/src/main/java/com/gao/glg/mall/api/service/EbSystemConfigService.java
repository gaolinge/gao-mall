package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSystemConfigDomain;

import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemConfigService {

    /**
     * 根据key获取配置信息
     * <p/>
     *
     * @param key
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    EbSystemConfigDomain getByKey(String key);

    /**
     * 根据key获取value
     *
     * @param key
     * @return
     */
    String getValueByKey(String key);

    /**
     * 根据formId获取配置信息
     * <p/>
     *
     * @param formId
     * @return
     * @author : gaolinge
     * @date : 2024/11/23
     * @version : 1.0.0
     */
    Map<String, String> info(int formId);

}
