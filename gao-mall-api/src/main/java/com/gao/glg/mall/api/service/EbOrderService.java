package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbOrderDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbOrderService {


    /**
     * 订单数量
     * <p/>
     *
     * @param status
     * @param userId
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    Integer orderNum(Integer userId, int status);

    /**
     * 根据用户id查询订单列表
     * <p/>
     *
     * @param userId
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    List<EbOrderDomain> listByUserId(Integer userId);

    /**
     * 根据id查询订单
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    EbOrderDomain getById(Integer id);

    /**
     * 根据id和用户id查询订单
     * <p/>
     *
     * @param id
     * @param userId
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    EbOrderDomain getByIdAndUid(Integer id, Integer userId);

    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    void update(EbOrderDomain domain);

    /**
     * 根据订单号查询订单
     * <p/>
     *
     * @param orderNo
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    EbOrderDomain getByOderId(String orderNo);
}
