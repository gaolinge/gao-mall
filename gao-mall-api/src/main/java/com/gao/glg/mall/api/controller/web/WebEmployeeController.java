package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeRequest;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeSearchRequest;
import com.gao.glg.mall.api.bean.request.web.WebEmployeeUpdateRequest;
import com.gao.glg.mall.api.bean.response.web.WebAdminResponse;
import com.gao.glg.mall.api.bean.response.web.WebEmployeeResponse;
import com.gao.glg.mall.api.bean.response.web.WebMenuResponse;
import com.gao.glg.mall.api.bean.response.web.WebUserInfoResponse;
import com.gao.glg.mall.api.business.IEmployeeBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 员工中心
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin/employee")
@Api(tags = "员工中心")
public class WebEmployeeController {

    @Autowired
    private IEmployeeBusiness employeeBusiness;

    //@PreAuthorize("hasAuthority('admin:info')")
    @ApiOperation(value="获取用户详情")
    @GetMapping(value = "/info")
    public RestResult<WebUserInfoResponse> info(String token) {
        return RestResult.success(employeeBusiness.info(token));
    }

    @ApiOperation(value = "获取目录")
    @RequestMapping(value = "/getMenus", method = RequestMethod.GET)
    public RestResult<List<WebMenuResponse>> menus() {
        return RestResult.success(employeeBusiness.menus());
    }

    //@PreAuthorize("hasAuthority('admin:system:admin:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public RestResult<PagerInfo<WebAdminResponse>> page(@Validated WebEmployeeSearchRequest request) {
        return RestResult.success(employeeBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:system:admin:save')")
    @ApiOperation(value = "新增后台管理员")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult create(@RequestBody WebEmployeeRequest request) {
        employeeBusiness.create(request);
        return RestResult.success("添加管理员成功");
    }

    //@PreAuthorize("hasAuthority('admin:system:admin:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public RestResult delete(@RequestParam(value = "id") Integer id) {
        employeeBusiness.delete(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:admin:update')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RestResult update(@RequestBody WebEmployeeUpdateRequest request) {
        employeeBusiness.update(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:admin:info')")
    @ApiOperation(value = "后台管理员详情")
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public RestResult<WebEmployeeResponse> detail(@RequestParam(value = "id") @Valid Integer id) {
        return RestResult.success(employeeBusiness.detail(id));
    }

    //@PreAuthorize("hasAuthority('admin:system:admin:update:status')")
    @ApiOperation(value = "修改后台管理员状态")
    @RequestMapping(value = "/updateStatus", method = RequestMethod.GET)
    public RestResult updateStatus(@RequestParam(value = "id") @Valid Integer id, @RequestParam(value = "status") @Valid Boolean status) {
        employeeBusiness.updateStatus(id, status);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:admin:update:sms')")
    @ApiOperation(value = "修改后台管理员是否接收状态")
    @RequestMapping(value = "/update/isSms", method = RequestMethod.GET)
    public RestResult updateIsSms(@RequestParam(value = "id") @Valid Integer id) {
        employeeBusiness.updateSms(id);
        return RestResult.success();
    }
}



