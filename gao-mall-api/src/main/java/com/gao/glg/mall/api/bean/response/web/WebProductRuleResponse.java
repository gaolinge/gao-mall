package com.gao.glg.mall.api.bean.response.web;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 商品规则
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/27
 */
@Data
public class WebProductRuleResponse implements Serializable {

    @ApiModelProperty(value = "主键")
    private Integer id;

    @ApiModelProperty(value = "规格名称")
    private String ruleName;

    @ApiModelProperty(value = "规格值")
    private String ruleValue;


}
