package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbCouponUserDomain;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbCouponUserService {

    /**
     * 根据userId获取用户优惠券列表
     * <p/>
     *
     * @param userId
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    List<EbCouponUserDomain> listByUid(Integer userId);

    /**
     * 可用优惠券数量
     * <p/>
     *
     * @param uid
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    Integer canUsedNum(Integer uid);

    /**
     * 根据uid和type获取用户优惠券列表
     * <p/>
     *
     * @param uid
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    List<EbCouponUserDomain> listByUidAndType(Integer uid, String type);

    /**
     * 根据uid和couponId获取优惠券
     * <p/>
     *
     * @param uid
     * @param couponId
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    List<EbCouponUserDomain> listByUidAndCouponId(Integer uid, Integer couponId);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    void save(EbCouponUserDomain domain);

    /**
     * 可以使用的优惠券
     * <p/>
     *
     * @param pids
     * @param maxPrice
     * @return
     * @author : gaolinge
     * @date : 2024/12/10
     * @version : 1.0.0
     */
    List<EbCouponUserDomain> listByCanUse(List<Integer> pids, BigDecimal maxPrice);

    EbCouponUserDomain getById(Integer couponId);
}
