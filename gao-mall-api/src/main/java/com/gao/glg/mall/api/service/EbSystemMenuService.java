package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSystemMenuDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemMenuService {

    /**
     * 根据角色查询权限
     * <p/>
     *
     * @param roles
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    List<String> selectPerms(List<String> roles);

    /**
     * 查询所有菜单
     * <p/>
     *
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    List<EbSystemMenuDomain> selectAll();

    /**
     * 根据角色查询菜单
     * <p/>
     *
     * @param roles
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    List<EbSystemMenuDomain> listByRoles(List<String> roles);

    /**
     * 查询所有菜单
     * <p/>
     *
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    List<EbSystemMenuDomain> all();

}
