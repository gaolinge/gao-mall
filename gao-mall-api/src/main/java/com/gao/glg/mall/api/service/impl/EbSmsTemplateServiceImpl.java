package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSmsTemplateDomain;
import com.gao.glg.mall.api.mapper.EbSmsTemplateMapper;
import com.gao.glg.mall.api.service.EbSmsTemplateService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSmsTemplateServiceImpl implements EbSmsTemplateService  {
    @Resource
    private EbSmsTemplateMapper ebSmsTemplateMapper;
}
