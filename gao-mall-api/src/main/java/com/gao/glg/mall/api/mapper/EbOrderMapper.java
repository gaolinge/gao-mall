package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbOrderDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbOrderMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbOrderDomain domain);

    /**
     * 查询
     */
    List<EbOrderDomain> select();

    /**
     * 更新
     */
    int update(EbOrderDomain domain);

    /**
     * 查询
     */
    Integer orderNum(@Param("status") int status, @Param("uid") Integer uid);

    /**
     * 查询
     */
    List<EbOrderDomain> listByUserId(Integer userId);

    /**
     * 查询
     */
    EbOrderDomain getById(Integer id);

    /**
     * 查询
     */
    EbOrderDomain getByIdAndUid(@Param("id") Integer id, @Param("uid") Integer userId);

    /**
     * 查询
     */
    EbOrderDomain getByOderId(String orderId);
}
