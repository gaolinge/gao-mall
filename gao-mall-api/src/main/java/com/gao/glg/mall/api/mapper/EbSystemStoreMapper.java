package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemStoreDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemStoreMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemStoreDomain domain);

    /**
     * 查询
     */
    List<EbSystemStoreDomain> select();

    /**
     * 更新
     */
    int update(EbSystemStoreDomain domain);

    /**
     * 查询
     */
    EbSystemStoreDomain getById(Integer id);

    /**
     * 查询
     */
    List<EbSystemStoreDomain> listByIds(List<Integer> ids);
}
