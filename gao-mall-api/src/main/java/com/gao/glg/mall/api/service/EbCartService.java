package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.app.AppCartRequest;
import com.gao.glg.mall.api.domain.EbCartDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbCartService {

    /**
     * <p/>
     *
     * @param userId
     * @param isValid
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    List<EbCartDomain> listByUserId(Integer userId, Boolean isValid);

    /**
     * <p/>
     *
     * @param userId
     * @param status
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    int countTotal(Integer userId, Boolean status);

    /**
     *
     * <p/>
     *
     * @param userId
     * @param status
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    int countNum(Integer userId, Boolean status);

    /**
     * 更新
     * <p/>
     *
     * @param id
     * @param num
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    Long update(Long id, Integer num);

    /**
     * 保存
     * <p/>
     *
     * @param uid
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    Long save(Integer uid, AppCartRequest request);

    /**
     * <p/>
     *
     * @param uid
     * @param attrId
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    List<EbCartDomain> listByUserIdAndAttrId(Integer uid, String attrId);

    /**
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    EbCartDomain getById(Long id);

    /**
     * 批量删除
     * <p/>
     *
     * @param ids
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    void batchDelete(List<Long> ids);
}
