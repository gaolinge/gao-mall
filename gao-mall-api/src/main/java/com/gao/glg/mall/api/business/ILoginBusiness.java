package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.app.AppLoginRequest;
import com.gao.glg.mall.api.bean.request.web.WebLoginRequest;
import com.gao.glg.mall.api.bean.response.app.AppLoginResponse;
import com.gao.glg.mall.api.bean.response.web.WebLoginResponse;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/3
 */
public interface ILoginBusiness {

    /**
     * 登入
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/3
     * @version : 1.0.0
     */
    AppLoginResponse appLogin(AppLoginRequest request);

    /**
     * 退出
     * <p/>
     *
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/12/3
     * @version : 1.0.0
     */
    void logout(int type);

    /**
     * 登入
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/3
     * @version : 1.0.0
     */
    WebLoginResponse webLogin(WebLoginRequest request);

}
