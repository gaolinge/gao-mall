package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbWechatReplyDomain;
import com.gao.glg.mall.api.mapper.EbWechatReplyMapper;
import com.gao.glg.mall.api.service.EbWechatReplyService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbWechatReplyServiceImpl implements EbWechatReplyService  {
    @Resource
    private EbWechatReplyMapper ebWechatReplyMapper;
}
