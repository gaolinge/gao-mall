package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemRoleMenuDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemRoleMenuMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer rid);

    /**
     * 保存
     */
    int save(EbSystemRoleMenuDomain domain);

    /**
     * 查询
     */
    List<EbSystemRoleMenuDomain> select();

    /**
     * 更新
     */
    int update(EbSystemRoleMenuDomain domain);

    /**
     * 保存
     */
    void batchSave(@Param("list") List<EbSystemRoleMenuDomain> domains);

    /**
     * 查询
     */
    List<EbSystemRoleMenuDomain> listByRid(Integer id);
}
