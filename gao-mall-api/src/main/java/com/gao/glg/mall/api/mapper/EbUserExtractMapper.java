package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserExtractDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserExtractMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserExtractDomain domain);

    /**
     * 查询
     */
    List<EbUserExtractDomain> select();

    /**
     * 更新
     */
    int update(EbUserExtractDomain domain);
}
