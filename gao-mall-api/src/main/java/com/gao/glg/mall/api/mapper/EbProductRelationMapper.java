package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.response.app.AppUserCollectResponse;
import com.gao.glg.mall.api.domain.EbProductRelationDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductRelationMapper {

    /**
     * 删除
     */
    int delete(@Param("uid") Integer uid, @Param("pid") Integer pid);

    /**
     * 删除
     */
    int batchDelete(@Param("uid") Integer uid, @Param("ids") List<String> ids, @Param("type") int type);

    /**
     * 保存
     */
    int save(EbProductRelationDomain domain);

    /**
     * 保存
     */
    void batchSave(@Param("list") List<EbProductRelationDomain> domains);

    /**
     * 查询
     */
    List<EbProductRelationDomain> select();

    /**
     * 更新
     */
    int update(EbProductRelationDomain domain);

    /**
     * 查询
     */
    List<EbProductRelationDomain> listByUidAndPidAndType(@Param("uid") Integer uid,
                                                         @Param("pid") Integer pid,
                                                         @Param("type") String type);
    /**
     * 查询
     */
    Integer collectNum(Integer uid);

    /**
     * 查询
     */
    List<AppUserCollectResponse> list(Integer uid);


    /**
     * 查询
     */
    List<EbProductRelationDomain> listByPidsAndType(@Param("pids") List<Integer> pids, @Param("type") String type);
}
