package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.web.WebUploadResponse;
import com.gao.glg.mall.api.business.IUploadBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


/**
 * 上传
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/19
 */
@Slf4j
@RestController
@RequestMapping("/admin/upload")
@Api(tags = "上传文件")
public class WebUploadController {

    @Autowired
    private IUploadBusiness uploadBusiness;

    @ApiOperation(value = "图片上传")
    @RequestMapping(value = "/image", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "model", value = "模块 用户user,商品product,微信wechat,news文章"),
            @ApiImplicitParam(name = "pid", value = "分类ID 0编辑器,1商品图片,2拼团图片,3砍价图片,4秒杀图片,5文章图片,6组合数据图,7前台用户,8微信系列 ", allowableValues = "range[0,1,2,3,4,5,6,7,8]")
    })
    public RestResult<WebUploadResponse> image(MultipartFile file, @RequestParam(value = "model") String model,
                                               @RequestParam(value = "pid") Integer pid) throws IOException {
        return RestResult.success(uploadBusiness.imageUpload(file, model, pid));
    }


    @ApiOperation(value = "文件上传")
    @RequestMapping(value = "/file", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "model", value = "模块 用户user,商品product,微信wechat,news文章"),
            @ApiImplicitParam(name = "pid", value = "分类ID 0编辑器,1商品图片,2拼团图片,3砍价图片,4秒杀图片,5文章图片,6组合数据图,7前台用户,8微信系列 ", allowableValues = "range[0,1,2,3,4,5,6,7,8]")
    })
    public RestResult<WebUploadResponse> file(MultipartFile file, @RequestParam(value = "model") String model,
                                              @RequestParam(value = "pid") Integer pid) throws IOException {
        return RestResult.success(uploadBusiness.fileUpload(file, model, pid));
    }

}



