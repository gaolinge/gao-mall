package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSeckillMangerDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSeckillMangerMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSeckillMangerDomain domain);

    /**
     * 查询
     */
    List<EbSeckillMangerDomain> select(int num);

    /**
     * 更新
     */
    int update(EbSeckillMangerDomain domain);

    /**
     * 查询
     */
    List<EbSeckillMangerDomain> listByCurrent();

    /**
     * 查询
     */
    EbSeckillMangerDomain getById(Integer timeId);
}
