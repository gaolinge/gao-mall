package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbStoreProductAttrDomain
 * @author gaolinge
 */
@Data
public class EbProductAttrDomain {
    /** 
     * 主键
     */
    private Integer id;

    /** 
     * 商品ID
     */
    private Integer productId;

    /** 
     * 属性名
     */
    private String attrName;

    /** 
     * 属性值
     */
    private String attrValues;

    /** 
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    private Boolean type;

    /** 
     * 是否删除,0-否，1-是
     */
    private Boolean isDel;


}
