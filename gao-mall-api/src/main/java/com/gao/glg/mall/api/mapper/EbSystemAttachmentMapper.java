package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemAttachmentDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemAttachmentMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer attId);

    /**
     * 保存
     */
    int save(EbSystemAttachmentDomain domain);

    /**
     * 查询
     */
    List<EbSystemAttachmentDomain> select();

    /**
     * 更新
     */
    int update(EbSystemAttachmentDomain domain);

    /**
     * 查询
     */
    List<EbSystemAttachmentDomain> listByPidAndTypes(@Param("pid") Integer pid, @Param("types") List<String> types);
}
