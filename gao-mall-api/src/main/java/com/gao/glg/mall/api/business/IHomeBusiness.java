package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.web.WebHomeRateResponse;

import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/26
 */
public interface IHomeBusiness {

    /**
     * 首页数据
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    WebHomeRateResponse indexDate();

    /**
     * 用户曲线图
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    Map<Object, Object> chartUser();

    /**
     * 用户购买统计
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    Map<String, Integer> chartUserBuy();

    /**
     * 订单量趋势
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    Map<String, Object> chartOrder();

    /**
     * 周订单量趋势
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    Map<String, Object> chartOrderInWeek();

    /**
     * 月订单量趋势
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    Map<String, Object> chartOrderInMonth();

    /**
     * 年订单量趋势
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    Map<String, Object> chartOrderInYear();
}
