package com.gao.glg.mall.api.bean.response.app;

import com.gao.glg.serializer.FileUrlSerializerField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 砍价商品详情响应
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/22
 */
@Data
public class AppBargainInfoResponse implements Serializable {

    @ApiModelProperty(value = "砍价商品ID")
    private Integer id;

    @ApiModelProperty(value = "关联商品ID")
    private Integer productId;

    @ApiModelProperty(value = "砍价活动名称")
    private String title;

    @FileUrlSerializerField
    @ApiModelProperty(value = "砍价活动图片")
    private String image;

    @ApiModelProperty(value = "砍价开启时间")
    private Long startTime;

    @ApiModelProperty(value = "砍价结束时间")
    private Long stopTime;

    @ApiModelProperty(value = "砍价商品最低价")
    private BigDecimal minPrice;

    @ApiModelProperty(value = "限购总数")
    private Integer quota;
}
