package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbStoreProductDescriptionDomain
 * @author gaolinge
 */
@Data
public class EbProductDescDomain {
    /** 
     * 商品ID
     */
    private Integer productId;

    /** 
     * 商品详情
     */
    private String description;

    /** 
     * 商品类型
     */
    private Boolean type;

    /** 
     * 
     */
    private Integer id;


}
