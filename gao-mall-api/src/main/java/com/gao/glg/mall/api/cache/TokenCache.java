package com.gao.glg.mall.api.cache;

import com.gao.glg.mall.api.constant.CacheConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbUserDomain;
import com.gao.glg.utils.JSONUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/3
 */
@Service
public class TokenCache {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 生成令牌
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/3
     * @version : 1.0.0
     */
    public String createAppToken(EbUserDomain domain) {
        String token = UUID.randomUUID().toString().replace("-", "");
        String key = CacheConstant.USER_APP_TOKEN_REDIS_KEY_PREFIX + token;
        stringRedisTemplate.opsForValue().set(key, domain.getUid().toString(),
                MallConstant.TOKEN_EXPRESS_MINUTES, TimeUnit.MINUTES);
        return token;
    }

    /**
     * 生成令牌
     * <p/>
     *
     * @param value
     * @return
     * @author : gaolinge
     * @date : 2024/12/3
     * @version : 1.0.0
     */
    public String createWebToken(String value) {
        String token = UUID.randomUUID().toString().replace("-", "");
        String key = CacheConstant.USER_WEB_TOKEN_REDIS_KEY_PREFIX + token;
        stringRedisTemplate.opsForValue().set(key, value,
                MallConstant.TOKEN_EXPRESS_MINUTES, TimeUnit.MINUTES);
        return token;
    }

    public <T> T getWebValue(String token, Class<T> clazz) {
        String key = CacheConstant.USER_WEB_TOKEN_REDIS_KEY_PREFIX + token;
        String val = stringRedisTemplate.opsForValue().get(key);
        if (val == null) {
            return null;
        }
        if (clazz == String.class) {
            return (T) val;
        }
        return JSONUtils.parseObject(val, clazz);
    }

    /**
     * 获取用户id
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/3
     * @version : 1.0.0
     */
    public Integer getUid() {
        String token = getToken();
        if (token == null) {
            return null;
        }
        String key = CacheConstant.USER_APP_TOKEN_REDIS_KEY_PREFIX + token;
        String uid = stringRedisTemplate.opsForValue().get(key);
        if (uid == null) {
            return null;
        }
        return Integer.parseInt(uid);
    }

    /**
     * 删除token
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/3
     * @version : 1.0.0
     */
    public void deleteToken(int type) {
        String token = getToken();
        if (token == null) {
            return;
        }
        String prefix = type == 1 ?
                CacheConstant.USER_APP_TOKEN_REDIS_KEY_PREFIX :
                CacheConstant.USER_WEB_TOKEN_REDIS_KEY_PREFIX;
        String key = prefix + token;
        stringRedisTemplate.delete(key);
    }

    /**
     * 获取token
     *
     * @return
     */
    public String getToken() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(
                RequestContextHolder.getRequestAttributes())).getRequest();
        return request.getHeader(MallConstant.HEADER_AUTHORIZATION_KEY);
    }
}
