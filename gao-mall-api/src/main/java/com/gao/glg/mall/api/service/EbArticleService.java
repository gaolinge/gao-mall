package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebArticleSearchRequest;
import com.gao.glg.mall.api.domain.EbArticleDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbArticleService {

    /**
     * 根据分类id查询列表
     * <p/>
     *
     * @param cid
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    List<EbArticleDomain> listByCid(String cid);

    /**
     * 热门列表
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    List<EbArticleDomain> hotList();

    /**
     * 轮播列表
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    List<EbArticleDomain> bannerList();

    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    void update(EbArticleDomain domain);

    /**
     * 根据id查询
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    EbArticleDomain getById(Integer id);

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    List<EbArticleDomain> page(WebArticleSearchRequest request);

    /**
     * 保存
     * <p/>
     *
     * @param article
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    void save(EbArticleDomain article);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2023/9/11
     * @version : 1.0.0
     */
    void delete(Integer id);
}
