package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.app.AppProductRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductSearchRequest;
import com.gao.glg.mall.api.domain.EbProductDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductDomain domain);

    /**
     * 查询
     */
    List<EbProductDomain> select();

    /**
     * 更新
     */
    int update(EbProductDomain domain);

    /**
     * 查询
     */
    List<EbProductDomain> listByType(Integer type);

    /**
     * 查询
     */
    EbProductDomain getById(Integer pid);

    /**
     * 查询
     */
    List<EbProductDomain> appSearch(@Param("request") AppProductRequest request);

    /**
     * 查询
     */
    List<EbProductDomain> listByIds(List<Integer> pids);

    /**
     * 查询
     */
    List<EbProductDomain> webSearch(@Param("request") WebProductSearchRequest request);

    /**
     * 查询
     */
    Integer countByType(@Param("type") int type, @Param("stock") Integer stock);
}
