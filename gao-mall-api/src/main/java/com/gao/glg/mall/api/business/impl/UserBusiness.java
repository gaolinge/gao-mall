package com.gao.glg.mall.api.business.impl;

import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.response.app.AppUserCapitalResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserExpResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserLevelResponse;
import com.gao.glg.mall.api.business.IUserBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbSystemUserLevelDomain;
import com.gao.glg.mall.api.domain.EbUserBillDomain;
import com.gao.glg.mall.api.domain.EbUserDomain;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.StreamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserBusiness implements IUserBusiness {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbUserBillService userBillService;
    @Autowired
    private EbUserVisitRecordService userVisitRecordService;
    @Autowired
    private EbSystemConfigService systemConfigService;
    @Autowired
    private EbSystemUserLevelService systemUserLevelService;
    @Autowired
    private EbSystemGroupDataService systemGroupDataService;
    @Autowired
    private EbUserExperienceRecordService userExperienceRecordService;
    @Autowired
    private EbCouponUserService couponUserService;
    @Autowired
    private EbProductRelationService productRelationService;

    @Override
    public AppUserInfoResponse appUserInfo() {
        EbUserDomain userDomain = userService.getUserInfo();
        if (userDomain == null) {
            throw new BusinessException(ErrorConstant.ARTICLE_NOT_EXIST, "您的登录已过期，请先登录");
        }

        String rechargeSwitch = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_RECHARGE_SWITCH);
        String funcStatus = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_BROKERAGE_FUNC_STATUS);
        EbSystemUserLevelDomain userLevelDomain = systemUserLevelService.getById(userDomain.getLevel());

        AppUserInfoResponse userCenterResponse = BeanCopierUtil.copy(userDomain, AppUserInfoResponse.class);
        userCenterResponse.setCouponCount(couponUserService.canUsedNum(userDomain.getUid()));
        userCenterResponse.setCollectCount(productRelationService.collectNum(userDomain.getUid()));
        userCenterResponse.setVip(false);

        if (userLevelDomain != null) {
            userCenterResponse.setVip(true);
            userCenterResponse.setVipIcon(userLevelDomain.getIcon());
            userCenterResponse.setVipName(userLevelDomain.getName());
        }
        if (rechargeSwitch != null) {
            userCenterResponse.setRechargeSwitch(Boolean.valueOf(rechargeSwitch));
        }
        if (!funcStatus.equals("1")) {
            userCenterResponse.setIsPromoter(false);
        }

        userVisitRecordService.addRecord(4);
        return userCenterResponse;
    }

    @Override
    public List<AppUserLevelResponse> level() {
        List<EbSystemUserLevelDomain> systemUserLevelDomains = systemUserLevelService.select();
        return BeanCopierUtil.copyList(systemUserLevelDomains, AppUserLevelResponse.class);
    }

    @Override
    public PagerInfo<AppUserExpResponse> expRecord(PagerDTO request) {
        Integer uid = userService.getUserIdException();
        return PagerUtil.page(request, () -> userExperienceRecordService.listByUid(uid));
    }

    @Override
    public AppUserCapitalResponse capital() {
        EbUserDomain info = userService.getUserInfo();
        List<EbUserBillDomain> incomes  = userBillService.listByPmAndType(info.getUid(), 1, MallConstant.USER_BILL_CATEGORY_MONEY);
        List<EbUserBillDomain> expenses = userBillService.listByPmAndType(info.getUid(), 0, MallConstant.USER_BILL_CATEGORY_MONEY);
        BigDecimal income  = StreamUtil.sumBigDecimal(incomes, EbUserBillDomain::getNumber);
        BigDecimal expense = StreamUtil.sumBigDecimal(expenses, EbUserBillDomain::getNumber);

        AppUserCapitalResponse response = new AppUserCapitalResponse();
        response.setNowMoney(info.getNowMoney());
        response.setRecharge(income);
        response.setOrderStatusSum(expense);
        return response;
    }

    @Override
    public Map<String, Object> menus() {
        Map<String, Object> map = new HashMap<>();
        map.put("routine_my_menus",  systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_USER_CENTER_MENU));
        map.put("routine_my_banner", systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_USER_CENTER_BANNER));
        return map;
    }
}
