package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbTemplateMessageDomain
 * @author gaolinge
 */
@Data
public class EbTemplateMessageDomain {
    /** 
     * 模板id
     */
    private Integer id;

    /** 
     * 0=订阅消息,1=微信模板消息
     */
    private Boolean type;

    /** 
     * 模板编号
     */
    private String tempKey;

    /** 
     * 模板名
     */
    private String name;

    /** 
     * 回复内容
     */
    private String content;

    /** 
     * 模板ID
     */
    private String tempId;

    /** 
     * 状态
     */
    private Byte status;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
