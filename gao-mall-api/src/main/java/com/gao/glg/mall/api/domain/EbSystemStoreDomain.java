package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemStoreDomain
 * @author gaolinge
 */
@Data
public class EbSystemStoreDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 门店名称
     */
    private String name;

    /** 
     * 简介
     */
    private String introduction;

    /** 
     * 手机号码
     */
    private String phone;

    /** 
     * 省市区
     */
    private String address;

    /** 
     * 详细地址
     */
    private String detailedAddress;

    /** 
     * 门店logo
     */
    private String image;

    /** 
     * 纬度
     */
    private String latitude;

    /** 
     * 经度
     */
    private String longitude;

    /** 
     * 核销有效日期
     */
    private String validTime;

    /** 
     * 每日营业开关时间
     */
    private String dayTime;

    /** 
     * 是否显示
     */
    private Boolean isShow;

    /** 
     * 是否删除
     */
    private Boolean isDel;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
