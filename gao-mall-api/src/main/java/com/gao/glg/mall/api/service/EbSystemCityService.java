package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbSystemCityDomain;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemCityService {

    /**
     * 查询
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    List<EbSystemCityDomain> select();

    /**
     * 根据城市名称查询
     * <p/>
     *
     * @param cityName
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    EbSystemCityDomain getByCityName(String cityName);

    /**
     * 根据城市id查询
     * <p/>
     *
     * @param cityId
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    EbSystemCityDomain getByCityId(Integer cityId);

    /**
     * 查询所有城市id
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    List<Integer> selectCityIds();

}
