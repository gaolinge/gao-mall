package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSmsTemplateDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSmsTemplateMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSmsTemplateDomain domain);

    /**
     * 查询
     */
    List<EbSmsTemplateDomain> select();

    /**
     * 更新
     */
    int update(EbSmsTemplateDomain domain);
}
