package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbWechatReplyDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbWechatReplyMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbWechatReplyDomain domain);

    /**
     * 查询
     */
    List<EbWechatReplyDomain> select();

    /**
     * 更新
     */
    int update(EbWechatReplyDomain domain);
}
