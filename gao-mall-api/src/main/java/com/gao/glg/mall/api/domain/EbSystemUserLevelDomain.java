package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemUserLevelDomain
 * @author gaolinge
 */
@Data
public class EbSystemUserLevelDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 会员名称
     */
    private String name;

    /** 
     * 达到多少升级经验
     */
    private Integer experience;

    /** 
     * 是否显示 1=显示,0=隐藏
     */
    private Boolean isShow;

    /** 
     * 会员等级
     */
    private Integer grade;

    /** 
     * 享受折扣
     */
    private Integer discount;

    /** 
     * 会员图标
     */
    private String icon;

    /** 
     * 是否删除.1=删除,0=未删除
     */
    private Boolean isDel;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
