package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.web.WebStaffResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/16
 */
public interface IStaffBusiness {

    /**
     * 列表
     * <p/>
     *
     * @param storeId
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    PagerInfo<WebStaffResponse> list(Integer storeId, PagerDTO request);
}
