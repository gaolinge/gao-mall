package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.response.web.WebCateSearchRequest;
import com.gao.glg.mall.api.domain.EbCategoryDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbCategoryMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbCategoryDomain domain);

    /**
     * 查询
     */
    List<EbCategoryDomain> select();

    /**
     * 更新
     */
    int update(EbCategoryDomain domain);

    /**
     * 查询
     */
    List<EbCategoryDomain> listByTypeAndStatus(@Param("type") Integer type, @Param("status") Integer status,
                                               @Param("name") String name, @Param("categoryIds") List<Integer> categoryIds);

    /**
     * 查询
     */
    List<EbCategoryDomain> listByCid(String cid);

    /**
     * 查询
     */
    List<EbCategoryDomain> listByCids(List<Integer> cids);

    /**
     * 查询
     */
    List<EbCategoryDomain> search(@Param("request") WebCateSearchRequest request);

    /**
     * 查询
     */
    int queryNum(@Param("name") String name, @Param("type") Integer type);

    /**
     * 查询
     */
    EbCategoryDomain getById(Integer id);

    /**
     * 查询
     */
    int queryChildNum(String path);

    /**
     * 更新
     */
    void updateStatus(Integer id);
}
