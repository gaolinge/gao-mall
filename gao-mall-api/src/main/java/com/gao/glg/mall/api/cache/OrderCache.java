package com.gao.glg.mall.api.cache;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.mall.api.bean.dto.AppOrderInfoDTO;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.utils.CrmebUtil;
import com.gao.glg.mall.api.utils.MallUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 订单缓存
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/6
 */
@Service
public class OrderCache {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 预下单
     *
     * @param uid
     * @param orderDTO
     * @return
     */
    public String addPreOrder(Integer uid, AppOrderInfoDTO orderDTO) {
        String key = uid + MallUtil.timestamp() + CrmebUtil.getUuid();
        stringRedisTemplate.opsForValue().set(buildKey(key), JSONObject.toJSONString(orderDTO),
                MallConstant.ORDER_CASH_CONFIRM, TimeUnit.MINUTES);
        return key;
    }

    /**
     * 获取预下单
     *
     * @param key
     * @return
     */
    public AppOrderInfoDTO getPreOrder(String key) {
        String value = stringRedisTemplate.opsForValue().get(buildKey(key));
        if (value == null) {
            return null;
        }
        return JSONObject.parseObject(value, AppOrderInfoDTO.class);
    }

    /**
     * 删除预下单
     *
     * @param key
     */
    public void delete(String key) {
        stringRedisTemplate.delete(buildKey(key));
    }

    /**
     * 添加超时取消
     *
     * @param orderNo
     */
    public void addTimeoutCancel(String orderNo) {
        stringRedisTemplate.opsForList().leftPush(MallConstant.ORDER_AUTO_CANCEL_KEY, orderNo);
    }

    /**
     * 构建key
     *
     * @param key
     * @return
     */
    public String buildKey(String key) {
        return "user_order:" + key;
    }


}
