package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserExtractDomain;
import com.gao.glg.mall.api.mapper.EbUserExtractMapper;
import com.gao.glg.mall.api.service.EbUserExtractService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserExtractServiceImpl implements EbUserExtractService  {
    @Resource
    private EbUserExtractMapper ebUserExtractMapper;
}
