package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebProductRuleRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductRuleSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebProductRuleResponse;
import com.gao.glg.page.PagerInfo;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/27
 */
public interface IProductRuleBusiness {

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    PagerInfo<WebProductRuleResponse> page(WebProductRuleSearchRequest request);

    /**
     * 新建
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void save(WebProductRuleRequest request);

    /**
     * 删除
     * <p/>
     *
     * @param ids
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void deletes(List<Integer> ids);

    /**
     * 修改
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void update(WebProductRuleRequest request);

    /**
     * 详情
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    WebProductRuleResponse detail(Integer id);
}
