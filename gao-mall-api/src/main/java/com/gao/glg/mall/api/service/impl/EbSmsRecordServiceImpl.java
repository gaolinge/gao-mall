package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSmsRecordDomain;
import com.gao.glg.mall.api.mapper.EbSmsRecordMapper;
import com.gao.glg.mall.api.service.EbSmsRecordService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSmsRecordServiceImpl implements EbSmsRecordService  {
    @Resource
    private EbSmsRecordMapper ebSmsRecordMapper;
}
