package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbUserDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbUserService {

    /**
     * 获取用户信息
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    EbUserDomain getUserInfo();

    /**
     * 获取用户id
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    Integer getUserId();

    /**
     * 获取用户id
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    Integer getUserIdException();

    /**
     * 获取用户信息
     * <p/>
     *
     * @param account
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    EbUserDomain getByAccount(String account);

    /**
     * 更新最后登录时间
     * <p/>
     *
     * @param uid
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void updateLastLoginTime(Integer uid);

    /**
     * 更新用户信息
     * <p/>
     *
     * @param userDomain
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void update(EbUserDomain userDomain);

    /**
     * 获取用户信息
     * <p/>
     *
     * @param uids
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbUserDomain> listByIds(List<Integer> uids);
}
