package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.bean.response.app.AppUserExpResponse;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserExperienceRecordDomain;
import com.gao.glg.mall.api.mapper.EbUserExperienceRecordMapper;
import com.gao.glg.mall.api.service.EbUserExperienceRecordService;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserExperienceRecordServiceImpl implements EbUserExperienceRecordService  {
    @Resource
    private EbUserExperienceRecordMapper userExperienceRecordMapper;

    @Override
    public List<AppUserExpResponse> listByUid(Integer uid) {
        return userExperienceRecordMapper.listByUid(uid);
    }

    @Override
    public void save(EbUserExperienceRecordDomain domain) {
        userExperienceRecordMapper.save(domain);
    }
}
