package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.web.WebArticleSearchRequest;
import com.gao.glg.mall.api.domain.EbArticleDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * @author gaolinge
 */
public interface EbArticleMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbArticleDomain domain);

    /**
     * 查询
     */
    List<EbArticleDomain> select();

    /**
     * 更新
     */
    int update(EbArticleDomain domain);

    /**
     * 查询
     */
    List<EbArticleDomain> listByCid(String cid);

    /**
     * 查询
     */
    List<EbArticleDomain> hotList();

    /**
     * 查询
     */
    List<EbArticleDomain> bannerList();

    /**
     * 查询
     */
    EbArticleDomain getById(Integer id);

    /**
     * 查询
     */
    List<EbArticleDomain> page(@Param("request") WebArticleSearchRequest request);
}
