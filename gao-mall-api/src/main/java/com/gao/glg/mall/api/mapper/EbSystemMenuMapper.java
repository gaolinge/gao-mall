package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemMenuDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemMenuMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemMenuDomain domain);

    /**
     * 查询
     */
    List<EbSystemMenuDomain> all();

    /**
     * 查询
     */
    List<EbSystemMenuDomain> selectAll();

    /**
     * 更新
     */
    int update(EbSystemMenuDomain domain);

    /**
     * 查询
     */
    List<String> selectPerms(List<String> roles);

    /**
     * 查询
     */
    List<EbSystemMenuDomain> listByRoles(List<String> roles);

}
