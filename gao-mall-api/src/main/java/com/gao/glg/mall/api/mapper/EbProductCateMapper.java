package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbProductCateDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbProductCateMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbProductCateDomain domain);

    /**
     * 查询
     */
    List<EbProductCateDomain> select();

    /**
     * 更新
     */
    int update(EbProductCateDomain domain);
}
