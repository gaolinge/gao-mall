package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbStoreProductCateDomain
 * @author gaolinge
 */
@Data
public class EbProductCateDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 商品id
     */
    private Integer productId;

    /** 
     * 分类id
     */
    private Integer cateId;

    /** 
     * 添加时间
     */
    private Integer addTime;


}
