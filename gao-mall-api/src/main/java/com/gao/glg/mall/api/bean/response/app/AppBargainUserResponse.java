package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 砍价用户详情
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/22
 */
@Data
public class AppBargainUserResponse implements Serializable {

    @ApiModelProperty(value = "当前用户砍价状态：1-可以参与砍价,2-参与次数已满，3-砍价中,4-已完成，5-可以帮砍，6-已帮砍,7-帮砍次数已满，8-已生成订单未支付，9-已支付，10-未支付，已取消")
    private Integer bargainStatus;

    @ApiModelProperty(value = "已砍金额")
    private BigDecimal alreadyPrice;

    @ApiModelProperty(value = "剩余金额")
    private BigDecimal surplusPrice;

    @ApiModelProperty(value = "砍价百分比")
    private Integer bargainPercent;

    @ApiModelProperty(value = "用户帮砍列表")
    private List<AppBargainHelpResponse> userHelpList;

    @ApiModelProperty(value = "用户砍价活动id")
    private Integer storeBargainUserId;

    @ApiModelProperty(value = "用户砍价活动昵称")
    private String storeBargainUserName;

    @ApiModelProperty(value = "用户砍价活动头像")
    private String storeBargainUserAvatar;
}
