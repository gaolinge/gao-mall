package com.gao.glg.mall.api.bean.response.app;

import com.gao.glg.serializer.FileUrlSerializerField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 秒杀商品
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/23
 */
@Data
public class AppSeckillProductResponse implements Serializable {
    /** 
     * 商品秒杀产品表id
     */
    private Integer id;

    /** 
     * 商品id
     */
    private Integer productId;

    /** 
     * 推荐图
     */
    @FileUrlSerializerField
    private String image;

    /** 
     * 轮播图
     */
    @FileUrlSerializerField
    private String images;

    /** 
     * 活动标题
     */
    private String title;

    /** 
     * 简介
     */
    private String info;

    /** 
     * 价格
     */
    private BigDecimal price;

    /** 
     * 成本
     */
    private BigDecimal cost;

    /** 
     * 原价
     */
    private BigDecimal otPrice;

    /** 
     * 返多少积分
     */
    private BigDecimal giveIntegral;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 库存
     */
    private Integer stock;

    /** 
     * 销量
     */
    private Integer sales;

    /** 
     * 单位名
     */
    private String unitName;

    /** 
     * 邮费
     */
    private BigDecimal postage;

    /** 
     * 内容
     */
    private String description;

    /** 
     * 开始时间
     */
    private Date startTime;

    /** 
     * 结束时间
     */
    private Date stopTime;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * 秒杀状态 0=关闭 1=开启
     */
    private Byte status;

    /** 
     * 是否包邮
     */
    private Byte isPostage;

    /**
     * 当天参与活动次数
     */
    private Integer num;

    /** 
     * 显示
     */
    private Byte isShow;

    /** 
     * 时间段ID
     */
    private Integer timeId;

    /** 
     * 运费模板ID
     */
    private Integer tempId;

    /** 
     * 重量
     */
    private BigDecimal weight;

    /** 
     * 体积
     */
    private BigDecimal volume;

    /** 
     * 限购总数,随减
     */
    private Integer quota;

    /** 
     * 限购总数显示.不变
     */
    private Integer quotaShow;

    /** 
     * 规格 0=单 1=多
     */
    private Boolean specType;


}
