package com.gao.glg.mall.api.business.impl;

import com.gao.glg.enums.YesNoEnum;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.app.AppUserAddrCityRequest;
import com.gao.glg.mall.api.bean.request.app.AppUserAddressRequest;
import com.gao.glg.mall.api.bean.response.app.AppUserAddressResponse;
import com.gao.glg.mall.api.business.IAddressBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.domain.EbSystemCityDomain;
import com.gao.glg.mall.api.domain.EbUserAddressDomain;
import com.gao.glg.mall.api.service.EbSystemCityService;
import com.gao.glg.mall.api.service.EbUserAddressService;
import com.gao.glg.mall.api.service.EbUserService;
import com.gao.glg.mall.api.utils.Expression;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AddressBusiness implements IAddressBusiness {

    @Autowired
    private EbSystemCityService systemCityService;
    @Autowired
    private EbUserService userService;
    @Autowired
    private EbUserAddressService userAddressService;

    @Override
    public PagerInfo<AppUserAddressResponse> list(PagerDTO request) {
        Integer uid = userService.getUserIdException();
        return PagerUtil.page(request, () -> {
            List<EbUserAddressDomain> userAddressDomains = userAddressService.listByUid(uid);
            return BeanCopierUtil.copyList(userAddressDomains, AppUserAddressResponse.class);
        });
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public AppUserAddressResponse create(AppUserAddressRequest request) {
        AppUserAddrCityRequest cityRequest = request.getAddress();
        EbSystemCityDomain systemCityDomain = checkCityParam(cityRequest);

        Integer uid = userService.getUserIdException();
        if (request.getIsDefault()) {
            userAddressService.canalDefault(uid);
        }

        int isDef = request.getIsDefault() ? YesNoEnum.YES.getValue() : YesNoEnum.NO.getValue();
        EbUserAddressDomain userAddressDomain = new EbUserAddressDomain();
        userAddressDomain.setUid(uid);
        userAddressDomain.setId(request.getId());
        userAddressDomain.setRealName(request.getRealName());
        userAddressDomain.setPhone(request.getPhone());
        userAddressDomain.setDetail(request.getDetail());
        userAddressDomain.setIsDefault(isDef);
        userAddressDomain.setDistrict(cityRequest.getDistrict());
        userAddressDomain.setProvince(cityRequest.getProvince());
        userAddressDomain.setCity(systemCityDomain.getName());
        userAddressDomain.setCityId(systemCityDomain.getCityId());
        userAddressDomain.setLatitude(systemCityDomain.getLat());
        userAddressDomain.setLongitude(systemCityDomain.getLng());
        userAddressDomain.setIsDel(YesNoEnum.NO.getValue());

        Expression.noResult(request.getId() == null,
                () -> userAddressService.save(userAddressDomain),
                () -> userAddressService.update(userAddressDomain));
        return BeanCopierUtil.copy(userAddressDomain, AppUserAddressResponse.class);
    }

    @Override
    public AppUserAddressResponse detail(Integer id) {
        Integer uid = userService.getUserIdException();
        EbUserAddressDomain domain = userAddressService.getByUidAndId(uid, id);
        return BeanCopierUtil.copy(domain, AppUserAddressResponse.class);
    }

    @Override
    public void setDefault(Object id) {
        Integer uid = userService.getUserIdException();
        EbUserAddressDomain domain = new EbUserAddressDomain();
        domain.setId(Integer.parseInt(id.toString()));
        domain.setUid(uid);
        domain.setIsDefault(YesNoEnum.YES.getValue());

        userAddressService.canalDefault(uid);
        userAddressService.update(domain);
    }

    @Override
    public void delete(Object id) {
        Integer uid = userService.getUserIdException();
        EbUserAddressDomain domain = new EbUserAddressDomain();
        domain.setId(Integer.parseInt(id.toString()));
        domain.setUid(uid);
        domain.setIsDel(YesNoEnum.YES.getValue());
        userAddressService.update(domain);
    }

    @Override
    public AppUserAddressResponse getDefault() {
        Integer uid = userService.getUserIdException();
        EbUserAddressDomain domain = userAddressService.getByDefault(uid);
        if (domain == null) {
            return null;
        }
        return BeanCopierUtil.copy(domain, AppUserAddressResponse.class);
    }

    /**
     * 校验城市参数
     * <p/>
     *
     * @param cityRequest
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    private EbSystemCityDomain checkCityParam(AppUserAddrCityRequest cityRequest) {
        if (cityRequest.getCityId() == null &&
                cityRequest.getCity() == null) {
            throw new BusinessException(ErrorConstant.NO_CITY_DATA);
        }

        EbSystemCityDomain systemCityDomain = null;
        if (cityRequest.getCity() != null) {
            systemCityDomain = systemCityService.getByCityName(cityRequest.getCity());
        }

        if (cityRequest.getCityId() != null) {
            systemCityDomain = systemCityService.getByCityId(cityRequest.getCityId());
        }

        if (systemCityDomain == null) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_CITY);
        }
        return systemCityDomain;
    }
}
