package com.gao.glg.mall.api.bean.response.app;

import com.gao.glg.serializer.FileUrlSerializerField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
@Data
public class AppUserCollectResponse implements Serializable {

    @ApiModelProperty(value = "收藏id")
    private Integer id;

    @ApiModelProperty(value = "商品ID")
    private Integer productId;

    @ApiModelProperty(value = "商品名称")
    private String storeName;

    @FileUrlSerializerField
    @ApiModelProperty(value = "商品图片")
    private String image;

    @ApiModelProperty(value = "商品价格")
    private BigDecimal price;
}
