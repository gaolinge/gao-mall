package com.gao.glg.mall.api.bean.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * 图片上传返回
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/7/3
 */
@Data
public class ImageUploadResponse implements Serializable {

    @ApiModelProperty("错误码")
    private Integer errno;
    @ApiModelProperty("值")
    private Map<String, Object> data;
}
