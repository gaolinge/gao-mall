package com.gao.glg.mall.api.business.impl;

import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.web.WebProductRuleRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductRuleSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebProductRuleResponse;
import com.gao.glg.mall.api.business.IProductRuleBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.domain.EbProductRuleDomain;
import com.gao.glg.mall.api.service.EbProductRuleService;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductRuleBusiness implements IProductRuleBusiness {

    @Autowired
    private EbProductRuleService productRuleService;

    @Override
    public PagerInfo<WebProductRuleResponse> page(WebProductRuleSearchRequest request) {
        return PagerUtil.page(request, () -> {
            List<EbProductRuleDomain> domains = productRuleService.page(request);
            return BeanCopierUtil.copyList(domains, WebProductRuleResponse.class);
        });
    }

    @Override
    public void save(WebProductRuleRequest request) {
        EbProductRuleDomain domain = productRuleService.getByRuleName(request.getRuleName());
        if (domain != null) {
            throw new BusinessException(ErrorConstant.PRODUCT_RULE_NAME_ALREADY_EXISTS);
        }
        domain = BeanCopierUtil.copy(request, EbProductRuleDomain.class);
        productRuleService.save(domain);
    }

    @Override
    public void deletes(List<Integer> ids) {
        productRuleService.deletes(ids);
    }

    @Override
    public void update(WebProductRuleRequest request) {
        EbProductRuleDomain domain = BeanCopierUtil.copy(request, EbProductRuleDomain.class);
        productRuleService.update(domain);
    }

    @Override
    public WebProductRuleResponse detail(Integer id) {
        EbProductRuleDomain domain = productRuleService.getById(id);
        return BeanCopierUtil.copy(domain, WebProductRuleResponse.class);
    }
}
