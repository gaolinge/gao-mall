package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.gao.glg.mall.api.bean.response.web.WebValidateCodeResponse;
import com.gao.glg.mall.api.business.IValidateBusiness;
import com.gao.glg.mall.api.cache.ValidateCodeCache;
import com.gao.glg.mall.api.utils.CrmebUtil;
import lombok.Data;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Random;

@Service
public class ValidateBusiness implements IValidateBusiness {
    private static final Random random = new Random();
    private static final String secret = "0123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
    private static final int width = 80;
    private static final int height = 34;
    private static final int number = 4;
    private static final int lineNum = 20;


    @Autowired
    private ValidateCodeCache validateCodeCache;

    @Override
    public WebValidateCodeResponse get() {
        Validate validate = buildRandomCode();
        if (ObjectUtil.isNull(validate)) {
            return null;
        }

        String val = validate.getValue().toLowerCase();
        String key = DigestUtils.md5Hex(val);
        String base64Str = validate.getImage();
        validateCodeCache.setTimeout(key, val);
        WebValidateCodeResponse response = new WebValidateCodeResponse();
        response.setKey(key);
        response.setCode(CrmebUtil.getBase64Image(base64Str));
        return response;
    }


    /**
     * 构建随机码
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/12
     * @version : 1.0.0
     */
    public static Validate buildRandomCode() {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
        Graphics Graphics = image.getGraphics();
        Graphics.fillRect(0, 0, width, height);
        Graphics.setFont(new Font("Times New Roman", Font.ROMAN_BASELINE, 18));
        Graphics.setColor(getRandColor(110, 133));
        for (int i = 0; i <= lineNum; i++) { drawLine(Graphics);}

        StringBuilder builder = new StringBuilder();
        for (int i = 1; i <= number; i++) { drawString(Graphics, builder, i); }
        Graphics.dispose();

        ByteArrayOutputStream bs = null;
        Validate validate = new Validate();
        try {
            bs = new ByteArrayOutputStream();
            ImageIO.write(image, "png", bs);
            String imgSrc = Base64.getEncoder().encodeToString(bs.toByteArray());
            validate.setImage(imgSrc);
            validate.setValue(builder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                bs.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                bs = null;
            }
        }
        return validate;
    }


    /**
     * 获得颜色
     * <p/>
     *
     * @param frontColor
     * @param backColor
     * @return
     * @author : gaolinge
     * @date : 2024/12/12
     * @version : 1.0.0
     */
    private static Color getRandColor(int frontColor, int backColor) {
        if (frontColor > 255) {
            frontColor = 255;
        }
        if (backColor > 255) {
            backColor = 255;
        }

        int red  = frontColor + random.nextInt(backColor - frontColor - 16);
        int gre  = frontColor + random.nextInt(backColor - frontColor - 14);
        int blue = frontColor + random.nextInt(backColor - frontColor - 18);
        return new Color(red, gre, blue);
    }

    /**
     * 绘制干扰线
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/12
     * @version : 1.0.0
     */
    private static void drawLine(Graphics graphics) {
        int x0 = random.nextInt(width);
        int y0 = random.nextInt(height);
        int x1 = random.nextInt(13);
        int y1 = random.nextInt(15);
        Color color = new Color(random.nextFloat(), random.nextFloat(), random.nextFloat());
        graphics.setColor(color);
        graphics.drawLine(x0, y0, x0 + x1, y0 + y1);
    }

    /**
     * 绘制字符串
     * <p/>
     *
     * @param graphics
     * @param builder
     * @param i
     * @return
     * @author : gaolinge
     * @date : 2024/12/12
     * @version : 1.0.0
     */
    private static void drawString(Graphics graphics, StringBuilder builder, int i) {
        Font font = new Font("Fixedsys", Font.CENTER_BASELINE, 25);
        int rot = RandomUtil.randomInt(1, 10);

        Graphics2D g2d = (Graphics2D) graphics;
        g2d.setFont(font);
        g2d.setColor(new Color(random.nextFloat(), random.nextFloat(), random.nextFloat()));

        int index = random.nextInt(secret.length());
        String randChar = String.valueOf(secret.charAt(index));
        builder.append(randChar);

        g2d.translate(random.nextInt(3), random.nextInt(3));
        g2d.rotate(rot * Math.PI / 180);
        g2d.drawString(randChar, 13 * i, 20);
        g2d.rotate(-rot * Math.PI / 180);
    }

    @Data
    public static class Validate {
        private String image;
        private String value;
    }
}
