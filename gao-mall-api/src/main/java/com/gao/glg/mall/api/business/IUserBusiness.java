package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.app.AppUserCapitalResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserExpResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppUserLevelResponse;
import com.gao.glg.mall.api.bean.response.web.WebMenuResponse;
import com.gao.glg.mall.api.bean.response.web.WebUserInfoResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/4
 */
public interface IUserBusiness {

    /**
     * 信息
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    AppUserInfoResponse appUserInfo();

    /**
     * 等级
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    List<AppUserLevelResponse> level();

    /**
     * 经验记录
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/7
     * @version : 1.0.0
     */
    PagerInfo<AppUserExpResponse> expRecord(PagerDTO request);

    /**
     * 用户资金
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/10
     * @version : 1.0.0
     */
    AppUserCapitalResponse capital();


    /**
     * 菜单
     * <p/>
     *
     * @param
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    Map<String, Object> menus();

}
