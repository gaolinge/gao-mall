package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbShippingTemplatesDomain
 * @author gaolinge
 */
@Data
public class EbShippingTempDomain {
    /** 
     * 编号
     */
    private Integer id;

    /** 
     * 模板名称
     */
    private String name;

    /** 
     * 计费方式
     */
    private Integer type;

    /** 
     * 指定包邮
     */
    private Boolean appoint;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
