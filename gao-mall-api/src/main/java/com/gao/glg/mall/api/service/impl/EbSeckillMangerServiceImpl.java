package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbSeckillMangerDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbSeckillMangerMapper;
import com.gao.glg.mall.api.service.EbSeckillMangerService;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSeckillMangerServiceImpl implements EbSeckillMangerService {
    @Resource
    private EbSeckillMangerMapper seckillMangerMapper;

    @Override
    public List<EbSeckillMangerDomain> listByCurrent() {
        return seckillMangerMapper.listByCurrent();
    }

    @Override
    public List<EbSeckillMangerDomain> select(int num) {
        return seckillMangerMapper.select(num);
    }

    @Override
    public EbSeckillMangerDomain getById(Integer timeId) {
        return seckillMangerMapper.getById(timeId);
    }
}
