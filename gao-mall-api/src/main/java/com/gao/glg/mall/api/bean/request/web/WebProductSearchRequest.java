package com.gao.glg.mall.api.bean.request.web;

import com.gao.glg.page.PagerDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * 商品
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/18
 */
@Data
public class WebProductSearchRequest extends PagerDTO {

    @ApiModelProperty(value = "类型（1：出售中（已上架），2：仓库中（未上架），3：已售罄，4：警戒库存，5：回收站）")
    @NotNull(message = "商品类型不能为空")
    @Range(min = 1, max = 5, message = "未知的商品类型")
    private int type;

    @ApiModelProperty(value = "分类ID， 多个逗号分隔")
    private String cateId;

    @ApiModelProperty(value = "关键字搜索， 支持(商品名称, 商品简介, 关键字, 商品条码)")
    private String keywords;

    @ApiModelProperty(value = "库存", hidden = true)
    private Integer stock;

}
