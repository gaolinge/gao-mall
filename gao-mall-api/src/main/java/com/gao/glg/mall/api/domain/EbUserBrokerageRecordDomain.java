package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbUserBrokerageRecordDomain
 * @author gaolinge
 */
@Data
public class EbUserBrokerageRecordDomain {
    /** 
     * 记录id
     */
    private Integer id;

    /** 
     * 用户uid
     */
    private Integer uid;

    /** 
     * 关联id（orderNo,提现id）
     */
    private String linkId;

    /** 
     * 关联类型（order,extract，yue）
     */
    private String linkType;

    /** 
     * 类型：1-增加，2-扣减（提现）
     */
    private Integer type;

    /** 
     * 标题
     */
    private String title;

    /** 
     * 金额
     */
    private BigDecimal price;

    /** 
     * 剩余
     */
    private BigDecimal balance;

    /** 
     * 备注
     */
    private String mark;

    /** 
     * 状态：1-订单创建，2-冻结期，3-完成，4-失效（订单退款），5-提现申请
     */
    private Boolean status;

    /** 
     * 冻结期时间（天）
     */
    private Integer frozenTime;

    /** 
     * 解冻时间
     */
    private Long thawTime;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 分销等级
     */
    private Integer brokerageLevel;


}
