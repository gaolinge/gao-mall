package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbStoreSeckillMangerDomain
 * @author gaolinge
 */
@Data
public class EbSeckillMangerDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 秒杀名称
     */
    private String name;

    /** 
     * 秒杀开始时间段
     */
    private Integer startTime;

    /** 
     * 秒杀结束时间段
     */
    private Integer endTime;

    /** 
     * 主图
     */
    private String img;

    /** 
     * 轮播图
     */
    private String silderImgs;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 状态 0=关闭 1=开启
     */
    private Integer status;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 删除标记 0=为删除 1=删除
     */
    private Integer isDel;


}
