package com.gao.glg.mall.api.business.impl;

import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.web.WebRoleRequest;
import com.gao.glg.mall.api.bean.request.web.WebRoleSearchRequest;
import com.gao.glg.mall.api.bean.response.app.AppCityTreeResponse;
import com.gao.glg.mall.api.bean.response.web.WebMenuResponse;
import com.gao.glg.mall.api.bean.response.web.WebRoleResponse;
import com.gao.glg.mall.api.bean.response.web.WebSystemRoleResponse;
import com.gao.glg.mall.api.business.IRoleBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.domain.EbSystemCityDomain;
import com.gao.glg.mall.api.domain.EbSystemMenuDomain;
import com.gao.glg.mall.api.domain.EbSystemRoleDomain;
import com.gao.glg.mall.api.domain.EbSystemRoleMenuDomain;
import com.gao.glg.mall.api.service.EbSystemMenuService;
import com.gao.glg.mall.api.service.EbSystemRoleMenuService;
import com.gao.glg.mall.api.service.EbSystemRoleService;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.StreamUtil;
import com.gao.glg.utils.TreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RoleBusiness implements IRoleBusiness {

    @Autowired
    private EbSystemRoleService systemRoleService;
    @Autowired
    private EbSystemMenuService systemMenuService;
    @Autowired
    private EbSystemRoleMenuService systemRoleMenuService;

    @Override
    public PagerInfo<WebSystemRoleResponse> page(WebRoleSearchRequest request) {
        return PagerUtil.page(request, () -> {
            List<EbSystemRoleDomain> domains = systemRoleService.page(request);
            return BeanCopierUtil.copyList(domains, WebSystemRoleResponse.class);
        });
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void create(WebRoleRequest request) {
        EbSystemRoleDomain domain = systemRoleService.getByRoleName(request.getRoleName());
        if (domain != null) {
            throw new BusinessException(ErrorConstant.ROLE_NAME_ALREADY_EXIST);
        }

        domain = BeanCopierUtil.copy(request, EbSystemRoleDomain.class);
        domain.setId(null);
        domain.setRules("");
        systemRoleService.save(domain);

        List<EbSystemRoleMenuDomain> roleMenuDomains = new ArrayList<>();
        for (String roleId : request.getRules().split(",")) {
            EbSystemRoleMenuDomain roleMenu = new EbSystemRoleMenuDomain();
            roleMenu.setRid(domain.getId());
            roleMenu.setMenuId(Integer.valueOf(roleId));
            roleMenuDomains.add(roleMenu);
        }
        systemRoleMenuService.batchSave(roleMenuDomains);
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void update(WebRoleRequest request) {
        EbSystemRoleDomain domain = systemRoleService.getById(request.getId());
        if (domain == null) {
            throw new BusinessException(ErrorConstant.ROLE_NOT_EXIST);
        }
        domain = systemRoleService.getByRoleName(request.getRoleName());
        if (domain != null && !domain.getId().equals(request.getId())) {
            throw new BusinessException(ErrorConstant.ROLE_NAME_ALREADY_EXIST);
        }

        domain = BeanCopierUtil.copy(request, EbSystemRoleDomain.class);
        domain.setRules("");
        systemRoleService.update(domain);

        List<EbSystemRoleMenuDomain> roleMenuDomains = new ArrayList<>();
        for (String roleId : request.getRules().split(",")) {
            EbSystemRoleMenuDomain roleMenu = new EbSystemRoleMenuDomain();
            roleMenu.setRid(domain.getId());
            roleMenu.setMenuId(Integer.valueOf(roleId));
            roleMenuDomains.add(roleMenu);
        }
        systemRoleMenuService.delete(domain.getId());
        systemRoleMenuService.batchSave(roleMenuDomains);
    }

    @Override
    public WebRoleResponse detail(Integer id) {
        EbSystemRoleDomain domain = systemRoleService.getById(id);
        if (domain == null) {
            throw new BusinessException(ErrorConstant.ROLE_NOT_EXIST);
        }

        List<EbSystemMenuDomain> menuDomains = systemMenuService.all();
        List<EbSystemRoleMenuDomain> roleMenuDomains = systemRoleMenuService.listByRid(domain.getId());
        Map<Integer, EbSystemRoleMenuDomain> menuMap = StreamUtil.listToMap(roleMenuDomains, EbSystemRoleMenuDomain::getMenuId);
        List<WebMenuResponse> menus = TreeUtil.buildTree(menuDomains, EbSystemMenuDomain::getPid, EbSystemMenuDomain::getId, 0,
                (list, dto) -> {
                    WebMenuResponse response = BeanCopierUtil.copy(dto, WebMenuResponse.class);
                    response.setChecked(menuMap.containsKey(dto.getId()));
                    response.setChildList(list);
                    return response;
                });

        WebRoleResponse response = BeanCopierUtil.copy(domain, WebRoleResponse.class);
        response.setMenuList(menus);
        return response;
    }

    @Transactional(rollbackFor = Throwable.class)
    @Override
    public void delete(Integer id) {
        EbSystemRoleDomain domain = systemRoleService.getById(id);
        if (domain == null) {
            throw new BusinessException(ErrorConstant.ROLE_NOT_EXIST);
        }
        systemRoleService.delete(id);
        systemRoleMenuService.delete(id);
    }

    @Override
    public void updateStatus(Integer id, Boolean status) {
        EbSystemRoleDomain domain = systemRoleService.getById(id);
        if (domain == null) {
            throw new BusinessException(ErrorConstant.ROLE_NOT_EXIST);
        }
        if (Objects.equals(domain.getStatus(), status)) {
            return;
        }

        domain = new EbSystemRoleDomain();
        domain.setId(id);
        domain.setStatus(status);
        systemRoleService.update(domain);
    }
}
