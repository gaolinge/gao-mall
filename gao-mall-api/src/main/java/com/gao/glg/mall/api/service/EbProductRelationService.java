package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.response.app.AppUserCollectResponse;
import com.gao.glg.mall.api.domain.EbProductRelationDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbProductRelationService {

    /**
     * 是否收藏或点赞
     * <p/>
     *
     * @param id
     * @param b
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    boolean isLike(Integer id, boolean b);

    /**
     * 收藏数量
     * <p/>
     *
     * @param uid
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    Integer collectNum(Integer uid);

    /**
     * 查询用户的收藏和点赞
     * <p/>
     *
     * @param uid
     * @param id
     * @param b
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductRelationDomain> likeOrCollect(Integer uid, Integer id, boolean b);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void save(EbProductRelationDomain domain);

    /**
     * 查询用户的收藏和点赞
     * <p/>
     *
     * @param uid
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<AppUserCollectResponse> list(Integer uid);

    /**
     * 删除收藏
     * <p/>
     *
     * @param uid
     * @param pid
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void delete(Integer uid, Integer pid);

    /**
     * 批量删除
     * <p/>
     *
     * @param uid
     * @param ids
     * @param type
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void batchDelete(Integer uid, List<String> ids, int type);

    /**
     * 批量保存
     * <p/>
     *
     * @param domains
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void batchSave(List<EbProductRelationDomain> domains);

    /**
     * 根据商品id和类型
     * <p/>
     *
     * @param pids
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductRelationDomain> listByPidsAndType(List<Integer> pids, String type);
}
