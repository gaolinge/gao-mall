package com.gao.glg.mall.api.bean.response.web;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/19
 */
@Data
public class WebUploadResponse implements Serializable {

    //可供访问的url 域名根据配置存储，代替了上面serverPath 的功能
    private String url;

    // 文件名
    private String fileName;

    // 扩展名
    private String extName;

    // 文件大小，字节
    private Long fileSize;

    //类型
    private String type;
}
