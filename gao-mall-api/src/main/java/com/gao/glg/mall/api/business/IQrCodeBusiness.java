package com.gao.glg.mall.api.business;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * 二维码
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/5
 */
public interface IQrCodeBusiness {

    /**
     * 获取二维码
     * <p/>
     *
     * @param data
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    Map<String, Object> get(JSONObject data);

    /**
     * base64
     * <p/>
     *
     * @param url
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    Map<String, Object> base64(String url);

    /**
     * 将字符串 转base64
     * <p/>
     *
     * @param text
     * @param width
     * @param height
     * @return
     * @author : gaolinge
     * @date : 2024/12/5
     * @version : 1.0.0
     */
    Map<String, Object> base64String(String text, int width, int height);
}
