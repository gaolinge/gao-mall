package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.app.AppArticleInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppArticleResponse;
import com.gao.glg.mall.api.bean.response.app.AppCategoryResponse;
import com.gao.glg.mall.api.business.IArticleBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 文章
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@Api(tags = "文章")
@RestController
@RequestMapping("/app/article")
public class AppArticleController {

    @Autowired
    private IArticleBusiness articleBusiness;


    @ApiOperation(value = "分页列表")
    @GetMapping(value = "/list/{cid}")
    public RestResult<PagerInfo<AppArticleResponse>> page(
            @PathVariable(name="cid") String cid, @Validated PagerDTO request) {
        return RestResult.success(articleBusiness.page(cid, request));
    }


    @ApiOperation(value = "热门列表")
    @GetMapping(value = "/hot/list")
    public RestResult<List<AppArticleResponse>> hotList() {
        return RestResult.success(articleBusiness.hotList());
    }


    @ApiOperation(value = "轮播列表")
    @GetMapping(value = "/banner/list")
    public RestResult<List<AppArticleInfoResponse>> bannerPage() {
        return RestResult.success(articleBusiness.bannerList());
    }


    @ApiOperation(value = "文章分类")
    @GetMapping(value = "/category/list")
    public RestResult<PagerInfo<AppCategoryResponse>> categoryPage() {
        return RestResult.success(articleBusiness.categoryPage());
    }


    @ApiOperation(value = "详情")
    @GetMapping(value = "/info")
    public RestResult<AppArticleResponse> info(@RequestParam(value = "id") Integer id) {
        return RestResult.success(articleBusiness.info(id));
   }
}



