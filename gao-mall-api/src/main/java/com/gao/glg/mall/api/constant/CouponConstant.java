package com.gao.glg.mall.api.constant;

/**
 * 优惠券常量类
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
public interface CouponConstant {

    /** 优惠券类型—手动领取 */
    Integer COUPON_TYPE_RECEIVE = 1;

    /** 优惠券类型—新人券 */
    Integer COUPON_TYPE_NEW_PEOPLE = 2;

    /** 优惠券类型—赠送券 */
    Integer COUPON_TYPE_GIVE_AWAY = 3;

    /** 优惠券使用类型-通用 */
    Integer COUPON_USE_TYPE_COMMON = 1;

    /** 优惠券使用类型-商品 */
    Integer COUPON_USE_TYPE_PRODUCT = 2;

    /** 优惠券使用类型-品类 */
    Integer COUPON_USE_TYPE_CATEGORY = 3;
    

    /** 用户优惠券领取类型—用户注册 */
    String STORE_COUPON_USER_TYPE_REGISTER = "new";

    /** 用户优惠券领取类型—用户领取 */
    String STORE_COUPON_USER_TYPE_GET = "receive";

    /** 用户优惠券领取类型—后台发放 */
    String STORE_COUPON_USER_TYPE_SEND = "send";

    /** 用户优惠券领取类型—买赠送 */
    String STORE_COUPON_USER_TYPE_BUY = "buy";

    /** 用户优惠券状态—未使用 */
    Integer STORE_COUPON_USER_STATUS_USABLE = 0;

    /** 用户优惠券状态—已使用 */
    Integer STORE_COUPON_USER_STATUS_USED = 1;

    /** 用户优惠券状态—已失效 */
    Integer STORE_COUPON_USER_STATUS_LAPSED = 2;



}
