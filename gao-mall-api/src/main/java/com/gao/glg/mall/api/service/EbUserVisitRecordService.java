package com.gao.glg.mall.api.service;
/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbUserVisitRecordService {

    /**
     * 添加访问记录
     * <p/>
     *
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    void addRecord(int type);
}
