package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.business.IStoreBusiness;
import com.gao.glg.page.PagerDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 提货点
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/store")
@Api(tags = "提货点")
public class AppStoreController {
    @Autowired
    private IStoreBusiness storeBusiness;

/*
    @ApiOperation(value = "附近的提货点")
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public RestResult<StoreNearResponse> register(@Validated StoreNearRequest request, @Validated PagerDTO PagerDTO){
        return RestResult.success(systemStoreService.getNearList(request, PagerDTO));
    }*/
}



