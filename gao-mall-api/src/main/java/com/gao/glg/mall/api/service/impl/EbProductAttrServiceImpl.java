package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbProductAttrDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbProductAttrMapper;
import com.gao.glg.mall.api.service.EbProductAttrService;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbProductAttrServiceImpl implements EbProductAttrService {
    @Resource
    private EbProductAttrMapper productAttrMapper;

    @Override
    public List<EbProductAttrDomain> listByPidAndType(Integer id, Integer type) {
        return productAttrMapper.listByPidAndType(id, type);
    }

    @Override
    public List<EbProductAttrDomain> listByPidsAndType(List<Integer> pids, Integer type) {
        return productAttrMapper.listByPidsAndType(pids, type);
    }
}
