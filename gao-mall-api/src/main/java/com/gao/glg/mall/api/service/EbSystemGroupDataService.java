package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebGroupDataSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemGroupDataDomain;

import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemGroupDataService {

    /**
     * 首页banner滚动图
     * <p/>
     *
     * @param gid
     * @return
     * @author : gaolinge
     * @date : 2024/12/4
     * @version : 1.0.0
     */
    List<Map<String, Object>> listByGid(Integer gid);

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    List<EbSystemGroupDataDomain> page(WebGroupDataSearchRequest request);

    /**
     * 新增
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void save(EbSystemGroupDataDomain domain);

    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void update(EbSystemGroupDataDomain domain);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 根据id获取
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    EbSystemGroupDataDomain getById(Integer id);
}
