package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.web.WebExpressSearchRequest;
import com.gao.glg.mall.api.bean.request.web.WebExpressUpdateRequest;
import com.gao.glg.mall.api.bean.response.web.WebExpressResponse;
import com.gao.glg.mall.api.business.IExpressBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.domain.EbExpressDomain;
import com.gao.glg.mall.api.service.EbExpressService;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExpressBusiness implements IExpressBusiness {

    @Autowired
    private EbExpressService expressService;

    @Override
    public PagerInfo<WebExpressResponse> page(WebExpressSearchRequest request) {
        return PagerUtil.page(request, () -> {
            List<EbExpressDomain> domains = expressService.page(request);
            return BeanCopierUtil.copyList(domains, WebExpressResponse.class);
        });
    }

    @Override
    public void syncExpress() {

    }

    @Override
    public JSONObject template(String no) {
        return null;
    }

    @Override
    public List<WebExpressResponse> all(String type) {
        List<EbExpressDomain> domains = expressService.listByType(type);
        return BeanCopierUtil.copyList(domains, WebExpressResponse.class);
    }

    @Override
    public WebExpressResponse detail(Integer id) {
        EbExpressDomain domain = expressService.getById(id);
        if (ObjectUtil.isNull(domain)) {
            throw new BusinessException(ErrorConstant.EXPRESS_DOES_NOT_EXIST);
        }
        return BeanCopierUtil.copy(domain, WebExpressResponse.class);
    }

    @Override
    public void update(WebExpressUpdateRequest request) {
        EbExpressDomain domain = expressService.getById(request.getId());
        if (domain == null) {
            throw new BusinessException(ErrorConstant.EXPRESS_DOES_NOT_EXIST);
        }

        domain = new EbExpressDomain();
        domain.setId(request.getId());
        domain.setAccount(request.getAccount());
        domain.setPassword(request.getPassword());
        domain.setNetName(request.getNetName());
        domain.setSort(request.getSort());
        domain.setStatus(request.getStatus());
        expressService.update(domain);
    }

    @Override
    public void updateShow(WebExpressUpdateRequest request) {
        EbExpressDomain domain = expressService.getById(request.getId());
        if (domain == null) {
            throw new BusinessException(ErrorConstant.EXPRESS_DOES_NOT_EXIST);
        }
        if (domain.getIsShow().equals(request.getIsShow())) {
            return;
        }

        domain = new EbExpressDomain();
        domain.setId(request.getId());
        domain.setIsShow(request.getIsShow());
        expressService.update(domain);
    }
}
