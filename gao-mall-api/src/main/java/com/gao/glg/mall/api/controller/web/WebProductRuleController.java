package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebProductRuleRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductRuleSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebProductRuleResponse;
import com.gao.glg.mall.api.business.IProductRuleBusiness;
import com.gao.glg.mall.api.utils.CrmebUtil;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/27
 */
@Slf4j
@RestController
@RequestMapping("/admin/product/rule")
@Api(tags = "商品规则")
public class WebProductRuleController {

    @Autowired
    private IProductRuleBusiness productRuleBusiness;

    //@PreAuthorize("hasAuthority('admin:product:rule:list')")
    @ApiOperation(value = "分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebProductRuleResponse>> page(@Validated WebProductRuleSearchRequest request) {
        return RestResult.success(productRuleBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:product:rule:save')")
    @ApiOperation(value = "新增")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult<String> save(@RequestBody @Validated WebProductRuleRequest request) {
        productRuleBusiness.save(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:rule:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete/{ids}", method = RequestMethod.GET)
    public RestResult<String> delete(@PathVariable String ids) {
        productRuleBusiness.deletes(CrmebUtil.stringToArray(ids));
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:rule:update')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RestResult<String> update(@RequestBody @Validated WebProductRuleRequest request) {
        productRuleBusiness.update(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:product:rule:info')")
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public RestResult<WebProductRuleResponse> detail(@PathVariable Integer id) {
        return RestResult.success(productRuleBusiness.detail(id));
   }
}