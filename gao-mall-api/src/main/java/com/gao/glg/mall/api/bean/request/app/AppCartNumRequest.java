package com.gao.glg.mall.api.bean.request.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/4
 */
@Data
public class AppCartNumRequest implements Serializable {

    @ApiModelProperty(value = "数量类型：total-商品数量，sum-购物数量", required = true)
    @NotNull(message = "数量类型不能为空")
    private String type;

    @ApiModelProperty(value = "商品类型：true-有效商品，false-无效商品", required = true)
    @NotNull(message = "商品类型不能为空")
    private Boolean numType;

}