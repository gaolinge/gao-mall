package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemUserLevelDomain;
import com.gao.glg.mall.api.mapper.EbSystemUserLevelMapper;
import com.gao.glg.mall.api.service.EbSystemUserLevelService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemUserLevelServiceImpl implements EbSystemUserLevelService  {
    @Resource
    private EbSystemUserLevelMapper systemUserLevelMapper;

    @Override
    public EbSystemUserLevelDomain getById(Integer levelId) {
        if (levelId == null) {
            return null;
        }
        return systemUserLevelMapper.getById(levelId);
    }

    @Override
    public List<EbSystemUserLevelDomain> select() {
        return systemUserLevelMapper.select();
    }
}
