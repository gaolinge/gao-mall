package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbUserTagDomain
 * @author gaolinge
 */
@Data
public class EbUserTagDomain {
    /** 
     * 
     */
    private Short id;

    /** 
     * 标签名称
     */
    private String name;


}
