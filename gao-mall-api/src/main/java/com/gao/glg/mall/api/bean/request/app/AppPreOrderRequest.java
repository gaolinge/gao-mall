package com.gao.glg.mall.api.bean.request.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 预下单
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/10
 */
@Data
public class AppPreOrderRequest {

    @ApiModelProperty(value = "预下单类型（“shoppingCart”：购物车下单，“buyNow”：立即购买，”again“： 再次购买，”video“: 视频号商品下单）")
    @NotBlank(message = "预下单类型不能为空")
    private String preOrderType;

    @ApiModelProperty(value = "订单详情列表")
    private List<AppPreOrderItemRequest> orderDetails;

}
