package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebGroupSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemGroupDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbSystemGroupService {

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    List<EbSystemGroupDomain> page(WebGroupSearchRequest request);

    /**
     * 保存
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void save(EbSystemGroupDomain domain);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void update(EbSystemGroupDomain domain);

    /**
     * 根据id查询
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    EbSystemGroupDomain getById(Integer id);
}
