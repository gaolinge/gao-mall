package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbUserIntegralRecordDomain
 * @author gaolinge
 */
@Data
public class EbUserIntegralRecordDomain {
    /** 
     * 记录id
     */
    private Integer id;

    /** 
     * 用户uid
     */
    private Integer uid;

    /** 
     * 关联id-orderNo,(sign,system默认为0）
     */
    private String linkId;

    /** 
     * 关联类型（order,sign,system）
     */
    private String linkType;

    /** 
     * 类型：1-增加，2-扣减
     */
    private Integer type;

    /** 
     * 标题
     */
    private String title;

    /** 
     * 积分
     */
    private Integer integral;

    /** 
     * 剩余
     */
    private Integer balance;

    /** 
     * 备注
     */
    private String mark;

    /** 
     * 状态：1-订单创建，2-冻结期，3-完成，4-失效（订单退款）
     */
    private Integer status;

    /** 
     * 冻结期时间（天）
     */
    private Integer frozenTime;

    /** 
     * 解冻时间
     */
    private Long thawTime;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
