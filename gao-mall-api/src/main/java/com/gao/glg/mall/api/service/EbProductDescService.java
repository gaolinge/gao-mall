package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.domain.EbProductDescDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbProductDescService {

    /**
     * 根据商品id获取商品详情
     * <p/>
     *
     * @param pid
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    EbProductDescDomain geByProductId(Integer pid, Integer type);

    /**
     * 根据商品id和类型
     * <p/>
     *
     * @param pids
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbProductDescDomain> listByPidsAndType(List<Integer> pids, Integer type);
}
