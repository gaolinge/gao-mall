package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserTokenDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserTokenMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserTokenDomain domain);

    /**
     * 查询
     */
    List<EbUserTokenDomain> select();

    /**
     * 更新
     */
    int update(EbUserTokenDomain domain);
}
