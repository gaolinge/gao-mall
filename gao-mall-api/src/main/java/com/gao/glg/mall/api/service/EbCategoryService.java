package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.response.web.WebCateSearchRequest;
import com.gao.glg.mall.api.domain.EbCategoryDomain;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbCategoryService {

    /**
     * 构建树
     * <p/>
     *
     * @param type
     * @param status
     * @param name
     * @param categoryIds
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbCategoryDomain> tree(Integer type, Integer status, String name, List<Integer> categoryIds);

    /**
     * 根据cid获取分类
     * <p/>
     *
     * @param cid
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbCategoryDomain> listByCid(Integer cid);

    /**
     * 根据类型获取分类
     * <p/>
     *
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbCategoryDomain> listByType(int type);

    /**
     * 根据cid获取分类
     * <p/>
     *
     * @param category2
     * @return
     * @author : gaolinge
     * @date : 2024/11/28
     * @version : 1.0.0
     */
    List<EbCategoryDomain> listByCids(List<Integer> category2);

    /**
     * 分页分类列表
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/6
     * @version : 1.0.0
     */
    List<EbCategoryDomain> search(WebCateSearchRequest request);

    /**
     * 保存
     * <p/>
     *
     * @param category
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    void save(EbCategoryDomain category);

    /**
     * 查询数量
     * <p/>
     *
     * @param name
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    int queryNum(String name, Integer type);

    /**
     * 根据id获取分类
     * <p/>
     *
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    EbCategoryDomain getById(Integer pid);

    /**
     * 删除
     * <p/>
     *
     * @param id
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    void delete(Integer id);

    /**
     * 查询子分类数量
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    int queryChildNum(Integer id);

    /**
     * 更新状态
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/16
     * @version : 1.0.0
     */
    void updateStatus(Integer id);
}
