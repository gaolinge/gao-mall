package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.web.WebGroupDataSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemGroupDataDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemGroupDataMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemGroupDataDomain domain);

    /**
     * 查询
     */
    List<EbSystemGroupDataDomain> select();

    /**
     * 更新
     */
    int update(EbSystemGroupDataDomain domain);

    /**
     * 查询
     */
    List<EbSystemGroupDataDomain> listByGid(Integer gid);

    /**
     * 查询
     */
    List<EbSystemGroupDataDomain> page(@Param("request") WebGroupDataSearchRequest request);

    /**
     * 查询
     */
    EbSystemGroupDataDomain getById(Integer id);
}
