package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbOrderDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.gao.glg.mall.api.mapper.EbOrderMapper;
import com.gao.glg.mall.api.service.EbOrderService;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbOrderServiceImpl implements EbOrderService {

    @Resource
    private EbOrderMapper orderMapper;

    @Override
    public Integer orderNum(Integer userId, int status) {
        return orderMapper.orderNum(status, userId);
    }

    @Override
    public List<EbOrderDomain> listByUserId(Integer userId) {
        return orderMapper.listByUserId(userId);
    }

    @Override
    public EbOrderDomain getById(Integer id) {
        return orderMapper.getById(id);
    }

    @Override
    public EbOrderDomain getByIdAndUid(Integer id, Integer userId) {
        return orderMapper.getByIdAndUid(id, userId);
    }

    @Override
    public void update(EbOrderDomain domain) {
        orderMapper.update(domain);
    }

    @Override
    public EbOrderDomain getByOderId(String orderNo) {
        return orderMapper.getByOderId(orderNo);
    }
}
