package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.web.WebRoleRequest;
import com.gao.glg.mall.api.bean.request.web.WebRoleSearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebRoleResponse;
import com.gao.glg.mall.api.bean.response.web.WebSystemRoleResponse;
import com.gao.glg.mall.api.business.IRoleBusiness;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 角色中心
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin/role")
@Api(tags = "角色中心")
public class WebRoleController {

    @Autowired
    private IRoleBusiness roleBusiness;

    //@PreAuthorize("hasAuthority('admin:system:role:list')")
    @ApiOperation(value = "分页列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public RestResult<PagerInfo<WebSystemRoleResponse>> page(@Validated WebRoleSearchRequest request) {
        return RestResult.success(roleBusiness.page(request));
    }

    //@PreAuthorize("hasAuthority('admin:system:role:save')")
    @ApiOperation(value = "新增身份")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public RestResult create(@RequestBody @Validated WebRoleRequest request) {
        roleBusiness.create(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:role:delete')")
    @ApiOperation(value = "删除")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public RestResult delete(@RequestParam(value = "id") Integer id) {
        roleBusiness.delete(id);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:role:update')")
    @ApiOperation(value = "修改")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public RestResult update(@RequestBody @Validated WebRoleRequest request) {
        roleBusiness.update(request);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:role:update:status')")
    @ApiOperation(value = "修改身份状态")
    @RequestMapping(value = "/updateStatus", method = RequestMethod.GET)
    public RestResult updateStatus(@Validated @RequestParam(value = "id") Integer id,
                                   @Validated @RequestParam(value = "status") Boolean status) {
        roleBusiness.updateStatus(id, status);
        return RestResult.success();
    }

    //@PreAuthorize("hasAuthority('admin:system:role:info')")
    @ApiOperation(value = "详情")
    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public RestResult<WebRoleResponse> detail(@PathVariable Integer id) {
        return RestResult.success(roleBusiness.detail(id));
    }


}



