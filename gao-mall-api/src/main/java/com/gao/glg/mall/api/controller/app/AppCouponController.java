package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.request.app.AppCouponReceiveRequest;
import com.gao.glg.mall.api.bean.response.app.AppCouponOrderResponse;
import com.gao.glg.mall.api.bean.response.app.AppCouponResponse;
import com.gao.glg.mall.api.bean.response.app.AppCouponUserResponse;
import com.gao.glg.mall.api.business.ICouponBusiness;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 优惠券
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@Api(tags = "优惠券")
@RestController("CouponFrontController")
@RequestMapping("/app/coupon")
public class AppCouponController {

    @Autowired
    private ICouponBusiness couponBusiness;


    @ApiOperation(value = "分页列表")
    @GetMapping(value = "/list")
    public RestResult<List<AppCouponResponse>> list(
            @RequestParam(value = "type", defaultValue = "0") int type,
            @RequestParam(value = "productId", defaultValue = "0") int productId, @Validated PagerDTO request) {
        return RestResult.success(couponBusiness.list(type, productId, request));
    }

    @ApiOperation(value = "我的优惠券")
    @GetMapping(value = "/my")
    public RestResult<PagerInfo<AppCouponUserResponse>> getList(
            @RequestParam(value = "type") String type, @Validated PagerDTO request) {
        return RestResult.success(couponBusiness.myList(type, request));
    }


    @ApiOperation(value = "领券")
    @RequestMapping(value = "/receive", method = RequestMethod.POST)
    public RestResult<String> receive(@RequestBody @Validated AppCouponReceiveRequest request) {
        couponBusiness.receive(request);
        return RestResult.success();
    }

    @ApiOperation(value = "可用优惠券")
    @GetMapping(value = "order/{preOrderNo}")
    public RestResult<List<AppCouponOrderResponse>> canCoupon(@PathVariable String preOrderNo) {
        return RestResult.success(couponBusiness.canCoupon(preOrderNo));
    }
}



