package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.web.WebSystemAttResponse;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/19
 */
public interface IMaterialBusiness {

    /**
     * 分页列表
     *
     * @param pid
     * @param attType
     * @param request
     * @return
     */
    PagerInfo<WebSystemAttResponse> list(Integer pid, String attType, PagerDTO request);
}
