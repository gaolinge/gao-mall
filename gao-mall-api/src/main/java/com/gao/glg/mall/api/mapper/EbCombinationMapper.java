package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbCombinationDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbCombinationMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbCombinationDomain domain);

    /**
     * 查询
     */
    List<EbCombinationDomain> select();

    /**
     * 更新
     */
    int update(EbCombinationDomain domain);

    /**
     * 查询
     */
    List<EbCombinationDomain> listByStock(int limit);

    /**
     * 查询
     */
    List<EbCombinationDomain> listByProductId(Integer productId);
}
