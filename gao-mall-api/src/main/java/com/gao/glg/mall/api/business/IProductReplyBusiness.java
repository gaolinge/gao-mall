package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebProductReplyCommentRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductReplyRequest;
import com.gao.glg.mall.api.bean.request.web.WebProductReplySearchRequest;
import com.gao.glg.mall.api.bean.response.web.WebProductReplyResponse;
import com.gao.glg.page.PagerInfo;

/**
 * 
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/27
 */
public interface IProductReplyBusiness {

    /**
     *
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    PagerInfo<WebProductReplyResponse> page(WebProductReplySearchRequest request);
    /**
     *
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void create(WebProductReplyRequest request);
    /**
     *
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void delete(Integer id);
    /**
     *
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void comment(WebProductReplyCommentRequest request);
    /**
     *
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    WebProductReplyResponse detail(Integer id);
}
