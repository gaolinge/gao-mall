package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreOrderDomain
 * @author gaolinge
 */
@Data
public class EbOrderDomain {
    /** 
     * 订单ID
     */
    private Integer id;

    /** 
     * 订单号
     */
    private String orderId;

    /** 
     * 用户id
     */
    private Integer uid;

    /** 
     * 用户姓名
     */
    private String realName;

    /** 
     * 用户电话
     */
    private String userPhone;

    /** 
     * 详细地址
     */
    private String userAddress;

    /** 
     * 运费金额
     */
    private BigDecimal freightPrice;

    /** 
     * 订单商品总数
     */
    private Integer totalNum;

    /** 
     * 订单总价
     */
    private BigDecimal totalPrice;

    /** 
     * 邮费
     */
    private BigDecimal totalPostage;

    /** 
     * 实际支付金额
     */
    private BigDecimal payPrice;

    /** 
     * 支付邮费
     */
    private BigDecimal payPostage;

    /** 
     * 抵扣金额
     */
    private BigDecimal deductionPrice;

    /** 
     * 优惠券id
     */
    private Integer couponId;

    /** 
     * 优惠券金额
     */
    private BigDecimal couponPrice;

    /** 
     * 支付状态
     */
    private Integer paid;

    /** 
     * 支付时间
     */
    private Date payTime;

    /** 
     * 支付方式
     */
    private String payType;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 订单状态（0：待发货；1：待收货；2：已收货，待评价；3：已完成；）
     */
    private Integer status;

    /** 
     * 0 未退款 1 申请中 2 已退款 3 退款中
     */
    private Integer refundStatus;

    /** 
     * 退款图片
     */
    private String refundReasonWapImg;

    /** 
     * 退款用户说明
     */
    private String refundReasonWapExplain;

    /** 
     * 前台退款原因
     */
    private String refundReasonWap;

    /** 
     * 不退款的理由
     */
    private String refundReason;

    /** 
     * 退款时间
     */
    private Date refundReasonTime;

    /** 
     * 退款金额
     */
    private BigDecimal refundPrice;

    /** 
     * 快递名称/送货人姓名
     */
    private String deliveryName;

    /** 
     * 发货类型
     */
    private String deliveryType;

    /** 
     * 快递单号/手机号
     */
    private String deliveryId;

    /** 
     * 消费赚取积分
     */
    private Integer gainIntegral;

    /** 
     * 使用积分
     */
    private Integer useIntegral;

    /** 
     * 给用户退了多少积分
     */
    private Integer backIntegral;

    /** 
     * 备注
     */
    private String mark;

    /** 
     * 是否删除
     */
    private Integer isDel;

    /** 
     * 管理员备注
     */
    private String remark;

    /** 
     * 商户ID
     */
    private Integer merId;

    /** 
     * 
     */
    private Integer isMerCheck;

    /** 
     * 拼团商品id0一般商品
     */
    private Integer combinationId;

    /** 
     * 拼团id 0没有拼团
     */
    private Integer pinkId;

    /** 
     * 成本价
     */
    private BigDecimal cost;

    /** 
     * 秒杀商品ID
     */
    private Integer seckillId;

    /** 
     * 砍价id
     */
    private Integer bargainId;

    /** 
     * 核销码
     */
    private String verifyCode;

    /** 
     * 门店id
     */
    private Integer storeId;

    /** 
     * 配送方式 1=快递 ，2=门店自提
     */
    private Integer shippingType;

    /** 
     * 店员id/核销员id
     */
    private Integer clerkId;

    /** 
     * 支付渠道(0微信公众号1微信小程序2余额)
     */
    private Integer isChannel;

    /** 
     * 消息提醒
     */
    private Integer isRemind;

    /** 
     * 后台是否删除
     */
    private Integer isSystemDel;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 快递公司简称
     */
    private String deliveryCode;

    /** 
     * 用户拼团活动id 0没有
     */
    private Integer bargainUserId;

    /** 
     * 订单类型:0-普通订单，1-视频号订单
     */
    private Integer type;

    /** 
     * 商品总价
     */
    private BigDecimal proTotalPrice;

    /** 
     * 改价前支付金额
     */
    private BigDecimal beforePayPrice;

    /** 
     * 是否改价,0-否，1-是
     */
    private Integer isAlterPrice;

    /** 
     * 商户系统内部的订单号,32个字符内、可包含字母, 其他说明见商户订单号
     */
    private String outTradeNo;


}
