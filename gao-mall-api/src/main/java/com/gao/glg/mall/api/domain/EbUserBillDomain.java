package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbUserBillDomain
 * @author gaolinge
 */
@Data
public class EbUserBillDomain {
    /** 
     * 用户账单id
     */
    private Integer id;

    /** 
     * 用户uid
     */
    private Integer uid;

    /** 
     * 关联id
     */
    private String linkId;

    /** 
     * 0 = 支出 1 = 获得
     */
    private Integer pm;

    /** 
     * 账单标题
     */
    private String title;

    /** 
     * 明细种类
     */
    private String category;

    /** 
     * 明细类型
     */
    private String type;

    /** 
     * 明细数字
     */
    private BigDecimal number;

    /** 
     * 剩余
     */
    private BigDecimal balance;

    /** 
     * 备注
     */
    private String mark;

    /** 
     * 0 = 带确定 1 = 有效 -1 = 无效
     */
    private Boolean status;

    /** 
     * 添加时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
