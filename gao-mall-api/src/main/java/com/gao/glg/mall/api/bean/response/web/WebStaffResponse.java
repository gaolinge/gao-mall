package com.gao.glg.mall.api.bean.response.web;

import com.gao.glg.mall.api.domain.EbSystemStoreDomain;
import com.gao.glg.mall.api.domain.EbUserDomain;
import com.gao.glg.serializer.FileUrlSerializerField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 门店店员表
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/16
 */
@Data
public class WebStaffResponse implements Serializable {

    @ApiModelProperty(value = "主键")
    private Integer id;

    @ApiModelProperty(value = "微信用户id")
    private Integer uid;

    @FileUrlSerializerField
    @ApiModelProperty(value = "店员头像")
    private String avatar;

    @ApiModelProperty(value = "用户信息")
    private EbUserDomain user;

    @ApiModelProperty(value = "门店id")
    private Integer storeId;

    @ApiModelProperty(value = "门店")
    private EbSystemStoreDomain systemStore;

    @ApiModelProperty(value = "店员名称")
    private String staffName;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "核销开关")
    private Integer verifyStatus;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
