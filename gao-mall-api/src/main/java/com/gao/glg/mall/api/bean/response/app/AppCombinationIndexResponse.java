
package com.gao.glg.mall.api.bean.response.app;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 拼团首页响应
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/25
 */
@Data
public class AppCombinationIndexResponse {

    @ApiModelProperty(value = "拼团3位用户头像")
    private List<String> avatarList;

    @ApiModelProperty(value = "拼团总人数")
    private Integer totalPeople;

    @ApiModelProperty(value = "拼团商品列表")
    private List<AppCombinationResponse> productList;

}
