package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbWechatPayInfoDomain
 * @author gaolinge
 */
@Data
public class EbWechatPayInfoDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 公众号唯一标识
     */
    private String appId;

    /** 
     * 商户号
     */
    private String mchId;

    /** 
     * 设备号,PC网页或公众号内支付可以传-WEB
     */
    private String deviceInfo;

    /** 
     * 用户的唯一标识
     */
    private String openId;

    /** 
     * 随机字符串
     */
    private String nonceStr;

    /** 
     * 签名
     */
    private String sign;

    /** 
     * 签名类型，默认为MD5，支持HMAC-SHA256和MD5
     */
    private String signType;

    /** 
     * 商品描述
     */
    private String body;

    /** 
     * 商品详细描述，对于使用单品优惠的商户，该字段必须按照规范上传
     */
    private String detail;

    /** 
     * 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用
     */
    private String attach;

    /** 
     * 商户订单号,要求32个字符内
     */
    private String outTradeNo;

    /** 
     * 标价币种：CNY：人民币 GBP：英镑 HKD：港币 USD：美元 JPY：日元 CAD：加拿大元 AUD：澳大利亚元 EUR：欧元 NZD：新西兰元 KRW：韩元 THB：泰铢
     */
    private String feeType;

    /** 
     * 标价金额
     */
    private Integer totalFee;

    /** 
     * 终端IP
     */
    private String spbillCreateIp;

    /** 
     * 交易起始时间
     */
    private String timeStart;

    /** 
     * 交易结束时间
     */
    private String timeExpire;

    /** 
     * 通知地址
     */
    private String notifyUrl;

    /** 
     * 交易类型,取值为：JSAPI，NATIVE，APP等
     */
    private String tradeType;

    /** 
     * 商品ID
     */
    private String productId;

    /** 
     * 场景信息
     */
    private String sceneInfo;

    /** 
     * 错误代码
     */
    private String errCode;

    /** 
     * 预支付交易会话标识
     */
    private String prepayId;

    /** 
     * 二维码链接
     */
    private String codeUrl;

    /** 
     * 是否关注公众账号
     */
    private String isSubscribe;

    /** 
     * 交易状态
     */
    private String tradeState;

    /** 
     * 付款银行
     */
    private String bankType;

    /** 
     * 现金支付金额
     */
    private Integer cashFee;

    /** 
     * 代金券金额
     */
    private Integer couponFee;

    /** 
     * 微信支付订单号
     */
    private String transactionId;

    /** 
     * 支付完成时间
     */
    private String timeEnd;

    /** 
     * 交易状态描述
     */
    private String tradeStateDesc;


}
