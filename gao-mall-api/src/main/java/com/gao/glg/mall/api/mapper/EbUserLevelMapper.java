package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserLevelDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserLevelMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserLevelDomain domain);

    /**
     * 查询
     */
    List<EbUserLevelDomain> select();

    /**
     * 更新
     */
    int update(EbUserLevelDomain domain);
}
