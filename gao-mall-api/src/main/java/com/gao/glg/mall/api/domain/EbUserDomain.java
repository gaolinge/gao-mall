package com.gao.glg.mall.api.domain;
import java.util.Date;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbUserDomain
 * @author gaolinge
 */
@Data
public class EbUserDomain {
    /** 
     * 用户id
     */
    private Integer uid;

    /** 
     * 用户账号
     */
    private String account;

    /** 
     * 用户密码
     */
    private String pwd;

    /** 
     * 真实姓名
     */
    private String realName;

    /** 
     * 生日
     */
    private String birthday;

    /** 
     * 身份证号码
     */
    private String cardId;

    /** 
     * 用户备注
     */
    private String mark;

    /** 
     * 合伙人id
     */
    private Integer partnerId;

    /** 
     * 用户分组id
     */
    private String groupId;

    /** 
     * 标签id
     */
    private String tagId;

    /** 
     * 用户昵称
     */
    private String nickname;

    /** 
     * 用户头像
     */
    private String avatar;

    /** 
     * 手机号码
     */
    private String phone;

    /** 
     * 添加ip
     */
    private String addIp;

    /** 
     * 最后一次登录ip
     */
    private String lastIp;

    /** 
     * 用户余额
     */
    private BigDecimal nowMoney;

    /** 
     * 佣金金额
     */
    private BigDecimal brokeragePrice;

    /** 
     * 用户剩余积分
     */
    private Integer integral;

    /** 
     * 用户剩余经验
     */
    private Integer experience;

    /** 
     * 连续签到天数
     */
    private Integer signNum;

    /** 
     * 1为正常，0为禁止
     */
    private Integer status;

    /** 
     * 等级
     */
    private Integer level;

    /** 
     * 推广员id
     */
    private Integer spreadUid;

    /** 
     * 推广员关联时间
     */
    private Date spreadTime;

    /** 
     * 用户类型
     */
    private String userType;

    /** 
     * 是否为推广员
     */
    private Boolean isPromoter;

    /** 
     * 用户购买次数
     */
    private Integer payCount;

    /** 
     * 下级人数
     */
    private Integer spreadCount;

    /** 
     * 详细地址
     */
    private String addres;

    /** 
     * 管理员编号 
     */
    private Integer adminid;

    /** 
     * 用户登陆类型，h5,wechat,routine
     */
    private String loginType;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;

    /** 
     * 最后一次登录时间
     */
    private Date lastLoginTime;

    /** 
     * 清除时间
     */
    private Date cleanTime;

    /** 
     * 推广等级记录
     */
    private String path;

    /** 
     * 是否关注公众号
     */
    private Integer subscribe;

    /** 
     * 关注公众号时间
     */
    private Date subscribeTime;

    /** 
     * 性别，0未知，1男，2女，3保密
     */
    private Boolean sex;

    /** 
     * 国家，中国CN，其他OTHER
     */
    private String country;

    /** 
     * 成为分销员时间
     */
    private Date promoterTime;


}
