package com.gao.glg.mall.api.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.mall.api.bean.request.app.AppSystemItemRequest;
import com.gao.glg.mall.api.bean.request.web.WebGroupDataSearchRequest;
import com.gao.glg.mall.api.domain.EbSystemGroupDataDomain;
import com.gao.glg.mall.api.mapper.EbSystemGroupDataMapper;
import com.gao.glg.mall.api.service.EbSystemGroupDataService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class EbSystemGroupDataServiceImpl implements EbSystemGroupDataService  {
    @Resource
    private EbSystemGroupDataMapper systemGroupDataMapper;

    @Override
    public List<Map<String, Object>> listByGid(Integer gid) {
        List<EbSystemGroupDataDomain> domains = systemGroupDataMapper.listByGid(gid);
        if (CollectionUtils.isEmpty(domains)) {
            return null;
        }
       return domains.stream().map(e -> {
                JSONObject jsonObject = JSONObject.parseObject(e.getValue());
                List<AppSystemItemRequest> systemItems =
                        JSONObject.parseArray(jsonObject.getString("fields"), AppSystemItemRequest.class);
                if (CollectionUtils.isEmpty(systemItems)) {
                    return null;
                }
                HashMap<String, Object> map = new HashMap<>();
                for (AppSystemItemRequest systemFormItemCheckRequest : systemItems) {
                    map.put(systemFormItemCheckRequest.getName(), systemFormItemCheckRequest.getValue());
                }
                map.put("id", e.getId());
                return map;
            }).collect(Collectors.toList());
    }

    @Override
    public List<EbSystemGroupDataDomain> page(WebGroupDataSearchRequest request) {
        return systemGroupDataMapper.page(request);
    }

    @Override
    public void save(EbSystemGroupDataDomain domain) {
        systemGroupDataMapper.save(domain);
    }

    @Override
    public void update(EbSystemGroupDataDomain domain) {
        systemGroupDataMapper.update(domain);
    }

    @Override
    public void delete(Integer id) {
        systemGroupDataMapper.delete(id);
    }

    @Override
    public EbSystemGroupDataDomain getById(Integer id) {
        return systemGroupDataMapper.getById(id);
    }
}
