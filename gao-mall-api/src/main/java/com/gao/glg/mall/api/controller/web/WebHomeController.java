package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.bean.response.web.WebHomeRateResponse;
import com.gao.glg.mall.api.business.IHomeBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * 统计 -- 主页 前端控制器
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * | Author: CRMEB Team <admin@crmeb.com>
 * +----------------------------------------------------------------------
 */
@Slf4j
@RestController
@RequestMapping("api/admin/statistics/home")
@Api(tags = "统计 -- 主页")
public class WebHomeController {

    @Autowired
    private IHomeBusiness homeBusiness;

    //@PreAuthorize("hasAuthority('admin:statistics:home:index')")
    @ApiOperation(value = "首页数据")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public RestResult<WebHomeRateResponse> indexDate() {
        return RestResult.success(homeBusiness.indexDate());
    }

    //@PreAuthorize("hasAuthority('admin:statistics:home:chart:user')")
    @ApiOperation(value = "用户曲线图")
    @RequestMapping(value = "/chart/user", method = RequestMethod.GET)
    public RestResult<Map<Object, Object>> chartUser() {
        return RestResult.success(homeBusiness.chartUser());
    }

    //@PreAuthorize("hasAuthority('admin:statistics:home:chart:user:buy')")
    @ApiOperation(value = "用户购买统计")
    @RequestMapping(value = "/chart/user/buy", method = RequestMethod.GET)
    public RestResult<Map<String, Integer>> chartUserBuy() {
        return RestResult.success(homeBusiness.chartUserBuy());
    }

    //@PreAuthorize("hasAuthority('admin:statistics:home:chart:order')")
    @ApiOperation(value = "30天订单量趋势")
    @RequestMapping(value = "/chart/order", method = RequestMethod.GET)
    public RestResult<Map<String, Object>> chartOrder() {
        return RestResult.success(homeBusiness.chartOrder());
    }

    //@PreAuthorize("hasAuthority('admin:statistics:home:chart:order:week')")
    @ApiOperation(value = "周订单量趋势")
    @RequestMapping(value = "/chart/order/week", method = RequestMethod.GET)
    public RestResult<Map<String, Object>> chartOrderInWeek() {
        return RestResult.success(homeBusiness.chartOrderInWeek());
    }

    //@PreAuthorize("hasAuthority('admin:statistics:home:chart:order:month')")
    @ApiOperation(value = "月订单量趋势")
    @RequestMapping(value = "/chart/order/month", method = RequestMethod.GET)
    public RestResult<Map<String, Object>> chartOrderInMonth() {
        return RestResult.success(homeBusiness.chartOrderInMonth());
    }

    //@PreAuthorize("hasAuthority('admin:statistics:home:chart:order:year')")
    @ApiOperation(value = "年订单量趋势")
    @RequestMapping(value = "/chart/order/year", method = RequestMethod.GET)
    public RestResult<Map<String, Object>> chartOrderInYear() {
        return RestResult.success(homeBusiness.chartOrderInYear());
    }
}



