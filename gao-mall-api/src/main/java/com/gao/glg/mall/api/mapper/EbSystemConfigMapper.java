package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemConfigDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemConfigMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemConfigDomain domain);

    /**
     * 查询
     */
    List<EbSystemConfigDomain> select();

    /**
     * 更新
     */
    int update(EbSystemConfigDomain domain);

    /**
     * 查询
     */
    EbSystemConfigDomain getByKey(String key);

    /**
     * 查询
     */
    List<EbSystemConfigDomain> getByFormId(int formId);
}
