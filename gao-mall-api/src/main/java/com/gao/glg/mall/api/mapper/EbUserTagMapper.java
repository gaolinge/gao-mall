package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserTagDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserTagMapper {

    /**
     * 删除
     */
    int delete(java.lang.Short id);

    /**
     * 保存
     */
    int save(EbUserTagDomain domain);

    /**
     * 查询
     */
    List<EbUserTagDomain> select();

    /**
     * 更新
     */
    int update(EbUserTagDomain domain);
}
