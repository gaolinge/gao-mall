package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebExpressSearchRequest;
import com.gao.glg.mall.api.domain.EbExpressDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbExpressService {

    /**
     * 分页查询
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    List<EbExpressDomain> page(WebExpressSearchRequest request);

    /**
     * 根据类型查询
     * <p/>
     *
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    List<EbExpressDomain> listByType(String type);

    /**
     * 根据id查询
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    EbExpressDomain getById(Integer id);

    /**
     * 更新
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/26
     * @version : 1.0.0
     */
    void update(EbExpressDomain domain);
}
