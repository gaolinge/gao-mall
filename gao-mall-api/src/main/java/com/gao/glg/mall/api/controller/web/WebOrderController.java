package com.gao.glg.mall.api.controller.web;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.business.IOrderBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色中心
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/admin/order")
@Api(tags = "订单中心")
public class WebOrderController {

    @Autowired
    private IOrderBusiness orderBusiness;
    
    //@PreAuthorize("hasAuthority('admin:order:list')")
//    @ApiOperation(value = "分页列表") //配合swagger使用
//    @RequestMapping(value = "/list", method = RequestMethod.GET)
//    public RestResult<CommonPage<StoreOrderDetailResponse>> page(@Validated StoreOrderSearchRequest request) {
//        return RestResult.success(orderBusiness.page(request));
//    }
    
//
//    //@PreAuthorize("hasAuthority('admin:order:status:num')")
//    @ApiOperation(value = "获取订单各状态数量")
//    @RequestMapping(value = "/status/num", method = RequestMethod.GET)
//    public RestResult<StoreOrderCountItemResponse> getOrderStatusNum(
//            @RequestParam(value = "dateLimit", defaultValue = "") String dateLimit,
//            @RequestParam(value = "type", defaultValue = "2") @Range(min = 0, max = 2, message = "未知的订单类型") Integer type) {
//        return RestResult.success(orderBusiness.orderNum(dateLimit, type));
//    }
//
//
//    //@PreAuthorize("hasAuthority('admin:order:delete')")
//    @ApiOperation(value = "订单删除")
//    @RequestMapping(value = "/delete", method = RequestMethod.GET)
//    public RestResult delete(@RequestParam(value = "orderNo") String orderNo) {
//        orderBusiness.delete(orderNo);
//        return RestResult.success();
//    }
//
//
//    //@PreAuthorize("hasAuthority('admin:order:mark')")
//    @ApiOperation(value = "备注")
//    @RequestMapping(value = "/mark", method = RequestMethod.POST)
//    public RestResult<String> mark(@RequestParam String orderNo, @RequestParam String mark) {
//        orderBusiness.mark(orderNo, mark);
//        return RestResult.success();
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:list:data')")
//    @ApiOperation(value = "获取订单统计数据")
//    @RequestMapping(value = "/list/data", method = RequestMethod.GET)
//    public RestResult<StoreOrderTopItemResponse> getOrderData(@RequestParam(value = "dateLimit", defaultValue = "")String dateLimit) {
//        return RestResult.success(orderBusiness.getOrderData(dateLimit));
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:update:price')")
//    @ApiOperation(value = "修改订单(改价)")
//    @RequestMapping(value = "/update/price", method = RequestMethod.POST)
//    public RestResult<String> updatePrice(@RequestBody @Validated StoreOrderUpdatePriceRequest request) {
//        orderBusiness.updatePrice(request);
//        return RestResult.success();
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:info')")
//    @ApiOperation(value = "详情")
//    @RequestMapping(value = "/info", method = RequestMethod.GET)
//    public RestResult<WebOrderInfoResponse> detail(@RequestParam(value = "orderNo") String orderNo) {
//        return RestResult.success(orderBusiness.detail(orderNo));
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:send')")
//    @ApiOperation(value = "发送货")
//    @RequestMapping(value = "/send", method = RequestMethod.POST)
//    public RestResult<Boolean> send(@RequestBody @Validated StoreOrderSendRequest request) {
//        orderBusiness.send(request);
//        return RestResult.success();
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:refund')")
//    @ApiOperation(value = "退款")
//    @RequestMapping(value = "/refund", method = RequestMethod.GET)
//    public RestResult<Boolean> send(@Validated StoreOrderRefundRequest request) {
//        return RestResult.success(orderBusiness.refund(request));
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:refund:refuse')")
//    @ApiOperation(value = "拒绝退款")
//    @RequestMapping(value = "/refund/refuse", method = RequestMethod.GET)
//    public RestResult<Object> refundRefuse(@RequestParam String orderNo, @RequestParam String reason) {
//        orderBusiness.refundRefuse(orderNo, reason);
//        return RestResult.success();
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:logistics:info')")
//    @ApiOperation(value = "快递查询")
//    @RequestMapping(value = "/getLogisticsInfo", method = RequestMethod.GET)
//    public RestResult<WebLogisticsResultVo> logisticsInfo(@RequestParam(value = "orderNo") String orderNo) {
//        return RestResult.success(orderBusiness.logisticsInfo(orderNo));
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:statistics')")
//    @ApiOperation(value = "核销订单头部数据")
//    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
//    public RestResult<StoreStaffTopDetail> getStatistics() {
//        return RestResult.success(storeOrderVerification.getOrderVerificationData());
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:statistics:data')")
//    @ApiOperation(value = "核销订单 月列表数据")
//    @RequestMapping(value = "/statisticsData", method = RequestMethod.GET)
//    public RestResult<List<StoreStaffDetail>> getStaffDetail(StoreOrderStaticsticsRequest request) {
//        return RestResult.success(storeOrderVerification.getOrderVerificationDetail(request));
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:write:update')")
//    @ApiOperation(value = "核销码核销订单")
//    @RequestMapping(value = "/writeUpdate/{vCode}", method = RequestMethod.GET)
//    public RestResult<Object> verificationOrder(@PathVariable String vCode) {
//        return RestResult.success(storeOrderVerification.verificationOrderByCode(vCode));
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:write:confirm')")
//    @ApiOperation(value = "核销码查询待核销订单")
//    @RequestMapping(value = "/writeConfirm/{vCode}", method = RequestMethod.GET)
//    public RestResult<Object> verificationConfirmOrder(
//            @PathVariable String vCode) {
//        return RestResult.success(storeOrderVerification.getVerificationOrderByCode(vCode));
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:time')")
//    @ApiOperation(value = "订单统计详情")
//    @RequestMapping(value = "/time", method = RequestMethod.GET)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "dateLimit", value = "today,yesterday,lately7,lately30,month,year,/yyyy-MM-dd hh:mm:ss,yyyy-MM-dd hh:mm:ss/",
//                    dataType = "String", required = true),
//            @ApiImplicitParam(name = "type", value = "1=price 2=order", required = true)
//    })
//    public RestResult<Object> statisticsOrderTime(@RequestParam String dateLimit,
//                                                    @RequestParam Integer type) {
//        return RestResult.success(orderBusiness.orderStatisticsByTime(dateLimit, type));
//    }
//
//    //@PreAuthorize("hasAuthority('admin:order:sheet:info')")
//    @ApiOperation(value = "获取面单默认配置信息")
//    @RequestMapping(value = "/sheet/info", method = RequestMethod.GET)
//    public RestResult<ExpressSheetVo> getDeliveryInfo() {
//        return RestResult.success(orderBusiness.getDeliveryInfo());
//    }
//

}



