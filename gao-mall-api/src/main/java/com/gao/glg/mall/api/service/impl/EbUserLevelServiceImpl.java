package com.gao.glg.mall.api.service.impl;
import com.gao.glg.mall.api.domain.EbUserDomain;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbUserLevelDomain;
import com.gao.glg.mall.api.mapper.EbUserLevelMapper;
import com.gao.glg.mall.api.service.EbUserLevelService;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbUserLevelServiceImpl implements EbUserLevelService  {
    @Resource
    private EbUserLevelMapper userLevelMapper;

    @Override
    public void update(EbUserDomain domain) {
        // TODO
    }
}
