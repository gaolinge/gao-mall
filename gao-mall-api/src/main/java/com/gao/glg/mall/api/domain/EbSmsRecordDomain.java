package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSmsRecordDomain
 * @author gaolinge
 */
@Data
public class EbSmsRecordDomain {
    /** 
     * 短信发送记录编号
     */
    private Integer id;

    /** 
     * 短信平台账号
     */
    private String uid;

    /** 
     * 接受短信的手机号
     */
    private String phone;

    /** 
     * 短信内容
     */
    private String content;

    /** 
     * 添加记录ip
     */
    private String addIp;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 短信模板ID
     */
    private String template;

    /** 
     * 状态码 100=成功,130=失败,131=空号,132=停机,133=关机,134=无状态
     */
    private Integer resultcode;

    /** 
     * 发送记录id
     */
    private Integer recordId;

    /** 
     * 短信平台返回信息
     */
    private String memo;


}
