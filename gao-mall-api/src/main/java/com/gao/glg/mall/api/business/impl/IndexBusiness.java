package com.gao.glg.mall.api.business.impl;

import com.gao.glg.enums.YesNoEnum;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.response.app.AppIndexInfoResponse;
import com.gao.glg.mall.api.bean.response.app.AppIndexProductResponse;
import com.gao.glg.mall.api.bean.response.app.AppSystemConfigResponse;
import com.gao.glg.mall.api.business.IIndexBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.EbProductDomain;
import com.gao.glg.mall.api.domain.EbSystemConfigDomain;
import com.gao.glg.mall.api.domain.EbUserDomain;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class IndexBusiness implements IIndexBusiness {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbUserVisitRecordService userVisitRecordService;
    @Autowired
    private EbProductService productService;
    @Autowired
    private EbSystemConfigService systemConfigService;
    @Autowired
    private EbSystemGroupDataService systemGroupDataService;
    
    @Override
    public AppIndexInfoResponse info() {
        List<Map<String, Object>> banner     = systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_INDEX_BANNER);
        List<Map<String, Object>> menus      = systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_INDEX_MENU);
        List<Map<String, Object>> newsBanner = systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_INDEX_NEWS_BANNER);
        List<Map<String, Object>> expMoney   = systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_INDEX_EX_BANNER);

        String siteLogo       = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_SITE_LOGO);
        String yzfH5Url       = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_YZF_H5_URL);
        String consumerHotline= systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_CONSUMER_HOTLINE);
        String serviceSwitch  = systemConfigService.getValueByKey(MallConstant.CONFIG_KEY_TELEPHONE_SERVICE_SWITCH);
        String categoryConfig = systemConfigService.getValueByKey(MallConstant.CONFIG_CATEGORY_CONFIG);
        String showCategory   = systemConfigService.getValueByKey(MallConstant.CONFIG_IS_SHOW_CATEGORY);
        String productsStyle  = systemConfigService.getValueByKey(MallConstant.CONFIG_IS_PRODUCT_LIST_STYLE);

        AppIndexInfoResponse response = new AppIndexInfoResponse();
        response.setBanner(banner);
        response.setMenus(menus);
        response.setRoll(newsBanner);
        response.setExplosiveMoney(expMoney);
        response.setSubscribe(false);
        response.setLogoUrl(siteLogo);
        response.setYzfUrl(yzfH5Url);
        response.setConsumerHotline(consumerHotline);
        response.setTelephoneServiceSwitch(serviceSwitch);
        response.setCategoryPageConfig(categoryConfig);
        response.setIsShowCategory(showCategory);
        response.setHomePageSaleListStyle(productsStyle);

        EbUserDomain user = userService.getUserInfo();
        if(user != null && YesNoEnum.YES.getValue() == user.getSubscribe()) {
            response.setSubscribe(true);
        }
        userVisitRecordService.addRecord(1);
        return response;
    }

    @Override
    public Map<String, String> share() {
        Map<String, String> info = systemConfigService.info(MallConstant.CONFIG_FORM_ID_PUBLIC);
        if(info == null) {
            throw new BusinessException(ErrorConstant.PLEASE_CONFIG_OFFICIAL_ACCOUNT);
        }

        Map<String, String> map = new HashMap<>();
        map.put("img",      info.get(MallConstant.CONFIG_KEY_ADMIN_WECHAT_SHARE_IMAGE));
        map.put("title",    info.get(MallConstant.CONFIG_KEY_ADMIN_WECHAT_SHARE_TITLE));
        map.put("synopsis", info.get(MallConstant.CONFIG_KEY_ADMIN_WECHAT_SHARE_SYNOSIS));
        return map;
    }

    @Override
    public AppSystemConfigResponse color() {
        EbSystemConfigDomain domain = systemConfigService.getByKey(MallConstant.CONFIG_KEY_CHANGE_COLOR);
        return BeanCopierUtil.copy(domain, AppSystemConfigResponse.class);
    }

    @Override
    public Map<String, Object> version() {
        Map<String, Object> record = new HashMap<>();
        record.put("appVersion",     systemConfigService.getValueByKey(MallConstant.CONFIG_APP_VERSION));
        record.put("androidAddress", systemConfigService.getValueByKey(MallConstant.CONFIG_APP_ANDROID_ADDRESS));
        record.put("iosAddress",     systemConfigService.getValueByKey(MallConstant.CONFIG_APP_IOS_ADDRESS));
        record.put("openUpgrade",    systemConfigService.getValueByKey(MallConstant.CONFIG_APP_OPEN_UPGRADE));
        return record;
    }

    @Override
    public List<Map<String, Object>> keywords() {
        return systemGroupDataService.listByGid(MallConstant.GROUP_DATA_ID_INDEX_KEYWORDS);
    }

    @Override
    public String imageDomain() {
        String localUploadUrl = systemConfigService.getValueByKey(MallConstant.CONFIG_APP_VERSION);
        return localUploadUrl == null ? "" : localUploadUrl;
    }

    @Override
    public PagerInfo productList(Integer type, PagerDTO request) {
        if (type < MallConstant.INDEX_RECOMMEND_BANNER || type > MallConstant.INDEX_BENEFIT_BANNER) {
            return PagerUtil.page(request, () -> Collections.emptyList());
        }

        PagerInfo pageInfo = PagerUtil.page(request, () -> productService.listByType(type));
        if (CollectionUtils.isEmpty(pageInfo.getList())) {
            pageInfo.setList(Collections.emptyList());
            return pageInfo;
        }

        List<AppIndexProductResponse> productResponses = new ArrayList<>();
        for (Object e : pageInfo.getList()) {
            EbProductDomain domain = (EbProductDomain) e;
            AppIndexProductResponse response = BeanCopierUtil.copy(domain, AppIndexProductResponse.class);
            productResponses.add(response);
        }
        pageInfo.setList(productResponses);
        return pageInfo;
    }
}
