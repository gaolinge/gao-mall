package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbSystemFormTempDomain
 * @author gaolinge
 */
@Data
public class EbSystemFormTempDomain {
    /** 
     * 表单模板id
     */
    private Integer id;

    /** 
     * 表单名称
     */
    private String name;

    /** 
     * 表单简介
     */
    private String info;

    /** 
     * 表单内容
     */
    private String content;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 更新时间
     */
    private Date updateTime;


}
