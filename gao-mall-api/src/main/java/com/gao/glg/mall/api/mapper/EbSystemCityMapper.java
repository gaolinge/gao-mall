package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbSystemCityDomain;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbSystemCityMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbSystemCityDomain domain);

    /**
     * 查询
     */
    List<EbSystemCityDomain> select();

    /**
     * 更新
     */
    int update(EbSystemCityDomain domain);

    /**
     * 查询
     */
    EbSystemCityDomain getByCityName(String cityName);

    /**
     * 查询
     */
    EbSystemCityDomain getByCityId(Integer cityId);

    /**
     * 查询
     */
    List<Integer> selectCityIds();

}
