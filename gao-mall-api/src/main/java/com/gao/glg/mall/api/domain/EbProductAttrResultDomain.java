package com.gao.glg.mall.api.domain;

import lombok.Data;
/**
 * EbStoreProductAttrResultDomain
 * @author gaolinge
 */
@Data
public class EbProductAttrResultDomain {
    /** 
     * 主键
     */
    private Integer id;

    /** 
     * 商品ID
     */
    private Integer productId;

    /** 
     * 商品属性参数
     */
    private String result;

    /** 
     * 上次修改时间
     */
    private Integer changeTime;

    /** 
     * 活动类型 0=商品，1=秒杀，2=砍价，3=拼团
     */
    private Boolean type;


}
