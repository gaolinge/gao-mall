package com.gao.glg.mall.api.controller.app;

import com.alibaba.fastjson.JSONObject;
import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.business.IQrCodeBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 验证码
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/qrcode")
@Api(tags = "二维码服务")
public class AppCodeController {

    @Autowired
    private IQrCodeBusiness qrCodeBusiness;


   /* @ApiOperation(value="获取二维码")
    @PostMapping(value = "/get")
    public RestResult<Map<String, Object>> get(@RequestBody JSONObject data) {
        return RestResult.success(qrCodeBusiness.get(data));
    }
    
    @ApiOperation(value="远程图片转base64")
    @PostMapping(value = "/base64")
    public RestResult<Map<String, Object>> get(@RequestParam String url) {
        return RestResult.success(qrCodeBusiness.base64(url));
    }

    @ApiOperation(value="将字符串 转base64")
    @PostMapping(value = "/str2base64")
    public RestResult<Map<String, Object>> getQrcodeByString(@RequestParam String text,
            @RequestParam int width, @RequestParam int height) {
        return RestResult.success(qrCodeBusiness.base64String(text, width, height));
    }*/
}



