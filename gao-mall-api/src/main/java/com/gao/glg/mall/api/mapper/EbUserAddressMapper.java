package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.domain.EbUserAddressDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbUserAddressMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbUserAddressDomain domain);

    /**
     * 查询
     */
    List<EbUserAddressDomain> select();

    /**
     * 更新
     */
    int update(EbUserAddressDomain domain);

    /**
     * 查询
     */
    List<EbUserAddressDomain> listByUid(Integer uid);

    /**
     * 更新
     */
    void canalDefault(Integer uid);

    /**
     * 查询
     */
    EbUserAddressDomain getByUidAndId(@Param("uid") Integer uid, @Param("id") Integer id);

    /**
     * 查询
     */
    EbUserAddressDomain getByDefault(Integer uid);

    /**
     * 查询
     */
    EbUserAddressDomain getById(Integer id);
}
