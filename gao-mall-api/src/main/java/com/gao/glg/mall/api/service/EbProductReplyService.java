package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebProductReplySearchRequest;
import com.gao.glg.mall.api.domain.EbProductReplyDomain;

import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
public interface EbProductReplyService {

    /**
     * 评论数量
     * <p/>
     *
     * @param pid
     * @param type
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    Long replyCount(Integer pid, Integer type);

    /**
     * 聚合分数
     * <p/>
     *
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    EbProductReplyDomain sumScore(Integer pid);

    /**
     * 根据商品id获取评论
     * <p/>
     *
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/11/29
     * @version : 1.0.0
     */
    EbProductReplyDomain getByPidLimitOne(Integer pid);

    /**
     *
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    List<EbProductReplyDomain> page(WebProductReplySearchRequest request);

    /**
     *
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void save(EbProductReplyDomain domain);

    /**
     *
     * <p/>
     *
     * @param domain
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    void update(EbProductReplyDomain domain);

    /**
     *
     * <p/>
     *
     * @param id
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    EbProductReplyDomain getById(Integer id);

    /**
     * 根据订单id和商品id查询评论
     * <p/>
     *
     * @param oid
     * @param pid
     * @param uid
     * @return
     * @author : gaolinge
     * @date : 2024/12/27
     * @version : 1.0.0
     */
    Integer countByOidAndPidAndUid(Integer oid, Integer pid, Integer uid);
}
