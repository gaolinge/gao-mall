package com.gao.glg.mall.api.domain;
import java.math.BigDecimal;

import lombok.Data;
/**
 * EbStoreBargainDomain
 * @author gaolinge
 */
@Data
public class EbBargainDomain {
    /** 
     * 砍价商品ID
     */
    private Integer id;

    /** 
     * 关联商品ID
     */
    private Integer productId;

    /** 
     * 砍价活动名称
     */
    private String title;

    /** 
     * 砍价活动图片
     */
    private String image;

    /** 
     * 单位名称
     */
    private String unitName;

    /** 
     * 库存
     */
    private Integer stock;

    /** 
     * 销量
     */
    private Integer sales;

    /** 
     * 砍价商品轮播图
     */
    private String images;

    /** 
     * 砍价开启时间
     */
    private Long startTime;

    /** 
     * 砍价结束时间
     */
    private Long stopTime;

    /** 
     * 砍价商品名称
     */
    private String storeName;

    /** 
     * 砍价金额
     */
    private BigDecimal price;

    /** 
     * 砍价商品最低价
     */
    private BigDecimal minPrice;

    /** 
     * 购买数量限制——单个活动每个用户发起砍价次数限制
     */
    private Integer num;

    /** 
     * 用户每次砍价的最大金额
     */
    private BigDecimal bargainMaxPrice;

    /** 
     * 用户每次砍价的最小金额
     */
    private BigDecimal bargainMinPrice;

    /** 
     * 帮砍次数——单个商品用户可以帮砍的次数
     */
    private Integer bargainNum;

    /** 
     * 砍价状态 0(到砍价时间不自动开启)  1(到砍价时间自动开启时间)
     */
    private Byte status;

    /** 
     * 反多少积分
     */
    private Integer giveIntegral;

    /** 
     * 砍价活动简介
     */
    private String info;

    /** 
     * 成本价
     */
    private BigDecimal cost;

    /** 
     * 排序
     */
    private Integer sort;

    /** 
     * 是否推荐0不推荐1推荐
     */
    private Byte isHot;

    /** 
     * 是否删除 0未删除 1删除
     */
    private Byte isDel;

    /** 
     * 添加时间
     */
    private Long addTime;

    /** 
     * 是否包邮 0不包邮 1包邮
     */
    private Byte isPostage;

    /** 
     * 邮费
     */
    private BigDecimal postage;

    /** 
     * 砍价规则
     */
    private String rule;

    /** 
     * 砍价商品浏览量
     */
    private Integer look;

    /** 
     * 砍价商品分享量
     */
    private Integer share;

    /** 
     * 运费模板ID
     */
    private Integer tempId;

    /** 
     * 重量
     */
    private BigDecimal weight;

    /** 
     * 体积
     */
    private BigDecimal volume;

    /** 
     * 限购总数
     */
    private Integer quota;

    /** 
     * 限量总数显示
     */
    private Integer quotaShow;

    /** 
     * 砍价人数——需要多少人砍价成功
     */
    private Integer peopleNum;


}
