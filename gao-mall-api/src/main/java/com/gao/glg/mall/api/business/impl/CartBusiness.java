package com.gao.glg.mall.api.business.impl;

import cn.hutool.core.util.ObjectUtil;
import com.gao.glg.exception.BusinessException;
import com.gao.glg.mall.api.bean.request.app.AppCartNumRequest;
import com.gao.glg.mall.api.bean.request.app.AppCartRequest;
import com.gao.glg.mall.api.bean.request.app.AppCartResetRequest;
import com.gao.glg.mall.api.bean.response.app.AppCartInfoResponse;
import com.gao.glg.mall.api.business.ICartBusiness;
import com.gao.glg.mall.api.constant.ErrorConstant;
import com.gao.glg.mall.api.constant.MallConstant;
import com.gao.glg.mall.api.domain.*;
import com.gao.glg.mall.api.service.*;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.gao.glg.utils.BeanCopierUtil;
import com.gao.glg.utils.StreamUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CartBusiness implements ICartBusiness {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbCartService cartService;
    @Autowired
    private EbProductService productService;
    @Autowired
    private EbProductAttrValueService productAttrValueService;
    @Autowired
    private EbSystemUserLevelService systemUserLevelService;

    @Override
    public PagerInfo page(PagerDTO dto, Boolean isValid) {
        Integer userId = userService.getUserIdException();
        PagerInfo page = PagerUtil.page(dto, () -> cartService.listByUserId(userId, isValid));
        if (CollectionUtils.isEmpty(page.getList())) {
            page.setList(null);
            return page;
        }

        List<EbCartDomain> list = page.getList();
        List<Integer> pids = StreamUtil.map(list, EbCartDomain::getProductId);

        EbUserDomain userDomain = userService.getUserInfo();
        EbSystemUserLevelDomain userLevel = systemUserLevelService.getById(userDomain.getLevel());
        List<EbProductDomain> productDomains = productService.listByIds(pids);
        List<EbProductAttrValueDomain> attrValueList = productAttrValueService.listByCart(list, MallConstant.PRODUCT_TYPE_NORMAL);

        Map<Integer, EbProductDomain> productMap = StreamUtil.listToMap(productDomains, EbProductDomain::getId);
        Map<Integer, EbProductAttrValueDomain> productAttrMap = StreamUtil.listToMap(attrValueList, EbProductAttrValueDomain::getProductId);

        List<AppCartInfoResponse> responses = new ArrayList<>();
        for (Object obj : page.getList()) {
            EbCartDomain cartDomain = (EbCartDomain) obj;
            EbProductDomain productDomain = productMap.get(cartDomain.getProductId());
            EbProductAttrValueDomain attrValueDomain = productAttrMap.get(cartDomain.getProductId());

            AppCartInfoResponse response = BeanCopierUtil.copy(cartDomain, AppCartInfoResponse.class);
            responses.add(response);
            if (!isValid) {
                response.setAttrStatus(false);
                responses.add(response);
                continue;
            }

            response.setImage(productDomain.getImage());
            response.setStoreName(productDomain.getStoreName());

            if (attrValueDomain == null) {
                response.setAttrStatus(false);
                continue;
            }
            response.setAttrId(attrValueDomain.getId());
            response.setSuk(attrValueDomain.getSuk());
            response.setPrice(attrValueDomain.getPrice());
            response.setAttrId(attrValueDomain.getId());
            response.setAttrStatus(attrValueDomain.getStock() > 0);
            response.setStock(attrValueDomain.getStock());
            if (attrValueDomain.getImage() != null) {
                response.setImage(attrValueDomain.getImage());
            }
            if (userLevel != null) {
                BigDecimal vipPrice = attrValueDomain.getPrice()
                        .multiply(new BigDecimal(userLevel.getDiscount()))
                        .divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
                response.setVipPrice(vipPrice);
            }
        }
        page.setList(responses);
        return page;
    }

    @Override
    public Map<String, Integer> count(AppCartNumRequest request) {
        Integer userId = userService.getUserIdException();
        int num = "total".equals(request.getType()) ?
                cartService.countTotal(userId, request.getNumType()) :
                cartService.countNum(userId, request.getNumType());

        Map<String, Integer> map = new HashMap<>();
        map.put("count", num);
        return map;
    }

    @Override
    public Map<String, String> save(AppCartRequest request) {
        EbProductDomain productDomain = productService.getById(request.getProductId());
        if (productDomain == null) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT);
        }
        EbProductAttrValueDomain attrValues = productAttrValueService.getByPidAndTypeAndAttrId(
                productDomain.getId(), request.getProductAttrUnique(), MallConstant.PRODUCT_TYPE_NORMAL);
        if (attrValues == null) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT_SKU);
        }

        Integer uid = userService.getUserIdException();
        List<EbCartDomain> cartDomains = cartService.listByUserIdAndAttrId(uid, request.getProductAttrUnique());

        Long id = !CollectionUtils.isEmpty(cartDomains) ?
                cartService.update(cartDomains.get(0).getId(), cartDomains.get(0).getCartNum() + request.getCartNum()) :
                cartService.save(uid, request);

        Map<String, String> map = new HashMap<>();
        map.put("cartId", id + "");
        return map;
    }

    @Override
    public void updateNum(Long id, Integer number) {
        if (ObjectUtil.isNull(number)) {
            throw new BusinessException(ErrorConstant.PRODUCT_NUM_NOT_LEGITIMATE);
        }
        if (number < 1 || number > 99) {
            throw new BusinessException(ErrorConstant.PRODUCT_NUM_1_99);
        }
        EbCartDomain cartDomain = cartService.getById(id);
        if (cartDomain == null) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT);
        }
        if (!cartDomain.getCartNum().equals(number)) {
            cartService.update(id, number);
        }
    }

    @Override
    public void delete(List<Long> ids) {
        cartService.batchDelete(ids);
    }

    @Override
    public void reset(AppCartResetRequest request) {
        EbCartDomain cartDomain = cartService.getById(request.getId());
        if (cartDomain == null) {
            throw new BusinessException(ErrorConstant.NOT_FOUND_PRODUCT);
        }
        if (request.getNum() < 1 || request.getNum() > 99) {
            throw new BusinessException(ErrorConstant.PRODUCT_NUM_1_99);
        }
        cartService.update(request.getId(), request.getNum());
    }
}
