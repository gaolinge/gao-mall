package com.gao.glg.mall.api.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.gao.glg.mall.api.domain.EbCouponUserDomain;
import com.gao.glg.mall.api.mapper.EbCouponUserMapper;
import com.gao.glg.mall.api.mapper.EbUserMapper;
import com.gao.glg.mall.api.service.EbCouponUserService;
import com.gao.glg.mall.api.service.EbProductService;
import com.gao.glg.mall.api.service.EbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

@Service
public class EbCouponUserServiceImpl implements EbCouponUserService {

    @Autowired
    private EbUserService userService;
    @Resource
    private EbCouponUserMapper couponUserMapper;
    @Autowired
    private EbProductService productService;

    @Override
    public List<EbCouponUserDomain> listByUid(Integer userId) {
        if (userId == null) {
            return Collections.emptyList();
        }
        return couponUserMapper.listByUid(userId);
    }

    @Override
    public Integer canUsedNum(Integer uid) {
        List<EbCouponUserDomain> couponUserDomains = listByUid(uid);
        if (CollectionUtils.isEmpty(couponUserDomains)) {
            return 0;
        }
        Date date = new Date();
        Iterator<EbCouponUserDomain> iterator = couponUserDomains.iterator();
        while (iterator.hasNext()) {
            EbCouponUserDomain couponUser = iterator.next();
            if (couponUser.getStartTime() != null && couponUser.getEndTime() != null) {
                if (date.compareTo(couponUser.getEndTime()) >= 0) {
                    iterator.remove();
                }
            }
        }
        return CollUtil.isEmpty(couponUserDomains) ? 0 : couponUserDomains.size();
    }

    @Override
    public List<EbCouponUserDomain> listByUidAndType(Integer uid, String type) {
        return couponUserMapper.listByUidAndType(uid, type);
    }

    @Override
    public List<EbCouponUserDomain> listByUidAndCouponId(Integer uid, Integer couponId) {
        return couponUserMapper.listByUidAndCouponId(uid, couponId);
    }

    @Override
    public void save(EbCouponUserDomain domain) {
        couponUserMapper.save(domain);
    }

    @Override
    public List<EbCouponUserDomain> listByCanUse(List<Integer> pids, BigDecimal maxPrice) {
        List<Integer> cids = productService.queryCategory2(pids);
        Integer uid = userService.getUserId();
        return couponUserMapper.listByCanUse(uid, pids, cids, maxPrice);
    }

    @Override
    public EbCouponUserDomain getById(Integer id) {
        return couponUserMapper.getById(id);
    }
}
