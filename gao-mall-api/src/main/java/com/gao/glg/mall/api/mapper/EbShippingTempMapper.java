package com.gao.glg.mall.api.mapper;
import com.gao.glg.mall.api.bean.request.web.WebLogisticsTempSearchRequest;
import com.gao.glg.mall.api.domain.EbShippingTempDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 *
 * @author gaolinge
 */
public interface EbShippingTempMapper {

    /**
     * 删除
     */
    int delete(java.lang.Integer id);

    /**
     * 保存
     */
    int save(EbShippingTempDomain domain);

    /**
     * 查询
     */
    List<EbShippingTempDomain> select();

    /**
     * 更新
     */
    int update(EbShippingTempDomain domain);

    /**
     * 查询
     */
    EbShippingTempDomain getById(Integer id);

    /**
     * 查询
     */
    List<EbShippingTempDomain> page(@Param("request") WebLogisticsTempSearchRequest request);

    /**
     * 查询
     */
    EbShippingTempDomain getByName(String name);
}
