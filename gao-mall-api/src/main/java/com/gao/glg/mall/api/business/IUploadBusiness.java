package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.response.web.WebUploadResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/19
 */
public interface IUploadBusiness {

    /**
     * 图片上传
     * <p/>
     *
     * @param file
     * @param model
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/12/19
     * @version : 1.0.0
     */
    WebUploadResponse imageUpload(MultipartFile file, String model, Integer pid) throws IOException;

    /**
     * 文件上传
     * <p/>
     *
     * @param file
     * @param model
     * @param pid
     * @return
     * @author : gaolinge
     * @date : 2024/12/19
     * @version : 1.0.0
     */
    WebUploadResponse fileUpload(MultipartFile file, String model, Integer pid);
}
