package com.gao.glg.mall.api.domain;
import java.util.Date;

import lombok.Data;
/**
 * EbUserTokenDomain
 * @author gaolinge
 */
@Data
public class EbUserTokenDomain {
    /** 
     * 
     */
    private Integer id;

    /** 
     * 用户 id
     */
    private Integer uid;

    /** 
     * token
     */
    private String token;

    /** 
     * 类型，1 公众号， 2 小程序, 3 unionid, 5AppIos,6AppAndroid,7ios
     */
    private Boolean type;

    /** 
     * 创建时间
     */
    private Date createTime;

    /** 
     * 到期时间
     */
    private Date expiresTime;

    /** 
     * 登录ip
     */
    private String loginIp;


}
