package com.gao.glg.mall.api.business.impl;

import com.gao.glg.mall.api.bean.request.app.AppUserCollectAllRequest;
import com.gao.glg.mall.api.bean.request.app.AppUserCollectRequest;
import com.gao.glg.mall.api.bean.response.app.AppUserCollectResponse;
import com.gao.glg.mall.api.business.ICollectBusiness;
import com.gao.glg.mall.api.domain.EbProductRelationDomain;
import com.gao.glg.mall.api.service.EbProductRelationService;
import com.gao.glg.mall.api.service.EbUserService;
import com.gao.glg.page.PagerDTO;
import com.gao.glg.page.PagerInfo;
import com.gao.glg.page.PagerUtil;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CollectBusiness implements ICollectBusiness {

    @Autowired
    private EbUserService userService;
    @Autowired
    private EbProductRelationService productRelationService;

    @Override
    public void add(AppUserCollectRequest request) {
        EbProductRelationDomain domain = new EbProductRelationDomain();
        domain.setUid(userService.getUserId());
        domain.setProductId(request.getProductId());
        domain.setCategory(request.getCategory());
        domain.setType("collect");
        productRelationService.save(domain);
    }

    @Override
    public void batchAdd(AppUserCollectAllRequest request) {
        List<String> ids = request.getId().stream().map(e -> e + "").collect(Collectors.toList());
        productRelationService.batchDelete(userService.getUserId(), ids, 2);

        List<EbProductRelationDomain> domains =
                request.getId().stream().map(pid -> {
                    EbProductRelationDomain domain = new EbProductRelationDomain();
                    domain.setUid(userService.getUserId());
                    domain.setProductId(pid);
                    domain.setCategory(request.getCategory());
                    domain.setType("collect");
                    return domain;
                }).collect(Collectors.toList());
        productRelationService.batchSave(domains);
    }

    @Override
    public PagerInfo<AppUserCollectResponse> list(PagerDTO request) {
        return PagerUtil.page(request, () -> productRelationService.list(userService.getUserId()));
    }

    @Override
    public void canal(Integer pid) {
        productRelationService.delete(userService.getUserId(), pid);
    }

    @Override
    public void delete(Map<String, String> map) {
        String[] ids = map.get("ids").split(",");
        productRelationService.batchDelete(userService.getUserId(), Lists.newArrayList(ids), 1);
    }


}
