package com.gao.glg.mall.api.service;

import com.gao.glg.mall.api.bean.request.web.WebGenerateRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/28
 */
public interface CodeService {

    /**
     * 分页
     * <p/>
     *
     * @param request
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    List<Map<String, Object>> page(WebGenerateRequest request);
}
