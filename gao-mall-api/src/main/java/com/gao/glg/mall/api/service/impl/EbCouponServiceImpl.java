package com.gao.glg.mall.api.service.impl;

import com.gao.glg.mall.api.bean.request.web.WebCouponSearchRequest;
import com.gao.glg.mall.api.domain.EbCouponDomain;
import com.gao.glg.mall.api.mapper.EbCouponMapper;
import com.gao.glg.mall.api.service.EbCouponService;
import com.gao.glg.mall.api.service.EbProductService;
import com.gao.glg.page.PagerDTO;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class EbCouponServiceImpl implements EbCouponService {
    @Resource
    private EbCouponMapper couponMapper;
    @Autowired
    private EbProductService productService;

    @Override
    public List<EbCouponDomain> listByReceive(int type, int productId, PagerDTO request) {
        List<Integer> pids = Lists.newArrayList(productId);
        List<Integer> cids = productService.queryCategory2(pids);
        int start = (request.getPageNum() - 1) * request.getPageSize();
        return couponMapper.listByReceive(type, pids, cids, start, request.getPageSize());
    }

    @Override
    public EbCouponDomain getById(Integer couponId) {
        return couponMapper.getById(couponId);
    }

    @Override
    public void deduct(Integer id, Integer isLimited, int num) {
        couponMapper.deduct(id, isLimited, num);
    }

    @Override
    public List<EbCouponDomain> page(WebCouponSearchRequest request) {
        return couponMapper.page(request);
    }

    @Override
    public void save(EbCouponDomain domain) {
        couponMapper.save(domain);
    }

    @Override
    public void update(EbCouponDomain domain) {
        couponMapper.update(domain);
    }

    @Override
    public void delete(Integer id) {
        couponMapper.delete(id);
    }

}
