package com.gao.glg.mall.api.service.impl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.gao.glg.mall.api.domain.EbSystemStoreStaffDomain;
import com.gao.glg.mall.api.mapper.EbSystemStoreStaffMapper;
import com.gao.glg.mall.api.service.EbSystemStoreStaffService;

import java.util.Collections;
import java.util.List;

/**
 *
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2023/9/11
 */
@Service
public class EbSystemStoreStaffServiceImpl implements EbSystemStoreStaffService  {
    @Resource
    private EbSystemStoreStaffMapper systemStoreStaffMapper;

    @Override
    public List<EbSystemStoreStaffDomain> listByStoreId(Integer storeId) {
        return systemStoreStaffMapper.listByStoreId(storeId);
    }
}
