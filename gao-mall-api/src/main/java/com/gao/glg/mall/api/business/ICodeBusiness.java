package com.gao.glg.mall.api.business;

import com.gao.glg.mall.api.bean.request.web.WebGenerateRequest;
import com.gao.glg.page.PagerInfo;

import java.util.Map;

/**
 * 
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/12/28
 */
public interface ICodeBusiness {

    /**
     * 分页
     * <p/>
     *
     * @param params
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    PagerInfo<Map<String, Object>> page(WebGenerateRequest params);

    /**
     * 生成
     * <p/>
     *
     * @param tables
     * @return
     * @author : gaolinge
     * @date : 2024/12/28
     * @version : 1.0.0
     */
    byte[] generator(String[] tables);
}
