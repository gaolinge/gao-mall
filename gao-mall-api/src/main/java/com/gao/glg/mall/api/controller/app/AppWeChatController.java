package com.gao.glg.mall.api.controller.app;

import com.gao.glg.bean.RestResult;
import com.gao.glg.mall.api.business.IWeChatBusiness;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 微信缓存表
 * <p/>
 *
 * @author : gaolinge
 * @version : 1.0.0
 * @date : 2024/11/21
 */
@Slf4j
@RestController
@RequestMapping("/app/wechat")
@Api(tags = "微信 -- 开放平台")
public class AppWeChatController {

    @Autowired
    private IWeChatBusiness weChatBusiness;

    @ApiOperation(value = "小程序获取授权logo")
    @RequestMapping(value = "/getLogo", method = RequestMethod.GET)
    public RestResult<Map<String, String>> logo(){
        Map<String, String> map = new HashMap<>();
        map.put("logoUrl", weChatBusiness.logo());
        return RestResult.success(map);
    }


    /*@ApiOperation(value = "微信登录公共号授权登录")
    @RequestMapping(value = "/authorize/login", method = RequestMethod.GET)
    public RestResult<LoginResponse> login(@RequestParam(value = "spread_spid", defaultValue = "0", required = false) Integer spreadUid,
                                             @RequestParam(value = "code") String code){
        return RestResult.success(userCenterService.weChatAuthorizeLogin(code, spreadUid));
    }

    @ApiOperation(value = "微信登录小程序授权登录")
    @RequestMapping(value = "/authorize/program/login", method = RequestMethod.POST)
    public RestResult<LoginResponse> programLogin(@RequestParam String code, @RequestBody @Validated RegisterThirdUserRequest request){
        return RestResult.success(userCenterService.weChatAuthorizeProgramLogin(code, request));
    }


    @ApiOperation(value = "微信注册绑定手机号")
    @RequestMapping(value = "/register/binding/phone", method = RequestMethod.POST)
    public RestResult<LoginResponse> registerBindingPhone(@RequestBody @Validated WxBindingPhoneRequest request){
        return RestResult.success(userCenterService.registerBindingPhone(request));
    }

    @ApiOperation(value = "获取微信公众号js配置")
    @RequestMapping(value = "/config", method = RequestMethod.GET)
    @ApiImplicitParam(name = "url", value = "页面地址url")
    public RestResult<WeChatJsSdkConfigResponse> configJs(@RequestParam(value = "url") String url){
        return RestResult.success(wechatNewService.getJsSdkConfig(url));
    }




    @ApiOperation(value = "订阅消息模板列表")
    @RequestMapping(value = "/program/my/temp/list", method = RequestMethod.GET)
    @ApiImplicitParam(name = "type", value = "支付之前：beforePay|支付成功：afterPay|申请退款：refundApply|充值之前：beforeRecharge|创建砍价：createBargain|参与拼团：pink|取消拼团：cancelPink")
    public RestResult<List<TemplateMessage>> programMyTempList(@RequestParam(name = "type") String type){
        return RestResult.success(systemNotificationService.getMiniTempList(type));
    }*/
}



